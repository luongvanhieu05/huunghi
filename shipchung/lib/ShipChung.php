<?php 
	class ShipChung {
		/*
		* Shipchung class
		* Implement with Shipchung service
		*/


		/*
		* @const string - Version number of the ShipChung SDK
		*/
		
		const VERSION = "0.0.1";

		/*
		* @var string - the api key ShipChung provide
		*/

		protected $apiKey 	= null;
		protected $URL 		= array(
			'createOrder' 	=> 'http://services.shipchung.vn/popup/checkout'
		);


		public function __construct($opts){
			header('Access-Control-Allow-Origin: *');
			header("Access-Control-Allow-Credentials: true");
			header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			header('Access-Control-Max-Age: 1000');
			header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

			/*
			* Require curl and json extension
			*/

			if (!function_exists('curl_init')) {
				throw new Exception('ShipChung needs the CURL PHP extension.');
			}
			if (!function_exists('json_decode')) {
				throw new Exception('ShipChung needs the JSON PHP extension.');
			}


			// set options variable
			if(isset($opts) && !empty($opts["apiKey"])){
				$this->apiKey = $opts["apiKey"];
			}else {
				throw new Exception("API key is required !");
			}
			$this->handler_sendOrder();
		}


		/*
		* sendOrder - Send order informatin to Shipchung service
		* @param array|null $items
		*/

		public function sendOrder($items){
			$items['MerchantKey'] 	= $this->apiKey;
			$items['BrowserInfo'] 	= $_SERVER['HTTP_USER_AGENT'];
			$data_string			= json_encode($items);

			$ch = curl_init($this->URL['createOrder']);

			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			    'Content-Type: application/json',
			    'Content-Length: ' . strlen($data_string))
			);

			$result = curl_exec($ch);
			curl_close($ch);
			header('Content-Type: application/json');
			echo $result;
		}

		/*
		* Receive and process when user send request to create order
		* return link popup
		*/
		public function handler_sendOrder (){
			if(isset($_POST) && isset($_GET['action']) && $_GET['action'] == 'sendOrder'){
				$data = json_decode(file_get_contents('php://input'), true);

				$err = array();
				if(empty($data)){
					$err[] = "No data";
				}

				if(empty($data['Item'])){
					$err[] = "Items is empty";
				}
				if(empty($data['Order'])){
					$err[] = "Order is empty";
				}

				if(!empty($err)){
					header('Content-Type: application/json');

					echo json_encode(array(
						"error"	=> true,
						"message" => $err,
						"data" => array()
					));
				}else {
					$this->sendOrder(array(
						"Order" => $data["Order"],
						"Item" => $data["Item"],
						"Domain" => $data["Domain"]
					));
				}
			}
		}

		public function return_json($error, $message = "", $data = array()){
			header('Content-Type: application/json');
			echo json_encode(array(
				"error" 	=> $error,
				"message" 	=> $message,
				"data" 		=> $data
			));
		}
	}
?>