<?php
// HTTP
define('HTTP_SERVER', 'http://hangjeans.com/admin/');
define('HTTP_CATALOG', 'http://hangjeans.com/');

// HTTPS
define('HTTPS_SERVER', 'http://hangjeans.com/admin/');
define('HTTPS_CATALOG', 'http://hangjeans.com/');

// DIR
define('DIR_APPLICATION', '/home/public_html/admin/');
define('DIR_SYSTEM', '/home/public_html/system/');
define('DIR_DATABASE', '/home/public_html/system/database/');
define('DIR_LANGUAGE', '/home/public_html/admin/language/');
define('DIR_TEMPLATE', '/home/public_html/admin/view/template/');
define('DIR_CONFIG', '/home/public_html/system/config/');
define('DIR_IMAGE', '/home/public_html/image/');
define('DIR_CACHE', '/home/public_html/system/cache/');
define('DIR_DOWNLOAD', '/home/public_html/download/');
define('DIR_LOGS', '/home/public_html/system/logs/');
define('DIR_CATALOG', '/home/public_html/catalog/');

// DB
define('DB_DRIVER', 'mysql');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'hangjeans@123');
define('DB_DATABASE', 'hangjean1_shop');
define('DB_PREFIX', 'oc_');
?>
