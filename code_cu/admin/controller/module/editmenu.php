<?php
class ControllerModuleEditMenu extends Controller
{
	private $error = array();
	function index()
	{
		$this->load->language('module/editmenu');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->load->model('setting/setting');
		if ($this->request->server['REQUEST_METHOD'] == 'POST' and $this->validate()) {
			$this->model_setting_setting->editSetting('editmenu', $this->request->post);
				
			$this->session->data['success'] = $this->language->get('text_success');
		}

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_content_top'] = $this->language->get('text_content_top');
		$this->data['text_content_bottom'] = $this->language->get('text_content_bottom');		
		$this->data['text_column_left'] = $this->language->get('text_column_left');
		$this->data['text_column_right'] = $this->language->get('text_column_right');
		
		$this->data['entry_layout'] = $this->language->get('entry_layout');
		$this->data['entry_position'] = $this->language->get('entry_position');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_add_module'] = $this->language->get('button_add_module');
		$this->data['button_remove'] = $this->language->get('button_remove');
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		$this->data['breadcrumbs'] = array();
		
		$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('text_home'),
				'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
				'separator' => false
		);
		
		$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('text_module'),
				'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
				'separator' => ' :: '
		);
		
		$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('heading_title'),
				'href'      => $this->url->link('module/editmenu', 'token=' . $this->session->data['token'], 'SSL'),
				'separator' => ' :: '
		);
		
		$this->data['action'] = $this->url->link('module/editmenu', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['modules'] = array();
		if (isset($this->request->post['editmenu_vna'])) {
			$this->data['modules'] = $this->request->post['editmenu_vna'];
		} elseif ($this->config->get('editmenu_vna')) {
			$this->data['modules'] = $this->config->get('editmenu_vna');
		}
		
		
		$action = array();
			
		$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('setting/setting', 'token=' . $this->session->data['token'], 'SSL')
		);
		$this->load->model('setting/store');
		
		$this->data['stores'] = array();
		$this->data['stores'][] = array(
				'store_id' => 0,
				'name'     => $this->config->get('config_name') . $this->language->get('text_default'),
				'url'      => HTTP_CATALOG,
				'selected' => isset($this->request->post['selected']) && in_array(0, $this->request->post['selected']),
				'action'   => $action
		);
		
		$results = $this->model_setting_store->getStores();
 
    	foreach ($results as $result) {
			$action = array();
						
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('setting/store/update', 'token=' . $this->session->data['token'] . '&store_id=' . $result['store_id'], 'SSL')
			);
						
			$this->data['stores'][] = array(
				'store_id' => $result['store_id'],
				'name'     => $result['name'],
				'url'      => $result['url'],
				'selected' => isset($this->request->post['selected']) && in_array($result['store_id'], $this->request->post['selected']),
				'action'   => $action
			);
		}	
		
		$this->load->model('menumanager/menu_group');
		$this->data['menu_groups'] = $this->model_menumanager_menu_group->getMenuGroups();
		
		$this->template = 'module/editmenu.tpl';
		$this->children = array(
				'common/header',
				'common/footer'
		);
		
		$this->response->setOutput($this->render());
	}
	
	function validate()
	{
		if (!$this->user->hasPermission('modify', 'module/category')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		$data = $this->request->post['editmenu_vna'];
		$result = true;
		for($i=0;$i<count($data)-1;$i++)
		{
			for($j=1;$j<count($data);$j++)
			{
				if($data[$i]['store_id'] == $data[$j]['store_id'])
				{
					$result = false;
					$this->error['warning'] = $this->language->get('error_duplicate_store');
				}
			}
		}
		if($this->error)
		{
			$result = false;
		}
		return $result;
	}
	
	function install()
	{
		$this->load->model('menumanager/menu');
		if( !$this->model_menumanager_menu->checkTable())
		{
			$this->session->data['error'] = 'Please install module \'Menu Manager\' before install this module!';
			$this->load->model('setting/extension');
			$this->model_setting_extension->uninstall('module', $this->request->get['extension']);
			$this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}
		
	}
}