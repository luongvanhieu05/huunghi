<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>Virtual Payment Client Example</title>
    <meta http-equiv='Content-Type' content='text/html; charset=utf8'>
    <style type="text/css">
        <!--
        h1 {
            font-family: Arial, sans-serif;
            font-size: 24pt;
            color: #08185A;
            font-weight: 100
        }

        h2.co {
            font-family: Arial, sans-serif;
            font-size: 24pt;
            color: #08185A;
            margin-top: 0.1em;
            margin-bottom: 0.1em;
            font-weight: 100
        }

        h3.co {
            font-family: Arial, sans-serif;
            font-size: 16pt;
            color: #000000;
            margin-top: 0.1em;
            margin-bottom: 0.1em;
            font-weight: 100
        }

        body {
            font-family: Verdana, Arial, sans-serif;
            font-size: 10pt;
            color: #08185A background-color: #FFFFFF
        }

        a:link {
            font-family: Verdana, Arial, sans-serif;
            font-size: 8pt;
            color: #08185A
        }

        a:visited {
            font-family: Verdana, Arial, sans-serif;
            font-size: 8pt;
            color: #08185A
        }

        a:hover {
            font-family: Verdana, Arial, sans-serif;
            font-size: 8pt;
            color: #FF0000
        }

        a:active {
            font-family: Verdana, Arial, sans-serif;
            font-size: 8pt;
            color: #FF0000
        }

        .shade {
            height: 25px;
            background-color: #CED7EF
        }

        tr.title {
            height: 25px;
            background-color: #0074C4
        }

        td {
            font-family: Verdana, Arial, sans-serif;
            font-size: 8pt;
            color: #08185A
        }

        th {
            font-family: Verdana, Arial, sans-serif;
            font-size: 10pt;
            color: #08185A;
            font-weight: bold;
            background-color: #CED7EF;
            padding-top: 0.5em;
            padding-bottom: 0.5em
        }

        .background-image {
            font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
            font-size: 12px;
            width: 100%;
            text-align: left;
            border-collapse: collapse;
            background: url("...") 330px 59px no-repeat;
            margin: 0px;
        }

        .background-image th {
            font-weight: normal;
            font-size: 14px;
            color: #339;
            padding: 12px;
        }

        .background-image td {
            color: #669;
            border-top: 1px solid #fff;
            padding: 9px 12px;
        }

        .background-image tfoot td {
            font-size: 11px;
        }

        .background-image tbody td {
            background: url("./back.png");
        }

        * html
.background-image tbody td {
            filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src = 'table-images/back.png', sizingMethod = 'crop');
            background: none;
        }

        .background-image tbody tr:hover td {
            color: #339;
            background: none;
        }

        .background-image .tb_title {
            font-family: Verdana, Arial, sans-serif;
            color: #08185A;
            background-color: #CED7EF;
            font-size: 14px;
            color: #339;
            padding: 12px;
        }

        -->
    </style>
</head>
<body>
<?php
    date_default_timezone_set('Asia/Krasnoyarsk');
?>
<form action="./do.php" method="post">
    <input type="hidden" name="Title" value="VPC 3-Party"/>
    <table width="100%" align="center" border="0" cellpadding='0'
           cellspacing='0'>
        <tr class="shade">
            <td width="1%">&nbsp;</td>
            <td width="40%" align="right"><strong><em>URL cổng thanh toán - Virtual Payment Client
                URL:&nbsp;</em></strong></td>
            <td width="59%"><input type="text" name="virtualPaymentClientURL"
                                   size="63" value="http://mtf.onepay.vn/vpcpay/vpcpay.op"
                                   maxlength="250"/></td>
        </tr>
    </table>
    <center>
        <table class="background-image" summary="Meeting Results">
            <thead>
            <tr>
                <th scope="col" width="250px">Name</th>
                <th scope="col" width="250px">Input</th>
                <th scope="col" width="250px">Chú thích</th>
                <th scope="col">Description</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><strong><em>Merchant ID</em></strong></td>
                <td><input type="text" name="vpc_Merchant" value="TESTONEPAY" size="20"
                           maxlength="16"/></td>
                <td>Được cấp bởi OnePAY</td>
                <td>Provided by OnePAY</td>
            </tr>
            <tr>
                <td><strong><em>Merchant AccessCode</em></strong></td>
                <td><input type="text" name="vpc_AccessCode" value="6BEB2546"
                           size="20" maxlength="8"/></td>
                <td>Được cấp bởi OnePAY</td>
                <td>Provided by OnePAY</td>
            </tr>
            <tr>
                <td><strong><em>Merchant Transaction Reference</em></strong></td>
                <td><input type="text" name="vpc_MerchTxnRef"
                           value="<?php
                echo date('YmdHis') . rand();
                           ?>" size="20"
                           maxlength="40"/></td>
                <td>ID giao dịch, giá trị phải khác nhau trong mỗi lần thanh(tối đa 40 ký tự)
                    toán
                </td>
                <td>ID Transaction - (unique per transaction) - (max 40 char)</td>
            </tr>
            <tr>
                <td><strong><em>Transaction OrderInfo</em></strong></td>
                <td><input type="text" name="vpc_OrderInfo" value="JSECURETEST01"
                           size="20" maxlength="34"/></td>
                <td>Tên hóa đơn - (tối đa 34 ký tự)</td>
                <td>Order Name will show on payment gateway (max 34 char)</td>
            </tr>
            <tr>
                <td><strong><em>Purchase Amount</em></strong></td>
                <td><input type="text" name="vpc_Amount" value="1000000" size="20"
                           maxlength="10"/></td>
                <td>Số tiền cần thanh toán,Đã được nhân với 100. VD: 1000000=10000VND</td>
                <td>Amount,Multiplied with 100, Ex: 1000000=10000VND</td>
            </tr>
            <tr>
                <td><strong><em>Receipt ReturnURL</em></strong></td>
                <td><input type="text" name="vpc_ReturnURL" size="45"
                           value="http://localhost/domestic_php_v2/source_code/dr.php"
                           maxlength="250"/></td>
                <td>Url nhận kết quả trả về sau khi giao dịch hoàn thành.</td>
                <td>URL for receiving payment result from gateway</td>
            </tr>
            <tr>
                <td><strong><em>VPC Version</em></strong></td>
                <td><input type="text" name="vpc_Version" value="2" size="20"
                           maxlength="8"/></td>
                <td>Phiên bản modul (cố định)</td>
                <td>Version (fixed)</td>
            </tr>
            <tr>
                <td><strong><em>Command Type</em></strong></td>
                <td><input type="text" name="vpc_Command" value="pay" size="20"
                           maxlength="16"/></td>
                <td>Loại request (cố định)</td>
                <td>Command Type(fixed)</td>
            </tr>
            <tr>
                <td><strong><em>Payment Server Display Language Locale</em></strong></td>
                <td><input type="text" name="vpc_Locale" value="en" size="20"
                           maxlength="5"/></td>
                <td>Ngôn ngữ hiện thị trên cổng (vn/en)</td>
                <td>Language use on gateway (vn/en)</td>
            </tr>
            </tbody>
        </table>
        <table class="background-image" summary="Meeting Results">
            <thead>
            <tr>
                <th scope="col" colspan="4">Addition Infomation</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td width="250"><strong><em>Note</em></strong></td>
                <td width="550" colspan="2">- Không sử dụng tiếng việt có dấu trong các tham số gửi sang cổng thanh toán<br>- Không
                    sử dụng số tiền lẻ với cổng thanh toán test(ví dụ 0.2 đồng tức amount = 20)
                </td>
                <td colspan="1">- do not use vietnamese with sign. Convert to vietnamese no sign before send it to
                    gateway<br>- do not use decimal for amount for testing (100=1VND -> right; 120=1.2VND -> wrong)
                </td>
            </tr>
            </tbody>
        </table>
		<table class="background-image" summary="Meeting Results">
            <thead>
            <tr>
                <th width="600" scope="col">Billing Address</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <td align="center" colspan="2"><input type="submit" value="Pay Now!"/></td>
            </tr>
            </tfoot>
            <tbody>
            </tbody>
        </table>
    </center>
</form>
</body>
</html>