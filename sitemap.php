<?php
	define( 'ROOT_PATH', strtr(dirname( __FILE__ ) ."/",array('\\'=>'/')));
	//set_time_limit(0);
	$content  = '<?xml version="1.0" encoding="UTF-8"?>';
	$content .= '
	<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';
	require_once ROOT_PATH.'packages/core/includes/system/config.php';
	$content .= '
		<url>
		  <loc>http://'.$_SERVER['SERVER_NAME'].'/</loc>
		  <priority>1.0</priority>
		  <changefreq>hourly</changefreq>
		</url>
	';// <lastmod>'.date('c').'</lastmod>
	$news_categories = DB::fetch_all('
		select
			category.id,category.name_id,category.structure_id
		from
			category
		where
			category.status <> "HIDE" and category.type="NEWS" AND '.IDStructure::child_cond(ID_ROOT,DB::structure_id('category',585)).'
		ORDER BY
				category.structure_id
	');
	foreach($news_categories as $key=>$value)
	{
		//$parent_zone = '';
		$content .= '
		<url>
		  <loc>http://'.$_SERVER['SERVER_NAME'].'/trang-tin/'.$value['name_id'].'/</loc>
		  <priority>0.7</priority>
		  <changefreq>daily</changefreq>
		</url>';
	}	
	$news = DB::fetch_all('
		select
			news.*,category.name_id as category_name_id
		from
			news
			inner join category on category.id = news.category_id
		where
			news.status <> "HIDE" and news.type="NEWS"
		ORDER BY
			news.id DESC
	');
	foreach($news as $key=>$value)
	{
		//$parent_zone = '';
		$content .= '
		<url>
		  <loc>http://'.$_SERVER['SERVER_NAME'].'/trang-tin/'.$value['category_name_id'].'/'.$value['name_id'].'.html</loc>
		  <priority>0.7</priority>
		  <changefreq>daily</changefreq>
		</url>';
	}//<lastmod>'.date('c').'</lastmod>
	$product = DB::fetch_all('
		select
			product.*,category.name_id as category_name_id
		from
			product
			INNER JOIN product_category ON product_category.product_id = product.id
					INNER JOIN category ON product_category.category_id = category.id AND category.type="PRODUCT"
		where
			product.status <> "HIDE" 
		ORDER BY
			product.id DESC
	');
	foreach($product as $key=>$value)
	{
		//$parent_zone = '';
		$content .= '
		<url>
		  <loc>http://'.$_SERVER['SERVER_NAME'].'/san-pham/'.$value['category_name_id'].'/'.$value['name_id'].'.html</loc>
		  <priority>0.7</priority>
		  <changefreq>daily</changefreq>
		</url>';
	}
	$product_categories = DB::fetch_all('
		select
			category.id,category.name_id,category.structure_id
		from
			category
		where
			category.status <> "HIDE" AND category.type="PRODUCT"
		ORDER BY
				category.structure_id
	');
	foreach($product_categories as $key=>$value)
	{
		$level = IDStructure::level($value['structure_id']);
		//$parent_zone = '';
		$parent_name_id = DB::fetch('select name_id from category where type="PRODUCT" AND structure_id= '.IDStructure::parent($value['structure_id']).'','name_id');
		if($level > 1){
			$url = 'http://'.$_SERVER['SERVER_NAME'].'/san-pham/'.$parent_name_id.'/'.$value['name_id'].'/';
		}else{
			$url = 'http://'.$_SERVER['SERVER_NAME'].'/san-pham/'.$value['name_id'].'/';
		}
		$content .= '
		<url>
		  <loc>'.$url.'</loc>
		  <priority>0.7</priority>
		  <changefreq>daily</changefreq>
		</url>';
	}	
	$content .= '</urlset>';
	echo $content;
?>