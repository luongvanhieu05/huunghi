<?php
	define( 'ROOT_PATH', strtr(dirname( __FILE__ ) ."/",array('\\'=>'/')));
	set_time_limit(0);
	$content  = '<?xml version="1.0" encoding="UTF-8"?>';
	$content .= '<urlset
		  xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
		  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		  xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
				http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
	<!-- created with Free Online Sitemap Generator www.xml-sitemaps.com -->';
	require_once ROOT_PATH.'packages/core/includes/system/config.php';
	$pages = DB::fetch_all('
		SELECT
			name as id,title_1
		FROM
			page
		WHERE
			params like "%portal=hotel%"
			and name!="chi-tiet"
			and name!="order_admin"
			and name!="dang-nhap"
			and name!="tin-tuc"
			and name!="xem-tin-tuc"
	');
	$content .= '
		<url>
		  <loc>http://'.$_SERVER['SERVER_NAME'].'/	</loc>
		</url>
	';
	foreach($pages as $id=>$page)
	{
		$content .= '
		<url>
		  <loc>http://'.$_SERVER['SERVER_NAME'].'/' .$page['id'].'.html</loc>
		  <changefreq>Daily</changefreq>
		  <priority>1.0</priority>
		</url>';
	}
	$categories = DB::fetch_all('select name_id as id ,name_1,type,url from category where status!="HIDE" and portal_id="#hotel" and name_id!="root" and url=""');
	foreach($categories as $id=>$category)
	{
		$content .= '
		<url>
		  <loc>http://'.$_SERVER['SERVER_NAME'].'/tin-tuc/' .$category['id'].'.html</loc>
		  <changefreq>Daily</changefreq>
		  <priority>1.0</priority>
		</url>';
	}
	$items = DB::fetch_all('select name_id as id,name_1 from news where type="NEWS" and status!="HIDE" and portal_id="#hotel" and category_id!=464 order by time desc limit 0,1000');
	foreach($items as $id=>$item)
	{
		$content .= '
		<url>
		  <loc>http://'.$_SERVER['SERVER_NAME'].'/xem-tin-tuc/' .$item['id'].'.html</loc>
		  <changefreq>Hourly</changefreq>
		  <priority>1.0</priority>
		</url>';
	}
	echo $content .= '</urlset>';
?>
