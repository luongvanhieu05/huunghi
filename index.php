<?php
	date_default_timezone_set('Asia/Saigon');
	if(!isset($_REQUEST['portal']))
	{
		$_REQUEST['portal'] = 'default';
	}
	if(isset($_REQUEST['page']))
	{
		$page = $_REQUEST['page'];
	}
	else
	{
		$page = 'trang-chu';
	}
	define('DEVELOPER',true);
	define( 'ROOT_PATH', strtr(dirname( __FILE__ ) ."/",array('\\'=>'/')));
	require_once ROOT_PATH.'packages/core/includes/system/config.php';
	Portal::run();
?>
