<style>
    img{max-width: 100%;}
    .c-ototayho{
        margin-top:40px;
    }
    .c-ototayho .img img{
        /*padding:0px;*/
    }  
    .c-ototayho .title{
        
        padding-bottom: 5px;
        color: #191919;
        font-size: 20px;
        font-family: semibold;
        margin-bottom: 39px;
        text-align: center;
        text-transform: uppercase;

    }
    .c-ototayho .bor{
        display: inline-block;
        border-bottom: 3px solid #e70000;
        padding-bottom: 10px;
    }
    .c-ototayho .txt .item:nth-child(1),.c-ototayho .txt .item:nth-child(2){
        margin-bottom: 30px
    }
    .c-ototayho .txt .item .tit{
        font-size:18px;
        margin-bottom:20px;
        font-weight: bold;
        color: #ff0000;
    }
    .au-ntt{
        margin-top:50px;
    }
    .au-ntt .title{
        color: #212121;
        color: #212121;
        font-size: 30px!important;
        font-weight: bold;
        margin-bottom: 30px;
    }
    .au-ntt .ct p{
        font-style: 18px!important;
    }
    .au-ntt{
        background: linear-gradient(to left, white 0%, #e9e9e9 100%);
        padding: 30px ;
    }
    .au-ntt .ct .btn i{font-size: 25px;}
    .au-ntt .ct .btn{
        border:0px;
        margin-right: 20px;
        margin-top: 30px;
        font-size: 18px;
        padding: 0 20px;
        background-color: #f44336;
        background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #f44336), color-stop(100% #d2190b));
        background-image: -webkit-linear-gradient(top, #f44336 0%, #d2190b 100%);
        background-image: -moz-linear-gradient(top, #f44336 0%, #d2190b 100%);
        background-image: -ms-linear-gradient(top, #f44336 0%, #d2190b 100%);
        background-image: -o-linear-gradient(top, #f44336 0%, #d2190b 100%);
        background-image: linear-gradient(top, #f44336 0%, #d2190b 100%);
        color: #fff;
        margin-right: 15px;
        display: inline-block;
        line-height: 35px;
        border-radius: 3px;
    }
    @media (min-width: 1200px){
        .au-ntt .ct{
            padding-right: 150px;
        }    
    }
    
</style>

<div class="container c-ototayho">
    <div class="title">
        <div class="bor">
            TẠI SAO NÊN CHỌN <span style="color:red;">HONDA Ô TÔ TÂY HỒ</span>?
        </div>
    </div>
    <div class="row">
        <div class="img col-sm-4">
            <img src="assets/standard/images/otohonda/taisaochon.jpg" alt="oto ho tay">
        </div>
        <div class="col-sm-8 txt">
            <div class="row">
                <div class="item col-sm-6">
                    <div class="tit"> GIÁ XE ÔTÔ HONDA - GIAO XE NHANH</div>
                    <div class="ct">Honda Ôtô Tây Hồ luôn cam kết mang lại mức giá ưu đãi nhất cho quý khách với thời gian giao xe nhanh nhất.</div>
                </div>
                <div class="item col-sm-6">
                    <div class="tit"> KHUYẾN MÃI Ô TÔ HONDA HẤP DẪN</div>
                    <div class="ct">Với hoạt động bán hàng sôi nổi, chúng tôi luôn cập nhật sớm nhất các chương trình khuyến mãi của hãng và đại lý.</div>
                </div>
                <div class="item col-sm-6">
                    <div class="tit">LÁI THỬ TẬN NHÀ - HONDA TÂY HỒ</div>
                    <div class="ct">
                        Lái thử xe là một bước rất quan trọng, Thành sẽ hỗ trợ Quý khách nhanh chóng trải nghiệm xe Honda mà không tốn thời gian.
                    </div>
                </div>
                <div class="item col-sm-6">
                    <div class="tit">TƯ VẤN TẬN TÌNH - ĐẠI LÝ HONDA</div>
                    <div class="ct">
                        Với nhiều năm kinh nghiệm tư vấn trong ngành ô tô, Thành sẽ hỗ trợ Quý Khách chọn được chiếc xe ô tô ưng ý nhất.
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="au-ntt container">
    <div class="row">
        <div class="col-lg-9 txt">
            <div class="title">Nguyễn Trung Thành</div>
            <div class="ct">
                <p>Nhân viên Kinh Doanh Showroom <span style="color:red;">Honda Tây Hồ</span></p>
                <p>Nhiều năm kinh nghiệm tư vấn xe và thủ tục ngân hàng. Xin hãy gọi ngay và gặp trực tiếp để được tư vấn chính xác và nhận giá tốt nhất.</p>

                <div class="btn"><i class="fa fa-phone" style="display: inline-block;margin-right: 10px;"></i>0981 738 999</div>
                <div class="btn"><i class="fa fa-envelope" style="display: inline-block;margin-right: 10px;"></i> thanhnt.honda@gmail.com</div>

            </div>
        </div>
        <div class="col-lg-3 img">
            <img src="assets/standard/images/otohonda/au.jpg" alt="oto tay ho">
        </div>
    </div>
</div>



