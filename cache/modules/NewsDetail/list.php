
    <div class="clearfix"></div>
    <section class="section_offset_gioithieu">
        <div class="container">
            <div class="row">
           <div id="dnn_contentpane" class="col-lg-12 col-md-12 col-sm-12">
            <div class="DnnModule DnnModule-alupnewsnewsdetail DnnModule-1179"><a name="1179"></a><div>
    
   
    <div id="dnn_ctr1179_ContentPane" class="">
            <!-- Start_Module_1179 -->
            <div id="dnn_ctr1179_ModuleContent" class="DNNModuleContent ModalupnewsnewsdetailC">
    
                    <div class="top_09" >
                        <ul class="tab-group">
                            
                                    <?php
					if(isset($this->map['item_related']) and is_array($this->map['item_related']))
					{
						foreach($this->map['item_related'] as $key1=>&$item1)
						{
							if($key1!='current')
							{
								$this->map['item_related']['current'] = &$item1;?>
                                    <li > <a href='/trang-tin/tin-tuc/<?php echo $this->map['item_related']['current']['name_id'];?>.html' style="display: block;max-width: 200px;height: 20px;white-space: nowrap; overflow: hidden;text-overflow: ellipsis;"><?php echo $this->map['item_related']['current']['name'];?></a></li>
                                    
							
						<?php
							}
						}
					unset($this->map['item_related']['current']);
					} ?>
                                

                                    
                                
                        </ul>


                        <div class="tab-body-group_details">
                            <div class="body-group" style="text-align: left;">
                                <div style="padding-right: 15px; padding-left: 15px;">
                                    <?php if(Url::get('name_id')!='gioi-thieu' && Url::get('name_id')!='tuyen-dung' && Url::get('name_id')!='lien-he'){ ?>
                                    <div id="dnn_ctr1179_view_titleDiv" style="text-align: left; font-weight: bold; font-size: 20px; margin-bottom: 8px; color: #e2001a;" class="title">
                                        <?php echo $this->map['name'];?>
                                    </div>
                                    
                                    <span style="Color:#aa2e3c;"><?php echo date('d/m/Y',$this->map['time']) ?></span>
                                    <?php }?>
                                    <div id="dnn_ctr1179_view_seoDiv" style="padding-bottom: 10px;display: none;">
                                        <!-- mang xa hoi -->
                                    </div>
                                    <?php echo $this->map['description'];?>
                        </div>

                    <!-- --------------------------------bài viết khác------------------------>


                                    <div id="dnn_ctr1179_view_authorDiv">
                                    </div>
                                    <div style="font-weight: bold; font-size: 14px; margin: 10px 0px; color: #e2001a;">
                                        Các bài viết khác:
                                    </div>
                                    <hr />
                                    <div class="newsDetail_OrtherNews">

                                                <?php
					if(isset($this->map['lastest_news']) and is_array($this->map['lastest_news']))
					{
						foreach($this->map['lastest_news'] as $key2=>&$item2)
						{
							if($key2!='current')
							{
								$this->map['lastest_news']['current'] = &$item2;?>
                                                <div style="margin-left: 10px;" class="newslq">
                                                    <a href='/trang-tin/tin-tuc/<?php echo $this->map['lastest_news']['current']['name_id'];?>.html' title='<?php echo $this->map['lastest_news']['current']['name'];?>'
                                                        class="refri-news-title-relt bold">
                                                        <i class="fa fa-circle" aria-hidden="true"></i>
                                                        <?php echo $this->map['lastest_news']['current']['name'];?>
                                                    </a>
                                                </div>
                                                
							
						<?php
							}
						}
					unset($this->map['lastest_news']['current']);
					} ?>
                                            
                                                
                                            
                                        <div style="clear: both">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <style>
                        .ModalupnewsnewsdetailC hr {
                            margin-top: -16px;
                            margin-left: 13%;
                            margin-right: 2%;
                        }

                        .box-content {
                            margin-top: 20px;
                        }

                        .newslq {
                            padding: 4px 0;
                        }

                        .newsDetail_OrtherNews a {
                            color: #444;
                            text-decoration: none;
                        }

                            .newsDetail_OrtherNews a:hover {
                                color: #e2001a;
                                text-decoration: none;
                            }

                        .fa-circle {
                            font-size: 8px;
                            margin-top: 0px !important;
                            color: #e2001a;
                        }
                    </style>

                    </div>
                <!-- End_Module_1179 -->
            </div>

    </div>
</div></div>
            </div>
        </div>
    </section>
    <div class="clearfix"></div>


    <style>
        .fa-circle{
            width: 5px;
            height: 5px;
            background: #cf0016;
            padding-top: -10px;
            /* margin-top: 8px!important; */
            position: absolute;
            top: 12px;
            left: 0px;
            border-radius: 50%;
        }
        .fa-circle:before{
            display: none;
        }
        .newslq{
            position: relative;
            padding-left: 20px;
        }
        img{
            max-width: 100%;
        }
    </style>