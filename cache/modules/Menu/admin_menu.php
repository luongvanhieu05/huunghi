<link rel="stylesheet" href="assets/default/css/global.css" type="text/css" />
<script src="packages/core/includes/js/common.js" type="text/javascript"></script>
<link rel="stylesheet" href="assets/admin/scripts/bootstrap_datepicker/css/bootstrap-datepicker.min.css" type="text/css" />
<script src="assets/admin/scripts/bootstrap_datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="assets/admin/scripts/bootstrap_datepicker/locales/bootstrap-datepicker.vi.min.js" charset="UTF-8"></script>
<div class="admin-menu">
  <div class="navbar navbar-default" role="navigation">
    <div class="container">
      <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a>
      </div>
    <div class="navbar-collapse collapse" id="bs-example-navbar-collapse-1">
      <?php 
				if(($this->map['categories']))
				{?>
      <ul class="nav navbar-nav">
      <?php
					if(isset($this->map['categories']) and is_array($this->map['categories']))
					{
						foreach($this->map['categories'] as $key1=>&$item1)
						{
							if($key1!='current')
							{
								$this->map['categories']['current'] = &$item1;?>
        <li>
          <a href="#" data-toggle="dropdown" class="dropdown-toggle">
          <?php echo Portal::language()==1?$this->map['categories']['current']['name_1']:$this->map['categories']['current']['name_2']; ?>
          <?php 
				if(($this->map['categories']['current']['child']))
				{?><b class="caret"></b>
				<?php
				}
				?>
          </a>
            <?php 
				if(($this->map['categories']['current']['child']))
				{?>
            <ul class="dropdown-menu">         
              <?php $group = false;?>
              <?php
					if(isset($this->map['categories']['current']['child']) and is_array($this->map['categories']['current']['child']))
					{
						foreach($this->map['categories']['current']['child'] as $key2=>&$item2)
						{
							if($key2!='current')
							{
								$this->map['categories']['current']['child']['current'] = &$item2;?>
                <?php	
                    if(!$this->map['categories']['current']['child']['current']['url']) $this->map['categories']['current']['child']['current']['url'] = '';
                ?>
                  <?php 
				if(($this->map['categories']['current']['child']['current']['child']))
				{?>
                  <li class="menu-item dropdown dropdown-submenu">
                  <a data-toggle="dropdown" role="menu">
                  <?php echo Portal::language()==1?$this->map['categories']['current']['child']['current']['name_1']:$this->map['categories']['current']['child']['current']['name_2']; ?>
                  </a>
                  <ul class="dropdown-menu">
                    <?php $sub_group = false; ?>
                    <?php
					if(isset($this->map['categories']['current']['child']['current']['child']) and is_array($this->map['categories']['current']['child']['current']['child']))
					{
						foreach($this->map['categories']['current']['child']['current']['child'] as $key3=>&$item3)
						{
							if($key3!='current')
							{
								$this->map['categories']['current']['child']['current']['child']['current'] = &$item3;?>
                    <?php	if(isset($this->map['categories']['current']['child']['current']['child']['current']['group_name']) and $sub_group != $this->map['categories']['current']['child']['current']['child']['current']['group_name'])
                        {
                          if($sub_group!="") echo '<li></li>';
                          $sub_group = $this->map['categories']['current']['child']['current']['child']['current']['group_name'];
                        }
                    ?>
                    <li>
                    <a href="<?php echo $this->map['categories']['current']['child']['current']['child']['current']['url'];?>">
                      <?php echo Portal::language()==1?$this->map['categories']['current']['child']['current']['child']['current']['name_1']:$this->map['categories']['current']['child']['current']['child']['current']['name_2']; ?>									
                    </a></li>
                    
							
						<?php
							}
						}
					unset($this->map['categories']['current']['child']['current']['child']['current']);
					} ?>
                  </ul>
                  </li>
                   <?php }else{ ?>
                  <li>
                  <a href="<?php echo $this->map['categories']['current']['child']['current']['url'];?>">
                  <?php echo Portal::language()==1?$this->map['categories']['current']['child']['current']['name_1']:$this->map['categories']['current']['child']['current']['name_2']; ?>
                  </a>
                 </li> 
                  
				<?php
				}
				?>						
              
							
						<?php
							}
						}
					unset($this->map['categories']['current']['child']['current']);
					} ?>
            </ul>            
            
				<?php
				}
				?>
        </li>		
      
							
						<?php
							}
						}
					unset($this->map['categories']['current']);
					} ?>
      </ul>
      
				<?php
				}
				?>
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" data-toggle="dropdown" class="dropdown-toggle"><?php echo Session::get('user_id'); ?> <b class="caret"></b></a>
          <ul class="dropdown-menu">
              <li><a href="trang-ca-nhan.html">Trang cá nhân</a></li>
              <li class="divider"></li>
              <li><a href="<?php echo Url::build('sign_out',array()); ?>"><?php echo Portal::language('sign_out');?></a></li>          
          </ul>
        </li>
      </ul>
    </div>
   </div>
  </div>
</div>