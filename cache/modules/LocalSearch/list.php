<link rel="stylesheet" type="text/css" href="assets/standard/css/fashion/product_list/product_list.css">
<div class="product-category-20_27_43 layout-2 left-col">
<div class="content_breadcum"></div>
<div class="container ">
 <form name="LocalSearch" id="LocalSearch" method="post" enctype="multipart/form-data">
    <h1 class="page-title">KẾT QUẢ TÌM KIẾM</h1><ul class="breadcrumb">
        <li><a href=""><i class="fa fa-home"></i></a></li>
    </ul>
    <div class="row">
        <!--=====CONTENT RIGHT=====-->
        <div class="col-sm-12">
            <div class="row category_thumb">
                <div class="col-sm-10 category_description"><p><br></p></div>
            </div>
            <div class="category_filter">
                <div class="col-md-4 btn-list-grid">
                    <div class="btn-group">
                        <button type="button" id="grid-view" class="btn btn-default grid active" data-toggle="tooltip" title="" data-original-title="Lưới"></button>
                        <button type="button" id="list-view" class="btn btn-default list" data-toggle="tooltip" title="" data-original-title="Danh sách"></button>
                    </div>
                </div>
                <div class="compare-total">                        
                	<select  name="sptrentrang" id="sptrentrang" class="form-control" onChange="LocalSearch.submit()">
                    <option value="42">Sản phẩm hiển thị trên trang</option>
                    <option value="42">42</option>
                    <option value="84">84</option>
                    <option value="84">168</option>
                  </select></div>
            </div>

            <div class="row">
                <div class="product_content">
                    <?php
					if(isset($this->map['items']) and is_array($this->map['items']))
					{
						foreach($this->map['items'] as $key1=>&$item1)
						{
							if($key1!='current')
							{
								$this->map['items']['current'] = &$item1;?>
                    <div class="product-layout product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="product-block product-thumb">
                            <div class="product-block-inner">
                                <div class="image">
                                    <a href="/san-pham/<?php echo $this->map['items']['current']['category_name_id'];?>/<?php echo $this->map['items']['current']['name_id'];?>.html"><img src="<?php if($this->map['items']['current']['small_thumb_url']!=null) echo $this->map['items']['current']['small_thumb_url'];else echo 'assets/standard/images/fashion/megnor/img.png' ?>" class="img-responsive"></a>
                                    <div class="saleback">
                                        <!--<span class="saleicon sale"><?php echo $this->map['items']['current']['name'];?></span>-->
                                    </div>
                                    <div class="action">
                                        <div class="action_inner">
                                            <div class="button-group">
                                                <button class="cart_button" type="button" data-toggle="tooltip" title="" onclick="window.location='cart.html?product_id=<?php echo $this->map['items']['current']['id'];?>';" data-original-title="Thêm vào giỏ hàng">Thêm vào giỏ hàng</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="caption">
                                    <h4><a href="/san-pham/<?php echo $this->map['items']['current']['category_name_id'];?>/<?php echo $this->map['items']['current']['name_id'];?>.html"><?php echo $this->map['items']['current']['name'];?></a></h4>
                                    <div class="rating_list">
                                        <?php $dem=isset($this->map['items']['current']['valuation']) ? $this->map['items']['current']['valuation'] : '0';if($this->map['items']['current']['valuation']>5)$this->map['items']['current']['valuation']=0;if($this->map['items']['current']['valuation']<0)$this->map['items']['current']['valuation']=0; ?>
                                        <?php for($i=1;$i<=$dem;$i++){if($this->map['items']['current']['valuation']>0){ ?>
                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                        <?php  }}?>
                                        <?php  for($j=1;$j<=(5-$dem);$j++){?>
                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i></span>
                                        <?php   }?>
                                    </div>
                                    <p class="desc"> <?php echo String::display_sort_title(strip_tags($this->map['items']['current']['brief']),20) ?></p>
                                    <p class="price">
                                        <span class="price-new"><?php echo Portal::language('sell_price');?>: <?php echo $this->map['items']['current']['price'];?></span>
                                        <?php 
				if((intval($this->map['items']['current']['publish_price']) > intval($this->map['items']['current']['price'])))
				{?>
                                        <br><span class="price-old"><?php echo Portal::language('price');?>: <?php echo $this->map['items']['current']['publish_price'];?></span>
                                        
				<?php
				}
				?>
                                    </p>
                                    <div class="addtolinks_list">
                                        <div class="button-group">
                                            <button class="cart_button" type="button" onClick="window.location='cart.html?product_id=<?php echo $this->map['items']['current']['id'];?>';">Thêm vào giỏ</button>
                                        </div>
                                    </div>
                                    <div class="rating">
                                        <?php $dem=isset($this->map['items']['current']['valuation']) ? $this->map['items']['current']['valuation'] : '0';if($this->map['items']['current']['valuation']>5)$this->map['items']['current']['valuation']=0;if($this->map['items']['current']['valuation']<0)$this->map['items']['current']['valuation']=0; ?>
                                            <?php for($i=1;$i<=$dem;$i++){if($this->map['items']['current']['valuation']>0){ ?>
                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                        <?php  }}?>
                                            <?php  for($j=1;$j<=(5-$dem);$j++){?>
                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i></span>
                                        <?php   }?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
							
						<?php
							}
						}
					unset($this->map['items']['current']);
					} ?>
                </div>
            </div>
            <div class="pagination-wrapper">
                <div class="col-sm-6 text-left page-link">
                    <div class="pt"><?php echo $this->map['paging'];?></div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="form_block_id" value="<?php echo isset(Module::$current->data)?Module::$current->data['id']:'';?>" />
			</form >
			
			
</div>
</div>