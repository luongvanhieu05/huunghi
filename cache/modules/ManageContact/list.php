<fieldset id="toolbar">

	<legend><?php echo Portal::language('manage_profile');?></legend>

	<div id="toolbar-personal"><?php echo Portal::language('manage_contact');?></div>

	<div id="toolbar-content" align="right">

	<table>

	  <tbody>

		<tr>

		  <?php if(User::can_view(false,ANY_CATEGORY)){?><td id="toolbar-help" align="center"><a href="<?php echo Url::build('help');?>#"> <span title="Help"> </span> <?php echo Portal::language('Help');?> </a> </td><?php }?>

		</tr>

	  </tbody>

	</table>

	</div>

</fieldset>

<br>

<fieldset id="toolbar">

<?php
					if(isset($this->map['items']) and is_array($this->map['items']))
					{
						foreach($this->map['items'] as $key1=>&$item1)
						{
							if($key1!='current')
							{
								$this->map['items']['current'] = &$item1;?>

<div style="margin-top:5px;"></div>

<form name="ContactList" method="post" action="?<?php echo htmlentities($_SERVER['QUERY_STRING']);?>">

	<table width="98%" cellpadding="5" cellspacing="0">

		<tr>

			<td width="1%" valign="top">

				<img src="skins/default/images/cms/toolbar/icon-32-forward.png">

			</td>

			<td width="90%" valign="top" style="line-height:18px;;">

					<b><?php echo Portal::language('full_name');?>:</b>&nbsp;<strong><?php echo $this->map['items']['current']['name'];?></strong> ~ <a ><?php echo $this->map['items']['current']['portal_id'];?></a><br />

					<?php 
				if(($this->map['items']['current']['time']))
				{?><b><?php echo Portal::language('date_send');?></b> :&nbsp;<?php echo date('H:i d/m/Y',$this->map['items']['current']['time']);?> <br />

					
				<?php
				}
				?>

					<?php 
				if(($this->map['items']['current']['phone']))
				{?><b><?php echo Portal::language('phone');?>:</b>&nbsp;<?php echo $this->map['items']['current']['phone'];?> <br />

					
				<?php
				}
				?>

					<?php 
				if(($this->map['items']['current']['address']))
				{?><b><?php echo Portal::language('address');?>:</b> &nbsp;<?php echo $this->map['items']['current']['address'];?><br>

					
				<?php
				}
				?>

					<?php 
				if(($this->map['items']['current']['email']))
				{?><b><?php echo Portal::language('email');?>:</b> <a href="mailto:<?php echo $this->map['items']['current']['email'];?>">&nbsp;<?php echo $this->map['items']['current']['email'];?></a><br>

					
				<?php
				}
				?>

					<?php 
				if(($this->map['items']['current']['content']))
				{?><b>Nội dung:</b> &nbsp;<?php echo $this->map['items']['current']['content'];?></a><br>

					
				<?php
				}
				?>

		  </td>

			<td valign="top" nowrap="nowrap" align="right">

				<?php 
				if(($this->map['items']['current']['is_check']==1))
				{?>

					Đã xem

				 <?php }else{ ?>

				<span style="color:#FF0000">Chưa xem</span>

				
				<?php
				}
				?>

				<a href="javascript:void(0)" onClick="change_display_status($('div_<?php echo $this->map['items']['current']['id'];?>'),this);"><?php echo Portal::language('detail');?></a>&nbsp;|

				<a href="<?php echo URL::build_current(array('cmd'=>'delete'));?>&id=<?php echo $this->map['items']['current']['id'];?>" title="Delete"><img src="<?php echo Portal::template('');?>/images/buttons/delete.gif" /></a>

			</td>

		</tr>

		<tr>

			<td colspan="3" align="center">

				<div id="div_<?php echo $this->map['items']['current']['id'];?>" style="display:none;">

					<table cellpadding="4" cellspacing="0" width="100%" style="#width:99%;margin-top:8px;background-color:#FFFFFF" border="1" bordercolor="#E7E7E7" align="center">

						<tr>

							<td width="42%" align="left"><b><?php echo Portal::language('content');?></b></td>

							<td colspan="2" align="right">

							<?php echo Portal::language('is_check');?>&nbsp; <input  name="confirm_<?php echo $this->map['items']['current']['id'];?>"  type="checkbox" <?php if($this->map['items']['current']['is_check']==1){ echo 'checked="checked"';}?> value="1" onclick="location='<?php echo Url::build_current(array('cmd'=>'check','id'=>$this->map['items']['current']['id']));?>'">

							<!--<?php echo Portal::language('answer');?>&nbsp;<a><img src="skins/default/images/buttons/right.jpg">--></a>

						  </td>

						</tr>

						<tr>

							<td colspan="3" ><?php echo $this->map['items']['current']['content'];?></td>

						</tr>

						<tr>

							<td  align="left"><b><?php echo Portal::language('address');?></b></td>

							<td colspan="2"  align="left"><b><?php echo Portal::language('email');?></b></td>

						</tr>

						<tr>

							<td  align="left"><?php echo $this->map['items']['current']['address'];?></td>

							<td colspan="2"  ><?php echo $this->map['items']['current']['email'];?></td>

							</tr>

						<tr>

							<td width="33%"  align="left"><b><?php echo Portal::language('phone');?></b></td>

							<td width="25%"  align="left"><b><?php echo Portal::language('name_sender');?></b></td>

						</tr>

						<tr>

							<td ><?php echo $this->map['items']['current']['phone'];?></td>

							<td ><?php echo $this->map['items']['current']['name'];?></td>

						</tr>

					</table>

				</div>

			</td>

		</tr>

	</table>


							
						<?php
							}
						}
					unset($this->map['items']['current']);
					} ?>

<input type="hidden" name="form_block_id" value="<?php echo isset(Module::$current->data)?Module::$current->data['id']:'';?>" />
			</form >
			
			

<table width="100%" cellpadding="6" cellspacing="0">

<tr>

	<td><?php echo $this->map['paging'];?>&nbsp;</td>

</tr>

</table>

</fieldset>

<script language="javascript">

function change_display_status(obj, detail_div)

{

	if(obj.style.display=='none')

	{

		obj.style.display='';

		detail_div.innerHTML='<?php echo Portal::language('close');?>';

		<?php
					if(isset($this->map['items']) and is_array($this->map['items']))
					{
						foreach($this->map['items'] as $key2=>&$item2)
						{
							if($key2!='current')
							{
								$this->map['items']['current'] = &$item2;?>

		if('div_<?php echo $this->map['items']['current']['id'];?>' != obj.id)

		{

			$('div_<?php echo $this->map['items']['current']['id'];?>').style.display = 'none';

		}

		
							
						<?php
							}
						}
					unset($this->map['items']['current']);
					} ?>

	}

	else

	{

		obj.style.display='none';

		detail_div.innerHTML='<?php echo Portal::language('detail');?>';

	}

}

</script>