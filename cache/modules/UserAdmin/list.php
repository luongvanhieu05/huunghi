<script>
	function check_selected()
	{
		var status = false;
		jQuery('form :checkbox').each(function(e){
			if(this.checked)
			{
				status = true;
			}
		});
		return status;
	}
	function make_cmd(cmd)
	{
		jQuery('#cmd').val(cmd);
		document.ListUserAdminForm.submit();
	}
</script>
<fieldset id="toolbar">
	<div id="toolbar-personal">
		Quản lý người dùng <span style="font-size:16px;color:#0B55C4;">[ <?php echo Portal::language(Url::get('cmd','list'));?> ]</span>
	</div>
	<div id="toolbar-content">
	<table align="right">
	  <tbody>
		<tr>
			<td id="toolbar-new"  align="center"><a href="<?php echo Url::build_current(array('cmd'=>'add'));?>#"> <span title="New"> </span> <?php echo Portal::language('New');?> </a> </td>
		  <?php if(Url::get('cmd')!='delete')
		  {?>
		  	<td id="toolbar-trash"  align="center"><a onclick="if(confirm('<?php echo Portal::language('are_you_sure_delete');?>')){if(check_selected()){make_cmd('delete')}else{alert('<?php echo Portal::language('You_must_select_atleast_item');?>');}}"> <span title="Trash"> </span> <?php echo Portal::language('Trash');?> </a> </td>
			<?php }else{?>
				<td id="toolbar-trash"  align="center"><a onclick="if(check_selected()){make_cmd('delete')}"> <span title="Trash"> </span> <?php echo Portal::language('Trash');?> </a> </td>
			<?php }?>
		</tr>
	  </tbody>
	</table>
	</div>
</fieldset>
<br>
<fieldset id="toolbar">
	<form method="post" name="SearchUserAdminForm">
		<table>
			<tr>
				<td align="right" nowrap style="font-weight:bold"><?php echo Portal::language('user_name');?></td>
				<td nowrap>
					<input  name="user_id" id="user_id" style="width:300" type ="text" value="<?php echo String::html_normalize(URL::get('user_id'));?>">
					<input type="submit" value="<?php echo Portal::language('search');?>">
				</td>
			</tr>
		</table>
		<input type="hidden" name="page_no" value="1" />
	<input type="hidden" name="form_block_id" value="<?php echo isset(Module::$current->data)?Module::$current->data['id']:'';?>" />
			</form >
			
			
	<a name="top"></a>
	<form name="ListUserAdminForm" method="post">
	<table cellpadding="6" cellspacing="0" width="100%" style="#width:99%;margin-top:8px;" border="1" bordercolor="#E7E7E7" align="center">
		<tr valign="middle" bgcolor="#EFEFEF" style="line-height:20px">
			<th width="1%" title="<?php echo Portal::language('check_all');?>"><input type="checkbox" value="1" id="UserAdmin_all_checkbox" onclick="jQuery('.selected-ids').attr('checked',this.checked)"<?php if(URL::get('cmd')=='delete') echo ' checked';?>></th>
			<th nowrap align="left" ><a><?php echo Portal::language('user_name');?></a></th>
				<th nowrap align="left" ><a><?php echo Portal::language('full_name');?></a></th>
				<th nowrap align="left" ><a><?php echo Portal::language('email');?></a></th>
				<th nowrap align="left" ><a><?php echo Portal::language('active');?></a></th>
				<th nowrap align="left" ><a><?php echo Portal::language('block');?></a></th>
				<th nowrap align="left" ><a><?php echo Portal::language('join_date');?></a></th>
				<th nowrap align="left" ><a><?php echo Portal::language('zone_id');?></a></th>
		        <th nowrap align="left" width="1%"><a><?php echo Portal::language('privilege');?></a></th>
		</tr>
		<?php $i = 1;?>
		<?php
					if(isset($this->map['items']) and is_array($this->map['items']))
					{
						foreach($this->map['items'] as $key1=>&$item1)
						{
							if($key1!='current')
							{
								$this->map['items']['current'] = &$item1;?>
		<?php $i++;?>
		<tr bgcolor="<?php if((URL::get('just_edited_id',0)==$this->map['items']['current']['id']) or (is_numeric(array_search($this->map['items']['current']['id'],$this->map['just_edited_ids'])))){ echo 'white';} else {echo 'white';}?>" valign="middle" <?php Draw::hover('#FFFFDD');?> style="cursor:hand;<?php if($i%2){echo 'background-color:#F9F9F9';}?>" id="UserAdmin_tr_<?php echo $this->map['items']['current']['id'];?>">
			<td><input name="selected_ids[]" type="checkbox" value="<?php echo $this->map['items']['current']['id'];?>" class="selected-ids" onclick="" id="UserAdmin_checkbox" <?php if(URL::get('cmd')=='delete') echo 'checked';?>></td>
			<td nowrap align="left" onclick="location='<?php echo URL::build_current();?>&cmd=edit&id=<?php echo $this->map['items']['current']['id'];?>';">
					<?php echo $this->map['items']['current']['id'];?>			</td>
			<td nowrap align="left" onclick="location='<?php echo URL::build_current();?>&cmd=edit&id=<?php echo $this->map['items']['current']['id'];?>';"><?php echo $this->map['items']['current']['full_name'];?></td>
			<td nowrap align="left" onclick="location='<?php echo URL::build_current();?>&cmd=edit&id=<?php echo $this->map['items']['current']['id'];?>';">
					<?php echo $this->map['items']['current']['email'];?>			</td>
			<td nowrap align="left" onclick="location='<?php echo URL::build_current();?>&cmd=edit&id=<?php echo $this->map['items']['current']['id'];?>';">
					<?php echo $this->map['items']['current']['active'];?>			</td>
			<td nowrap align="left" onclick="location='<?php echo URL::build_current();?>&cmd=edit&id=<?php echo $this->map['items']['current']['id'];?>';">
					<?php echo $this->map['items']['current']['block'];?>			</td>
			<td nowrap align="left" onclick="location='<?php echo URL::build_current();?>&cmd=edit&id=<?php echo $this->map['items']['current']['id'];?>';">
					<?php echo $this->map['items']['current']['create_date'];?>			</td>
			<td align="left" nowrap onclick="location='<?php echo URL::build_current();?>&cmd=edit&id=<?php echo $this->map['items']['current']['id'];?>';">
					<?php echo $this->map['items']['current']['zone_id'];?>			</td>
		    <td align="left" nowrap onclick="location='<?php echo URL::build_current();?>&cmd=edit&id=<?php echo $this->map['items']['current']['id'];?>';">
				<a href="<?php echo Url::build('grant_privilege',array('account_id'=>$this->map['items']['current']['id'],'cmd'=>'grant'));?>"><img src="assets/default/images/buttons/list_button.gif"></a>
			</td>
		</tr>
		
							
						<?php
							}
						}
					unset($this->map['items']['current']);
					} ?>
	  </table>
		<table  width="100%" cellpadding="6" cellspacing="0" style="background-color:#F0F0F0;border:1px solid #E7E7E7;#width:99%" align="center">
			<tr>
				<td><?php echo $this->map['paging'];?></td>
				
			</tr>
		</table>
		<input type="hidden" name="cmd" value="delete"/>
		<input type="hidden" name="page_no" value="1"/>
		<?php 
				if((URL::get('cmd')=='delete'))
				{?>
		<input type="hidden" name="confirm" value="1" />
		
				<?php
				}
				?>
<input type="hidden" name="form_block_id" value="<?php echo isset(Module::$current->data)?Module::$current->data['id']:'';?>" />
			</form >
			
			
<div style="#height:8px;"></div>
</fieldset>