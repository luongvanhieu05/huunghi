<link rel="stylesheet" type="text/css" href="assets/standard/css/fashion/detail/detail.css"/>
<div class="content_breadcum">
    <ul class="breadcrumb">
        <li><a href=""><i class="fa fa-home"></i></a></li>
        <li><a href="/dang-nhap.html">Đăng nhập</a></li>
    </ul>
</div>
<div class="container">  
  <div class="row">
     <div class="col-md-12">
        <table class="table">
            <tr>
              <td class="sign-in-welcome"><?php echo Portal::language('welcome');?> : <a target="_blank" href="personal.html"><b style="color:#009EE7;"><?php echo Session::get('user_id')?></b></a></td>
            </tr>
            <?php if(User::is_login()){?>
            <tr>
              <td class="sign-in-welcome"><a target="_blank" href="?page=product_admin">Quản lý sản phẩm</a><td width="5%"/>
            </tr>
            <?php }?>
            <?php 
				if((User::can_view(MODULE_SETTING,ANY_CATEGORY)))
				{?>
            <tr>
              <td class="sign-in-welcome"><a target="_blank" href="<?php echo URL::build('user_admin','',false);?>">Quản lý người dùng</a><td width="5%"/>
            </tr>
            <tr>
              <td class="sign-in-welcome"><a target="_blank" href="<?php echo URL::build('setting','',false);?>">Cấu hình website</a><td/>
            </tr>
            
				<?php
				}
				?>
            <tr>
              <td class="sign-in-welcome"><a class="sign-in-link" href="<?php echo URL::build('sign_out');?>&href=?<?php echo urlencode($_SERVER['QUERY_STRING'])?>">Thoát</a></td>
            </tr>
          </table>
    </div>
  </div>
</div>  