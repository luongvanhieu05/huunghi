<section id="blog" class="container">
     <div class="row">        	
      <div class="col-sm-12">
          <ol class="breadcrumb">
            <li><a href=""><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a></li>
            <li><a href="faq.html">Hỏi đáp</a></li>
          </ol>
      </div>
      <div class="col-md-12 faq-list-bound"> 
          <div class="title">	
              <h1><?php echo Portal::language('FAQ');?></h1>
          </div>
          <div class="faq-list-content">
            <?php 
				if(($this->map['faqs']))
				{?>
            <?php $i=1; ?>
            <?php
					if(isset($this->map['faqs']) and is_array($this->map['faqs']))
					{
						foreach($this->map['faqs'] as $key1=>&$item1)
						{
							if($key1!='current')
							{
								$this->map['faqs']['current'] = &$item1;?>
                <div class="faq-list-item">
                    <h3><img src="assets/standard/images/question.gif" alt="FAQ"> <?php echo strip_tags($this->map['faqs']['current']['name']); ?></h3>
                    <div class="faq-list-answer" id="answer_<?php echo $this->map['faqs']['current']['id'];?>"><?php echo strip_tags($this->map['faqs']['current']['description']); ?></div>
                </div>
            
							
						<?php
							}
						}
					unset($this->map['faqs']['current']);
					} ?>
            <div class="faq-list-paging"><?php echo $this->map['paging'];?></div>
             <?php }else{ ?>
            <div class="notice"><?php echo Portal::language('data_is_updating');?></div>
            
				<?php
				}
				?>
          </div>
      </div>
   </div>    
</section>     