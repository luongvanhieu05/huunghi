<section id="recent-works" class="gallery">
  <div class="container">
    <div class="col-sm-12">
        <ol class="breadcrumb">
          <li><a href="#"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a></li>
          <li><a href="album-anh">Thư viện ảnh</a></li>
           <?php 
				if(($this->map['category_name_id'] != 'album-anh'))
				{?>
           <li><a href="album-anh/<?php echo $this->map['category_name_id'];?>.html"><?php echo $this->map['category_name'];?></a></li>         
           
				<?php
				}
				?>
        </ol>
    </div>
    <div class="center">
        <h1><?php echo $this->map['category_name'];?></h1>
    </div>
    <div class="row sub-gallery">
    	<div class="col-xs-12 col-sm-4 col-md-12">
    	 <div class="desc"><?php $desc = preg_replace("/width=\"[^\"]+\"/","",$this->map['description']); $desc = preg_replace("/height=\"[^\"]+\"/","",$desc); echo $desc; ?> </div>
     	</div>
      <?php
					if(isset($this->map['photos']) and is_array($this->map['photos']))
					{
						foreach($this->map['photos'] as $key1=>&$item1)
						{
							if($key1!='current')
							{
								$this->map['photos']['current'] = &$item1;?>
        <div class="col-xs-12 col-sm-4 col-md-3">
            <div class="recent-work-wrap">
                <a href="<?php echo $this->map['photos']['current']['image_url'];?>" rel="prettyPhoto[pp_gal]"><img src="<?php echo $this->map['photos']['current']['small_thumb_url'];?>" class="img-responsive" alt="<?php echo $this->map['photos']['current']['name'];?>"/></a>
                
            </div>
        </div>   
      
							
						<?php
							}
						}
					unset($this->map['photos']['current']);
					} ?>
    </div><!--/.row-->
    <div class="paging"><?php echo $this->map['paging'];?></div>
  </div>
</section>
<link href="assets/standard/js/prettyPhoto/css/prettyPhoto.css" rel="stylesheet">
<script src="assets/standard/js/prettyPhoto/jquery.prettyPhoto.js"></script>
<script type="text/javascript" charset="utf-8">
 jQuery(document).ready(function(){
	jQuery("a[rel^='prettyPhoto']").prettyPhoto({animation_speed:'normal',slideshow:5000, autoplay_slideshow: false,social_tools:false,inline_markup: '<div class="pp_inline">123</div>'});
	// jQuery(".gallery:gt(0) a[rel^='prettyPhoto']").prettyPhoto({animation_speed:'fast',slideshow:10000, hideflash: true});
 });
</script>