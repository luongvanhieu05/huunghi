<section id="recent-works" class="gallery">
  <div class="container">
    <div class="col-sm-12">
        <ol class="breadcrumb">
          <li><a href="#"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a></li>
          <li><a href="album-anh.html">Thư viện ảnh</a></li>
        </ol>
    </div>
    <div class="center">
        <h1>Thư viện ảnh</h1>
    </div>
    <div class="row">
      <?php
					if(isset($this->map['albums']) and is_array($this->map['albums']))
					{
						foreach($this->map['albums'] as $key1=>&$item1)
						{
							if($key1!='current')
							{
								$this->map['albums']['current'] = &$item1;?>
        <div class="col-xs-12 col-sm-4 col-md-3">
            <div class="recent-work-wrap">
                <a href="album-anh/<?php echo $this->map['albums']['current']['name_id'];?>.html" title="<?php echo $this->map['albums']['current']['name_1'];?>"><img src="<?php echo $this->map['albums']['current']['icon_url'];?>" class="img-responsive" alt="<?php echo $this->map['albums']['current']['name_1'];?>"/></a>
                <div class="overlay">
                    <div class="recent-work-inner">
                        <h3><a href="<?php echo $this->map['albums']['current']['image_url'];?>"><?php echo $this->map['albums']['current']['name_1'];?></a> </h3>
                    </div> 
                </div>
            </div>
        </div>   
      
							
						<?php
							}
						}
					unset($this->map['albums']['current']);
					} ?>
    </div><!--/.row-->
  </div>
</section>