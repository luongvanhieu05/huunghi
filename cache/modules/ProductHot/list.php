<!--==phan ngay sau banner=-->
<div class="subbanner_top_outer container">
    <div class="subbanner_top">
        <div class="banner banner1">
            <div class="banner_inner">
                <div class="img"></div>
                <div class="data">
                    <div class="h1">Vận chuyển toàn quốc</div>
                    <div class="h2">Thanh toán tại nhà</div>
                </div>
            </div>
        </div>
        <div class="banner banner2">
            <div class="banner_inner">
                <div class="img"></div>
                <div class="data">
                    <div class="h1">Miễn phí </div>
                    <div class="h2">đổi trả trong 7 ngày</div>
                </div>
            </div>
        </div>
        <div class="banner banner3">
            <div class="banner_inner">
                <div class="img"></div>
                <div class="data">
                    <div class="h1">Miễn phí </div>
                    <div class="h2">cắt gấu, bóp bụng, bóp ống</div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--==and ngay sau banner=-->
<!--san pham hot-->
<div class="staticbanner_outer">
		<div class="staticbanner_inner">
          <div class="staticbanner1">
              <a href="<?php echo Portal::get_setting('home_banner_1_url');?>">
                  <div class="staticbanner1_inner">
                      <img class="img-product-hot-a" src="<?php echo Portal::get_setting('home_banner_1_image_url');?>" alt="">
                  </div>
              </a>
          </div>
          <div class="staticbanner2">
              <a href="<?php echo Portal::get_setting('home_banner_2_url');?>">
                  <div class="staticbanner2_inner">
                      <img class="img-product-hot-a" src="<?php echo Portal::get_setting('home_banner_2_image_url');?>" alt="">
                  </div>
              </a>
          </div>
         <div class="staticbanner3">
          <div class="staticbanner3_inner">
              <a href="<?php echo Portal::get_setting('home_banner_3_url');?>"><div class="staticbanner3_inner1"><img src="<?php echo Portal::get_setting('home_banner_3_image_url');?>" alt=""></div> </a>
               <a href="<?php echo Portal::get_setting('home_banner_4_url');?>"><div class="staticbanner3_inner2"><img src="<?php echo Portal::get_setting('home_banner_4_image_url');?>" alt=""></div> </a>
          </div>
        </div>
        <div class="staticbanner4">
              <a href="<?php echo Portal::get_setting('home_banner_5_url');?>">
                  <div class="staticbanner4_inner">
                      <img class="img-product-hot-a" src="<?php echo Portal::get_setting('home_banner_5_image_url');?>" alt="">
                  </div>
              </a>
          </div>
		</div>
</div>
<br clear="all">