<?php 
$page = (Portal::language() == 1)?'san-pham':'product';
?>
<section id="recent-works">
  <div class="container">
  		<div class="col-sm-12">
          <ol class="breadcrumb">
              <li><a href=""><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a></li>
              <li><a href="<?php echo $page.'/';?>"><?php echo Portal::language('product');?></a></li>
              <?php 
				if(($this->map['category_name_id']!='san-pham' and $this->map['category_name_id']!='product'))
				{?>
              <li><a href="<?php echo $page.'/';?><?php echo $this->map['category_name_id'];?>/"><?php echo $this->map['category_name'];?></a></li>
              
				<?php
				}
				?>
          </ol>
      </div>
      <div class="center wow fadeInDown animated" style="visibility: visible; animation-name: fadeInDown; -webkit-animation-name: fadeInDown;">
          <h2><?php echo $this->map['category_name'];?></h2>
          <p class="lead">Công ty chúng tôi cam kết đáp ứng và hỗ trợ khách hàng một cách tối đa các yêu cầu về chất lượng,<br> quy cách sản phẩm và tiến độ giao hàng.
          </p>
      </div>
      <div class="row">
      	<?php
					if(isset($this->map['items']) and is_array($this->map['items']))
					{
						foreach($this->map['items'] as $key1=>&$item1)
						{
							if($key1!='current')
							{
								$this->map['items']['current'] = &$item1;?>
        	<?php $url = $page.'/'.($this->map['items']['current']['name_id']).'/'.(Url::get('tags')?'tags/'.Url::get('tags'):'');?>
          <div class="col-xs-12 col-sm-4 col-md-3">
              <div class="recent-work-wrap">
                  <a href="<?php echo $this->map['items']['current']['url']?$this->map['items']['current']['url']:$url;?>"><img src="<?php echo $this->map['items']['current']['image_url'];?>" class="img-responsive"/></a>
                  <div class="overlay">
                      <div class="recent-work-inner">
                          <h3><a href="<?php echo $this->map['items']['current']['url']?$this->map['items']['current']['url']:$url;?>"><?php echo $this->map['items']['current']['name'];?></a> </h3>
                      </div> 
                  </div>
              </div>
          </div>   
				
							
						<?php
							}
						}
					unset($this->map['items']['current']);
					} ?>
      </div><!--/.row-->
  </div><!--/.container-->
</section>