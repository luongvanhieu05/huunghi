<script>
	function check_selected()
	{
		var status = false;
		jQuery('form :checkbox').each(function(e){
			if(this.checked)
			{
				status = true;
			}
		});
		return status;
	}
	function make_cmd(cmd)
	{
		jQuery('#cmd').val(cmd);
		document.ProductAdmin.submit();
	}
</script>
<fieldset id="toolbar">
	<div id="toolbar-title">
		Quản lý sản phẩm <span>[ <?php echo Portal::language(Url::get('cmd','list'));?> ]</span>
	</div>
	<div id="toolbar-content">
	<table align="right">
	  <tbody>
		<tr>
		  <?php if(User::can_add(false,ANY_CATEGORY)){?><td id="toolbar-new"  align="center"><a href="<?php echo Url::build_current(array('cmd'=>'add'));?>"> <span title="Thêm"> </span> Thêm </a> </td><?php }?>
  		  <?php if(User::can_delete(false,ANY_CATEGORY)){?><td id="toolbar-trash"  align="center"><a onclick="if(confirm('<?php echo Portal::language('are_you_sure_delete');?>')){if(check_selected()){make_cmd('delete')}else{alert('<?php echo Portal::language('You_must_select_atleast_item');?>');}}"> <span title="Xóa"> </span> Xóa </a> </td><?php }?>
		</tr>
	  </tbody>
	</table>
  </div>
</fieldset>
<br>
<fieldset id="toolbar">
	<form name="ProductAdmin" method="post" action="?<?php echo htmlentities($_SERVER['QUERY_STRING']);?>">
		<a name="top_anchor"></a>
			<table cellpadding="0" cellspacing="0" width="100%" class="table">
				<tr>
					<td width="100%">
						<?php echo Portal::language('Filter');?>:
						<input  name="search" id="search" size="20" style="width:250px;" placeholder="Có phân biệt chữ hoa chữ thường" type ="text" value="<?php echo String::html_normalize(URL::get('search'));?>">
						<button onclick="document.ProductAdmin.submit();">Tìm kiếm</button>
					</td>
					<td nowrap="nowrap">
					<select  name="category_id" class="inputbox"  id="category_id" size="1" onchange="document.ProductAdmin.submit();"><?php
					if(isset($this->map['category_id_list']))
					{
						foreach($this->map['category_id_list'] as $key=>$value)
						{
							if($key>1000000000){
								echo '<optgroup label=" - - '.$value.'">';
							}else{
								echo '<option value="'.$key.'"';
								echo '>'.$value.'</option>';
							}
							
						}
					}
					?><?php if(Url::get("category_id")){?><script type="text/javascript">getId('category_id').value = "<?php echo addslashes(URL::get('category_id',isset($this->map['category_id'])?$this->map['category_id']:''));?>";</script><?php }?>
	</select>
					<select  name="user_id" id="user_id" class="inputbox" size="1" onchange="document.ProductAdmin.submit();"><?php
					if(isset($this->map['user_id_list']))
					{
						foreach($this->map['user_id_list'] as $key=>$value)
						{
							if($key>1000000000){
								echo '<optgroup label=" - - '.$value.'">';
							}else{
								echo '<option value="'.$key.'"';
								echo '>'.$value.'</option>';
							}
							
						}
					}
					?><?php if(Url::get("user_id")){?><script type="text/javascript">getId('user_id').value = "<?php echo addslashes(URL::get('user_id',isset($this->map['user_id'])?$this->map['user_id']:''));?>";</script><?php }?>
	</select>
					<select  name="status" id="status" class="inputbox" size="1" onchange="document.ProductAdmin.submit();"><?php
					if(isset($this->map['status_list']))
					{
						foreach($this->map['status_list'] as $key=>$value)
						{
							if($key>1000000000){
								echo '<optgroup label=" - - '.$value.'">';
							}else{
								echo '<option value="'.$key.'"';
								echo '>'.$value.'</option>';
							}
							
						}
					}
					?><?php if(Url::get("status")){?><script type="text/javascript">getId('status').value = "<?php echo addslashes(URL::get('status',isset($this->map['status'])?$this->map['status']:''));?>";</script><?php }?>
	</select>
				  </td>
				</tr>
		</table>
		<table width="100%" class="table">
			<thead>
					<tr valign="middle" bgcolor="#F0F0F0" style="line-height:20px">
					<th width="1%" align="left" nowrap><a>#</a></th>
					<th width="1%" title="<?php echo Portal::language('check_all');?>">
					  <input type="checkbox" value="1" id="ProductAdmin_all_checkbox" onclick="select_all_checkbox(this.form,'ProductAdmin',this.checked,'<?php echo Portal::get_setting('crud_list_item_selected_bgcolor','#FFFFEC');?>','<?php echo Portal::get_setting('crud_item_bgcolor','white');?>');"<?php if(URL::get('cmd')=='delete') echo ' checked';?>></th>
					<th width="36%" align="left" nowrap><a>Sản phẩm</a></th>
					<th width="5%" align="center" nowrap>SL</th>
					<th width="5%" align="center" nowrap>Giá</th>
					<th width="5%" align="left" nowrap>Ảnh</th>
					<th width="2%" align="left" nowrap><a><?php echo Portal::language('status');?></a></th>
					<th width="8%" align="left" nowrap><a><?php echo Portal::language('positon');?></a><img src="assets/default/images/cms/menu/filesave.png" onclick="jQuery('#cmd').val('update_position');document.ProductAdmin.submit();" style="cursor:pointer"></th>
					<th width="12%" align="left" nowrap><a>Danh mục</a></th>
					<th width="7%" align="left" nowrap>Tài khoản</th>
					<th width="4%" align="left" nowrap><a><?php echo Portal::language('date');?></a></th>
					<th width="5%" align="left" nowrap><a><?php echo Portal::language('hitcount');?></a></th>
					<?php if(User::can_edit(false,ANY_CATEGORY))
					{?>
					<th width="2%" align="left" nowrap><a><?php echo Portal::language('edit');?></a></th>
					<?php }?>
				</tr>
		  </thead>
				<tbody>
				<?php
					if(isset($this->map['items']) and is_array($this->map['items']))
					{
						foreach($this->map['items'] as $key1=>&$item1)
						{
							if($key1!='current')
							{
								$this->map['items']['current'] = &$item1;?>
				<tr bgcolor="<?php if((URL::get('just_edited_id',0)==$this->map['items']['current']['id']) or (is_numeric(array_search($this->map['items']['current']['id'],$this->map['just_edited_ids'])))){ echo Portal::get_setting('crud_just_edited_item_bgcolor','#FFFFDD');} else {echo Portal::get_setting('crud_item_bgcolor','white');}?>" valign="middle" <?php Draw::hover(Portal::get_setting('crud_item_hover_bgcolor','#FFFFDD'));?> style="cursor:hand;<?php if($this->map['items']['current']['index']%2){echo 'background-color:#F9F9F9';}?>" id="ProductAdmin_tr_<?php echo $this->map['items']['current']['id'];?>">
					<th width="1%" align="left" nowrap><a><?php echo $this->map['items']['current']['index'];?></a></th>
					<td width="1%"><input name="selected_ids[]" type="checkbox" value="<?php echo $this->map['items']['current']['id'];?>" onclick="select_checkbox(this.form,'ProductAdmin',this,'<?php echo Portal::get_setting('crud_list_item_selected_bgcolor','#FFFFEC');?>','<?php echo Portal::get_setting('crud_item_bgcolor','white');?>');" id="ProductAdmin_checkbox" <?php if(URL::get('cmd')=='delete') echo 'checked';?>></td >
					<td align="left">[Mã: <strong><?php echo $this->map['items']['current']['id'];?></strong>] <a href="<?php echo Url::build_current(array('id'=>$this->map['items']['current']['id'],'cmd'=>'edit'));?>"><?php echo $this->map['items']['current']['name'];?></a>
					  <div style="color:#999;">Sản phẩm liên quan: <?php echo $this->map['items']['current']['related_ids'];?></div>
					  <?php 
				if(($this->map['items']['current']['open_promotion_time'] != '0000-00-00 00:00:00'))
				{?>
					  <div style="color:#F60;">Khuyến mại: <?php echo $this->map['items']['current']['open_promotion_time'];?> - <?php echo $this->map['items']['current']['close_promotion_time'];?></div>
					  
				<?php
				}
				?>
				  </td>
					<td align="left" nowrap><?php echo $this->map['items']['current']['quantity'];?> </td>
					<td align="right" nowrap="nowrap"><?php echo System::display_number($this->map['items']['current']['price']);?> </td>
					<td align="center"><img src="<?php echo $this->map['items']['current']['small_thumb_url'];?>" width="60" height="50"></td>
					<td align="left" nowrap><?php echo $this->map['items']['current']['status'];?>			  </td>
					<td align="left" nowrap><input  name="position_<?php echo $this->map['items']['current']['id'];?>" id="position_<?php echo $this->map['items']['current']['id'];?>" style="width:40px;" value="<?php echo $this->map['items']['current']['position'];?>" type ="text" value="<?php echo String::html_normalize(URL::get('position_'.$this->map['items']['current']['id']));?>"></td>
					<td align="left"><?php echo $this->map['items']['current']['categories'];?></td>
					<td align="left" nowrap><?php echo $this->map['items']['current']['user_id'];?></td>
					<td align="left" nowrap><?php echo date('h\h:i d/m/Y',$this->map['items']['current']['time']);?></td>
					<td align="left" nowrap><?php echo $this->map['items']['current']['hitcount'];?></td>
					<?php if(User::can_edit(false,ANY_CATEGORY))
					{?>
					<td align="left" nowrap width="2%"><a href="<?php echo Url::build_current(array('id'=>$this->map['items']['current']['id'],'cmd'=>'edit'));?>"><img src="assets/default/images/buttons/button-edit.png"></a></td>
					<?php }?>
				</tr>
				
							
						<?php
							}
						}
					unset($this->map['items']['current']);
					} ?>
				</tbody>
	  </table>
	<table width="100%" cellpadding="6" cellspacing="0" style="background-color:#F0F0F0;border:1px solid #E7E7E7;height:8px;#width:99%" align="center">
		<tr>
			<td width="48%" align="left">
				<?php echo Portal::language('select');?>:&nbsp;
				<a href="javascript:void(0)" onclick="select_all_checkbox(document.ProductAdmin,'ProductAdmin',true,'<?php echo Portal::get_setting('crud_list_item_selected_bgcolor','#FFFFEC');?>','<?php echo Portal::get_setting('crud_item_bgcolor','white');?>');"><?php echo Portal::language('select_all');?></a>&nbsp;
				<a href="javascript:void(0)" onclick="select_all_checkbox(document.ProductAdmin,'ProductAdmin',false,'<?php echo Portal::get_setting('crud_list_item_selected_bgcolor','#FFFFEC');?>','<?php echo Portal::get_setting('crud_item_bgcolor','white');?>');"><?php echo Portal::language('select_none');?></a>
				<a href="javascript:void(0)" onclick="select_all_checkbox(document.ProductAdmin,'ProductAdmin',-1,'<?php echo Portal::get_setting('crud_list_item_selected_bgcolor','#FFFFEC');?>','<?php echo Portal::get_setting('crud_item_bgcolor','white');?>');"><?php echo Portal::language('select_invert');?></a>		</td>
			<td width="18%">&nbsp;<a><?php echo Portal::language('display');?></a>
			  <select  name="item_per_page" class="select" style="width:50px" size="1" onchange="document.ProductAdmin.submit( );" id="item_per_page" ><?php
					if(isset($this->map['item_per_page_list']))
					{
						foreach($this->map['item_per_page_list'] as $key=>$value)
						{
							if($key>1000000000){
								echo '<optgroup label=" - - '.$value.'">';
							}else{
								echo '<option value="'.$key.'"';
								echo '>'.$value.'</option>';
							}
							
						}
					}
					?><?php if(Url::get("item_per_page")){?><script type="text/javascript">getId('item_per_page').value = "<?php echo addslashes(URL::get('item_per_page',isset($this->map['item_per_page'])?$this->map['item_per_page']:''));?>";</script><?php }?>
	</select>&nbsp;<?php echo Portal::language('of');?>&nbsp;<?php echo $this->map['total'];?></td>
			<td width="31%">
      	<div class="pt"><?php echo $this->map['paging'];?></div>
      </td>
			<td width="3%">
				<a name="bottom_anchor" href="#top_anchor"><img src="assets/default/images/top.gif" title="<?php echo Portal::language('top');?>" border="0" alt="<?php echo Portal::language('top');?>"></a>		</td>
			</tr></table>
			<table width="100%" class="table_page_setting">
	</table>
		<input type="hidden" name="cmd" value="" id="cmd">
  <input type="hidden" name="form_block_id" value="<?php echo isset(Module::$current->data)?Module::$current->data['id']:'';?>" />
			</form >
			
			
  <div style="#height:8px"></div>
</fieldset>