<script>
	function check_selected()
	{
		var status = false;
		jQuery('form :checkbox').each(function(e){
			if(this.checked && this.id=='PublisherForm_checkbox')
			{
				status = true;
			}
		});
		return status;
	}
	function make_cmd(cmd)
	{
		jQuery('#cmd').val(cmd);
		document.PublisherForm.submit();
	}
</script><fieldset id="toolbar">
	<legend><?php echo Portal::language('content_manage_system');?></legend>
	<div id="toolbar-title"><?php echo Portal::language('publisher');?></div>
	<div id="toolbar-content" align="right">
	<table>
	  <tbody>
		<tr>
		 <?php if(User::can_edit(false,ANY_CATEGORY)){?> <td id="toolbar-save"  align="center"><a onclick="make_cmd('save');"> <span title="<?php echo Portal::language('Save');?>"> </span> <?php echo Portal::language('Save');?> </a> </td><?php }?>
		  <?php if(User::can_delete(false,ANY_CATEGORY)){?><td id="toolbar-trash"  align="center"><a onclick="if(confirm('<?php echo Portal::language('are_you_sure_delete');?>')){if(check_selected()){make_cmd('delete')}else{alert('<?php echo Portal::language('You_must_select_atleast_item');?>');}}"> <span title="Trash"> </span> <?php echo Portal::language('Trash');?> </a> </td><?php }?>
		  <?php if(User::can_view(false,ANY_CATEGORY)){?><td id="toolbar-help" align="center"><a href="<?php echo Url::build_current();?>#"> <span title="Help"> </span> <?php echo Portal::language('Help');?> </a> </td><?php }?>
		</tr>
	  </tbody>
	</table>
	</div>
</fieldset>
<br>
<fieldset id="toolbar">
<form name="PublisherForm" method="post" action="?<?php echo htmlentities($_SERVER['QUERY_STRING']);?>">
	<table cellpadding="6" cellspacing="0" width="100%" style="#width:99%;margin-top:8px;" border="1" bordercolor="#E7E7E7" align="center">
	  <tr style="background-color:#F0F0F0">
		<th width="3%" align="left"><input type="checkbox" value="1" id="PublisherForm_all_checkbox" onclick="select_all_checkbox(this.form,'PublisherForm',this.checked,'<?php echo Portal::get_setting('crud_list_item_selected_bgcolor','#FFFFEC');?>','<?php echo Portal::get_setting('crud_item_bgcolor','white');?>');"<?php if(URL::get('cmd')=='delete') echo ' checked';?>></th>
		<th width="29%" align="left"><a><?php echo Portal::language('title');?></a></th>
		<th width="11%" align="left"><a><?php echo Portal::language('category_id');?></a></th>
		<th width="6%" align="left"><a><?php echo Portal::language('user_id');?></a></th>
		<th width="6%" align="left"><a><?php echo Portal::language('status');?></a></th>
		<th width="6%" align="left"><a><?php echo Portal::language('publish');?></a></th>
		<th width="8%" align="left"><a><?php echo Portal::language('time');?></a></th>
		<th width="8%" align="left"><a><?php echo Portal::language('last_time_update');?></a></th>
		<th width="5%" align="left">&nbsp;</th>
	  </tr>
	  <?php $ids='';$first = true;$i=0;?>
	<?php
					if(isset($this->map['items']) and is_array($this->map['items']))
					{
						foreach($this->map['items'] as $key1=>&$item1)
						{
							if($key1!='current')
							{
								$this->map['items']['current'] = &$item1;?>
	  <tr valign="middle" <?php Draw::hover(Portal::get_setting('crud_item_hover_bgcolor','#FFFFDD'));?> style="cursor:hand;<?php if($i%2){echo 'background-color:#F9F9F9';}?>" id="Publisher_tr_<?php echo $this->map['items']['current']['id'];?>">
		<td>
		<?php
		$i++;
		if($first)
		{
			$ids .= $this->map['items']['current']['id'];
			$first = false;
		}
		else
		{
			$ids .= ','.$this->map['items']['current']['id'];
		}
		?>
		<input name="selected_ids[]" type="checkbox" value="<?php echo $this->map['items']['current']['id'];?>" onclick="select_checkbox(this.form,'PublisherForm',this,'<?php echo Portal::get_setting('crud_list_item_selected_bgcolor','#FFFFEC');?>','<?php echo Portal::get_setting('crud_item_bgcolor','white');?>');" id="PublisherForm_checkbox" <?php if(URL::get('cmd')=='delete') echo 'checked';?>></td>
		<td><?php echo $this->map['items']['current']['name'];?></td>
		<td><?php echo $this->map['items']['current']['category_name'];?></td>
		<td><?php echo $this->map['items']['current']['user_id'];?></td>
		<td><select  name="status_<?php echo $this->map['items']['current']['id'];?>" id="status_<?php echo $this->map['items']['current']['id'];?>">
		  <?php foreach($this->map['status'] as $key=>$value){?><option value="<?php echo $key;?>"><?php echo $value;?></option><?php }?>
		  </select>
		  <script>document.getElementById('status_<?php echo $this->map['items']['current']['id'];?>').value='<?php echo $this->map['items']['current']['status'];?>';</script>
		  </td>
		<td>
			<input  name="publish_<?php echo $this->map['items']['current']['id'];?>" type="checkbox" id="publish_<?php echo $this->map['items']['current']['id'];?>" value="1">
			<?php 
				if(($this->map['items']['current']['publish']!=0))
				{?><script>document.getElementById('publish_<?php echo $this->map['items']['current']['id'];?>').checked=true;</script>
				<?php
				}
				?>
		</td>
		<td nowrap="nowrap"><?php echo date('h:i  d/m/Y',$this->map['items']['current']['time']);?></td>
		<td nowrap="nowrap"><?php echo date('h:i  d/m/Y',$this->map['items']['current']['last_time_update']?$this->map['items']['current']['last_time_update']:0);?></td>
		<td align="center"><a target="_blank" href="xem-tin/<?php echo $this->map['items']['current']['name_id'];?>.html"><img src="assets/default/images/buttons/search.gif" width="25"></a></td>
	  </tr>
	
							
						<?php
							}
						}
					unset($this->map['items']['current']);
					} ?>
	</table>
	<table width="100%" cellpadding="6" cellspacing="0" style="background-color:#F0F0F0;border:1px solid #E7E7E7;height:8px;#width:99%" align="center">
	  <tr>
		<td colspan="7">
			<b><?php echo Portal::language('select');?></b>:&nbsp;
			<a href="javascript:void(0)" onclick="select_all_checkbox(document.PublisherForm,'PublisherForm',true,'<?php echo Portal::get_setting('crud_list_item_selected_bgcolor','#FFFFEC');?>','<?php echo Portal::get_setting('crud_item_bgcolor','white');?>');"><?php echo Portal::language('select_all');?></a> |&nbsp;
			<a href="javascript:void(0)" onclick="select_all_checkbox(document.PublisherForm,'PublisherForm',false,'<?php echo Portal::get_setting('crud_list_item_selected_bgcolor','#FFFFEC');?>','<?php echo Portal::get_setting('crud_item_bgcolor','white');?>');"><?php echo Portal::language('select_none');?></a>
			| <a href="javascript:void(0)" onclick="select_all_checkbox(document.PublisherForm,'PublisherForm',-1,'<?php echo Portal::get_setting('crud_list_item_selected_bgcolor','#FFFFEC');?>','<?php echo Portal::get_setting('crud_item_bgcolor','white');?>');"><?php echo Portal::language('select_invert');?></a>
			&nbsp;&nbsp;<b><?php echo Portal::language('total');?></b>:<?php echo $this->map['total'];?>  <?php echo Portal::language('items');?>			</td>
		<td align="right" colspan="4">&nbsp;<?php echo $this->map['paging'];?></td>
	  </tr>
	</table>
	<input type="hidden" name="cmd" value="" id="cmd"/>
	<input type="hidden" name="ids" value="<?php echo $ids;?>" id="ids"/>
<input type="hidden" name="form_block_id" value="<?php echo isset(Module::$current->data)?Module::$current->data['id']:'';?>" />
			</form >
			
			
<div style="#height:8px;"></div>
</fieldset>
