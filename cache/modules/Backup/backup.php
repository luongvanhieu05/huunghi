<script>
	function check_selected()
	{
		var status = false;
		jQuery('form :checkbox').each(function(e){
			if(this.checked)
			{
				status = true;
			}
		});
		return status;
	}
	function make_cmd(cmd)
	{
		jQuery('#cmd').val(cmd);
		document.TableBackup.submit();
	}
</script>
<fieldset id="toolbar">
	<legend><?php echo Portal::language('utils');?></legend>
	<div id="toolbar-title">
		<?php echo Portal::language('backup_data');?>
	</div>
	<div id="toolbar-content" align="right">
	<table>
	  <tbody>
		<tr>
   		  <?php if(User::can_admin(false,ANY_CATEGORY)){?><td id="toolbar-list"  align="center"><a onclick="if(check_selected()){make_cmd('backup')}else{alert('<?php echo Portal::language('You_must_select_atleast_item');?>');}"> <span title="<?php echo Portal::language('Update');?>"> </span> <?php echo Portal::language('Backup');?> </a> </td><?php }?>
		  <?php if(User::can_view(false,ANY_CATEGORY)){?><td id="toolbar-help" align="center"><a href="<?php echo Url::build_current();?>#"> <span title="Help"> </span> <?php echo Portal::language('Help');?> </a> </td><?php }?>
		</tr>
	  </tbody>
	</table>
	</div>
</fieldset>
<br>
<fieldset id="toolbar">
<?php if(Form::$current->is_error()){echo Form::$current->error_messages();}?>
<form name="TableBackup" method="post">
<table cellpadding="6" cellspacing="0" width="100%" style="#width:99%;margin-top:8px;" border="1" bordercolor="#E7E7E7" align="center">
	<tr valign="middle" bgcolor="#F0F0F0" style="line-height:20px">
		<th width="1%" align="center"><input type="checkbox" value="1" id="TableBackup_all_checkbox" onclick="select_all_checkbox(this.form, 'TableBackup',this.checked,'#FFFFEC','white');"></th>
		<th width="10%" align="left" nowrap="nowrap"><a><?php echo Portal::language('table_name');?></a></th>
		<th width="10%" align="left" nowrap="nowrap"><a><?php echo Portal::language('Engine');?></a></th>
		<th width="10%" align="left" nowrap="nowrap"><a><?php echo Portal::language('Version');?></a></th>
		<th width="10%" align="left" nowrap="nowrap"><a><?php echo Portal::language('Row_format');?></a></th>
		<th width="10%" align="left" nowrap="nowrap"><a><?php echo Portal::language('Rows');?></a></th>
		<th width="10%" align="left" nowrap="nowrap"><a><?php echo Portal::language('Avg_row_length');?></a></th>
		<th width="10%" align="left" nowrap="nowrap"><a><?php echo Portal::language('Data_length');?></a></th>
		<th width="10%" align="left" nowrap="nowrap"><a><?php echo Portal::language('Max_data_length');?></a></th>
		<th width="10%" align="left" nowrap="nowrap"><a><?php echo Portal::language('Index_length');?></a></th>
		<th width="10%" align="left" nowrap="nowrap"><a><?php echo Portal::language('Auto_increment');?></a></th>
		<th width="10%" align="left" nowrap="nowrap"><a><?php echo Portal::language('Create_time');?></a></th>
		<th width="10%" align="left" nowrap="nowrap"><a><?php echo Portal::language('Update_time');?></a></th>
	</tr>
	<?php $i=0;?>
	<?php
					if(isset($this->map['tables']) and is_array($this->map['tables']))
					{
						foreach($this->map['tables'] as $key1=>&$item1)
						{
							if($key1!='current')
							{
								$this->map['tables']['current'] = &$item1;?>
	<tr valign="middle" <?php Draw::hover(Portal::get_setting('crud_item_hover_bgcolor','#FFFFDD'));?> style="cursor:hand;<?php if($i%2){echo 'background-color:#F9F9F9';}?>" id="TableBackup_tr_<?php echo $this->map['items']['current']['Name'];?>">
		<td align="center"><?php $i++;?><input name="selected_ids[]" type="checkbox" value="<?php echo $this->map['tables']['current']['Name'];?>" onclick="select_checkbox(this.form,'TableBackup',this,'#FFFFEC','white');"  /></td>
		<td><?php echo $this->map['tables']['current']['Name'];?></td>
		<td><?php echo $this->map['tables']['current']['Engine'];?></td>
		<td><?php echo $this->map['tables']['current']['Version'];?></td>
		<td><?php echo $this->map['tables']['current']['Row_format'];?></td>
		<td><?php echo $this->map['tables']['current']['Rows'];?></td>
		<td><?php echo $this->map['tables']['current']['Avg_row_length'];?></td>
		<td><?php echo $this->map['tables']['current']['Data_length'];?></td>
		<td><?php echo $this->map['tables']['current']['Max_data_length'];?></td>
		<td><?php echo $this->map['tables']['current']['Index_length'];?></td>
		<td><?php echo $this->map['tables']['current']['Auto_increment'];?></td>
		<td nowrap="nowrap"><?php echo $this->map['tables']['current']['Create_time'];?></td>
		<td nowrap="nowrap"><?php echo $this->map['tables']['current']['Update_time'];?></td>
	</tr>
	
							
						<?php
							}
						}
					unset($this->map['tables']['current']);
					} ?>
</table>
<table width="100%" cellpadding="6" cellspacing="0" style="background-color:#F0F0F0;border:1px solid #E7E7E7;border-top:0px;height:8px;#width:99%;" align="center">
<tr>
	<td align="left">
		<?php echo Portal::language('select');?>:&nbsp;
		<a  onclick="select_all_checkbox(document.TableBackup,'TableBackup',true,'#FFFFEC','white');"><?php echo Portal::language('select_all');?></a>&nbsp;
		<a  onclick="select_all_checkbox(document.TableBackup,'TableBackup',false,'#FFFFEC','white');"><?php echo Portal::language('select_none');?></a>
		<a  onclick="select_all_checkbox(document.TableBackup,'TableBackup',-1,'#FFFFEC','white');"><?php echo Portal::language('select_invert');?></a>
		<b><?php echo Portal::language('Total');?> : <?php echo $this->map['total'];?> <?php echo Portal::language('table');?></b>
	</td>
</tr>
</table>
<input  name="cmd" type ="hidden" id="d" value="<?php echo String::html_normalize(URL::get('cmd','backup'));?>">
<input type="hidden" name="form_block_id" value="<?php echo isset(Module::$current->data)?Module::$current->data['id']:'';?>" />
			</form >
			
			
<div style="#height:8px"></div>
</fieldset>
