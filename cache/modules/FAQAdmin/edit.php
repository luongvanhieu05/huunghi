<script src="assets/default/css/tabs/tabpane.js" type="text/javascript"></script>
<fieldset id="toolbar">
 	<div id="toolbar-title">
		<?php echo Portal::language('faq_admin');?> <span style="font-size:16px;color:#0B55C4;">[ <?php echo Portal::language(Url::get('cmd','list'));?> ]</span>
	</div>
	<div id="toolbar-content">
	<table align="right">
	  <tbody>
		<tr>
		  <td id="toolbar-preview"  align="center"><a href="<?php echo Url::build_current();?>#"> <span title="Move"> </span> <?php echo Portal::language('Preview');?> </a> </td>
		  <td id="toolbar-save"  align="center"><a onclick="EditFAQAdmin.submit();"> <span title="Edit"> </span> <?php echo Portal::language('Save');?> </a> </td>
		  <td id="toolbar-cancel"  align="center"><a href="<?php echo Url::build_current(array('cmd'=>'list'));?>#"> <span title="New"> </span> <?php echo Portal::language('Cancel');?> </a> </td>
		</tr>
	  </tbody>
	</table>
	</div>
 </fieldset>
  <br clear="all">
<fieldset id="toolbar">
	<?php if(Form::$current->is_error()){echo Form::$current->error_messages();}?>
	<form name="EditFAQAdmin" id="EditFAQAdmin" method="post" enctype="multipart/form-data">
		<table cellspacing="4" cellpadding="4" border="0" width="100%" style="background-color:#FFFFFF;">
		<tr>
		  <td valign="top">
				<table  cellpadding="4" cellspacing="0" border="0" width="100%" style="background-color:#F9F9F9;border:1px solid #D5D5D5" align="center">
					<tr>
						<td>
						<div class="tab-pane-1" id="tab-pane-category">
						<?php
					if(isset($this->map['languages']) and is_array($this->map['languages']))
					{
						foreach($this->map['languages'] as $key1=>&$item1)
						{
							if($key1!='current')
							{
								$this->map['languages']['current'] = &$item1;?>
						<div class="tab-page" id="tab-page-category-<?php echo $this->map['languages']['current']['id'];?>">
							<h2 class="tab"><?php echo $this->map['languages']['current']['name'];?></h2>
							<div class="form_input_label"><?php echo Portal::language('name_question');?> (<span class="require">*</span>)</div>
							<div class="form_input">
								 <input  name="name_<?php echo $this->map['languages']['current']['id'];?>" id="name_<?php echo $this->map['languages']['current']['id'];?>" class="input" style="width:60%"  / type ="text" value="<?php echo String::html_normalize(URL::get('name_'.$this->map['languages']['current']['id']));?>">
							</div>
							<div class="form_input_label"><?php echo Portal::language('brief_question');?></div>
							<div class="form_input">
								<textarea id="brief_<?php echo $this->map['languages']['current']['id'];?>" name="brief_<?php echo $this->map['languages']['current']['id'];?>" cols="75" rows="20" style="width:99%; height:200px;overflow:hidden" ><?php echo Url::get('brief_'.$this->map['languages']['current']['id'],'');?></textarea><br />
								<script>simple_mce('brief_<?php echo $this->map['languages']['current']['id'];?>');</script>
							</div>
							<div class="form_input_label"><?php echo Portal::language('description_question');?></div>
							<div class="form_input">
								<textarea id="description_<?php echo $this->map['languages']['current']['id'];?>" name="description_<?php echo $this->map['languages']['current']['id'];?>" cols="75" rows="20" style="width:99%; height:350px;overflow:hidden"><?php echo Url::get('description_'.$this->map['languages']['current']['id'],'');?></textarea><br />
								<script>advance_mce('description_<?php echo $this->map['languages']['current']['id'];?>');</script>
							</div>
						</div>
						
							
						<?php
							}
						}
					unset($this->map['languages']['current']);
					} ?>
						</div>
						</td>
				   </tr>
				</table>
			</td>
			<td valign="top" style="width:320px;">
				<table width="100%" style="border: 1px dashed silver;" cellpadding="4" cellspacing="2">
				<tr>
					<td><strong><?php echo Portal::language('Status');?></strong></td>
					<td><?php echo Url::get('status','0');?></td>
				</tr>
				<tr>
				  <td><strong><?php echo Portal::language('Rating');?></strong></td>
				  <td><?php echo Url::get('rating','0');?></td>
				  </tr>
				<tr>
					<td><strong><?php echo Portal::language('Hitcount');?></strong></td>
					<td><?php echo Url::get('hitcount','0');?></td>
				</tr>
				<tr>
					<td><strong><?php echo Portal::language('Created');?></strong></td>
					<td><?php echo date('h\h:i d/m/Y',Url::get('time',time()));?></td>
				</tr>
				<tr>
					<td><strong><?php echo Portal::language('Modified');?></strong></td>
					<td><?php echo Url::get('last_time_update')?date('h\h:i d/m/Y',Url::get('last_time_update')):'Not modified';?></td>
				</tr>
				</table>
				<div id="panel">
					<div id="panel_1"  style="margin-top:8px;">
					<span><?php echo Portal::language('Parameters_article');?></span>
					<table cellpadding="6" cellspacing="0" width="100%" border="1" bordercolor="#E9E9E9">
						<tr>
							<td align="right" width="50%"><?php echo Portal::language('status');?></td>
							<td align="left"><select  name="status" id="status" class="select-large"><?php
					if(isset($this->map['status_list']))
					{
						foreach($this->map['status_list'] as $key=>$value)
						{
							if($key>1000000000){
								echo '<optgroup label=" - - '.$value.'">';
							}else{
								echo '<option value="'.$key.'"';
								echo '>'.$value.'</option>';
							}
							
						}
					}
					?><script type="text/javascript">getId('status').value = "<?php echo addslashes(URL::get('status',isset($this->map['status'])?$this->map['status']:''));?>";</script>
	</select></td>
						</tr>
						<tr>
							<td align="right"><?php echo Portal::language('position');?></td>
							<td align="left"><input  name="position" id="position" class="input-large" type ="text" value="<?php echo String::html_normalize(URL::get('position'));?>"></td>
						</tr>
					</table>
					</div>
				</div>
			</td>
			</tr>
			</table>
		</tr>
		</table>
	<input type="hidden" name="form_block_id" value="<?php echo isset(Module::$current->data)?Module::$current->data['id']:'';?>" />
			</form >
			
			
</fieldset>