<link rel="stylesheet" type="text/css" href="assets/standard/css/fashion/detail/detail.css"/>
<div class="content_breadcum">
    <ul class="breadcrumb">
        <li><a href=""><i class="fa fa-home"></i></a></li>
        <li><a href="/dang-ky.html">Đăng ký</a></li>
    </ul>
</div>


<div class="row">
    <column id="column-left" class="col-sm-3 hidden-xs">
        <div class="box">
            <div class="box-heading">Tài khoản</div>
            <div class="list-group">
                <a href="/dang-nhap.html" class="list-group-item">Đăng nhập</a>
                <a href="/dang-ky.html" class="list-group-item">Đăng ký</a>
                <a href="" class="list-group-item">Quên mật khẩu</a>
                <a href="" class="list-group-item">Tài khoản của tôi</a>
            </div>
        </div>
    </column>

        <?php if (Url::get('act') == 'success') { ?>
            </br></br>
            <div class="register-thanks center text-center">Cám ơn bạn đã đăng ký tài khoản tại Hằng jeans<br><br>
            <span><strong style="color:#FF0004">Vào hòm thư</strong> của bạn để xác nhận đăng ký và <strong
                    style="color:#FF0004">kích hoạt</strong> tài khoản !</span>
            </div>
                <?php } elseif (Url::get('act') == 'actived') { ?>
            <div class="title"><h2 class="text-center">Tài khoản đã được kích hoạt thành công</h2>
                <div><p class="text-center">Nhấn chuột vào <a href="dang-nhap.html"><strong>đây</strong></a> để đăng nhập!</p></div>
                <div><a href="/"><p class="text-center" style="color:#0044cc;">Về trang chủ</p></a></div>
            </div>
        <?php } elseif (Url::get('act') == 'not_actived') { ?>
            <div class="register-thanks">Tài khoản chưa được kích hoạt <a
                    href="<?php echo Url::build('trang-chu', '', REWRITE); ?>"><?php echo Portal::get_setting('website_title'); ?></a>
            </div>
        <?php } elseif (Url::get('act') == 'invalid') { ?>
            <div class="register-thanks">Tài khoản không tồn tại<a
                    href="<?php echo Url::build('trang-chu', '', REWRITE); ?>"><?php echo Portal::get_setting('website_title'); ?></a>
            </div>
        <?php } else { ?>
    <div id="content" class="col-sm-9">
        <div class="col-md-5"></div>
        <br><br>
            <p>Nếu bạn đã có một tài khoản với chúng tôi, xin vui lòng đăng nhập vào trang<a href="/dang-nhap.html"> Đăng nhập</a>.</p>
            <form method="post" enctype="multipart/form-data" name="Register" id="Register">
                <fieldset id="account">
                <div class="register_error"><?php echo Form::$current->error_messages(); ?></div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="email">Email <strong class="notnull">*</strong></label>
                            <input  name="email" id="email" class="form-control" maxlength="255" placeholder="Email dùng để đăng nhập và kích hoạt tài khoản" required type ="email" value="<?php echo String::html_normalize(URL::get('email'));?>">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="full_name">Họ và tên <strong class="notnull">*</strong></label>
                            <input  name="full_name" id="full_name" class="form-control" maxlength="100" required type ="text" value="<?php echo String::html_normalize(URL::get('full_name'));?>">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="register_password">Mật khẩu <strong class="notnull">*</strong></label>
                            <input  name="register_password" id="register_password" class="form-control" maxlength="50" autocomplete="off" required/ type ="password" value="<?php echo String::html_normalize(URL::get('register_password'));?>">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="retype_password">Nhập lại mật khẩu
                                <strong>*</strong></label>
                            <input  name="retype_password" id="retype_password" class="form-control" maxlength="50" autocomplete="off" required/ type ="password" value="<?php echo String::html_normalize(URL::get('retype_password'));?>">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="gender">Giới tính</label>
                            <span><select  name="gender" id="gender"
                                          class="form-control"><?php
					if(isset($this->map['gender_list']))
					{
						foreach($this->map['gender_list'] as $key=>$value)
						{
							if($key>1000000000){
								echo '<optgroup label=" - - '.$value.'">';
							}else{
								echo '<option value="'.$key.'"';
								echo '>'.$value.'</option>';
							}
							
						}
					}
					?><script type="text/javascript">getId('gender').value = "<?php echo addslashes(URL::get('gender',isset($this->map['gender'])?$this->map['gender']:''));?>";</script>
	</select></span>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="gender">Địa chỉ</label>
                            <span><input  name="address" id="address" class="form-control"  maxlength="255"/ type ="text" value="<?php echo String::html_normalize(URL::get('address'));?>"></span>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="phone">Điện thoại <strong class="notnull">*</strong></label>
                            <span><input  name="phone" id="phone" class="form-control"
                                         maxlength="255"/ type ="text" value="<?php echo String::html_normalize(URL::get('phone'));?>"></span>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="gender">Tỉnh / thành phố</label>
                            <span><select  name="zone_id" id="zone_id" class="form-control"><?php
					if(isset($this->map['zone_id_list']))
					{
						foreach($this->map['zone_id_list'] as $key=>$value)
						{
							if($key>1000000000){
								echo '<optgroup label=" - - '.$value.'">';
							}else{
								echo '<option value="'.$key.'"';
								echo '>'.$value.'</option>';
							}
							
						}
					}
					?><script type="text/javascript">getId('zone_id').value = "<?php echo addslashes(URL::get('zone_id',isset($this->map['zone_id'])?$this->map['zone_id']:''));?>";</script>
	</select></span>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <span><label for="verify_comfirm_code">Mã bảo vệ <strong class="notnull">*</strong></label></span>
                            <span><img id="imgCaptcha" src="capcha.php"/></span>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <input  name="verify_comfirm_code" id="verify_comfirm_code" maxlength="4" class="form-control"/ type ="text" value="<?php echo String::html_normalize(URL::get('verify_comfirm_code'));?>">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="">&nbsp;</label>
                    <div class="alert alert-danger">Tôi đồng ý với <a target="_blank" href="tin-tuc/game-show/dieu-khoan-dieu-kien.html" class="term"><strong>chính sách và điều khoản</strong></a></div>
                    <div class="alert alert-success"><strong>Chú ý*:</strong> <br>Sau khi đăng ký bạn phải <strong>vào hòm thư email</strong> mà bạn đã chọn để đăng ký để <strong> kích hoạt</strong> tài khoản.
                    </div>
                </div>
                <div class="register-buotton-wrapper">

                    <center><input  name="register_button" id="register_button"
                                   value="Đăng ký" conclick="smform()"
                                   class="btn btn-primary btn-lg" type ="submit" value="<?php echo String::html_normalize(URL::get('register_button'));?>">
                    </center>
                    <br clear="all"><br>
                    <script>
                        function smform) {
                            $("#Register").submit();
                        }
                    </script>

                </div>
            <input type="hidden" name="form_block_id" value="<?php echo isset(Module::$current->data)?Module::$current->data['id']:'';?>" />
			</form >
			
			
    </div>
    <?php } ?>
</div>






