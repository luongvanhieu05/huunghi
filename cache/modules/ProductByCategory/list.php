<style>
    .justify-content-center{
        -webkit-box-pack: center!important;
        -webkit-justify-content: center!important;
        -ms-flex-pack: center!important;
        justify-content: center!important;
    }
    .d-flex{
        display: -webkit-box!important;
        display: -webkit-flex!important;
        display: -ms-flexbox!important;
        display: flex!important;
    }

</style>
<?php
					if(isset($this->map['category_home']) and is_array($this->map['category_home']))
					{
						foreach($this->map['category_home'] as $key1=>&$item1)
						{
							if($key1!='current')
							{
								$this->map['category_home']['current'] = &$item1;?>
<br clear="all">
<div class="container">
    <div class="module_title"><a href="san-pham/<?php echo $this->map['category_home']['current']['name_id'];?>" style="border-bottom: 3px solid #74A82A;padding-bottom: 5px;"><?php echo $this->map['category_home']['current']['name_1'];?></a></div>
    <!--Sản phẩm bán chạy-->

    <div class="row justify-content-center d-flex">
        <?php
					if(isset($this->map['category_home']['current']['item_pro']) and is_array($this->map['category_home']['current']['item_pro']))
					{
						foreach($this->map['category_home']['current']['item_pro'] as $key2=>&$item2)
						{
							if($key2!='current')
							{
								$this->map['category_home']['current']['item_pro']['current'] = &$item2;?>
            <div class="item col-lg-4">
                <div class="product-block product-thumb transition">
                    <div class="product-block-inner">

                        <div class="image">
                            <a href="/san-pham/<?php echo $this->map['category_home']['current']['name_id'];?>/<?php echo $this->map['category_home']['current']['item_pro']['current']['name_id'];?>.html">
                                <img src="<?php echo $this->map['category_home']['current']['item_pro']['current']['small_thumb_url'];?>" class="img-responsive" alt="<?php echo $this->map['category_home']['current']['item_pro']['current']['name'];?>"/></a>

                            <div class="action">
                                <div class="action_inner">
                                    <div class="button-group">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="caption">
                            <h4><a href="/san-pham/<?php echo $this->map['category_home']['current']['name_id'];?>/<?php echo $this->map['category_home']['current']['item_pro']['current']['name_id'];?>.html"><?php echo $this->map['category_home']['current']['item_pro']['current']['name'];?></a></h4>

                            <p class="price">
                                <span class="price-new"><?php echo number_format($this->map['category_home']['current']['item_pro']['current']['price']); ?> đ</span> <span class="price-old"></span>
                            </p>


                        </div>
                    </div>
                </div>
            </div>
            
							
						<?php
							}
						}
					unset($this->map['category_home']['current']['item_pro']['current']);
					} ?>
    </div>

</div>
<style>
    .calrousel-hieudev-<?php echo $this->map['category_home']['current']['id'];?> .owl-pagination{display: none;}
    .calrousel-hieudev-<?php echo $this->map['category_home']['current']['id'];?> .customNavigation a.next {
        background: transparent url(../../../../../assets/standard/images/fashion/megnor/sprite.png) no-repeat scroll -31px -31px;
        height: 32px;
        right: 0;
        top: 150px;
        transition: none 0s ease 0s;
        -webkit-transition: none 0s ease 0s;
        -moz-transition: none 0s ease 0s;
        -ms-transition: none 0s ease 0s;
        -o-transition: none 0s ease 0s;
        width: 32px;
        z-index: 1;
    }
    .calrousel-hieudev-<?php echo $this->map['category_home']['current']['id'];?> .customNavigation a.prev {
        background: transparent url(../../../../../assets/standard/images/fashion/megnor/sprite.png) no-repeat scroll 0 -31px;
        height: 32px;
        left: 0;
        top: 150px;
        transition: none 0s ease 0s;
        -webkit-transition: none 0s ease 0s;
        -moz-transition: none 0s ease 0s;
        -ms-transition: none 0s ease 0s;
        -o-transition: none 0s ease 0s;
        width: 32px;
        z-index: 1;
    }

</style>

<script>
    $(document).ready(function() {

        $("#calrousel-hieudev-<?php echo $this->map['category_home']['current']['id'];?>").owlCarousel({
            navigation : false,
            autoPlay: 3000, //Set AutoPlay to 3 seconds
            items : 4,
            itemsDesktop : [1199,3],
            itemsDesktopSmall : [979,3]

        });

        var owl = $('#calrousel-hieudev-<?php echo $this->map['category_home']['current']['id'];?>');
        $('.calrousel-hieudev-<?php echo $this->map['category_home']['current']['id'];?> .next').click(function(){
            owl.trigger('owl.next');
        });
        $('.calrousel-hieudev-<?php echo $this->map['category_home']['current']['id'];?> .prev').click(function(){
            owl.trigger('owl.prev');
        });

    });
</script>


							
						<?php
							}
						}
					unset($this->map['category_home']['current']);
					} ?>
