<style>
	th,td{padding:5px !important;}
	.print-title{display:none;}
</style>
<style media="print">
	.print-title{display:block;}
</style>

<fieldset id="toolbar">
<form  name="ManageOrderForm" id="ManageOrderForm" method="post">
<div class="list">
<div class="title">
    <h4>Danh sách đặt hàng</h4>
</div><!--End .title-->
<div class="m-list">
<table border="1" cellspacing="0" cellpadding="5" bordercolor="#CCC">
  <tr bgcolor="#EFEFEF">
    <td>&nbsp;</td>
    <td>ID</td>
    <td>Chi tiết</td>
    <td>Họ và tên</td>
    <td>Email</td>
    <td>Điện thoại</td>
    <td>Tổng tiền</td>
    <td align="center">TT</td>
    <td><input type="button" value="In hoặc PDF" style="width:100%;" onclick="printWebPart('printable_area');" /></td>
  </tr>
  <tr>
    <td>Tìm kiếm</td>
    <td><input  name="id" id="id" style="width:100px" / type ="text" value="<?php echo String::html_normalize(URL::get('id'));?>"></td>
    <td><input  name="detail" id="detail" style="width:100px" / type ="text" value="<?php echo String::html_normalize(URL::get('detail'));?>"></td>
    <td><input  name="full_name" id="full_name" style="width:100px" / type ="text" value="<?php echo String::html_normalize(URL::get('full_name'));?>"></td>
    <td><input  name="email" id="email" style="width:100px" / type ="text" value="<?php echo String::html_normalize(URL::get('email'));?>"></td>
    <td><input  name="phone" id="phone" style="width:100px" / type ="text" value="<?php echo String::html_normalize(URL::get('phone'));?>"></td>
    <td>&nbsp;</td>
    <td align="center"><input class="excel-opt" name="paid" type="checkbox" value="1" id="paid" /></td>
    <td><input type="submit" value="Tìm kiếm" style="width:100%;" /></td>
  </tr>
  <tr>
    <td align="center">Tất cả <input class="excel-opt" name="check_all" type="checkbox" value="id" id="check_all" title="Chọn tất cả" /></td>
    <td align="center"><input class="excel-opt" name="excel_opt[]" type="checkbox" value="id" id="excel_opt[]" /></td>
    <td align="center"><input class="excel-opt" name="excel_opt[]" type="checkbox" value="detail" id="excel_opt[]" /></td>
    <td align="center"><input class="excel-opt" name="excel_opt[]" type="checkbox" value="full_name" /></td>
    <td align="center"><input class="excel-opt" name="excel_opt[]" type="checkbox" value="email" /></td>
    <td align="center"><input class="excel-opt" name="excel_opt[]" type="checkbox" value="phone"/></td>
    <td align="center"><input class="excel-opt" name="excel_opt[]" type="checkbox" value="total" /></td>
    <td align="center"><input class="excel-opt" name="excel_opt[]" type="checkbox" value="paid" id="excel_opt[]" /></td>
    <td><input  name="export_excel" value="Export excel" style="width:100%;" / type ="submit" value="<?php echo String::html_normalize(URL::get('export_excel'));?>"></td>
  </tr>
</table><br />
<div id="printable_area">
<div class="print-title">
    <center><h2>Danh sách đơn đặt hàng</h2></center>
</div><!--End .title-->
<table width="100%" border="1" cellspacing="0" cellpadding="5" bordercolor="#CCCCCC">
  <tr bgcolor="#EFEFEF">
    	<th width="5%" align="center">ID</th>
    	<th width="15%">Chi tiết đơn hàng</th>
    	<th width="80" align="left" valign="middle" bordercolor="#E7E7E7" bgcolor="#F0F0F0"><a>Ngày đặt</a></th>
    	<th width="15%">Người đặt</th>
        <th width="20%" align="center">Email</th>
    	<th align="center">Điện thoại</th>
        <th width="10%" align="center">Tổng tiền</th>
        <th width="100" align="center">Thanh toán</th>
        <?php if(User::can_delete(false,ANY_CATEGORY)){?><th width="50" align="center" class="print-hide">&nbsp;</th><?php }?>
    </tr>
    <?php
					if(isset($this->map['orders']) and is_array($this->map['orders']))
					{
						foreach($this->map['orders'] as $key1=>&$item1)
						{
							if($key1!='current')
							{
								$this->map['orders']['current'] = &$item1;?>
     <tr>
    	<td align="center"><?php echo $this->map['orders']['current']['id'];?></td>
    	<td><pre><?php echo $this->map['orders']['current']['detail'];?></pre></td>
    	<td><?php echo date('h\h:i d/m/Y',$this->map['orders']['current']['time']);?></td>
    	<td><strong><?php echo $this->map['orders']['current']['full_name'];?></strong>
  	  </td>
        <td><?php echo $this->map['orders']['current']['email'];?></td>        
		<td align="center"><?php echo $this->map['orders']['current']['phone'];?></td>
        <td align="right"><?php echo System::display_number($this->map['orders']['current']['total']);?></td>
    	<td align="center"><?php echo ($this->map['orders']['current']['paid'] == 1)?' Đã TT ':' <strong>Chưa</strong> ';?></td>
    	<?php if(User::can_delete(false,ANY_CATEGORY)){?><td align="right" class="print-hide"><a href="<?php echo Url::build_current(array('cmd'=>'delete','id'=>$this->map['orders']['current']['id']));?>">Xóa</a></td><?php }?>
     </tr>
    
							
						<?php
							}
						}
					unset($this->map['orders']['current']);
					} ?>
    <tr>
    	<td align="center">&nbsp;</td>
    	<td>&nbsp;</td>
    	<td>&nbsp;</td>
        <td>&nbsp;</td>
        <td colspan="2" align="right">Tổng thanh toán</td>
		<td align="right"><strong><?php echo $this->map['total'];?></strong></td>
    	<td align="center">&nbsp;</td>
    	<?php if(User::can_delete(false,ANY_CATEGORY)){?><td align="right" class="print-hide">&nbsp;</td><?php }?>
    </tr>
</table>
</div>
</div>
</div>
<input type="hidden" name="form_block_id" value="<?php echo isset(Module::$current->data)?Module::$current->data['id']:'';?>" />
			</form >
			
			
</fieldset>
<script>
jQuery(document).ready(function(){
	jQuery('#check_all').click(function(){
		if(!($check = jQuery(this).attr('checked'))){
			$check = false;
		}
		
		jQuery('.excel-opt').attr('checked',$check);
	});
});
</script>