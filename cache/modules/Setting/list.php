<script>
function make_cmd(cmd)
{
	jQuery('#cmd').val(cmd);
	document.SeoConfigForm.submit();
}
</script><fieldset id="toolbar">
	<legend><?php echo Portal::language('config_manage');?></legend>
	<div id="toolbar-info"><?php echo Portal::language('SEO_config');?></div>
	<div id="toolbar-content" align="right">
	<table>
	  <tbody>
		<tr>
		<td id="toolbar-save"  align="center"><a onclick="make_cmd('seo');"> <span title="<?php echo Portal::language('Save');?>"> </span> <?php echo Portal::language('Save');?> </a> </td>
		 <td id="toolbar-help" align="center"><a href="<?php echo Url::build_current();?>#"> <span title="<?php echo Portal::language('Help');?>"> </span> <?php echo Portal::language('Help');?> </a> </td>
		</tr>
	  </tbody>
	</table>
	</div>
</fieldset>
<div style="height:8px;"></div>
 <fieldset id="toolbar">
<form name="SeoConfigForm" method="post" action="?<?php echo htmlentities($_SERVER['QUERY_STRING']);?>">
	<table  cellpadding="6" cellspacing="0" width="100%" style="#width:99%;margin-top:4px;" border="1" bordercolor="#E7E7E7" align="center">
		<tr style="background-color:#F0F0F0">
			<th width="26%" align="left"><a><?php echo Portal::language('Setting_name');?></a></th>
			<th width="74%" align="left"><a><?php echo Portal::language('Value');?></a></th>
		</tr>
        <?php
					if(isset($this->map['languages']) and is_array($this->map['languages']))
					{
						foreach($this->map['languages'] as $key1=>&$item1)
						{
							if($key1!='current')
							{
								$this->map['languages']['current'] = &$item1;?>
		<tr>
			<td valign="top" title="website_keywords"><?php echo Portal::language('website_keywords');?> <?php echo $this->map['languages']['current']['code'];?></td>
			<td><textarea  name="website_keywords_<?php echo $this->map['languages']['current']['id'];?>" class="textarea-small" id="website_keywords_<?php echo $this->map['languages']['current']['id'];?>"><?php echo Url::get('website_keywords_'.$this->map['languages']['current']['id'],'');?></textarea></td>
		</tr>
		<tr>
			<td valign="top" title="website_description"><?php echo Portal::language('website_description');?> <?php echo $this->map['languages']['current']['code'];?></td>
			<td><textarea  name="website_description_<?php echo $this->map['languages']['current']['id'];?>" class="textarea-small" id="website_description_<?php echo $this->map['languages']['current']['id'];?>"><?php echo Url::get('website_description_'.$this->map['languages']['current']['id'],'');?></textarea></td>
		</tr>
        
							
						<?php
							}
						}
					unset($this->map['languages']['current']);
					} ?>
		<tr>
			<td valign="top" title="google_analytics"><?php echo Portal::language('google_analytics');?></td>
			<td><textarea  name="google_analytics" class="textarea-medium" id="google_analytics"><?php echo String::html_normalize(URL::get('google_analytics',''.$this->map['google_analytics']));?></textarea></td>
		</tr>
    <tr>
			<td valign="top" title="google_analytics">Auto link</td>
			<td><textarea  name="auto_link" class="textarea-medium" id="auto_link"><?php echo String::html_normalize(URL::get('auto_link',''.$this->map['auto_link']));?></textarea></td>
		</tr>
	</table>
<input type="hidden" name="form_block_id" value="<?php echo isset(Module::$current->data)?Module::$current->data['id']:'';?>" />
			</form >
			
			
<table width="100%" cellpadding="6" cellspacing="0" style="background-color:#F0F0F0;border:1px solid #E7E7E7;height:8px;#width:99%" align="center">
	<tr>
		<td align="right">&nbsp;</td>
	</tr>
</table>
<div style="#height:8px"></div>
</fieldset> 