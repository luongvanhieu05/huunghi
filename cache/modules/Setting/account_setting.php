<script>
function make_cmd(cmd)
{
	jQuery('#cmd').val(cmd);
	document.AccountSettingForm.submit();
}
</script><fieldset id="toolbar">
	<legend><?php echo Portal::language('config_manage');?></legend>
	<div id="toolbar-info"><?php echo Portal::language('Account_setting');?></div>
	<div id="toolbar-content">
	<table align="right">
	  <tbody>
		<tr>
		<td id="toolbar-save"  align="center"><a onclick="make_cmd('save');"><span title="[[.Save.]"> </span> <?php echo Portal::language('Save');?> </a> </td>
		
		</tr>
	  </tbody>
	</table>
	</div>
</fieldset>
<div style="height:8px;"></div>
 <fieldset id="toolbar">
<form name="AccountSettingForm" method="post" action="?<?php echo htmlentities($_SERVER['QUERY_STRING']);?>" id="AccountSettingForm" enctype="multipart/form-data">
    <div class="tab-pane-1" id="tab-pane-category">
	<?php 
				if((User::can_admin(false,ANY_CATEGORY)))
				{?>
    <div class="tab-page" id="tab-page-category-1">
    <h2 class="tab"><?php echo Portal::language('Front_back_config');?></h2>
    <div id="front_back_config" class="form_input">
		<br>
			<table  cellpadding="6" cellspacing="0" width="100%" style="#width:99%;margin-top:4px;" border="1" bordercolor="#E7E7E7" align="center">
				<tr style="background-color:#F0F0F0">
					<th width="20%" align="left"><a><?php echo Portal::language('Setting_name');?></a></th>
					<th width="80%" align="left"><a><?php echo Portal::language('Value');?></a></th>
				</tr>
				<tr>
				  <td width="20%" align="left" valign="top" title="site_title"><?php echo Portal::language('site_title');?></td>
				  <td width="80%" align="left"><input  name="config_<?php echo $this->map['prefix'];?>site_title" type="text" id="<?php echo $this->map['prefix'];?>site_title" value="<?php echo $_REQUEST['config_'.$this->map['prefix'].'site_title'];?>" class="input-big-huge"></td>
				  </tr>
				<tr>
					<td width="20%" align="left" valign="top" title="site_name"><?php echo Portal::language('site_name');?></td>
					<td width="80%" align="left"><input  name="config_<?php echo $this->map['prefix'];?>site_name" type="text" id="<?php echo $this->map['prefix'];?>site_name" value="<?php echo $_REQUEST['config_'.$this->map['prefix'].'site_name'];?>" class="input-big-huge"></td>
				</tr>
				<tr>
					<td width="20%" align="left" valign="top" title="site_icon"><?php echo Portal::language('icon_on_address');?> (*.icon)</td>
					<td width="80%" align="left">
						<input name="config_<?php echo $this->map['prefix'];?>site_icon" type="file" id="<?php echo $this->map['prefix'];?>site_icon" class="file">
						<div id="delete_site_icon"><?php if(Url::get('config_'.$this->map['prefix'].'site_icon') and file_exists(Url::get('config_'.$this->map['prefix'].'site_icon'))){?>[<a href="<?php echo Url::get('config_'.$this->map['prefix'].'site_icon');?>" target="_blank" style="color:#FF0000"><?php echo Portal::language('view');?></a>]&nbsp;[<a href="<?php echo Url::build_current(array('cmd'=>'unlink','link'=>Url::get('config_'.$this->map['prefix'].'site_icon')));?>" onclick="jQuery('#delete_site_icon').html('');" target="_blank" style="color:#FF0000"><?php echo Portal::language('delete');?></a>]<?php }?></div>
					</td>
				</tr>
                  <tr>
				    <td align="left" valign="top" title="email_support">Mã giảm giá</td>
				    <td align="left"><input  name="config_discount_code" id="config_discount_code" style="width:500px;" / type ="text" value="<?php echo String::html_normalize(URL::get('config_discount_code'));?>"></td>
			    </tr>
                <tr>
					<td width="20%" align="left" valign="top" title="email_support_online_bac">Email hỗ trợ KH</td>
					<td width="80%" align="left"><input  name="config_email_support_online_bac" id="email_support" class="input-big-huge" type ="text" value="<?php echo String::html_normalize(URL::get('config_email_support_online_bac'));?>"></td>
				</tr>
                <tr>
					<td width="20%" align="left" valign="top" title="email_webmaster"><?php echo Portal::language('email_webmaster');?></td>
					<td width="80%" align="left"><input  name="config_email_webmaster" id="email_webmaster" class="input-big-huge" type ="text" value="<?php echo String::html_normalize(URL::get('config_email_webmaster'));?>"></td>
				</tr>
		        <tr>
		          <td align="left" valign="top" title="phone_ban_buon">Số bán buôn</td>
				          <td align="left"><input  name="config_phone_ban_buon" id="phone_ban_buon" class="input-big-huge" type ="text" value="<?php echo String::html_normalize(URL::get('config_phone_ban_buon'));?>"></td>
	          </tr>
		        <tr>
				  <td width="20%" align="left" valign="top" title="hot_line"><?php echo Portal::language('Hot_line');?> (<?php echo Portal::language('name');?>:<?php echo Portal::language('Phone_number');?>,..) - hot_line</td>
				  <td width="80%" align="left"><input  name="config_hot_line" id="hot_line" class="input-big-huge" type ="text" value="<?php echo String::html_normalize(URL::get('config_hot_line'));?>"></td>
				  </tr>
                <tr bgcolor="#C8FFBB">
                  <td colspan="2" align="left" valign="top"><strong>Mục thông tin liên hệ</strong></td>
                </tr>
                <tr bgcolor="#FFF">
                  <td align="left" valign="top" title="contact_text">Thông tin liên hệ dạng text</td>
                  <td align="left"><textarea  name="config_contact_text" type="text" id="contact_text" class="input-big-huge" rows="10"><?php echo String::html_normalize(URL::get('config_contact_text',''));?></textarea>
                  </td>
                </tr>
                <tr bgcolor="#FFFFCC">
					<td width="20%" align="left" valign="top" title="company_name_1">Tên tổ chức (Tiếng Việt)</td>
					<td width="80%" align="left"><input  name="config_company_name_1" id="config_company_name" class="input-big-huge" type ="text" value="<?php echo String::html_normalize(URL::get('config_company_name_1'));?>"></td>
				</tr>  
                <tr bgcolor="#FFFFCC">
					<td width="20%" align="left" valign="top" title="company_name_3">Tên tổ chức (Tiếng Anh)</td>
					<td width="80%" align="left"><input  name="config_company_name_3" id="company_name_3" class="input-big-huge" type ="text" value="<?php echo String::html_normalize(URL::get('config_company_name_3'));?>"></td>
				</tr>
                <!--Dia chi phia bac-->
                <tr bgcolor="#FFCC99">
                  <td align="left" valign="top" title="company_address_1">ĐC trụ sở chính (Tiếng Việt)</td>
                  <td align="left"><input  name="config_company_address_1" id="company_address_1" class="input-big-huge" / type ="text" value="<?php echo String::html_normalize(URL::get('config_company_address_1'));?>"></td>
                </tr>
                <tr bgcolor="#FFCC99">
                  <td align="left" valign="top" title="config_company_address_3">ĐC trụ sở chính (Tiếng Anh)</td>
                  <td align="left"><input  name="config_company_address_3" id="company_address_3" class="input-big-huge" / type ="text" value="<?php echo String::html_normalize(URL::get('config_company_address_3'));?>"></td>
                </tr>
                <tr bgcolor="#FFCC99">
					<td width="20%" align="left" valign="top" title="company_phone_bac">Điện thoại</td>
					<td width="80%" align="left"><input  name="config_company_phone_bac" id="company_phone_bac" class="input-big-huge" type ="text" value="<?php echo String::html_normalize(URL::get('config_company_phone_bac'));?>"></td>
				</tr>
                <tr bgcolor="#FFCC99">
                  <td align="left" valign="top" title="company_fax">Skype</td>
                  <td align="left"><input  name="config_skype" id="skype" class="input-big-huge" type ="text" value="<?php echo String::html_normalize(URL::get('config_skype'));?>"></td>
                </tr>
                <tr bgcolor="#FFCC99">
					<td width="20%" align="left" valign="top" title="company_fax"><?php echo Portal::language('company_fax');?></td>
					<td width="80%" align="left"><input  name="config_company_fax" id="company_fax" class="input-big-huge" type ="text" value="<?php echo String::html_normalize(URL::get('config_company_fax'));?>"></td>
				</tr>
                <tr bgcolor="#FFCC99">
					<td width="20%" align="left" valign="top" title="company_email">Email</td>
					<td width="80%" align="left"><input  name="config_company_email" id="company_email" class="input-big-huge" type ="text" value="<?php echo String::html_normalize(URL::get('config_company_email'));?>"></td>
				</tr>
                <tr bgcolor="#EFEFEF">
                  <td align="left" valign="top" title="company_address_nam_1">ĐC văn phòng Miền Nam (Tiếng Việt)</td>
                  <td align="left"><input  name="config_company_address_nam_1" id="company_address_nam_1" class="input-big-huge" / type ="text" value="<?php echo String::html_normalize(URL::get('config_company_address_nam_1'));?>"></td>
                </tr>
                <tr bgcolor="#EFEFEF">
                  <td align="left" valign="top" title="company_address_nam_3">ĐC văn phòng Miền Nam (Tiếng Anh)</td>
                  <td align="left"><input  name="config_company_address_nam_3" id="company_address_nam_3" class="input-big-huge" / type ="text" value="<?php echo String::html_normalize(URL::get('config_company_address_nam_3'));?>"></td>
                </tr>
                <tr bgcolor="#EFEFEF">
                  <td width="20%" align="left" valign="top" title="company_phone_nam">Điện thoại VPMN</td>
                  <td width="80%" align="left"><input  name="config_company_phone_nam" id="company_phone_nam" class="input-big-huge" type ="text" value="<?php echo String::html_normalize(URL::get('config_company_phone_nam'));?>"></td>
            </tr>
                <tr bgcolor="#EFEFEF">
                  <td align="left" valign="top" title="company_email_nam">Email VPMN</td>
                  <td align="left"><input  name="config_company_email_nam" id="company_email_nam" class="input-big-huge" type ="text" value="<?php echo String::html_normalize(URL::get('config_company_email_nam'));?>"></td>
                </tr>
                <tr bgcolor="#EFEFEF">
                  <td align="left" valign="top" bgcolor="#D7FFD9" title="company_address_nn1_1">ĐC văn phòng nước ngoài 1 (Tiếng Việt)</td>
                  <td align="left" bgcolor="#D7FFD9"><input  name="config_company_address_nn1_1" id="company_address_nn1_1" class="input-big-huge" / type ="text" value="<?php echo String::html_normalize(URL::get('config_company_address_nn1_1'));?>"></td>
            </tr>
                <tr bgcolor="#EFEFEF">
                  <td align="left" valign="top" bgcolor="#D7FFD9" title="company_address_nn1_3">ĐC văn phòng nước ngoài 1 (Tiếng Anh)</td>
                  <td align="left" bgcolor="#D7FFD9"><input  name="config_company_address_nn1_3" id="company_address_nn1_3" class="input-big-huge" / type ="text" value="<?php echo String::html_normalize(URL::get('config_company_address_nn1_3'));?>"></td>
            </tr>
                <tr bgcolor="#EFEFEF">
                  <td align="left" valign="top" bgcolor="#D7FFD9" title="company_phone_nn1">Điện thoại</td>
                  <td align="left" bgcolor="#D7FFD9"><input  name="config_company_phone_nn1" id="company_phone_nn1" class="input-big-huge" type ="text" value="<?php echo String::html_normalize(URL::get('config_company_phone_nn1'));?>"></td>
            </tr>
                <tr bgcolor="#EFEFEF">
                  <td align="left" valign="top" bgcolor="#D7FFD9" title="company_email_nn1">Email </td>
                  <td align="left" bgcolor="#D7FFD9"><input  name="config_company_email_nn1" id="company_email_nn1" class="input-big-huge" type ="text" value="<?php echo String::html_normalize(URL::get('config_company_email_nn1'));?>"></td>
            </tr>
                <tr>
                  <td align="left" valign="top" bgcolor="#C5FCFF" title="company_address_nn2_1">ĐC văn phòng nước ngoài 2 (Tiếng Việt)</td>
                  <td align="left" bgcolor="#C5FCFF"><input  name="config_company_address_nn2_1" id="company_address_nn2_1" class="input-big-huge" / type ="text" value="<?php echo String::html_normalize(URL::get('config_company_address_nn2_1'));?>"></td>
            </tr>
                <tr>
                  <td align="left" valign="top" bgcolor="#C5FCFF" title="company_address_nn2_3">ĐC văn phòng nước ngoài 2 (Tiếng Anh)</td>
                  <td align="left" bgcolor="#C5FCFF"><input  name="config_company_address_nn2_3" id="company_address_nn2_3" class="input-big-huge" / type ="text" value="<?php echo String::html_normalize(URL::get('config_company_address_nn2_3'));?>"></td>
            </tr>
                <tr>
                  <td align="left" valign="top" bgcolor="#C5FCFF" title="company_phone_nn2">Điện thoại</td>
                  <td align="left" bgcolor="#C5FCFF"><input  name="config_company_phone_nn2" id="company_phone_nn2" class="input-big-huge" type ="text" value="<?php echo String::html_normalize(URL::get('config_company_phone_nn2'));?>"></td>
            </tr>
                <tr>
                  <td align="left" valign="top" bgcolor="#C5FCFF" title="company_email_nn2">Email </td>
                  <td align="left" bgcolor="#C5FCFF"><input  name="config_company_email_nn2" id="company_email_nn2" class="input-big-huge" type ="text" value="<?php echo String::html_normalize(URL::get('config_company_email_nn2'));?>"></td>
            </tr>
                <tr>
                  <td width="20%" align="left" valign="top">&nbsp;</td>
                  <td width="80%" align="left">&nbsp;</td>
            </tr>
			</table>
	</div>
    </div> <!-- End #tab-page-category-1 -->
    <div class="tab-page" id="tab-page-category-2">
    <h2 class="tab"><?php echo Portal::language('System_config');?></h2>
	<div id="system_config" class="form_input">
		<br>
			<table  cellpadding="6" cellspacing="0" width="100%" style="#width:99%;margin-top:4px;" border="1" bordercolor="#E7E7E7" align="center">
				<tr style="background-color:#F0F0F0">
				  <th width="20%" align="left"><a><?php echo Portal::language('Setting_name');?></a></th>
				  <th width="80%" align="left"><a><?php echo Portal::language('Value');?></a></th>
				  </tr>
				<tr>
				  <td align="left" title="use_cache">Kích hoạt site</td>
				  <td align="left"><select  name="config_is_active" id="config_is_active"><?php
					if(isset($this->map['config_is_active_list']))
					{
						foreach($this->map['config_is_active_list'] as $key=>$value)
						{
							if($key>1000000000){
								echo '<optgroup label=" - - '.$value.'">';
							}else{
								echo '<option value="'.$key.'"';
								echo '>'.$value.'</option>';
							}
							
						}
					}
					?><?php if(Url::get("config_is_active")){?><script type="text/javascript">getId('config_is_active').value = "<?php echo addslashes(URL::get('config_is_active',isset($this->map['config_is_active'])?$this->map['config_is_active']:''));?>";</script><?php }?>
	</select>
				    <script>jQuery('#config_is_active').val(<?php echo Url::get('config_is_active',0)?>);</script></td>
				  </tr>
				<tr>
				  <td width="20%" align="left" title="portal_template">Thông báo dừng site</td>
				  <td width="80%" align="left"><input  name="config_notification_when_interrption" id="notification_when_interrption" class="input-big-huge" type ="text" value="<?php echo String::html_normalize(URL::get('config_notification_when_interrption'));?>"></td>
				  </tr>
				<tr>
					<td width="20%" align="left" title="size_upload"><?php echo Portal::language('size_upload');?> /(1<?php echo Portal::language('times');?> <?php echo Portal::language('upload');?>)</td>
					<td width="80%" align="left"><input  name="config_size_upload" id="size_upload" class="input-large" type ="text" value="<?php echo String::html_normalize(URL::get('config_size_upload'));?>"></td>
				</tr>
				<tr>
					<td width="20%" align="left" title="type_image_upload"><?php echo Portal::language('type_image_upload');?></td>
					<td width="80%" align="left"><input  name="config_type_image_upload" id="type_image_upload" class="input-big-huge" type ="text" value="<?php echo String::html_normalize(URL::get('config_type_image_upload'));?>"></td>
				</tr>
				<tr>
					<td width="20%" align="left" title="type_file_upload"><?php echo Portal::language('type_file_upload');?></td>
					<td width="80%" align="left"><input  name="config_type_file_upload" id="type_file_upload" class="input-big-huge" type ="text" value="<?php echo String::html_normalize(URL::get('config_type_file_upload'));?>"></td>
				</tr>
				<tr>
					<td width="20%" align="left" title="use_cache"><?php echo Portal::language('Use_cache');?></td>
					<td width="80%" align="left">
						<select  name="config_use_cache" class="select" id="use_cache"></select>
						<script>jQuery('#use_cache').val(<?php echo Url::get('config_use_cache',0)?>);</script>
					</td>
				</tr>
				<tr>
					<td valign="top"  align="left" title="rewrite"><?php echo Portal::language('rewrite_url');?></td>
					<td  align="left"><select  name="config_rewrite" id="rewrite" class="select"></select>
					<script>jQuery('#rewrite').val(<?php echo Url::get('config_rewrite',0)?>);</script>
					</td>
				</tr>
				<tr>
					<td valign="top"  align="left" title="use_double_click"><?php echo Portal::language('use_double_click');?></td>
					<td  align="left"><select  name="config_use_double_click" class="select" id="use_double_click"></select>
					<script>jQuery('#use_double_click').val(<?php echo Url::get('config_use_double_click',0)?>);</script>
					</td>
				</tr>
				<tr>
					<td valign="top"  align="left" title="use_log"><?php echo Portal::language('use_log');?></td>
					<td  align="left"><select  name="config_use_log" class="select" id="use_log"></select>
					<script>jQuery('#use_log').val(<?php echo Url::get('config_use_log',0)?>);</script>
					</td>
				</tr>
				<tr>
					<td valign="top"  align="left" title="use_recycle_bin"><?php echo Portal::language('use_recycle_bin');?></td>
					<td  align="left"><select  name="config_use_recycle_bin" class="select" id="use_recycle_bin"></select>
					<script>jQuery('#use_recycle_bin').val(<?php echo Url::get('config_use_recycle_bin',0)?>);</script>
					</td>
				</tr>
			</table>
	</div>
    </div> <!-- End #tab-page-category-2 -->
	
				<?php
				}
				?>
</div>
<input  name="cmd" type ="hidden" id="d" value="<?php echo String::html_normalize(URL::get('cmd','save'));?>">
<input type="hidden" name="form_block_id" value="<?php echo isset(Module::$current->data)?Module::$current->data['id']:'';?>" />
			</form >
			
			
<table width="100%" cellpadding="6" cellspacing="0" style="background-color:#F0F0F0;border:1px solid #E7E7E7;height:8px;#width:99%" align="center">
	<tr>
		<td align="right">&nbsp;</td>
	</tr>
</table>
<div style="#height:8px;"></div>
</fieldset>
