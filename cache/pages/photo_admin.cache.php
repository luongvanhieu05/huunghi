<?php

Module::invoke_event('ONLOAD',System::$false,System::$false);
global $blocks;
global $plugins;
$plugins = array (
);$blocks = array (
  71492 => 
  array (
    'id' => '71492',
    'module_id' => '5897',
    'page_id' => '10214',
    'container_id' => '0',
    'region' => 'banner',
    'position' => '1',
    'skin_name' => 'default',
    'layout' => 'default',
    'name' => '',
    'settings' => 
    array (
    ),
    'module' => 
    array (
      'id' => '5897',
      'name' => 'Menu',
      'path' => 'packages/backend/modules/Menu/',
      'type' => '',
      'action_module_id' => '0',
      'use_dblclick' => '0',
      'package_id' => '332',
    ),
  ),
  71494 => 
  array (
    'id' => '71494',
    'module_id' => '5913',
    'page_id' => '10214',
    'container_id' => '0',
    'region' => 'center',
    'position' => '1',
    'skin_name' => 'default',
    'layout' => 'default',
    'name' => '',
    'settings' => 
    array (
    ),
    'module' => 
    array (
      'id' => '5913',
      'name' => 'MediaAdmin',
      'path' => 'packages/backend/modules/MediaAdmin/',
      'type' => '',
      'action_module_id' => '0',
      'use_dblclick' => '0',
      'package_id' => '332',
    ),
  ),
);
		Portal::$page = array (
  'id' => '10214',
  'package_id' => '332',
  'layout_id' => '0',
  'layout' => 'packages/core/layouts/simple.php',
  'skin' => 'default',
  'help_id' => '0',
  'name' => 'photo_admin',
  'title_1' => '',
  'title_2' => '',
  'description_1' => '',
  'description_2' => '',
  'description' => '',
  'customer_id' => '0',
  'read_only' => '0',
  'show' => '1',
  'cachable' => '0',
  'cache_param' => '',
  'params' => '',
  'site_map_show' => '1',
  'type' => 'SYSTEM',
  'condition' => '',
  'is_use_sapi' => '0',
  'title' => '',
);
		foreach($blocks as $id=>$block){
			if($block['module']['type'] == 'WRAPPER'){
				require_once $block['wrapper']['path'].'class.php';
				$blocks[$id]['object'] = new $block['wrapper']['name']($block);
				if(URL::get('form_block_id')==$id){
					$blocks[$id]['object']->submit();
				}
			}
			else
			if($block['module']['type'] != 'HTML' and $block['module']['type'] != 'content' and $block['module']['name'] != 'HTML'){
				require_once $block['module']['path'].'class.php';
				$blocks[$id]['object'] = new $block['module']['name']($block);
				if(URL::get('form_block_id')==$id){
					$blocks[$id]['object']->submit();
				}
			}
		}
		require_once 'packages/core/includes/utils/draw.php';
		?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="content-language" content="vi" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="CACHE-CONTROL" content="NO-CACHE" />
<meta name="keywords" content="<?php echo (Portal::$meta_keywords)?''.strip_tags(Portal::$meta_keywords):Portal::get_setting('website_keywords_'.Portal::language());?>" />
<meta name="description" content="<?php echo Portal::$meta_description?Portal::$meta_description:Portal::get_setting('website_description_'.Portal::language());?>" />
<meta name="ROBOTS" content="ALL" />
<meta name="author" content="<?php echo Portal::get_setting('site_name_'.Portal::language());?>" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="google-site-verification" content="-i9LD9qZVrpqB4DvpRVpKCadP3Wk1MRAk1S5JeO2u94" />
<meta property="fb:app_id" content="208527286162605"/>
<title><?php echo (Portal::$document_title ? Portal::$document_title : Portal::get_setting(PREFIX.'site_title_'.Portal::language()))?></title>
<base href="<?php echo 'http://',$_SERVER['HTTP_HOST'],'/';?>" />
<meta property="og:image" content="<?php echo Portal::$image_url?Portal::$image_url:('http://hay.tv/'.Portal::get_setting('og_image'));?>" />
<meta property="og:title" content="<?php echo (Portal::$document_title?(Portal::$document_title):Portal::get_setting('site_title_'.Portal::language()));?>" />
<meta property="og:description" content="<?php echo Portal::$meta_description?Portal::$meta_description:Portal::get_setting('website_description_'.Portal::language());?>" />
<meta property="og:url" content="<?php echo 'http://'.$_SERVER['HTTP_HOST'].''.$_SERVER['REQUEST_URI'];?>" />
<link rel="image_src" href="<?php echo Portal::$image_url?Portal::$image_url:('http://hay.tv/'.Portal::get_setting('og_image'));?>" />
<link rel="shortcut icon" href="<?php echo Portal::get_setting('site_icon');?>" />
<script type="text/javascript">	query_string = "?<?php echo urlencode($_SERVER['QUERY_STRING']);?>";PORTAL_ID = "<?php echo substr(PORTAL_ID,1);?>";</script>
<?php echo Portal::$extra_header;?>
</head>
<body class="common-home"><div class="wrapper">
	<div class="container">
		<div class="row"><?php $blocks[71492]['object']->on_draw();?></div>
		<div class="row"><?php $blocks[71494]['object']->on_draw();?></div>
		<div class="row"></div>
	</div>
</div></body>
</html>
<?php Module::invoke_event('ONUNLOAD',System::$false,System::$false);?>