<?php

Module::invoke_event('ONLOAD',System::$false,System::$false);
global $blocks;
global $plugins;
$plugins = array (
);$blocks = array (
  73018 => 
  array (
    'id' => '73018',
    'module_id' => '6002',
    'page_id' => '10438',
    'container_id' => '0',
    'region' => 'banner',
    'position' => '1',
    'skin_name' => 'default',
    'layout' => 'default',
    'name' => '',
    'settings' => 
    array (
    ),
    'module' => 
    array (
      'id' => '6002',
      'name' => 'Banner',
      'path' => 'packages/frontend/modules/Banner/',
      'type' => '',
      'action_module_id' => '0',
      'use_dblclick' => '0',
      'package_id' => '331',
    ),
  ),
  73019 => 
  array (
    'id' => '73019',
    'module_id' => '6005',
    'page_id' => '10438',
    'container_id' => '0',
    'region' => 'footer',
    'position' => '1',
    'skin_name' => 'default',
    'layout' => 'default',
    'name' => '',
    'settings' => 
    array (
    ),
    'module' => 
    array (
      'id' => '6005',
      'name' => 'Footer',
      'path' => 'packages/frontend/modules/Footer/',
      'type' => '',
      'action_module_id' => '0',
      'use_dblclick' => '0',
      'package_id' => '331',
    ),
  ),
  73020 => 
  array (
    'id' => '73020',
    'module_id' => '6063',
    'page_id' => '10438',
    'container_id' => '0',
    'region' => 'center',
    'position' => '1',
    'skin_name' => 'default',
    'layout' => 'default',
    'name' => '',
    'settings' => 
    array (
    ),
    'module' => 
    array (
      'id' => '6063',
      'name' => 'GoogleSearch',
      'path' => 'packages/frontend/modules/GoogleSearch/',
      'type' => '',
      'action_module_id' => '0',
      'use_dblclick' => '0',
      'package_id' => '331',
    ),
  ),
);
		Portal::$page = array (
  'id' => '10438',
  'package_id' => '331',
  'layout_id' => '0',
  'layout' => 'packages/frontend/layouts/default.php',
  'skin' => 'default',
  'help_id' => '0',
  'name' => 'search',
  'title_1' => 'search',
  'title_2' => 'search',
  'description_1' => '',
  'description_2' => '',
  'description' => '',
  'customer_id' => '0',
  'read_only' => '0',
  'show' => '1',
  'cachable' => '0',
  'cache_param' => '',
  'params' => '',
  'site_map_show' => '1',
  'type' => 'SYSTEM',
  'condition' => '',
  'is_use_sapi' => '0',
  'title' => 'search',
);
		foreach($blocks as $id=>$block)
		{
			if($block['module']['type'] == 'WRAPPER')
			{
				require_once $block['wrapper']['path'].'class.php';
				$blocks[$id]['object'] = new $block['wrapper']['name']($block);
				if(URL::get('form_block_id')==$id)
				{
					$blocks[$id]['object']->submit();
				}
			}
			else
			if($block['module']['type'] != 'HTML' and $block['module']['type'] != 'content' and $block['module']['name'] != 'HTML')
			{
				require_once $block['module']['path'].'class.php';
				$blocks[$id]['object'] = new $block['module']['name']($block);
				if(URL::get('form_block_id')==$id)
				{
					$blocks[$id]['object']->submit();
				}
			}
		}
		require_once 'packages/core/includes/utils/draw.php';
		?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="content-language" content="vi" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="CACHE-CONTROL" content="NO-CACHE" />
<meta name="keywords" content="<?php echo (Portal::$meta_keywords)?''.strip_tags(Portal::$meta_keywords):Portal::get_setting('website_keywords_'.Portal::language());?>" />
<meta name="description" content="<?php echo Portal::$meta_description?Portal::$meta_description:Portal::get_setting('website_description_'.Portal::language());?>" />
<meta name="ROBOTS" content="ALL" />
<meta name="author" content="<?php echo Portal::get_setting('site_name');?>" />
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<meta name="google-site-verification" content="-i9LD9qZVrpqB4DvpRVpKCadP3Wk1MRAk1S5JeO2u94" />
<title><?php echo (Portal::$document_title ? Portal::$document_title : Portal::get_setting(PREFIX.'site_title'))?></title>
<base href="<?php echo 'http://',$_SERVER['HTTP_HOST'],'/';?>" />
<meta property="og:image" content="<?php echo Portal::$image_url?Portal::$image_url:'';?>" />
<meta property="og:title" content="<?php echo (Portal::$document_title?(Portal::$document_title):Portal::get_setting('site_title'));?>" />
<meta property="og:description" content="<?php echo Portal::$meta_description?Portal::$meta_description:Portal::get_setting('website_description_'.Portal::language());?>" />
<meta property="og:url" content="<?php echo 'http://'.$_SERVER['HTTP_HOST'].''.$_SERVER['REQUEST_URI'];?>" />
<link rel="image_src" href="<?php echo Portal::$image_url?Portal::$image_url:'';?>" />
<link rel="shortcut icon" href="<?php echo Portal::get_setting('site_icon');?>" />
<script type="text/javascript">	query_string = "?<?php echo urlencode($_SERVER['QUERY_STRING']);?>";PORTAL_ID = "<?php echo substr(PORTAL_ID,1);?>";</script>
<script src="packages/core/includes/js/common.js" type="text/javascript"></script>
<?php echo Portal::$extra_header;?></head>
<body><div class="container-inner">
    <header id="header"><?php $blocks[73018]['object']->on_draw();?></header>
    <div class="container">
        <div class="row">
            <div id="content" class="col-sm-12">
                <?php $blocks[73020]['object']->on_draw();?><?php $blocks[73019]['object']->on_draw();?>
            </div>
        </div>
    </div>
</div><?php echo Portal::get_setting('google_analytics');?>
</body>
</html>
<?php Module::invoke_event('ONUNLOAD',System::$false,System::$false);?>