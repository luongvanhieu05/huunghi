<?php $currency = array (
  'USD' =>
  array (
    'id' => 'USD',
    'name' => 'USD',
    'brief' => 'US DOLLAR',
    'exchange' => '17812',
    'position' => '16',
  ),
  'THB' =>
  array (
    'id' => 'THB',
    'name' => 'THB',
    'brief' => 'THAI BAHT',
    'exchange' => '559.39',
    'position' => '15',
  ),
  'SGD' =>
  array (
    'id' => 'SGD',
    'name' => 'SGD',
    'brief' => 'SINGAPORE DOLLAR',
    'exchange' => '12802.25',
    'position' => '14',
  ),
  'SEK' =>
  array (
    'id' => 'SEK',
    'name' => 'SEK',
    'brief' => 'SWEDISH KRONA',
    'exchange' => '2467.53',
    'position' => '13',
  ),
  'NOK' =>
  array (
    'id' => 'NOK',
    'name' => 'NOK',
    'brief' => 'NORWEGIAN KRONER',
    'exchange' => '2950.45',
    'position' => '12',
  ),
  'MYR' =>
  array (
    'id' => 'MYR',
    'name' => 'MYR',
    'brief' => 'MALAYSIAN RINGGIT',
    'exchange' => '5232.48',
    'position' => '11',
  ),
  'KWD' =>
  array (
    'id' => 'KWD',
    'name' => 'KWD',
    'brief' => 'KUWAITI DINAR',
    'exchange' => '64733.81',
    'position' => '10',
  ),
  'KRW' =>
  array (
    'id' => 'KRW',
    'name' => 'KRW',
    'brief' => 'SOUTH KOREAN WON',
    'exchange' => '15.45',
    'position' => '9',
  ),
  'JPY' =>
  array (
    'id' => 'JPY',
    'name' => 'JPY',
    'brief' => 'JAPANESE YEN',
    'exchange' => '194.93',
    'position' => '8',
  ),
  'INR' =>
  array (
    'id' => 'INR',
    'name' => 'INR',
    'brief' => 'INDIAN RUPEE',
    'exchange' => '390.73',
    'position' => '7',
  ),
  'HKD' =>
  array (
    'id' => 'HKD',
    'name' => 'HKD',
    'brief' => 'HONGKONG DOLLAR',
    'exchange' => '2383.56',
    'position' => '6',
  ),
  'GBP' =>
  array (
    'id' => 'GBP',
    'name' => 'GBP',
    'brief' => 'BRITISH POUND',
    'exchange' => '30460.16',
    'position' => '5',
  ),
  'EUR' =>
  array (
    'id' => 'EUR',
    'name' => 'EUR',
    'brief' => 'EURO',
    'exchange' => '26163.75',
    'position' => '4',
  ),
  'DKK' =>
  array (
    'id' => 'DKK',
    'name' => 'DKK',
    'brief' => 'DANISH KRONE',
    'exchange' => '3523.35',
    'position' => '3',
  ),
  'CHF' =>
  array (
    'id' => 'CHF',
    'name' => 'CHF',
    'brief' => 'SWISS FRANCE',
    'exchange' => '17216.9',
    'position' => '2',
  ),
  'CAD' =>
  array (
    'id' => 'CAD',
    'name' => 'CAD',
    'brief' => 'CANADIAN DOLLAR',
    'exchange' => '16940.9',
    'position' => '1',
  ),
  'AUD' =>
  array (
    'id' => 'AUD',
    'name' => 'AUD',
    'brief' => 'AUST.DOLLAR',
    'exchange' => '15041.84',
    'position' => '0',
  ),
);?>