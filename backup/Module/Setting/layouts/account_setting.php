<script>
function make_cmd(cmd)
{
	jQuery('#cmd').val(cmd);
	document.AccountSettingForm.submit();
}
</script><fieldset id="toolbar">
	<legend>[[.config_manage.]]</legend>
	<div id="toolbar-info">[[.Account_setting.]]</div>
	<div id="toolbar-content" align="right">
	<table>
	  <tbody>
		<tr>
		<td id="toolbar-save"  align="center"><a onclick="make_cmd('save');"><span title="[[.Save.]"> </span> [[.Save.]] </a> </td>
		 <td id="toolbar-help" align="center"><a href="<?php echo Url::build_current();?>#"> <span title="[[.Help.]"> </span> [[.Help.]] </a> </td>
		</tr>
	  </tbody>
	</table>
	</div>
</fieldset>
<script type="text/javascript">
	jQuery(function() {
		jQuery('#AccountSetting').tabs();
		});
</script>
<div style="height:8px;"></div>
 <fieldset id="toolbar">
<form name="AccountSettingForm" method="post" action="?<?php echo htmlentities($_SERVER['QUERY_STRING']);?>" id="AccountSettingForm" enctype="multipart/form-data">
  <div id="AccountSetting" align="center">
	<ul>
		<li><a href="#front_back_config"><span>[[.Front_back_config.]]</span></a></li>
		<!--IF:cond(User::is_admin())--><li><a href="#system_config"><span>[[.System_config.]]</span></a></li><!--/IF:cond-->
	</ul>
	<!--IF:cond1(User::is_admin())-->
	<div id="system_config">
		<br>
			<table  cellpadding="6" cellspacing="0" width="100%" style="#width:99%;margin-top:4px;" border="1" bordercolor="#E7E7E7" align="center">
				<tr style="background-color:#F0F0F0">
					<th width="20%" align="left"><a>[[.Setting_name.]]</a></th>
					<th width="80%" align="left"><a>[[.Value.]]</a></th>
				</tr>
				<tr>
					<td width="20%" align="left" title="portal_template">[[.templates.]]</td>
					<td width="80%" align="left"><input name="config_portal_template" type="text" id="portal_template" class="input-big-huge"></td>
				</tr>
				<tr>
					<td width="20%" align="left" title="size_upload">[[.size_upload.]] /(1[[.times.]] [[.upload.]])</td>
					<td width="80%" align="left"><input name="config_size_upload" type="text" id="size_upload" class="input-large"></td>
				</tr>
				<tr>
					<td width="20%" align="left" title="type_image_upload">[[.type_image_upload.]]</td>
					<td width="80%" align="left"><input name="config_type_image_upload" type="text" id="type_image_upload" class="input-big-huge"></td>
				</tr>
				<tr>
					<td width="20%" align="left" title="type_file_upload">[[.type_file_upload.]]</td>
					<td width="80%" align="left"><input name="config_type_file_upload" type="text" id="type_file_upload" class="input-big-huge"></td>
				</tr>
				<tr>
					<td width="20%" align="left" title="use_cache">[[.Use_cache.]]</td>
					<td width="80%" align="left">
						<select name="config_use_cache" class="select" id="use_cache"></select>
						<script>jQuery('#use_cache').val(<?php echo Url::get('config_use_cache',0)?>);</script>
					</td>
				</tr>
				<tr>
					<td valign="top"  align="left" title="rewrite">[[.rewrite_url.]]</td>
					<td  align="left"><select name="config_rewrite" id="rewrite" class="select"></select>
					<script>jQuery('#rewrite').val(<?php echo Url::get('config_rewrite',0)?>);</script>
					</td>
				</tr>
				<tr>
					<td valign="top"  align="left" title="use_double_click">[[.use_double_click.]]</td>
					<td  align="left"><select name="config_use_double_click" class="select" id="use_double_click"></select>
					<script>jQuery('#use_double_click').val(<?php echo Url::get('config_use_double_click',0)?>);</script>
					</td>
				</tr>
				<tr>
					<td valign="top"  align="left" title="use_log">[[.use_log.]]</td>
					<td  align="left"><select name="config_use_log" class="select" id="use_log"></select>
					<script>jQuery('#use_log').val(<?php echo Url::get('config_use_log',0)?>);</script>
					</td>
				</tr>
				<tr>
					<td valign="top"  align="left" title="use_recycle_bin">[[.use_recycle_bin.]]</td>
					<td  align="left"><select name="config_use_recycle_bin" class="select" id="use_recycle_bin"></select>
					<script>jQuery('#use_recycle_bin').val(<?php echo Url::get('config_use_recycle_bin',0)?>);</script>
					</td>
				</tr>
			</table>
	</div>
	<!--/IF:cond1-->
	<div id="front_back_config">
		<br>
			<table  cellpadding="6" cellspacing="0" width="100%" style="#width:99%;margin-top:4px;" border="1" bordercolor="#E7E7E7" align="center">
				<tr style="background-color:#F0F0F0">
					<th width="20%" align="left"><a>[[.Setting_name.]]</a></th>
					<th width="80%" align="left"><a>[[.Value.]]</a></th>
				</tr>
				<tr>
					<td width="20%" align="left" title="language_default">[[.language_default.]]</td>
					<td width="80%" align="left"><select name="config_language_default" class="select" id="language_default"></select>
					<script>jQuery('#language_default').val(<?php echo Url::get('config_language_default',0)?>);</script>
					</td>
				</tr>
				<tr>
					<td width="20%" align="left" valign="top" title="site_title">[[.site_title.]]</td>
					<td width="80%" align="left"><input name="config_site_title" type="text" id="site_title" class="input-big-huge"></td>
				</tr>
				<tr>
					<td width="20%" align="left" valign="top" title="site_name">[[.site_name.]]</td>
					<td width="80%" align="left"><input name="config_site_name" type="text" id="site_name" class="input-big-huge"></td>
				</tr>
				<tr>
					<td width="20%" align="left" valign="top" title="site_icon">[[.icon_on_address.]] (*.icon)</td>
					<td width="80%" align="left">
						<input name="config_site_icon" type="file" id="site_icon" class="file">
						<div id="delete_site_icon"><?php if(Url::get('config_site_icon') and file_exists(Url::get('config_site_icon'))){?>[<a href="<?php echo Url::get('config_site_icon');?>" target="_blank" style="color:#FF0000">[[.view.]]</a>]&nbsp;[<a href="<?php echo Url::build_current(array('cmd'=>'unlink','link'=>Url::get('config_site_icon')));?>" onclick="jQuery('#delete_site_icon').html('');" target="_blank" style="color:#FF0000">[[.delete.]]</a>]<?php }?></div>
					</td>
				</tr>
				<tr>
					<td width="20%" align="left" valign="top" title="email_support_online">[[.email_support_online.]]</td>
					<td width="80%" align="left"><input name="config_email_support_online" type="text" id="email_support" class="input-big-huge"></td>
				</tr>
				<tr>
					<td width="20%" align="left" valign="top" title="email_webmaster">[[.email_webmaster.]]</td>
					<td width="80%" align="left"><input name="config_email_webmaster" type="text" id="email_webmaster" class="input-big-huge"></td>
				</tr>
				<tr>
					<td width="20%" align="left" valign="top" title="weblink">[[.weblink.]] ([[.name.]]:[[.path_link.]],..)</td>
					<td width="80%" align="left"><textarea name="config_weblink" class="textarea-small" id="weblink"></textarea></td>
				</tr>
				<tr>
					<td width="20%" align="left" valign="top" title="support_online">[[.support_online.]] ([[.nick_name.]]:[[.nick.]],..)</td>
					<td width="80%" align="left"><input name="config_support_online" type="text" id="support_online" class="input-big-huge"></td>
				</tr>
				<tr>
					<td width="20%" align="left" valign="top" title="support_online_sales">[[.support_online_sales.]] ([[.nick_name.]]:[[.nick.]],..)</td>
					<td width="80%" align="left"><input name="config_support_online_sales" type="text" id="support_online_sales" class="input-big-huge"></td>
				</tr>
                <tr>
					<td width="20%" align="left" valign="top" title="support_online_en">[[.support_online_en.]] ([[.nick_name.]]:[[.nick.]],..)</td>
					<td width="80%" align="left"><input name="config_support_online_en" type="text" id="support_online_en" class="input-big-huge"></td>
				</tr>
				<tr>
					<td width="20%" align="left" valign="top" title="support_skype">[[.support_skype.]] ([[.skype_name.]]:[[.skype.]],..)</td>
					<td width="80%" align="left"><input name="config_support_skype" type="text" id="support_skype" class="input-big-huge"></td>
				</tr>
				<tr>
					<td width="20%" align="left" valign="top" title="hot_line">[[.Hot_line.]] ([[.name.]]:[[.Phone_number.]],..) - hot_line</td>
					<td width="80%" align="left"><input name="config_hot_line" type="text" id="hot_line" class="input-big-huge"></td>
				</tr>
				<tr>
					<td width="20%" align="left" valign="top" title="company_address">[[.company_adress.]]</td>
					<td width="80%" align="left"><input name="config_company_address" type="text" id="company_address" class="input-big-huge"></td>
				</tr>
                <tr>
					<td width="20%" align="left" valign="top" title="company_address_en">[[.company_adress_en.]]</td>
					<td width="80%" align="left"><input name="config_company_address_en" type="text" id="company_address_en" class="input-big-huge"></td>
				</tr>
				<tr>
					<td width="20%" align="left" valign="top" title="company_phone">[[.company_phone.]]</td>
					<td width="80%" align="left"><input name="config_company_phone" type="text" id="company_phone" class="input-big-huge"></td>
				</tr>
				<tr>
					<td width="20%" align="left" valign="top" title="company_fax">[[.company_fax.]]</td>
					<td width="80%" align="left"><input name="config_company_fax" type="text" id="company_fax" class="input-big-huge"></td>
				</tr>
				<tr>
					<td width="20%" align="left" valign="top" title="contact_information">[[.contact_information.]]</td>
					<td width="80%" align="left"><textarea name="config_contact_information" class="textarea-small" id="contact_information"></textarea><script>simple_mce('contact_information');</script></td>
				</tr>
				<tr>
					<td width="20%" align="left" valign="top" title="extra_information">[[.extra_information.]]</td>
					<td width="80%" align="left"><textarea name="config_extra_information" class="textarea-small" id="extra_information"></textarea><script>//simple_mce('extra_information');</script></td>
				</tr>
				<tr>
					<td width="20%" align="left" valign="top" title="received_notification_from_contact">[[.received_notification_from_contact_by_email.]]</td>
					<td width="80%" align="left">
					<select name="config_received_notification_from_contact" class="select-large" id="received_notification_from_contact"></select>
					<script>jQuery('#received_notification_from_contact').val(<?php echo Url::get('config_received_notification_from_contact',0)?>);</script>
					</td>
				</tr>
				<tr>
					<td width="20%" align="left" valign="top" title="is_active">[[.site_status.]]</td>
					<td width="80%" align="left">
						<select name="config_is_active" class="select-large" id="is_active"></select>
						<script>jQuery('#is_active').val(<?php echo Url::get('config_is_active',0)?>);</script>
					</td>
				</tr>
				<tr>
					<td width="20%" align="left" valign="top" title="notification_when_interrption">[[.notification_when_interrption.]]</td>
					<td width="80%" align="left"><textarea name="config_notification_when_interrption" class="textarea-small" id="notification_when_interrption"></textarea></td>
				</tr>
			</table>
	</div>
</div>
<input name="cmd" type="hidden" id="cmd" value="save">
</form>
<table width="100%" cellpadding="6" cellspacing="0" style="background-color:#F0F0F0;border:1px solid #E7E7E7;height:8px;#width:99%" align="center">
	<tr>
		<td align="right">&nbsp;</td>
	</tr>
</table>
<div style="#height:8px;"></div>
</fieldset>
