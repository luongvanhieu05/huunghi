<?php
class SettingForm extends Form
{
	function SettingForm()
	{
		Form::Form('SettingForm');
		$this->link_css('skins/default/css/cms.css');
	}
	function on_submit()
	{
		if(Url::get('cmd')=='seo')
		{
			Portal::set_setting('website_keywords',Url::get('website_keywords'),false,'PORTAL');
			Portal::set_setting('website_description',Url::get('website_description'),false,'PORTAL');
			Portal::set_setting('google_analytics',Url::get('google_analytics'),false,'PORTAL');
			Session::delete('portal');
			Url::redirect_current(array('cmd'=>'seo'));
		}
	}
	function draw()
	{
		$this->parse_layout('list',array(
			'website_keywords'=>Portal::get_setting('website_keywords','')
			,'website_description'=>Portal::get_setting('website_description',''),
			'google_analytics'=>Portal::get_setting('google_analytics','')
		));
	}
}
?>