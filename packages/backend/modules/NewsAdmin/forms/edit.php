<?php
class EditNewsAdminForm extends Form{
	function EditNewsAdminForm(){
		Form::Form('EditNewsAdminForm');
		$languages = DB::select_all('language');
		foreach($languages as $language){
			
		}
		$this->add('name_1',new TextType(true,'invalid_name_1',0,2000));
		$this->add('category_id',new TextType(true,'invalid_category_id',0,2000));
		//$this->link_css('assets/default/css/tabs/tabpane.css');
		//$this->link_js('assets/default/css/tabs/tabpane.js');
	}
	function save_item(){
		$rows = array();
		$languages = DB::select_all('language');
		foreach($languages as $language){
			$brief = Url::get('brief_'.$language['id'].'');//String::display_sort_title(strip_tags(Url::get('description_'.$language['id'].'')),50);
			$rows += array('name_'.$language['id'].''=>Url::get('name_'.$language['id'].'',1));
			$rows += array('brief_'.$language['id'].''=>$brief);//$this->set_autolink
			$rows += array('description_'.$language['id'].''=>Url::get('description_'.$language['id'].''));
		}
		require_once 'packages/core/includes/utils/vn_code.php';
		require_once 'packages/core/includes/utils/search.php';
		$rows['keywords']=str_replace(array(", ...",''),'',str_replace(array(' ','"','!','\'','"','"'),', ',strip_tags($rows['name_1'])));
		$rows += array(
			'category_id'
			,'publish'=>Url::get('publish')==1?1:0
			,'hitcount'
			,'status'
			,'type'=>'NEWS'
			,'author'
			,'tags'
			,'show_comment'
			,'portal_id'=>PORTAL_ID
			);
			if(Url::get('position')==''){
				$position = DB::fetch('select max(position)+1 as id from news where type="NEWS"');
				$rows['position'] = $position['id'];
			}else{
				$rows['position'] = Url::get('position');
			}
			$name_id = convert_utf8_to_url_rewrite($rows['name_1']);
			if(!DB::fetch('select name_id from news where name_id="'.$name_id.'" and portal_id="'.PORTAL_ID.'" and news.type="NEWS"')){
				$rows+=array('name_id'=>$name_id);
			}else{
				if(Url::get('id') and Url::get('cmd')=='edit'){
					$rows+=array('name_id'=>$name_id);
				}else{
					$this->error('name','duplicate_name');
				}
			}
		return ($rows);
	}
	function save_image($file,$id){
		require_once 'packages/core/includes/utils/upload_file.php';
		$dir = substr(PORTAL_ID,1).'/content';
		update_upload_file('small_thumb_url',$dir);
		update_upload_file('image_url',$dir);
		update_upload_file('file',$dir,'FILE');
		$row = array();
		if(Url::get('small_thumb_url')!=''){
			$row = array_merge($row,array('small_thumb_url'));
		}
		if(Url::get('image_url')!=''){
			$row = array_merge($row,array('image_url'));
		}
		if(Url::get('file')!=''){
			$row = array_merge($row,array('file'));
		}
		DB::update_id('news',$row,$id);
	}
	function on_submit(){
		if($this->check()){
			$rows = $this->save_item();
			if(!$this->is_error()){
				if(Url::get('cmd')=='edit' and $item = DB::exists_id('news',Url::get('id'))){
					$id = intval(Url::get('id'));
					$rows += array('last_time_update'=>time());
					if(!$item['publisher'] and $rows['publish']){
							$rows['publisher'] = Session::get('user_id');
							$rows['published_time'] = time();
					}
					if($rows['publish']==0){
						$rows['publisher'] = '';
						$rows['published_time'] = 0;
					}
					DB::update_id('news',$rows+array('time'=>Date_Time::to_time(Url::get('time'))),$id);
					if(strip_tags($item['keywords'])){
						$rows['keywords'] = $item['keywords'];
					}
				}else{
					$rows += array('time'=>time(),'user_id'=>Session::get('user_id'));
					$id = DB::insert('news',$rows);
				}
				$this->save_image($_FILES,$id);
				//save_log($id);
				if($id){
					$category_id = DB::fetch('select id,category_id from news where id = '.$id.'','category_id');
					echo '<script>if(confirm("'.Portal::language('update_success_are_you_continous').'")){location="'.Url::build_current(array('cmd'=>'add')).'";}else{location="'.Url::build_current(array('cmd'=>'list','just_edited_id'=>$id,'search_category_id','page_no')).'";}</script>';
				}
			}
		}
	}
	function draw(){
		$this->map = array();
		require_once 'cache/config/status.php';
		$languages = DB::select_all('language');
		$arr = array('1'=>'YES','0'=>'NO');
		$this->map['publish'] = 0;
		$this->map['publisher'] = '';
		$this->map['published_time'] = 0;
		if(Url::get('cmd')=='edit' and Url::get('id') and $news = DB::exists_id('news',intval(Url::get('id')))){
			$this->map['publish'] = $news['publish'];
			$this->map['publisher'] = $news['publisher'];
			$this->map['published_time'] = date('H:i\' d/m/y',$news['published_time']);
			$news['time'] = date('d/m/Y',$news['time']);
			foreach($news as $key=>$value){
				if(is_string($value) and !isset($_REQUEST[$key])){
					$_REQUEST[$key] = $value;
				}
			}
		}else{
			$_REQUEST['time'] = date('d/m/Y');
		}
		$categories = NewsAdminDB::get_category('');
		require_once 'packages/core/includes/utils/category.php';
		combobox_indent($categories);
		$this->map += array(
			'category_id_list'=>String::get_list($categories,false,false,true),
			'status_list'=>$status,
			'languages'=>$languages,
			'show_image_list'=>$arr,
			'show_email_list'=>$arr,
			'show_print_list'=>$arr,
			'show_time_list'=>$arr,
			'show_author_list'=>$arr,
			'show_comment_list'=>$arr,
			'front_page_list'=>$arr
		);
		$this->parse_layout('edit',$this->map);
	}
	function set_autolink($content){
		$str = Portal::get_setting('auto_link');
		$arr = explode("\n",$str);
		foreach($arr as $key=>$value){
			$tmp_arr = explode("=>",$value);
			if(preg_match("/\<a ([^\>]+)\>$tmp_arr[0]<\/a\>/i",$content)){
				continue;
			}else{
				if(isset($tmp_arr[1])){
					$content = preg_replace("/".$tmp_arr[0]."/","<a class=\"no-highlight\" target=\"_blank\" href=\"$tmp_arr[1]\">$tmp_arr[0]</a>",$content);	
				}
			}
		}
		return $content;
	}
}
?>
