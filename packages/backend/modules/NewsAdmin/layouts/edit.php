<?php if(Url::get('id') != 47){?>
<script src="assets/admin/scripts/tinymce/tinymce.min.js"></script>
<script>
tinymce.init({
  selector: '#description_1',
  height: 500,
  theme: 'modern',
  plugins: [
    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
    'searchreplace wordcount visualblocks visualchars code fullscreen',
    'insertdatetime media nonbreaking save table contextmenu directionality',
    'emoticons template paste textcolor colorpicker textpattern imagetools'
  ],
  toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
  toolbar2: 'print preview media | forecolor backcolor emoticons',
	theme_advanced_buttons1 : "openmanager",
  image_advtab: true,
  content_css: [
    'assets/admin/scripts/tinymce/skins/lightgray/skin.min.css'
  ],
	automatic_uploads: false,
	file_browser_callback: function() {
		window.open('<?php echo Url::build('file_manager')?>','File manager','width=880,height=600');
  },
	open_manager_upload_path: 'uploads/'
 });
</script>
<?php }?>
<script>
jQuery(document).ready(function(){
	jQuery('#time').datepicker({
			format: "dd/mm/yyyy",
			language: "vi"
	});
});
</script>
<fieldset id="toolbar">
 	<div id="toolbar-title">
		Quản lý nội dung <span>[ <?php echo Portal::language(Url::get('cmd','list'));?> ]</span>
	</div>
	<div id="toolbar-content" align="right">
	<table align="right">
	  <tbody>
		<tr>
		  <td id="toolbar-save"  align="center"><a onclick="EditNewsAdmin.submit();"> <span title="Edit"> </span> Ghi lại </a> </td>
		  <td id="toolbar-back"  align="center"><a href="<?php echo Url::build_current(array('cmd'=>'list'));?>#"> <span title="New"> </span> Quay lại </a> </td>
		</tr>
	  </tbody>
	</table>
    </div>
</fieldset>
<br clear="all"/>
<fieldset id="toolbar">
	<?php if(Form::$current->is_error()){echo Form::$current->error_messages();}?>
	<form name="EditNewsAdmin" id="EditNewsAdmin" method="post" enctype="multipart/form-data">
		<table cellspacing="4" cellpadding="4" border="0" width="100%" style="background-color:#FFFFFF;">
		<tr>
		  <td valign="top">
	    <table class="table">
					<tr>
					  <td width="16%" align="left">[[.category_id.]] (<span class="require">*</span>)</td>
					  <td width="28%" align="left"><select name="category_id" id="category_id" class="form-control"></select></td>
					  <td width="12%" align="left"><?php if(User::can_admin(false,ANY_CATEGORY)){?>Duyệt bài<?php }?></td>
					  <td width="44%" align="left"><?php if(User::can_admin(false,ANY_CATEGORY)){?><input  name="publish" type="checkbox" value="1" id="publish" <?php if(Url::get('publish')==1){echo 'checked="checked"';}?>> <!--IF:cond([[=publish=]] and [[=publisher=]])-->Người duyệt: <strong>[[|publisher|]]</strong>/[[|published_time|]]<!--/IF:cond--><?php }?></td>
				  </tr>
					<tr>
						<td align="left">[[.status.]]</td>
						<td align="left"><select name="status" id="status" class="form-control"></select></td>
						<td align="left">Vị trí</td>
						<td align="left"><input name="position" type="text" id="position" class="form-control"/></td>
					</tr>
					<tr>
					  <td align="left"><!--Nhãn--></td>
					  <td align="left"><!--<select name="label" id="label" class="form-control">
				      </select>--></td>
					  <td align="left">Ngày đăng</td>
					  <td align="left">
            <div class="input-group date">
                <input name="time" type="text" id="time" class="form-control"/>
                <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
            </div>
            </td>
		    </tr>
				</table>
<br/>
				<table class="table">
					<tr>
						<td>
						<div class="tab-pane-12" id="tab-pane-category">
						<!--LIST:languages-->
            <!--IF:cond([[=languages.id=]]==1)-->
						<div class="tab-page1" id="tab-page-category-[[|languages.id|]]">
							<h2 class="tab hide">[[|languages.name|]]</h2>
							<div class="form_input_label"><h3>[[.name.]] (<span class="require">*</span>)</h3></div>
							<div class="form_input">
								 <input name="name_[[|languages.id|]]" type="text" id="name_[[|languages.id|]]" class="form-control"  />
							</div>
              <div class="form_input_label"><h3>Thông tin vắn</h3></div>
							<div class="form_input">
                  <textarea id="brief_[[|languages.id|]]" name="brief_[[|languages.id|]]" cols="75" rows="10" class="form-control"><?php echo Url::get('brief_'.[[=languages.id=]],'');?></textarea><br />
								
							</div>
							<div class="form_input_label"><h3>Thông tin đầy đủ</h3></div>
							<div class="form_input">
              <textarea id="description_[[|languages.id|]]" name="description_[[|languages.id|]]" cols="75" rows="20" style="width:99%; height:350px;overflow:hidden"><?php echo Url::get('description_'.[[=languages.id=]],'');?></textarea><br />
							</div>
						</div>
            <!--/IF:cond-->
						<!--/LIST:languages-->
						</div>
						</td>
				   </tr>
				</table>
			</td>
			<td valign="top" style="width:320px;">
				<table width="100%" style="border: 1px dashed silver;" cellpadding="4" cellspacing="2">
				<tr>
					<td><strong>[[.Status.]]</strong></td>
					<td><?php echo Url::get('status','0');?></td>
				</tr>
				<tr>
				  <td><strong>[[.Rating.]]</strong></td>
				  <td><?php echo Url::get('rating','0');?></td>
				  </tr>
				<tr>
					<td><strong>[[.Hitcount.]]</strong></td>
					<td><?php echo Url::get('hitcount','0');?></td>
				</tr>
				</table>
				<div id="panel">
					<div id="panel_1"  style="margin-top:8px;">
					<span>[[.images.]]</span>
					<table class="table">
						<tr>
							<td width="30%" align="right">Ảnh đại diện</td>
						    <td width="70%" align="left"><input name="small_thumb_url" type="file" id="small_thumb_url" class="file" size="18"><div id="delete_small_thumb_url"><?php if(Url::get('small_thumb_url') and file_exists(Url::get('small_thumb_url'))){?>[<a href="<?php echo Url::get('small_thumb_url');?>" target="_blank" style="color:#FF0000">[[.view.]]</a>]&nbsp;[<a href="<?php echo Url::build_current(array('cmd'=>'unlink','link'=>Url::get('small_thumb_url')));?>" onclick="jQuery('#delete_small_thumb_url').html('');" target="_blank" style="color:#FF0000">[[.delete.]]</a>]<?php }?></div></td>
						</tr>
						<tr>
							<td width="30%" align="right">Ảnh lớn</td>
						    <td width="70%" align="left"><input name="image_url" type="file" id="image_url" class="file" size="18"><div id="delete_image_url"><?php if(Url::get('image_url') and file_exists(Url::get('image_url'))){?>[<a href="<?php echo Url::get('image_url');?>" target="_blank" style="color:#FF0000">[[.view.]]</a>]&nbsp;[<a href="<?php echo Url::build_current(array('cmd'=>'unlink','link'=>Url::get('image_url')));?>" onclick="jQuery('#delete_image_url').html('');" target="_blank" style="color:#FF0000">[[.delete.]]</a>]<?php }?></div></td>
						</tr>
						<tr>
							<td width="30%" align="right">File đính kèm</td>
						    <td width="70%" align="left"><input name="file" type="file" id="file" class="file" size="18"><div id="delete_file"><?php if(Url::get('file') and file_exists(Url::get('file'))){?>[<a href="<?php echo Url::get('file');?>" target="_blank" style="color:#FF0000">[[.view.]]</a>]&nbsp;[<a href="<?php echo Url::build_current(array('cmd'=>'unlink','link'=>Url::get('file')));?>" onclick="jQuery('#delete_file').html('');" target="_blank" style="color:#FF0000">[[.delete.]]</a>]<?php }?></div></td>
						</tr>
					</table>
					</div>
					<div id="panel_1" style="margin-top:8px;">
					<span>[[.Parameters_article.]]</span>
					<table class="table">
						<tr>
							<td width="49%" align="right">[[.author.]]</td>
						  <td width="51%" align="left"><input name="author" type="text" id="author" class="form-control"></td>
						</tr>
						<tr>
							<td width="49%" align="right">[[.hitcount.]]</td>
						  <td width="51%" align="left"><input name="hitcount" type="text" id="hitcount" class="form-control"></td>
						</tr>
						<tr>
							<td width="49%" align="right">[[.show_comment.]]</td>
						  <td width="51%" align="left"><select name="show_comment" id="show_comment" class="select"></select></td>
						</tr>
					</table>
					</div>
					<div id="panel_1"  style="margin-top:8px;">
					<span>[[.Metadata_information.]]</span>
					<table cellpadding="4" cellspacing="0" width="100%" border="1" bordercolor="#E9E9E9">
						<tr>
							<td width="30%" align="right" valign="top">[[.keywords.]]</td>
						  <td width="70%" align="left"><textarea name="keywords" id="keywords" class="input-large" style="height:80px;"></textarea></td>
						</tr>
						<tr>
							<td width="30%" align="right" valign="top">[[.tags.]]</td>
						  <td width="70%" align="left"><textarea name="tags" id="tags" class="input-large" style="height:50px;"></textarea></td>
						</tr>
					</table>
					</div>
				</div>
			</td>
		</tr>
		</table>
	</form>
</fieldset>