<?php
/******************************
COPY RIGHT BY NYN PORTAL - TCV
WRITTEN BY thedeath
******************************/
class DrivingQuestion extends Module
{
	function DrivingQuestion($row)
	{
		Module::Module($row);
		if(User::can_admin(false,ANY_CATEGORY))
		{
			if(
				Url::check('driving_kind_id') and $survey=DB::exists_id('driving_question',$_REQUEST['driving_kind_id']) and Url::check('block_id') and $block=DB::exists_id('block',$_REQUEST['block_id'])
			)
			{
				DB::query('replace block_setting(block_id, setting_id, value) values('.$_REQUEST['block_id'].',"survey_id","'.DB::escape($_REQUEST['survey_id']).'")');
				require_once 'packages/core/includes/portal/update_page.php';
				update_page($block['page_id']);
				if(URL::check('href'))
				{
					Url::redirect_url($_REQUEST['href']);
				}
				else
				{
					URL::redirect_current();
				}
			}
			else
			if(URL::get('cmd')=='delete' and is_array(URL::get('selected_ids')) and sizeof(URL::get('selected_ids'))>0 and User::can_delete(false,ANY_CATEGORY))
			{
				Module::Module($row);
				require_once 'db.php';
				if(sizeof(URL::get('selected_ids'))>0)
				{
					foreach(URL::get('selected_ids') as $key)
					{
						$this->delete($this,$key);
					}
					Url::redirect_current(array());
				}
			}
			else
			if(
				(((URL::check(array('cmd'=>'delete'))and User::can_delete(false,ANY_CATEGORY))
					or (URL::check(array('cmd'=>'edit')) and User::can_edit(false,ANY_CATEGORY)))
					and Url::check('id') and $record = DB::exists_id('driving_question',$_REQUEST['id'])and User::can_edit(false,ANY_CATEGORY))
				or
				(URL::check(array('cmd'=>'add')) and User::can_edit(false,ANY_CATEGORY))
				or !URL::check('cmd')
			)
			{
				require_once 'db.php';
				switch(URL::get('cmd'))
				{
				case 'edit':
				case 'add':
					require_once 'forms/edit.php';
					$this->add_form(new EditDrivingQuestionForm());break;
				default:
					require_once 'forms/list.php';
					$this->add_form(new ListDrivingQuestionForm());
					break;
				}
			}
			else
			{
				Url::redirect_current();
			}
		}
		else
		{
			URL::access_denied();
		}
	}
	function delete(&$form,$id)
	{
		$row = DB::select('driving_question',$id);
		if(!User::can_admin())
		{
			URL::redirect_current();
		}
		$item_row = DB::select_all('driving_question_option','driving_question_id='.$id);
		DB::delete('driving_question_option', 'driving_question_id='.$id);
		DB::delete_id('driving_question', $id);
	}
}
?>
