<?php
class ListDrivingQuestionForm extends Form
{
	function ListDrivingQuestionForm()
	{
		Form::Form('ListDrivingQuestionForm');
		$this->link_css('assets/default/css/cms.css');
	}
	function on_submit()
	{
		if(URL::get('confirm'))
		{
			require_once 'detail.php';
			foreach(URL::get('selected_ids') as $id)
			{
				if($item = DB::exists_id('driving_question',$id))
				{
					DB::delete_id('driving_question',$id);
				}
				if($this->is_error())
				{
					return;
				}
			}
			Url::redirect_current(array(
	  ));
		}
	}
	function draw()
	{
		$languages = DB::select_all('language');
		$item_per_page = 50;
		$cond = '1';
		DB::query('
			select count(*) as acount
			from
				`driving_question`
				left outer join driving_kind on driving_kind.id = driving_question.driving_kind_id
			where
				'.$cond.'
			'.(URL::get('order_by')?'order by '.URL::get('order_by').(URL::get('order_dir')?' '.URL::get('order_dir'):''):'').'
			limit 0,1
		');
		$count = DB::fetch();
		require_once 'packages/core/includes/utils/paging.php';
		$paging = paging($count['acount'],$item_per_page);
		DB::query('
			select
				`driving_question`.id
				,driving_kind.name_'.Portal::language().' as driving_kind_id
				,`driving_question`.name_'.Portal::language().' as name
				,`driving_question`.description_'.Portal::language().' as description
			from
			 	`driving_question`
				left outer join driving_kind on driving_kind.id = driving_question.driving_kind_id
			where
			'.$cond.'
			'.(URL::get('order_by')?'order by '.URL::get('order_by').(URL::get('order_dir')?' '.URL::get('order_dir'):''):'order by id DESC').'
			limit '.((page_no()-1)*$item_per_page).','.$item_per_page.'
		');
		$items = DB::fetch_all();
		$i=1;
		foreach ($items as $key=>$value)
		{
			$items[$key]['i']=$i++;
		}
		$just_edited_id['just_edited_ids'] = array();
		if (UrL::get('selected_ids'))
		{
			if(is_string(UrL::get('selected_ids')))
			{
				if (strstr(UrL::get('selected_ids'),','))
				{
					$just_edited_id['just_edited_ids']=explode(',',UrL::get('selected_ids'));
				}
				else
				{
					$just_edited_id['just_edited_ids']=array('0'=>UrL::get('selected_ids'));
				}
			}
		}
		$this->parse_layout('list',$just_edited_id+
			array(
				'items'=>$items,
				'paging'=>$paging,
			)
		);
	}
}
?>