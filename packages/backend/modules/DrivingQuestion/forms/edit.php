<?php
class EditDrivingQuestionForm extends Form
{
	function EditDrivingQuestionForm()
	{
		Form::Form('EditDrivingQuestionForm');
		if(URL::get('cmd')=='edit')
		{
			$this->add('id',new IDType(true,'object_not_exists','driving_question'));
		}
		$languages = DB::select_all('language');
		foreach($languages as $language)
		{
			$this->add('name_'.$language['id'],new TextType(true,'invalid_name',0,2000));
			$this->add('driving_question_option.name_'.$language['id'],new TextType(true,'invalid_name',0,200000));
		}
		$this->add('driving_question_option.position',new IntType(false,'invalid_position','0','100000000000'));
		$this->link_css('assets/default/css/cms.css');
		$this->link_css('assets/default/css/tabs/tabpane.css');
		$this->link_js('assets/default/css/tabs/tabpane.js');
	}
	function on_submit()
	{
		if(URL::get('cmd')=='edit')
		{
			$row = DB::select('driving_question',$_REQUEST['id']);
		}
		if($this->check() and URL::get('confirm_edit'))
		{
			$extra = array();
			$languages = DB::select_all('language');
			foreach($languages as $language)
			{
				$extra=$extra+array('name_'.$language['id']=>Url::get('name_'.$language['id'],''));
				$extra=$extra+array('description_'.$language['id']=>Url::get('description_'.$language['id'],''));
			}
			$new_row = $extra+array('driving_kind_id','answer');
			if(URL::get('cmd')=='edit')
			{
				$id = $_REQUEST['id'];
				DB::update_id('driving_question', $new_row,$id);
			}
			else
			{
				require_once 'packages/core/includes/system/si_database.php';
				$id = DB::insert('driving_question', $new_row);
			}
			if(URl::get('deleted_ids'))
			{
				foreach(URl::get('deleted_ids') as $delete_id)
				{
					DB::delete_id('driving_question_option',$delete_id);
				}
			}
			if(isset($_REQUEST['mi_survey_options']))
			{
				foreach($_REQUEST['mi_survey_options'] as $key=>$record)
				{
					$empty = true;
					foreach($record as $record_value)
					{
						if($record_value)
						{
							$empty = false;
						}
					}
					if(!$empty)
					{
						$record['driving_question_id'] = $id;
						if($record['id'])
						{
							DB::update('driving_question_option',$record,'id='.$record['id']);
						}
						else
						{
							unset($record['id']);
							DB::insert('driving_question_option',$record);
						}
					}
				}
			}
			Url::redirect_current(array(
	  )+array('just_edited_id'=>$id));
		}
	}
	function draw()
	{
		$languages = DB::select_all('language');
		if(URL::get('cmd')=='edit' and $row=DB::select('driving_question',URL::sget('id')))
		{
			foreach($row as $key=>$value)
			{
				if(is_string($value) and !isset($_POST[$key]))
				{
					$_REQUEST[$key] = $value;
				}
			}
			$edit_mode = true;
		}
		else
		{
			$edit_mode = false;
		}
		if(!isset($_REQUEST['mi_survey_options']) and $edit_mode)
		{
			$additions = '';
			foreach($languages as $language)
			{
				$additions .= '
			,`driving_question_option`.`name_'.$language['id'].'`
				';
			}
			DB::query('
				select
					`driving_question_option`.id
					,`driving_question_option`.`position`
					'.$additions.'
				from
					`driving_question_option`
				where
					`driving_question_option`.driving_question_id="'.URL::sget('id').'"'
			);
			$mi_survey_options = DB::fetch_all();
			foreach($mi_survey_options as $key=>$value)
			{
					$mi_survey_options[$key]['position'] = System::display_number($value['position']);
			}
			$_REQUEST['mi_survey_options'] = $mi_survey_options;
		}
		require_once Portal::template_js('core').'/tinymce/init_tinyMCE.php';
		$this->parse_layout('edit',
			($edit_mode?$row:array())+
			array(
			'languages'=>$languages,
			'driving_kind_id_list'=>String::get_list(DrivingQuestionDB::get_parent())
			)
		);
	}
}
?>
