<?php
/******************************
COPY RIGHT BY NYN PORTAL - TCV
WRITTEN BY minhtc
******************************/
class ManageTimeContact extends Module
{
	function ManageTimeContact($row)
	{
		Module::Module($row);
		require_once 'db.php';
		$_REQUEST['month'] = Url::get('month')?Url::get('month'):date('m');
		$_REQUEST['year'] = Url::get('year')?Url::get('year'):date('Y');
		if(User::can_view(false,ANY_CATEGORY))
		{
			$cmd = Url::get('cmd');
			switch($cmd){
				case 'save': $this->save(); exit();
				case 'delete': $this->delete(); exit();
				case 'edit': $this->edit(); exit();
				default: $this->show();
			}
		}else{
			URL::access_denied();
		}
	}
	function save(){
		if($id=Url::get('id')){
			$item = explode('_',$id);
			$user_id = $item[0];
			$date = explode('-',$item[1]);
			$time = mktime(0,0,0,$date[0],$date[1],$date[2])+46800;
			$account = ManageTimeContactDB::get_user('account.id="'.$user_id.'"');
			//echo $date[0].'-'.$date[1].'-'.$date[2]; exit();
			if($info = ManageTimeContactDB::get_item('time='.$time)){
				if(DB::update_id('received_contact',array('user_id'=>$user_id,'email'=>$account['email'],'name'=>$account['name']),$info['id'])){
					echo 'var name="'.$account['name'].'"; var email="'.$account['email'].'";';
				}else{
					echo 'error';
				}
			}else{
				if(DB::insert('received_contact',array('user_id'=>$user_id,'email'=>$account['email'],'name'=>$account['name'],'time'=>$time))){
					echo 'var name="'.$account['name'].'"; var email="'.$account['email'].'";';
				}else{
					echo 'error';
				}
			}
		}else{
			echo 'error';
		}
		exit();
	}
	function show(){
		require_once 'forms/list.php';
		$this->add_form(new ManageTimeContactForm());
	}
}
?>