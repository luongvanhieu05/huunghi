<div class="time-contact-bound">
	<form name="timeForm" method="post">
	<div style="padding:5px;">
    	<label>[[.year.]]</label>
        <select name="year" id="year" onchange="document.timeForm.submit();"></select>
    	<label>[[.month.]]</label>
        <select name="month" id="month" onchange="document.timeForm.submit();"></select>
    </div>
    </form>
    <div>
        <table width="100%" border="1" cellspacing="0" cellpadding="5" class="manage-time-contact-bound">
            <tr bgcolor="#cccccc">
	            <th>[[.day.]]</th>
	            <th>[[.user_id.]]</th>
	            <th>[[.full_name.]]</th>
	            <th>[[.email.]]</th>
            </tr>
            <?php for($i=1;$i<=[[=date_number=]];$i++){?>
            <tr<?php echo date('D',mktime(0, 0, 0, Url::get('month'), $i, Url::get('year')))=='Sun'?' bgcolor="#efefef"':''; echo ($i<10?'0'.$i:$i).'-'.Url::get('month').'-'.Url::get('year')==date('d-m-Y')?' bgcolor="#FFD8B0"':'';?>>
	            <th style="font-weight:normal;"><?php echo date('D',mktime(0, 0, 0, Url::get('month'), $i, Url::get('year'))).' - '.($i<10?'0'.$i:$i).'/'.Url::get('month').'/'.Url::get('year');?></th>
	            <th>
                	<select  name="user_id_<?php echo Url::get('month').'-'.($i<10?'0'.$i:$i).'-'.Url::get('year');?>" id="user_id_<?php echo Url::get('month').'-'.($i<10?'0'.$i:$i).'-'.Url::get('year');?>" onchange="save_time_contact(this);">
                    	<!--LIST:user_id-->
                        <option value="[[|user_id.id|]]_<?php echo Url::get('month').'-'.($i<10?'0'.$i:$i).'-'.Url::get('year');?>"<?php echo Url::get([[=user_id.id=]].'_'.Url::get('month').'-'.($i<10?'0'.$i:$i).'-'.Url::get('year'))?' selected':'';?>>[[|user_id.id|]]</option>
                    	<!--/LIST:user_id-->
                    </select>
                </th>
	            <th width="25%"><?php echo Url::get('name_'.($i<10?'0'.$i:$i))?Url::get('name_'.($i<10?'0'.$i:$i)):'';?></th>
	            <th width="25%"><?php echo Url::get('email_'.($i<10?'0'.$i:$i))?Url::get('email_'.($i<10?'0'.$i:$i)):'';?></th>
            </tr>
            <?php }?>
        </table>
    </div>
</div>
<script type="text/javascript">
function save_time_contact(obj){
	var id = jQuery(obj).val();
	jQuery.ajax({
		method: "POST",
		url: 'form.php?block_id=<?php echo Module::block_id(); ?>',
		data : {
			'id':id,
			'cmd':'save'
		},
		beforeSend: function(){
			//jQuery('#load').fadeIn(10).animate({opacity: 1.0}, 10);
		},
		success: function(content){
			if(content == 'error'){
				alert('[[.insert_data_is_invalid.]]');
			}else{
				eval(content);
				var id=jQuery(obj).attr('id');
				jQuery('#'+id).parent().next().html(name);
				jQuery('#'+id).parent().next().next().html(email);
			}
		}
	});
}
</script>