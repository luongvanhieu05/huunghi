<?php
class ManageTimeContactForm extends Form
{
	function ManageTimeContactForm()
	{
		Form::Form('ManageTimeContactForm');
		$this->link_css('assets/default/css/cms.css');
		$this->link_js('packages/core/includes/js/jquery/jquery.blockUI.js');
	}
	function on_submit(){
		if(Url::get('year')){
			Url::redirect_current(array('year'=>Url::get('year'),'month'));
		}
		if(Url::get('month')){
			Url::redirect_current(array('month'=>Url::get('month'),'year'));
		}
	}
	function draw()
	{
		$this->map = array();
		$current_year = date('Y');
		$month = array();
		for($i=1;$i<=12;$i++){
			$mon = $i<10?'0'.$i:$i;
			$month+=array($mon=>$mon);
		}
		$this->map['year_list'] = array($current_year=>$current_year,$current_year+1=>$current_year+1);
		$this->map['month_list'] = $month;
		$this->map['date_number'] = date('t');
		$this->map['user_id'] = ManageTimeContactDB::get_account();
		$items = ManageTimeContactDB::get_items();
		foreach($items as $key=>$value){
			$uid = $value['user_id'].'_'.$value['month'].'-'.$value['day'].'-'.$value['year'];
			$_REQUEST[$uid] = 1;
			$_REQUEST['name_'.$key] = $value['name'];
			$_REQUEST['email_'.$key] = $value['email'];
		}
		//System::debug($_REQUEST);
		$this->parse_layout('list',$this->map);
	}
}
?>