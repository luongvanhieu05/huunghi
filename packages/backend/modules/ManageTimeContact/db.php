<?php
class ManageTimeContactDB
{
	function get_account($cond=1){
		$sql = '
			select
				account.id
				,party.name_'.Portal::language().' as name
				,party.email
			from
				account
				INNER JOIN party ON account.id=party.user_id
			where
				account.is_active and account.type="USER" and '.$cond.'
			order by
				account.id
		';
		return DB::fetch_all($sql);
	}
	function get_items(){
		$sql = '
			SELECT
				received_contact.*
				,FROM_UNIXTIME(received_contact.time,"%d") as id
				,FROM_UNIXTIME(received_contact.time,"%d") as day
				,FROM_UNIXTIME(received_contact.time,"%m") as month
				,FROM_UNIXTIME(received_contact.time,"%Y") as year
			FROM
				received_contact
			WHERE
				FROM_UNIXTIME(received_contact.time,"%m")='.Url::get('month').' and FROM_UNIXTIME(received_contact.time,"%Y")='.Url::get('year').'
			ORDER BY
				received_contact.time
		';
		return DB::fetch_all($sql);
	}
	function get_user($cond=1){
		$sql = '
			select
				account.id
				,party.full_name as name
				,party.email
			from
				account
				INNER JOIN party ON account.id=party.user_id
			where
				account.is_active and account.type="USER" and '.$cond.'
			order by
				account.id
		';
		return DB::fetch($sql);
	}
	function get_item($cond){
		$sql = '
			SELECT
				received_contact.*
			FROM
				received_contact
			WHERE
				'.$cond.'
		';
		return DB::fetch($sql);
	}
}
?>