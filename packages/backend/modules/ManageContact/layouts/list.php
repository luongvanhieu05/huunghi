<fieldset id="toolbar">

	<legend>[[.manage_profile.]]</legend>

	<div id="toolbar-personal">[[.manage_contact.]]</div>

	<div id="toolbar-content" align="right">

	<table>

	  <tbody>

		<tr>

		  <?php if(User::can_view(false,ANY_CATEGORY)){?><td id="toolbar-help" align="center"><a href="<?php echo Url::build('help');?>#"> <span title="Help"> </span> [[.Help.]] </a> </td><?php }?>

		</tr>

	  </tbody>

	</table>

	</div>

</fieldset>

<br>

<fieldset id="toolbar">

<!--LIST:items-->

<div style="margin-top:5px;"></div>

<form name="ContactList" method="post" action="?<?php echo htmlentities($_SERVER['QUERY_STRING']);?>">

	<table width="98%" cellpadding="5" cellspacing="0">

		<tr>

			<td width="1%" valign="top">

				<img src="skins/default/images/cms/toolbar/icon-32-forward.png">

			</td>

			<td width="90%" valign="top" style="line-height:18px;;">

					<b>[[.full_name.]]:</b>&nbsp;<strong>[[|items.name|]]</strong> ~ <a >[[|items.portal_id|]]</a><br />

					<!--IF:cond([[=items.time=]])--><b>[[.date_send.]]</b> :&nbsp;<?php echo date('H:i d/m/Y',[[=items.time=]]);?> <br />

					<!--/IF:cond-->

					<!--IF:cond([[=items.phone=]])--><b>[[.phone.]]:</b>&nbsp;[[|items.phone|]] <br />

					<!--/IF:cond-->

					<!--IF:cond([[=items.address=]])--><b>[[.address.]]:</b> &nbsp;[[|items.address|]]<br>

					<!--/IF:cond-->

					<!--IF:cond([[=items.email=]])--><b>[[.email.]]:</b> <a href="mailto:[[|items.email|]]">&nbsp;[[|items.email|]]</a><br>

					<!--/IF:cond-->

					<!--IF:cond([[=items.content=]])--><b>Nội dung:</b> &nbsp;[[|items.content|]]</a><br>

					<!--/IF:cond-->

		  </td>

			<td valign="top" nowrap="nowrap" align="right">

				<!--IF:cond([[=items.is_check=]]==1)-->

					Đã xem

				<!--ELSE-->

				<span style="color:#FF0000">Chưa xem</span>

				<!--/IF:cond-->

				<a href="javascript:void(0)" onClick="change_display_status($('div_[[|items.id|]]'),this);">[[.detail.]]</a>&nbsp;|

				<a href="<?php echo URL::build_current(array('cmd'=>'delete'));?>&id=[[|items.id|]]" title="Delete"><img src="<?php echo Portal::template('');?>/images/buttons/delete.gif" /></a>

			</td>

		</tr>

		<tr>

			<td colspan="3" align="center">

				<div id="div_[[|items.id|]]" style="display:none;">

					<table cellpadding="4" cellspacing="0" width="100%" style="#width:99%;margin-top:8px;background-color:#FFFFFF" border="1" bordercolor="#E7E7E7" align="center">

						<tr>

							<td width="42%" align="left"><b>[[.content.]]</b></td>

							<td colspan="2" align="right">

							[[.is_check.]]&nbsp; <input  name="confirm_[[|items.id|]]"  type="checkbox" <?php if([[=items.is_check=]]==1){ echo 'checked="checked"';}?> value="1" onclick="location='<?php echo Url::build_current(array('cmd'=>'check','id'=>[[=items.id=]]));?>'">

							<!--[[.answer.]]&nbsp;<a><img src="skins/default/images/buttons/right.jpg">--></a>

						  </td>

						</tr>

						<tr>

							<td colspan="3" >[[|items.content|]]</td>

						</tr>

						<tr>

							<td  align="left"><b>[[.address.]]</b></td>

							<td colspan="2"  align="left"><b>[[.email.]]</b></td>

						</tr>

						<tr>

							<td  align="left">[[|items.address|]]</td>

							<td colspan="2"  >[[|items.email|]]</td>

							</tr>

						<tr>

							<td width="33%"  align="left"><b>[[.phone.]]</b></td>

							<td width="25%"  align="left"><b>[[.name_sender.]]</b></td>

						</tr>

						<tr>

							<td >[[|items.phone|]]</td>

							<td >[[|items.name|]]</td>

						</tr>

					</table>

				</div>

			</td>

		</tr>

	</table>

<!--/LIST:items-->

</form>

<table width="100%" cellpadding="6" cellspacing="0">

<tr>

	<td>[[|paging|]]&nbsp;</td>

</tr>

</table>

</fieldset>

<script language="javascript">

function change_display_status(obj, detail_div)

{

	if(obj.style.display=='none')

	{

		obj.style.display='';

		detail_div.innerHTML='[[.close.]]';

		<!--LIST:items-->

		if('div_[[|items.id|]]' != obj.id)

		{

			$('div_[[|items.id|]]').style.display = 'none';

		}

		<!--/LIST:items-->

	}

	else

	{

		obj.style.display='none';

		detail_div.innerHTML='[[.detail.]]';

	}

}

</script>