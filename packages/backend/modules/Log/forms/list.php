<?php
class ListLogForm extends Form
{
	function ListLogForm()
	{
		Form::Form('ListLogForm');
		$this->link_css('assets/default/css/cms.css');
	}
	function on_submit()
	{
		if(Url::get('cmd') == 'delete' and Url::get('selected_ids') and count(Url::get('selected_ids'))>0)
		{
			foreach(Url::get('selected_ids') as $key=>$value)
			{
				DB::delete_id('log',$value);
			}
		}
		Url::redirect_current();
	}
	function draw()
	{
		$cond = '';
		$total = LogDB::get_total_item($cond);
		require_once 'packages/core/includes/utils/paging.php';
		$item_per_page = 50;
		$paging = paging($total,$item_per_page,10);
		$items = LogDB::get_items($cond,$item_per_page);
		$this->parse_layout('list',array(
			'paging'=>$paging
			,'items'=>$items
			,'total'=>$total
		));
	}
}
?>