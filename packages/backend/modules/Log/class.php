<?php
/******************************
COPY RIGHT BY NYN PORTAL - TCV
WRITTEN BY thedeath
******************************/
class Log extends Module
{
	function Log($row)
	{
		Module::Module($row);
		require_once 'db.php';
		if(User::can_admin(false,ANY_CATEGORY))
		{
			require_once 'forms/list.php';
			$this->add_form(new ListLogForm());
		}
		else
		{
			URL::access_denied();
		}
	}
}
?>