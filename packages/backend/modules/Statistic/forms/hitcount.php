<?php
class HitcountForm extends Form
{
	function HitcountForm()
	{
		Form::Form('HitcountForm');
		$this->link_css('assets/default/css/cms.css');
	}
	function draw()
	{
		$cond = '1 and news.type="NEWS"';
		require_once 'packages/core/includes/utils/paging.php';
		$count = StatisticDB::GetTotal($cond);
		$item_per_page = 50;
		$paging = paging($count['acount'],$item_per_page,10,false,'page_no',array('cmd'));
		$items = StatisticDB::GetItems($cond,'news.hitcount DESC',$item_per_page);
		$i = 1;
		foreach($items as $key=>$value)
		{
			$value['indexs'] = $i++;
			$items[$key] = $value;
		}
		$this->parse_layout('hitcount',array(
			'items'=>$items,
			'paging'=>$paging,
			'total'=>$count['acount']
		));
	}
}
?>