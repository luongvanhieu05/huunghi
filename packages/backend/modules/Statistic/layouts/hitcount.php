<fieldset id="toolbar">
	<legend>[[.System_manage.]]</legend>
	<div id="toolbar-info">[[.top_of_daily_news.]]</div>
	<div id="toolbar-content" align="right">
	<table>
	  <tbody>
		<tr>
		  <?php if(User::can_view(false,ANY_CATEGORY)){?><td id="toolbar-help" align="center"><a href="<?php echo Url::build_current();?>#"> <span title="[[.Help.]]"> </span> [[.Help.]] </a> </td><?php }?>
		</tr>
	  </tbody>
	</table>
	</div>
</fieldset>
<div style="height:8px;"></div>
<fieldset id="toolbar">
<table  cellpadding="6" cellspacing="0" width="100%" style="#width:99%;margin-top:4px;" border="1" bordercolor="#E7E7E7" align="center">
  <tr style="background-color:#F0F0F0">
	<th width="3%" align="left"><a>#</a></th>
	<th align="left"><a>[[.title.]]</a></th>
	<th width="20%" align="left"><a>[[.category_name.]]</a></th>
	<th width="10%" align="left"><a>[[.hitcount.]]</a></th>
	<th width="5%" align="center"><a>[[.view.]]</a></th>
  </tr>
  <!--LIST:items-->
  <tr <?php Draw::hover(Portal::get_setting('crud_item_hover_bgcolor','#FFFFDD'));?> style="cursor:hand;<?php if([[=items.indexs=]]%2){echo 'background-color:#F9F9F9';}?>">
	<td>[[|items.indexs|]]</td>
	<td>[[|items.name|]]</td>
	<td>[[|items.category_name|]]</td>
	<td>[[|items.hitcount|]]</td>
	<td align="center"><a href="<?php echo Url::build('tin-tuc',array('name_id'=>[[=items.name_id=]]),REWRITE);?>"><img src="assets/default/images/buttons/search.gif" width="20"></a></td>
  </tr>
  <!--/LIST:items-->
 </table>
 <table width="100%" cellpadding="6" cellspacing="0" style="background-color:#F0F0F0;border:1px solid #E7E7E7;height:8px;#width:99%" align="center">
<tr>
	<td align="right" width="1%">&nbsp;</td>
	<td align="right">[[|paging|]]</td>
</tr>
</table>
<div style="#height:8px;"></div>
</fieldset>