<?php
/******************************
COPY RIGHT BY NYN PORTAL - TCV
WRITTEN BY DUCNM
******************************/
class ProductOrder extends Module
{
	function ProductOrder($row)
	{
		if(($selected_ids = URL::get('selected_ids')) and is_array($selected_ids) and sizeof($selected_ids)>0 and Url::get('confirm')=='delete')
		{
			Module::Module($row);
			require_once 'db.php';
			if(sizeof(URL::get('selected_ids'))>1 or 1)
			{
				require_once 'forms/delete_selected.php';
				$this->add_form(new DeleteSelectedProductOrderForm());
			}
			else
			{
				$ids = URL::get('selected_ids');
				$_REQUEST['id'] = $ids[0];
				require_once 'forms/delete.php';
				$this->add_form(new DeleteProductOrderForm());
			}
		}
		else
		if(
			(((URL::check(array('cmd'=>'delete'))and User::can_delete(false,ANY_CATEGORY))
				and Url::check('id') and DB::exists_id('transaction',$_REQUEST['id'])))
			or
			(URL::check(array('cmd'=>'add')))
			or !URL::check('cmd')
		)
		{
			Module::Module($row);
			require_once 'db.php';
			switch(URL::get('cmd'))
			{
			case 'delete':
				require_once 'forms/delete.php';
				$this->add_form(new DeleteProductOrderForm());break;
			case 'add':
				require_once 'forms/add.php';
				$this->add_form(new AddProductOrderForm());break;
			default:
				if(URL::check('id') and DB::exists_id('transaction',$_REQUEST['id']))
				{
					if(User::can_edit(false,ANY_CATEGORY))
					{
						require_once 'forms/detail.php';
						$this->add_form(new ProductOrderForm());
					}
					else
					{
						Url::redirect('default');
					}
				}
				else
				{
					if(User::can_edit(false,ANY_CATEGORY))
					{
						require_once 'forms/list.php';
						$this->add_form(new ListProductOrderForm());
					}
					else
					{
						Url::redirect('default');
					}
				}
				break;
			}
		}
		else
		{
			if(Url::get('action')=='finish')
			{
				require_once 'forms/finish.php';
				$this->add_form(new FinishProductOrderForm());
			}
			else
			{
				Url::redirect_current();
			}
		}
	}
}
?>