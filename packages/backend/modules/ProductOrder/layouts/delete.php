<div class="back_ground">
	<font class="title">[[.delete_title.]]</font> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>&raquo;&nbsp;&nbsp;<a style="color:#FFFFFF;font-size:12px" href="<?php echo URL::build('item_order');?>">[[.item_order_list.]]</a></strong><br>
	<br>
	<div class="body">
	<table cellspacing="0" width="100%" cellpadding="5"	 style="bitem_order:1px solid #0074C7;">
	<form name="DeleteProductOrderForm" method="post">
	<input type="hidden" name="id" value="[[|item_order_id|]]"><input type="hidden" name="cmd" value="delete">
	<tr>
	  <td colspan="9">&nbsp;</td>
	 </tr>
	<tr bgcolor="#9CBFF9" valign="top">
		<td bgcolor="#FFFFFF">
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
		  <td>&nbsp;</td>
		  <td><strong>[[.item_order_id.]]</strong></td>
		  <td>&nbsp;</td>
		  <td><strong>[[|item_order_id|]]</strong></td>
		  <td>&nbsp;</td>
		</tr>
		<tr>
		  <td width="19%">&nbsp;</td>
		  <td width="19%"><strong>[[.item_order_name.]]</strong></td>
		  <td width="3%">&nbsp;</td>
		  <td width="25%">[[|name|]]</td>
		  <td width="53%">&nbsp;</td>
		</tr>
		<tr>
		  <td>&nbsp;</td>
		  <td><strong>[[.reservation_date.]]</strong></td>
		  <td>&nbsp;</td>
		  <td>
		  	[[|reservation_date|]]</td>
		  <td>&nbsp;</td>
		  </tr>
		<tr>
		  <td>&nbsp;</td>
		  <td><strong>[[.total.]] ($)</strong></td>
		  <td>&nbsp;</td>
		  <td align="left">[[|total_amount|]]</td>
		  <td>&nbsp;</td>
		</tr>
		<tr>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
		  <td align="left">&nbsp;</td>
		  <td>&nbsp;</td>
		</tr>
		<tr>
		  <td>&nbsp;</td>
		  <td colspan="3" style="bitem_order-bottom:1px solid #000000;"><strong style="font-size:12px; color:#000066">[[.customer_info.]]</strong></td>
		  <td>&nbsp;</td>
		</tr>
		<tr>
		  <td>&nbsp;</td>
		  <td><strong>[[.full_name.]]</strong></td>
		  <td>&nbsp;</td>
		  <td align="left" style="padding-top:7px;">[[|bta_name|]]</td>
		  <td>&nbsp;</td>
		</tr>
		<tr>
		  <td>&nbsp;</td>
		  <td valign="top"><strong>[[.address.]]</strong></td>
		  <td>&nbsp;</td>
		  <td align="left"><span style="padding-top:7px;">[[|bta_address|]]</span></td>
		  <td>&nbsp;</td>
		</tr>
		<tr>
		  <td>&nbsp;</td>
		  <td><strong>[[.zip_code.]]</strong></td>
		  <td>&nbsp;</td>
		  <td align="left"><span style="padding-top:7px;">[[|bta_zip_code|]]</span></td>
		  <td>&nbsp;</td>
		</tr>
		<tr>
		  <td>&nbsp;</td>
		  <td><strong>[[.phone.]]</strong></td>
		  <td>&nbsp;</td>
		  <td align="left"><span style="padding-top:7px;">[[|bta_phone|]]</span></td>
		  <td>&nbsp;</td>
		</tr>
		<tr>
		  <td>&nbsp;</td>
		  <td><strong>[[.fax.]]</strong></td>
		  <td>&nbsp;</td>
		  <td align="left"><span style="padding-top:7px;">[[|bta_fax|]]</span></td>
		  <td>&nbsp;</td>
		</tr>
		<tr>
		  <td>&nbsp;</td>
		  <td><strong>[[.email.]]</strong></td>
		  <td>&nbsp;</td>
		  <td align="left"><span style="padding-top:7px;">[[|bta_email|]]</span></td>
		  <td>&nbsp;</td>
		</tr>
		<tr>
		  <td>&nbsp;</td>
		  <td><strong>[[.zone_id.]]</strong></td>
		  <td>&nbsp;</td>
		  <td align="left"><span style="padding-top:7px;">[[|bta_zone_id|]]</span></td>
		  <td>&nbsp;</td>
		</tr>
		<tr>
		  <td>&nbsp;</td>
		  <td nowrap="nowrap"><strong>[[.payment_terms_id.]]</strong></td>
		  <td>&nbsp;</td>
		  <td align="left"><span style="padding-top:7px;">[[|payment_terms_id|]]</span></td>
		  <td>&nbsp;</td>
		</tr>
		<tr>
		  <td valign="top">&nbsp;</td>
		  <td valign="top"><strong>[[.note.]]</strong></td>
		  <td>&nbsp;</td>
		  <td align="left"><span style="padding-top:7px;">[[|note|]]</span></td>
		  <td>&nbsp;</td>
		</tr>
		<tr>
		  <td>&nbsp;</td>
		  <td colspan="4">
		  	[[.confirm_question.]]<br>
		  	<?php Draw::button('  [[.Delete.]]  ',false,false,true,'DeleteProductOrderForm');?></td>
		</tr>
		</table>
</td>
</tr>
</form>
</table>
</div>
</div>