<link href="<?php echo Portal::template('transaction');?>/css/order.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
.style1 {color: #FF0000}
-->
    </style>
	<br>
<table width="100%" cellpadding="0" cellspacing="0" border="0" height="100%" bgcolor="#FFFFFF">
  <tr>
	<td style="padding:10px;">
	<div style="text-align:left;">
		<a class="dtab" href="<?php echo Url::build('product_cart',array('cmd'=>'cart'));?>">&nbsp;&nbsp;&nbsp;[[.cart.]]&nbsp;&nbsp;&nbsp;</a>
		<a class="dtab" href="<?php echo Url::build('product_cart',array('cmd'=>'confirm'));?>">&nbsp;&nbsp;&nbsp;[[.confirm.]]&nbsp;&nbsp;&nbsp;</a>
		<a class="dtab_hover" href="<?php echo Url::build('product_order',array('cmd'=>'add'));?>">&nbsp;&nbsp;&nbsp;[[.address_contact.]]&nbsp;&nbsp;&nbsp;</a>
	</div>
	<form name="AddProductOrderForm" method="post" >
	<div class="main">
	<table cellspacing="0" width="100%" cellpadding="5">
		<tr>
		  <td colspan="9">
			<?php if(Form::$current->is_error()){?>
			<strong>B&#225;o l&#7895;i</strong><br>
			<?php echo Form::$current->error_messages();?><br>
			<?php } ?></td>
		 </tr
		><tr bgcolor="#9CBFF9" valign="top">
			<td bgcolor="#FFFFFF">
			<table width="100%" cellpadding="2" cellspacing="0">
			<tr>
			  <td>&nbsp;</td>
			  <td colspan="3" align="center" nowrap="nowrap">[[.note_is_correct.]]</td>
			  <td>&nbsp;</td>
			  </tr>
			<tr>
			  <td width="19%">&nbsp;</td>
			  <td width="19%" nowrap="nowrap" align="left"><strong>[[.item_order_name.]] <span class="style1">*</span></strong></td>
			  <td width="3%">&nbsp;</td>
			  <td width="25%"><input name="name"  type="text" id="name" style="width:250px" value="<?php if(User::is_login()){ echo Portal::language('order_of').Session::get('user_id'); } ?>" ></td>
			  <td width="53%">&nbsp;</td>
			</tr>
			<tr>
			  <td>&nbsp;</td>
			  <td nowrap="nowrap" align="left"><strong>[[.total.]]</strong></td>
			  <td>&nbsp;</td>
			  <td align="left"><input name="total_amount" type="text" id="total_amount" style="width:250px; text-align:right" value="[[|total_amount|]]" readonly="readonly"></td>
			  <td>&nbsp;</td>
			</tr>
			<tr>
			  <td>&nbsp;</td>
			  <td nowrap="nowrap">&nbsp;</td>
			  <td>&nbsp;</td>
			  <td align="left">&nbsp;</td>
			  <td>&nbsp;</td>
			</tr>
			<tr>
			  <td>&nbsp;</td>
			  <td colspan="3" nowrap="nowrap" style="border-bottom:1px solid #000000;" align="left"><strong style="font-size:12px; color:#000066">[[.customer_info.]]</strong></td>
			  <td>&nbsp;</td>
			</tr>
			<tr>
			  <td>&nbsp;</td>
			  <td nowrap="nowrap" align="left"><strong>[[.full_name.]] <span class="style1">*</span></strong></td>
			  <td>&nbsp;</td>
			  <td align="left" style="padding-top:7px;"><input name="full_name" type="text" id="full_name" style="width:250px" value="<?php if(isset($_REQUEST['name_1'])){echo $_REQUEST['name_1'];}else{ echo '';}?>"></td>
			  <td>&nbsp;</td>
			</tr>
			<tr>
			  <td>&nbsp;</td>
			  <td valign="top" nowrap="nowrap" align="left"><strong>[[.address.]] <span class="style1">*</span></strong></td>
			  <td>&nbsp;</td>
			  <td align="left"><textarea name="address" id="address" style="width:250px" rows="3"></textarea></td>
			  <td>&nbsp;</td>
			</tr>
			<tr>
			  <td>&nbsp;</td>
			  <td nowrap="nowrap" align="left"><strong>[[.phone.]] <span class="style1">*</span> </strong></td>
			  <td>&nbsp;</td>
			  <td align="left"><input name="phone" type="text" id="phone" style="width:100"></td>
			  <td>&nbsp;</td>
			</tr>
			<tr>
			  <td>&nbsp;</td>
			  <td nowrap="nowrap" align="left"><strong>[[.fax.]]</strong></td>
			  <td>&nbsp;</td>
			  <td align="left"><input name="fax" type="text" id="fax" style="width:100"></td>
			  <td>&nbsp;</td>
			</tr>
			<tr>
			  <td>&nbsp;</td>
			  <td nowrap="nowrap" align="left"><strong>[[.email.]]</strong></td>
			  <td>&nbsp;</td>
			  <td align="left"><input name="email" type="text" id="email" style="width:200"></td>
			  <td>&nbsp;</td>
			</tr>
			<tr>
			  <td>&nbsp;</td>
			  <td nowrap="nowrap" align="left"><strong>[[.country.]] <span class="style1">*</span></strong></td>
			  <td>&nbsp;</td>
			  <td align="left"><select name="zone_id" id="zone_id" style="width:250px;">
				</select></td>
			  <td>&nbsp;</td>
			</tr>
			<tr>
			  <td valign="top">&nbsp;</td>
			  <td colspan="3" align="left" valign="top" style="border-bottom:1px solid #000000;padding-top:10px" ><strong>[[.note.]]</strong></td>
			  <td>&nbsp;</td>
			  </tr>
			<tr>
			  <td valign="top">&nbsp;</td>
			  <td valign="top" align="left">&nbsp;</td>
			  <td>&nbsp;</td>
			  <td align="left" style="padding-top:5px"><textarea name="note" id="note" style="width:250px" rows="5"></textarea></td>
			  <td>&nbsp;</td>
			</tr>
			<tr>
			  <td valign="top">&nbsp;</td>
			  <td valign="top" nowrap="nowrap">&nbsp;</td>
			  <td>&nbsp;</td>
			  <td align="left">&nbsp;</td>
			  <td>&nbsp;</td>
			  </tr>
			<tr>
				 <td colspan="5">
					<div id="button_back_ground" class="button_back_ground" >
						<div class="shopping_button">
							<a class="shopping_button" href="<?php echo Url::build('product');?>">[[.buy.]]</a>						</div>
						<div class="shopping_button">
							<a class="shopping_button" href="javascript:void(0)" onclick="window.history.go(-1);">[[.back.]]</a>						</div>
						<div class="shopping_button">
							<a class="shopping_button" href="javascript:void(0)" onclick="AddProductOrderForm.submit();">[[.finish.]]</a>						</div>
					</div>					</td>
			</tr>
			</table>
			  </td>
			</tr>
			</table>
		</div>
		</form>
	</td>
</tr>
</table>
<script>
	function clone_input(obj)
	{
		document.getElementById('sta_'+obj).value=document.getElementById(''+obj).value;
	}
</script>