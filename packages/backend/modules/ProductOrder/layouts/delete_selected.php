<?php System::set_page_title(Portal::get_setting('company_name','').' '.Portal::language('delete_title'));?>
<form method="post" name="delete_selected_form">
<table cellspacing="0" width="100%">
	<tr valign="top" bgcolor="#FFFFFF">
		<td align="left" colspan="2" bgcolor="#A0D3EE">
			<table width="100%" cellspacing="0">
				<tr><td nowrap width="100%">
					<font size="3" style="line-height:32px;vertical-align:middle;text-indent:36px;"><b>[[.delete_selected_confirm.]]</b></font>
				</td>
				<td>&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr bgcolor="#E2F0DF" valign="top">
		<td width="100%">
			<table bgcolor="#E2F0DF" cellspacing="0" width="100%">
			<tr>
				<td bgcolor="#B7DAB0"><div style="width:10px;">&nbsp;</div></td>
				<td width="100%" bgcolor="#E2F1DF">
					<form name="DeleteSelectedProductOrderForm" method="post" action="?<?php echo htmlentities($_SERVER['QUERY_STRING']);?>">
					<input type="hidden" name="confirm" value="1">
					<table width="100%" cellpadding="5" cellspacing="0" bitem_ordercolor="#8BB4A4" bitem_order="1" style="bitem_order-collapse:collapse">
						<tr valign="middle" bgcolor="#9ECA95">
							<th width="1%"><input type="checkbox" value="1" checked onclick="var checkboxes = document.getElementsByName('selected_ids[]');for(var i=0;i<checkboxes.length;i++) checkboxes[i].checked=this.checked;"></th>
							<th width="125" align="left" nowrap bgcolor="#5597F7" >
								<a href="<?php echo URL::build_current(((URL::get('item_order_by')=='transaction.name' and URL::get('item_order_dir')!='desc')?array('item_order_dir'=>'desc'):array())+array('item_order_by'=>'transaction.name'));?>" style="color:#EEEEEE;font-weight:700" title="[[.sort.]]">
								<?php if(URL::get('item_order_by')=='transaction.name') echo '<img src="'.Portal::template('portal').'/images/'.((URL::get('item_order_dir')!='desc')?'down':'up').'_arrow.gif" alt="">';?>
								[[.name.]]</a></th>
							<th width="142" align="center" nowrap="nowrap" bgcolor="#5597F7" >
								<a href="<?php echo URL::build_current(((URL::get('item_order_by')=='transaction.reservation_date' and URL::get('item_order_dir')!='desc')?array('item_order_dir'=>'desc'):array())+array('item_order_by'=>'transaction.reservation_date'));?>" style="color:#EEEEEE;font-weight:700" title="[[.sort.]]">
								<?php if(URL::get('item_order_by')=='transaction.reservation_date') echo '<img src="'.Portal::template('portal').'/images/'.((URL::get('item_order_dir')!='desc')?'down':'up').'_arrow.gif" alt="">';?>
								[[.reservation_date.]]</a></th>
							<th width="142" align="center" nowrap="nowrap" bgcolor="#5597F7" >
							<a href="<?php echo URL::build_current(((URL::get('item_order_by')=='transaction.time' and URL::get('item_order_dir')!='desc')?array('item_order_dir'=>'desc'):array())+array('item_order_by'=>'transaction.time'));?>" style="color:#EEEEEE;font-weight:700" title="[[.sort.]]">
								<?php if(URL::get('item_order_by')=='transaction.time') echo '<img src="'.Portal::template('portal').'/images/'.((URL::get('item_order_dir')!='desc')?'down':'up').'_arrow.gif" alt="">';?>
								[[.create_time.]]</a></th>
						</tr>
						<!--LIST:items-->
						<tr bgcolor="white" valign="middle" <?php Draw::hover('#E2F1DF');?> onclick="if(typeof(just_click)=='undefined' || !just_click){location='<?php echo URL::build_current();?>&id=[[|items.id|]]';}else{just_click=false;}" style="cursor:pointer;">
							<td><input name="selected_ids[]" type="checkbox" value="[[|items.id|]]" onclick="just_click=true;" checked></td>
							<td width="30%" align="left" nowrap bgcolor="<?php if((URL::get('just_edited_id',0)==[[=items.id=]]) or (is_numeric(array_search(MAP['items']['current']['id'],MAP['just_edited_ids'])))){ echo '#EFFFDF';} else {echo 'white';}?>" onclick="location='<?php echo URL::build_current();?>&id=[[|items.id|]]';">
									[[|items.name|]]						  </td>
							<td align="center" nowrap="nowrap" onclick="location='<?php echo URL::build_current();?>&amp;id=[[|items.id|]]';"> [[|items.reservation_date|]] </td>
							<td align="center" nowrap="nowrap" onclick="location='<?php echo URL::build_current();?>&amp;id=[[|items.id|]]';"> [[|items.time|]] </td>
						</tr>
						<!--/LIST:items-->
					</table>
				</td>
			</tr>
			</table>
			<p>
			<table><tr>
				<td><input type="submit" name="delete_selected" value="[[.delete_selected.]]" /></td>
				<td><input type="submit" name="list_order" value="[[.list.]]" /></td>
			</tr></table>
			</p>
			</form>
		</td>
	</tr>
</table>
</form>