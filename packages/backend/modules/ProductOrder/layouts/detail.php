<div class="back_ground" style="background-color:#FFFFFF;padding:10px;">
	<div style="padding:5px;">
	<span style="font-size:14px; font-weight:bold;">[[.item_order_detail.]]</span>
	<span><a style="font-size:12px; padding-left:40px;" href="<?php echo URL::build('order_admin',array(),REWRITE);?>">[[.view_item_order_list.]]</a></span>
	<span style="padding-left:40px;"><a href="javascript:void(0)" onclick="if(confirm('Ban co chac muon xoa don dat [[|name|]] ?')) {location='<?php echo URL::build_current(array('act'=>'delete'));?>&order_id=[[|id|]]';}">[[.delete_this_order.]]<img src="<?php echo Portal::template('duk');?>/images/delete.png" alt="[[.Delete.]]" width="12" height="12" border="0"></a></span>
	</div>
	<div class="body">
		<table cellspacing="0" width="100%" cellpadding="5" style="border:1px solid #CCCCCC;">
			<tr bgcolor="#9CBFF9" valign="top">
				<td bgcolor="#FFFFFF">
					<table width="100%" cellpadding="2" cellspacing="2">
						<tr>
							<td width="5%" rowspan="10" align="center" valign="top"></td>
							<td nowrap="nowrap"><strong>[[.item_order_id.]]</strong></td>
							<td>&nbsp;</td>
							<td nowrap="nowrap"><strong>[[|id|]]</strong></td>
						</tr>
						<tr>
							<td width="19%" nowrap="nowrap"><strong>[[.item_order_name.]]</strong></td>
							<td width="3%">&nbsp;</td>
							<td width="25%" nowrap="nowrap">[[|name|]]</td>
						</tr>
						<tr>
							<td nowrap="nowrap"><strong>[[.reservation_date.]]</strong></td>
							<td>&nbsp;</td>
							<td nowrap="nowrap"><b>[[|reservation_date|]]</b></td>
						</tr>
						<tr>
							<td nowrap="nowrap"><strong>[[.total.]] </strong></td>
							<td>&nbsp;</td>
							<td align="left" nowrap="nowrap"><b>[[|total_amount|]]&nbsp;[[|currency_id|]]</b></td>
						</tr>
						<tr>
							<td>[[.shipping_fee.]]</td>
							<td>&nbsp;</td>
							<td align="left">[[|shipping_fee|]]</td>
						</tr>
						<tr>
							<td nowrap="nowrap">[[.shipping_info.]]</td>
							<td>&nbsp;</td>
							<td nowrap="nowrap" align="left">[[|shipping_info|]]</td>
						</tr>
						<tr>
							<tr>
							<td nowrap="nowrap">[[.payment.]]</td>
							<td>&nbsp;</td>
							<td nowrap="nowrap" align="left"><b>[[|payment|]]</b></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td colspan="3" style="bitem_order-bottom:1px solid #000000;"><strong style="font-size:14px; color:#000066; font-weight:bold;">[[.customer_info.]]</strong></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td nowrap="nowrap"><strong>[[.full_name.]]</strong></td>
							<td>&nbsp;</td>
							<td align="left" style="padding-top:7px;" nowrap="nowrap"><b>[[|full_name|]]</b></td>
						</tr>
						<tr>
							<td width="5%">&nbsp;</td>
							<td valign="top"><strong>[[.address.]]</strong></td>
							<td>&nbsp;</td>
							<td align="left"><span style="padding-top:7px;"><b>[[|address|]]</b></span></td>
						</tr>
						<tr>
							<td width="5%">&nbsp;</td>
							<td><strong>[[.phone.]]</strong></td>
							<td>&nbsp;</td>
							<td align="left"><span style="padding-top:7px;"><b>[[|phone|]]</b></span></td>
						</tr>
						<tr>
							<td width="5%">&nbsp;</td>
							<td><strong>[[.email.]]</strong></td>
							<td>&nbsp;</td>
							<td align="left"><span style="padding-top:7px;"><a href="mailto:[[|email|]]" style="color:#FF3300">[[|email|]]</a></span></td>
						</tr>
						<tr>
							<td width="5%">&nbsp;</td>
							<td><strong>[[.city.]]</strong></td>
							<td>&nbsp;</td>
							<td align="left"><span style="padding-top:7px;">[[|zone|]]</span></td>
						</tr>
						<tr>
							<td width="5%" valign="top">&nbsp;</td>
							<td valign="top"><strong>[[.note_order.]]</strong></td>
							<td>&nbsp;</td>
							<td align="left"><span style="padding-top:7px;">[[|note|]]</span></td>
						</tr>
					</table>
				</td>
				<td width="53%" rowspan="15" align="left" valign="top" bgcolor="#EFEFEF" style="padding:10px; font-size:14px;">
					<h3>[[.product_in_order.]]</h3>
					<?php $i = 1;?>
					<ul style="list-style:none;">
					<!--LIST:item-->
					<li><?php echo $i++;?>. <a target="_blank" href="<?php echo URl::build('xem-san-pham',array('name_id'=>[[=item.name_id=]]),REWRITE); ?>">[[|item.item_name|]]</a>&nbsp;<strong>([[|item.quantity|]] x [[|item.price|]] - [[|item.currency_id|]])</strong></li>
					<!--/LIST:item-->
					</ul>
				</td>
			</tr>
		</table>
	</div>
</div>