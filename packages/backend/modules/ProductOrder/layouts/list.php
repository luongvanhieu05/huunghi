<script>
	item_order_array_items = {
		'length':'<?php echo sizeof(MAP['items']);?>'
		<!--LIST:items-->
		,'[[|items.i|]]':'[[|items.id|]]'
		<!--/LIST:items-->
	}
</script>
<form name="OrderListForm" method="post">
	<table cellpadding="6" cellspacing="0" width="100%" style="#width:99%;margin-top:8px;" border="1" bordercolor="#E7E7E7" align="center">
	<tr valign="top">
		<td width="100%" align="center">
		<table cellspacing="0" width="100%">
			<tr>
				<td>&nbsp;</td>
				<td width="100%">
				<fieldset id="toolbar">
					<legend>[[.manage_product.]]</legend>
					<div id="toolbar-title">[[.manage_product_order.]]</div>
					<div id="toolbar-content" align="right">
					<h4>[[.search.]]&nbsp;[[.name_order.]]&nbsp;
					  <input name="name" type="text" id="name" size="30" style="height:20px;font-size:14px;"><input type="submit" value="[[.ok.]]" class="medium_button"></h4>
					</div>
				</fieldset><br clear="all">
					<table width="100%" cellpadding="5" cellspacing="0" bordercolor="#CCCCCC" border="1" style="border-collapse:collapse">
						<tr style="background-color:#F0F0F0">
						  <th width="20"title="[[.check_all.]]"><input type="checkbox" value="1" id="OrderList_all_checkbox" onclick="select_all_checkbox(this.form,'OrderList',this.checked,'#FFFFEC','white');"<?php if(URL::get('cmd')=='delete') echo ' checked';?>></th>
							<th width="1%" align="left" nowrap>[[.no.]]</th>
							<th width="125" align="left" nowrap>
								<a href="<?php echo URL::build_current(((URL::get('order_by')=='transaction.name' and URL::get('order_dir')!='desc')?array('order_dir'=>'desc'):array())+array('order_by'=>'transaction.name'));?>" title="[[.sort.]]">
								<?php if(URL::get('order_by')=='transaction.name') echo '<img src="'.Portal::template('portal').'/images/buttons/'.((URL::get('order_dir')!='desc')?'down':'up').'_arrow.gif">';?>
								[[.name.]]</a></th>
							<th width="142" align="center" nowrap="nowrap">[[.country.]]</th>
							<th width="71" align="center" nowrap="nowrap">
							<a href="<?php echo URL::build_current(((URL::get('order_by')=='transaction.time' and URL::get('order_dir')!='desc')?array('order_dir'=>'desc'):array())+array('order_by'=>'transaction.time'));?>" title="[[.sort.]]">
								<?php if(URL::get('order_by')=='transaction.time') echo '<img src="'.Portal::template('portal').'/images/buttons/'.((URL::get('order_dir')!='desc')?'down':'up').'_arrow.gif">';?>
								[[.create_time.]]</a></th>
							<th width="71" align="center" nowrap="nowrap">[[.portal.]]</th>
							<th width="114" align="center" nowrap="nowrap">
							<a href="<?php echo URL::build_current(((URL::get('order_by')=='transaction.status' and URL::get('order_dir')!='desc')?array('order_dir'=>'desc'):array())+array('order_by'=>'transaction.status'));?>" title="[[.sort.]]">
								<?php if(URL::get('order_by')=='transaction.status') echo '<img src="'.Portal::template('portal').'/images/buttons/'.((URL::get('order_dir')!='desc')?'down':'up').'_arrow.gif">';?>
								[[.checked.]]</a>							</th>
							<?php if(User::can_edit())
							{
							?>
							<th width="20">[[.Delete.]]</th>
							<?php
							} ?>
						</tr>
						<?php $i=1;?>
						<!--LIST:items-->
						<tr bgcolor="<?php if((URL::get('just_edited_id',0)==[[=items.id=]]) or (is_numeric(array_search(MAP['items']['current']['id'],MAP['just_edited_ids'])))){ echo '#EFFFDF';} else {echo 'white';}?>" valign="middle" valign="middle" <?php Draw::hover(Portal::get_setting('crud_item_hover_bgcolor','#FFFFDD'));?> style="cursor:hand;" id="OrderList_tr_[[|items.id|]]">
							<td><input name="selected_ids[]" type="checkbox" value="[[|items.id|]]" onclick="select_checkbox(this.form,'OrderList',this,'#FFFFEC','white');" id="OrderList_checkbox" <?php if(URL::get('cmd')=='delete') echo 'checked';?>></td>
							<td width="1%" align="left" nowrap onclick="location='<?php echo URL::build_current();?>&id=[[|items.id|]]';"><?php echo $i++;?></td>
							<td width="30%" align="left" nowrap onclick="location='<?php echo URL::build_current();?>&id=[[|items.id|]]';">[[|items.name|]]</td>
							<td align="center" nowrap="nowrap" onclick="location='<?php echo URL::build_current();?>&amp;id=[[|items.id|]]';">[[|items.zone|]] </td>
							<td align="center" nowrap="nowrap" onclick="location='<?php echo URL::build_current();?>&amp;id=[[|items.id|]]';"> [[|items.time|]] </td>
							<td align="center" nowrap="nowrap" onclick="location='<?php echo URL::build_current();?>&amp;id=[[|items.id|]]';">[[|items.portal_id|]]</td>
							<td align="center" nowrap="nowrap" onclick="location='<?php echo URL::build_current();?>&amp;id=[[|items.id|]]';">[[|items.status|]]</td>
							<?php
							if(User::can_edit())
							{
							?>
							<td nowrap width="30">
								<a  href="javascript:if(confirm('Ban co chac muon xoa don dat [[|items.name|]] ?')) {location='<?php echo URL::build_current(array('act'=>'delete'));?>&order_id=[[|items.id|]]';}">
									<img src="<?php echo Portal::template('portal');?>/images/buttons/delete.gif" alt="[[.Delete.]]" width="12" height="12" border="0">								</a>
						<?php
							}
							?>						</tr>
						<!--/LIST:items-->
				  </table>
			  </td>
			</tr>
			</table>
			[[|paging|]]
			<li style="list-style:none;float:left;padding:2px 2px 2px 10px;font-weight:400;color:blue;cursor:hand;"><input type="button" onclick="OrderListForm.confirm.value='delete';OrderListForm.submit();" value="[[.Delete.]]"  style="width:70px;height:25px;text-align:center" class="medium_button"/></li>
		</td>
	</tr>
	</table>
<input type="hidden" name="confirm" value="1">
</form>
</div>	