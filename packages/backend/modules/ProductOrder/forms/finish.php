<?php
class FinishProductOrderForm extends Form
{
	function FinishProductOrderForm()
	{
		Form::Form('FinishProductOrderForm');
	}
	function draw()
	{
		?>
		<table width="100%" bitem_order="0" cellspacing="0" cellpadding="0" height="200" bgcolor="#FFFFFF">
		  <tr>
			<td align="center">
				<font style="color:#006699;font-size:16px;text-transform:uppercase;font-weight:bold;">
					[[.success_full.]]
				</font>
			</td>
		  </tr>
		  <tr>
		  		<td align="center">
					<font style="color:#FF6699;font-size:12px;text-transform:uppercase;font-weight:bold;">
						[[.continue.]]&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;[[.home.]]
					</font>
				</td>
		  </tr>
		</table>
		<?php
	}
}
?>