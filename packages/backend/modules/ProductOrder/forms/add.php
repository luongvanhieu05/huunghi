<?php
class AddProductOrderForm extends Form
{
	function AddProductOrderForm()
	{
		Form::Form('AddProductOrderForm');
		$this->add('name',new TextType(true,'name',0,255));
		$this->add('full_name',new TextType(true,'full_name',0,255));
		$this->add('email',new EmailType(false,'email'));
		$this->add('address',new TextType(true,'address',0,500));
		$this->add('phone',new TextType(true,'phone',0,100));
		$this->add('zone_id',new IDType(true,'zone_id','country'));
		$this->add('fax',new TextType(false,'fax',0,255));
		$this->add('note',new TextType(false,'invalid_note',0,200000));
	}
	function on_submit()
	{
		if($this->check())
		{
			$i='';
			if(Session::is_set('products'))
			{
				$products = Session::get('products');
				foreach($products as $key=>$value)
				{
					if($value['portal_id']<>$i)
					{
						$id=DB::insert('transaction',
										array(
										'name',
										'time'=>time(),
										'user_id'=>User::is_login()?Session::get('user_id'):'Guest',
										'full_name',
										'portal_id'=>$value['portal_id'],
										'address',
										'phone',
										'email',
										'type'=>'ORDER',
										'reservation_date'=>time(),
										'note',
										'total_amount'=>str_replace('.','',Url::get('total_amount')),
										'status'=>'UNCENSOR',
										'zone_id',
										)
									);
					}
					$i=$value['portal_id'];
					DB::insert('transaction_detail',array(
						'transaction_id'=>$id,
						'item_id'=>$key,
						'price'=>$value['product_price'],
						'currency_id'=>$value['currency_id'],
						'quantity'=>$value['product_quantity'],
					));
				}
				Session::delete('products');
				Session::delete('total_amount');
			}
			Url::redirect('product_cart',array('action'=>'finish'));
		}
	}
	function draw()
	{
		//tao So hieu hoa don
		if(DB::query('select id from `transaction` order by id desc limit 0,1')) {
			$vochr = DB::fetch();
			$vid = String::to_number($vochr['id'],3)+1;
		}
		else {
			$vid = 1;
		}
		$transaction_id = 'WOR';
		for($i=0;$i<4-strlen($vid);$i++) {
			$transaction_id .= '0';
		}
		$transaction_id .= $vid;
		$zone_id_list = String::get_list(DB::fetch_all('select id,structure_id,name_'.Portal::language().' as name from `zone` where '.IDStructure::direct_child_cond(ID_ROOT).' order by structure_id'));
		$total_amount=0;
		if(Session::is_set('total_amount'))
		{
			$total_amount=Session::get('total_amount');
		}
		$row['full_name'] ='';
		$row['address'] ='';
		$row['phone'] ='';
		$row['full_name'] ='';
		if(User::is_login())
		{
			$row = DB::select('party','user_id="'.Session::get('user_id').'"');
			foreach($row as $key=>$value)
			{
				if(is_string($key) and !isset($_REQUEST[$key]))
				{
					$_REQUEST[$key] = $value;
				}
			}
		}
		$this->parse_layout('add',$row+array(
			'zone_id_list'=>$zone_id_list,
			'zone_id'=>104,
			'transaction_id'=>$transaction_id,
			'current_date'=>date('d/m/Y',time()),
			'total_amount'=>($total_amount!='Contact')?number_format($total_amount,'.',',',''):'Contact'
		));
	}
}
?>