<?php
class DeleteSelectedProductOrderForm extends Form
{
	function DeleteSelectedProductOrderForm()
	{
		Form::Form("DeleteSelectedProductOrderForm");
		$this->add('confirm',new TextType(true,false,0, 20));
	}
	function on_submit()
	{
		if(Url::get('delete_selected'))
		{
			foreach(URL::get('selected_ids') as $id)
			{
				DB::delete('transaction','id="'.$id.'"');
				DB::delete('transaction_detail','transaction_id="'.$id.'"');
			}
		}
		if(Url::get('list_order'))	Url::redirect_current();
	}
	function draw()
	{
		DB::query('
			select
				*
			from
			 	`transaction`
			where `transaction`.id in ("'.join(URL::get('selected_ids'),'","').'")
		');
		$items = DB::fetch_all();
		$this->parse_layout('delete_selected',
			array(
				'items'=>$items
			)
		);
	}
}
?>