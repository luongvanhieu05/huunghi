<?php
class DeleteProductOrderForm extends Form
{
	function DeleteProductOrderForm()
	{
		Form::Form("DeleteProductOrderForm");
		$this->add('id',new IDType(true,'object_not_exists','item_order'));
	}
	function on_submit()
	{
		if($this->check())
		{
			DB::delete('item_order','id="'.Url::get('id').'"');
			DB::delete('item_order_product','item_order_id="'.Url::get('id').'"');
			Url::redirect_current();
		}
	}
	function draw()
	{
		/*DB::query('
			select
				`item_order`.id as item_order_id,`item_order`.time
				,`item_order`.reservation_date,`item_order`.total_amount
				,`item_order`.bta_email
				,`item_order`.`name`,`item_order`.`description` ,`item_order`.`bta_name`
				,`item_order`.`bta_address` ,`item_order`.`bta_zip_code`
				,`item_order`.`bta_phone` ,`item_order`.`bta_fax`	,`item_order`.`bta_zip_code`
				,`item_order`.`note`
				,`country`.`name` as bta_zone_id
				,`payment_type`.name_'.Portal::language().' as payment_terms_id
			from
			 	`item_order`
				left outer join `country` on `country`.id=`item_order`.bta_zone_id
				left outer join `payment_type` on `payment_type`.id=`item_order`.payment_terms_id
			where
				`item_order`.id = "'.URL::get('id').'"');
		if($row = DB::fetch())
		{
			$row['bta_zip_code'] = System::display_number($row['bta_zip_code']);
			$row['reservation_date'] = Date_Time::to_common_date($row['reservation_date']);
		}
		$this->parse_layout('delete',$row);*/
	}
}
?>