<?php
class ProductOrderForm extends Form
{
	function ProductOrderForm()
	{
		Form::Form("ProductOrderForm");
		$this->add('id',new IDType(true,'object_not_exists','transaction'));
		DB::update_id('transaction',array('status'=>'VIOLATE'),intval(Url::sget('id')));
	}
	function draw()
	{
		$this->map = array();
		$id = intval(Url::get('id'));
		$this->map = ProductOrderDB::transaction($id);
		$this->map['item'] = ProductOrderDB::item($id);
		$this->parse_layout('detail',$this->map);
	}
}
?>