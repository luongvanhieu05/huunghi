<?php
class ListProductOrderForm extends Form
{
	function ListProductOrderForm()
	{
		Form::Form('ListProductOrderForm');
		$this->link_css('assets/default/css/cms.css');
		if(Url::check(array('act'=>'delete')))
		{
			DB::delete('transaction','id="'.intval(Url::sget('order_id')).'"');
			DB::delete('transaction_detail','transaction_id="'.intval(Url::sget('order_id')).'"');
			//Url::redirect_current();
		}
		if(Url::get('confirm') == 'delete' and URL::get('selected_ids'))
		{
			foreach(URL::get('selected_ids') as $id)
			{
				DB::delete('transaction','id="'.$id.'"');
				DB::delete('transaction_detail','transaction_id="'.$id.'"');
			}
			Url::redirect_current();
		}
	}
	function draw()
	{
		$cond = '1 '
		.(Url::get('name')?' and transaction.name like "%'.Url::sget('name').'%"':'')
		.((Url::get('selected_ids') and is_array(Url::get('selected_ids')))?' and transaction.id in ('.implode(',',Url::get('selected_ids')).')':'');
		$cond.=' and portal_id="'.PORTAL_ID.'" and transaction.type="ORDER"';
		$item_per_page = 20;
		DB::query('
			select count(*) as acount
			from
				`transaction`
			where '.$cond.'
			limit 0,1
		');
		$count = DB::fetch();
		require_once 'packages/core/includes/utils/paging.php';
		$paging = paging($count['acount'],$item_per_page);
		DB::query('
			select
				transaction.*,zone.name_'.Portal::language().' as zone
			from
				`transaction`
				inner join zone on zone.id = transaction.zone_id
			where '.$cond.'
			'.(URL::get('order_by')?'order by '.URL::get('order_by').(URL::get('order_dir')?' '.URL::get('order_dir'):''):' order by time desc').'
			limit '.((page_no()-1)*$item_per_page).','.$item_per_page.'
		');
		$items = DB::fetch_all();
		foreach($items as $key=>$item)
		{
			  $items[$key]['time']=date('H:i\' - d/m/Y',$item['time']);
			  if($item['status']=='VIOLATE')
			  {
				$items[$key]['status']='OK';
			  }
			  else
			  {
					$items[$key]['status']='<img src="'.Portal::template('portal').'/images/cmd_Tim.gif" />';
			  }
		}
		$i=1;
		foreach ($items as $key=>$value)
		{
			$items[$key]['i']=$i++;
		}
		$just_edited_id['just_edited_ids'] = array();
		/*if (UrL::get('selected_ids'))
		{
			if (strstr(UrL::get('selected_ids'),','))
			{
				$just_edited_id['just_edited_ids']=explode(',',UrL::get('selected_ids'));
			}
			else
			{
				$just_edited_id['just_edited_ids']=array('0'=>UrL::get('selected_ids'));
			}
		}*/
		$this->parse_layout('list',$just_edited_id+
			array(
				'items'=>$items,
				'paging'=>$paging
			)
		);
	}
}
?>