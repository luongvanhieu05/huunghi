<?php
class ProductOrderDB{
	static function transaction($id){
		return DB::fetch('
			SELECT
				transaction.*,zone.name_'.Portal::language().' as zone,transaction_detail.price
			FROM
				transaction
				inner join zone on zone.id = transaction.zone_id
				left outer join transaction_detail on transaction.id = transaction_detail.transaction_id
			WHERE
				transaction.id = '.$id.' and transaction.portal_id = "'.PORTAL_ID.'"
		');
	}
	static function item($id){
		return DB::fetch_all('
			SELECT
				transaction_detail.*,product.name_'.Portal::language().' as item_name,product.category_id,
				product.name_id
			FROM
				transaction_detail
				inner join product on product.id = transaction_detail.item_id
			WHERE
				transaction_detail.transaction_id = '.$id.'
		');
	}
}
?>