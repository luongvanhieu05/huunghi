<link rel="stylesheet" href="assets/default/css/global.css" type="text/css" />
<script src="packages/core/includes/js/common.js" type="text/javascript"></script>
<link rel="stylesheet" href="assets/admin/scripts/bootstrap_datepicker/css/bootstrap-datepicker.min.css" type="text/css" />
<script src="assets/admin/scripts/bootstrap_datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="assets/admin/scripts/bootstrap_datepicker/locales/bootstrap-datepicker.vi.min.js" charset="UTF-8"></script>
<div class="admin-menu">
  <div class="navbar navbar-default" role="navigation">
    <div class="container">
      <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a>
      </div>
    <div class="navbar-collapse collapse" id="bs-example-navbar-collapse-1">
      <!--IF:cond([[=categories=]])-->
      <ul class="nav navbar-nav">
      <!--LIST:categories-->
        <li>
          <a href="#" data-toggle="dropdown" class="dropdown-toggle">
          <?php echo Portal::language()==1?[[=categories.name_1=]]:[[=categories.name_2=]]; ?>
          <!--IF:cond2([[=categories.child=]])--><b class="caret"></b><!--/IF:cond2-->
          </a>
            <!--IF:cond2([[=categories.child=]])-->
            <ul class="dropdown-menu">         
              <?php $group = false;?>
              <!--LIST:categories.child-->
                <?php	
                    if(![[=categories.child.url=]]) [[=categories.child.url=]] = '';
                ?>
                  <!--IF:cond5([[=categories.child.child=]])-->
                  <li class="menu-item dropdown dropdown-submenu">
                  <a data-toggle="dropdown" role="menu">
                  <?php echo Portal::language()==1?[[=categories.child.name_1=]]:[[=categories.child.name_2=]]; ?>
                  </a>
                  <ul class="dropdown-menu">
                    <?php $sub_group = false; ?>
                    <!--LIST:categories.child.child-->
                    <?php	if(isset([[=categories.child.child.group_name=]]) and $sub_group != [[=categories.child.child.group_name=]])
                        {
                          if($sub_group!="") echo '<li></li>';
                          $sub_group = [[=categories.child.child.group_name=]];
                        }
                    ?>
                    <li>
                    <a href="[[|categories.child.child.url|]]">
                      <?php echo Portal::language()==1?[[=categories.child.child.name_1=]]:[[=categories.child.child.name_2=]]; ?>									
                    </a></li>
                    <!--/LIST:categories.child.child-->
                  </ul>
                  </li>
                  <!--ELSE-->
                  <li>
                  <a href="[[|categories.child.url|]]">
                  <?php echo Portal::language()==1?[[=categories.child.name_1=]]:[[=categories.child.name_2=]]; ?>
                  </a>
                 </li> 
                  <!--/IF:cond5-->						
              <!--/LIST:categories.child-->
            </ul>            
            <!--/IF:cond2-->
        </li>		
      <!--/LIST:categories-->
      </ul>
      <!--/IF:cond-->
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" data-toggle="dropdown" class="dropdown-toggle"><?php echo Session::get('user_id'); ?> <b class="caret"></b></a>
          <ul class="dropdown-menu">
              <li><a href="trang-ca-nhan.html">Trang cá nhân</a></li>
              <li class="divider"></li>
              <li><a href="<?php echo Url::build('sign_out',array()); ?>">[[.sign_out.]]</a></li>          
          </ul>
        </li>
      </ul>
    </div>
   </div>
  </div>
</div>