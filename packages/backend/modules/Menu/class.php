<?php
class Menu extends Module{
	function Menu($row){
		if(User::is_login()){
			Module::Module($row);
			require_once 'db.php';
			$_SESSION['language_id'] = 1;
			if(User::can_edit(MODULE_PRODUCTADMIN,ANY_CATEGORY)){
				require_once 'forms/admin_menu.php';
				$this->add_form(new MenuForm());
			}else{
				require_once 'forms/user_menu.php';
				$this->add_form(new UserMenuForm());
			}
		}
		else
		{
			Url::redirect('dang-nhap',array('href'=>'?page='.Url::get('page').''.(Url::get('id')?'&id='.Url::get('id'):'').''));
		}
	}
}
?>
