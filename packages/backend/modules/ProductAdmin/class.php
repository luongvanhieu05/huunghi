<?php
class ProductAdmin extends Module{
	function ProductAdmin($row){
		Module::Module($row);
		require_once 'db.php';
		if(User::can_view(false,ANY_CATEGORY)){
			switch(Url::get('cmd')){
				case 'convert':
					$this->convert();
					break;
				case 'add':
					$this->add_cmd();
					break;
				case 'edit':
					$this->edit_cmd();
					break;
				case 'unlink':
					$this->delete_file();
					break;
				case 'copy':
					$this->copy_items();
					break;
				case 'move':
					$this->copy_items();
					break;
				default:
					$this->list_cmd();
					break;
			}
		}else{
			//Url::access_denied();
		}
	}
	function copy_items(){
		if(User::can_edit(false,ANY_CATEGORY) and isset($_REQUEST['selected_ids']) and count($_REQUEST['selected_ids'])>0){
			require_once 'forms/copy.php';
			$this->add_form(new CopyProductAdminForm());
		}else{
			Url::redirect_current(array('cmd'=>'list'));
		}
	}
	function delete_file(){
		if(Url::get('link') and file_exists(Url::get('link')) and User::can_delete(false,ANY_CATEGORY)){
			@unlink(Url::get('link'));
		}
		echo '<script>window.close();</script>';
	}
	function list_cmd(){
		require_once 'forms/list.php';
		$this->add_form(new ListProductAdminForm());
	}
	function add_cmd(){
		if(User::can_add(false,ANY_CATEGORY)){
			require_once 'forms/edit.php';
			$this->add_form(new EditProductAdminForm());
		}else{
			Url::access_denied();
		}
	}
	function edit_cmd(){
		if(User::can_edit(false,ANY_CATEGORY)){
			require_once 'forms/edit.php';
			$this->add_form(new EditProductAdminForm());
		}else{
			Url::access_denied();
		}
	}
	function convert($parent_id=false){
		//echo ID_ROOT;die;
		mysql_query("SET NAMES UTF8");
		//header("Content-Type: text/html; charset=utf-8");
		require_once 'packages/core/includes/system/si_database.php';
		require_once 'packages/core/includes/utils/vn_code.php';
		require_once 'packages/core/includes/utils/search.php';
		$sql = '
			select
				opd.product_id as id,opd.*,op.*,ops.price as publish_price,ops.date_start,ops.date_end
			from
				oc_product_description as opd
				inner join oc_product AS op ON op.product_id = opd.product_id
				left outer join oc_product_special as ops ON ops.product_id = op.product_id
			where
				1=1
			order by
				op.product_id
		';
		$old_products = DB::fetch_all($sql);
		$i=0;
		foreach($old_products as $key=>$val){
			$utf8 = $val['name']; // file must be UTF-8 encoded
			$name = $utf8;
			$name_id = convert_utf8_to_url_rewrite($name);
			$arr = array('id'=>$key,
				'name_1'=>$name,
				'description_1'=>preg_replace("/data-sheets-value=\"([^\"]+)\"/",'',preg_replace("/data-sheets-userformat=\"([^\"]+)\"/",'',html_entity_decode($val['description']))),
				'name_id'=>$name_id,
				'meta_description'=>preg_replace("/data-sheets-value=\"([^\"]+)\"/",'',preg_replace("/data-sheets-userformat=\"([^\"]+)\"/",'',html_entity_decode($val['meta_description']))),
				'keywords'=>($val['meta_keyword']),
				'tags'=>($val['tag']),
				'code'=>$val['model'],
				'quantity'=>$val['quantity'],
				'image_url'=>'image/'.$val['image'],
				'small_thumb_url'=>'image/'.$val['image'],
				'shipping'=>$val['shipping'],
				'price'=>$val['publish_price']*1000?$val['publish_price']*1000:$val['price']*1000,
				'publish_price'=>$val['price']*1000,
				'open_promotion_time'=>$val['date_start'],
				'close_promotion_time'=>$val['date_end'],
				'weight'=>$val['weight'],
				'minimum'=>$val['minimum'],
				'position'=>$val['sort_order'],	
				'status'=>$val['status']?'SHOW':'HIDE',	
				'hitcount'=>$val['viewed'],		
				'time'=>strtotime($val['date_added']),
				'last_time_update'=>strtotime($val['date_modified'])
			);
			if(!($product_id = DB::fetch('select id from product where id = '.$key.'','id'))){
				$product_id = DB::insert('product',$arr);
			}
			//$this->convert_product_category($product_id);
			//$this->convert_product_image($product_id);
			$i++;
		}
		echo $i;
	}
	function convert_product_category($product_id){
		$sql = '
		select
			opc.*,concat(opc.product_id,"-",opc.category_id) as id
		from
			oc_product_to_category as opc
		where
			opc.product_id = '.$product_id.'
		';
		$items = DB::fetch_all($sql);
		foreach($items as $key=>$val){
			if(!DB::exists('select id from product_category where product_id = '.$product_id.' and category_id = '.$val['category_id'].'')){
				DB::insert('product_category',array('product_id'=>$product_id,'category_id'=>$val['category_id']));
			}
		}
	}
	function convert_product_image($product_id){
		$sql = '
		select
			opi.*,product_image_id as id
		from
			oc_product_image as opi
		where
			opi.product_id = '.$product_id.'
		';
		$items = DB::fetch_all($sql);
		foreach($items as $key=>$val){
			if(!DB::exists('select id from product_image where product_id = '.$product_id.' LIKE image_url = "'.$val['image'].'"')){
				DB::insert('product_image',array('product_id'=>$product_id
					,'image_url'=>'image/'.$val['image']
					,'thumb_url'=>'image/'.$val['image']
					,'small_thumb_url'=>'image/'.$val['image']
				));
			}
		}
	}
}
?>
