<?php
class ListProductAdminForm extends Form
{
	function ListProductAdminForm()
	{
		Form::Form('ListProductAdminForm');
		$this->link_css('assets/default/css/cms.css');
	}
	function save_position()
	{
		foreach($_REQUEST as $key=>$value)
		{
			if(preg_match('/position_([0-9]+)/',$key,$match) and isset($match[1]))
			{
				DB::update_id('product',array('position'=>Url::get('position_'.$match[1])),$match[1]);
			}
		}
		Url::redirect_current();
	}
	function delete()
	{
		if(isset($_REQUEST['selected_ids']) and count($_REQUEST['selected_ids'])>0)
		{
			foreach($_REQUEST['selected_ids'] as $key)
			{
				if($item = DB::exists_id('product',$key))
				{
					save_recycle_bin('product',$item);
					DB::delete_id('product',intval($key));
					@unlink($item['image_url']);
					@unlink($item['small_thumb_url']);
					save_log($key);
				}
			}
		}
		Url::redirect_current();
	}
	function on_submit()
	{
		switch(Url::get('cmd'))
		{
			case 'update_position':
				$this->save_position();
				break;
			case 'delete':
				$this->delete();
				break;
		}
	}
	function draw()
	{
		/*$products = DB::select_all('product');
		$i=0;
		foreach($products as $key=>$val){
			$desc = preg_replace("/data-sheets-value=\"([^\"]+)\"/",'',preg_replace("/data-sheets-userformat=\"([^\"]+)\"/",'',html_entity_decode($val['description_1'])));
			DB::update('product',array('description_1'=>$desc),'id='.$key);
		}
		die;
		/*$str = '<p><span data-sheets-userformat="[null,null,513,[null,0],null,null,null,null,null,null,null,null,0]" data-sheets-value="[null,2,&quot;\u00c1o kho\u00e1c d\u1ea1 x\u00f9 8822-1&quot;]" style="font-size:13px;font-family:Arial;">Áo khoác dạ xù 8822-2</span></p> <p><span style="line-height: 20.8px;">- Màu sắc: </span><span data-sheets-userformat="[null,null,513,[null,0],null,null,null,null,null,null,null,null,0]" data-sheets-value="[null,2,&quot;v\u00e0ng&quot;]" style="font-size:13px;font-family:Arial;">vàng</span></p> <p>- Chất liệu:&nbsp;<span data-sheets-userformat="[null,null,513,[null,0],null,null,null,null,null,null,null,null,0]" data-sheets-value="[null,2,&quot;len d\u00e0y d\u1eb7n&quot;]" style="font-size:13px;font-family:Arial;">len dày dặn</span></p> <p><span style="line-height: 20.8px;">- Size:&nbsp;</span><span data-sheets-userformat="[null,null,513,[null,0],null,null,null,null,null,null,null,null,0]" data-sheets-value="[null,2,&quot;Free size&quot;]" style="font-size:13px;font-family:Arial;">Free size</span></p> <p>- Kiểu dáng: <span data-sheets-userformat="[null,null,513,[null,0],null,null,null,null,null,null,null,null,0]" data-sheets-value="[null,2,&quot;d\u00e1ng d\u00e0i, c\u1ed5 l\u00f4ng, m\u1eb7c c\u1ef1c sang&quot;]" style="font-size:13px;font-family:Arial;">dáng dài, mặc cực sang</span></p> <p><img alt="" src="http://hangjeans.com/image/data/AO_NU/AoKhoacNu/25-12-2015/Apkhoacdanu 8822-L (6).jpg" style="width: 870px; height: 1100px;" /><img alt="" src="http://hangjeans.com/image/data/AO_NU/AoKhoacNu/25-12-2015/Apkhoacdanu 8822-L (5).jpg" style="width: 870px; height: 1100px;" /><img alt="" src="http://hangjeans.com/image/data/AO_NU/AoKhoacNu/25-12-2015/Apkhoacdanu 8822-L (7).jpg" style="width: 870px; height: 1100px;" /><img alt="" src="http://hangjeans.com/image/data/AO_NU/AoKhoacNu/25-12-2015/Apkhoacdanu 8822-L (8).jpg" style="width: 870px; height: 1100px;" /><img alt="" src="http://hangjeans.com/image/data/AO_NU/AoKhoacNu/25-12-2015/Apkhoacdanu 8822-L (9).jpg" style="width: 870px; height: 1100px;" /></p>';
		echo preg_replace("/data-sheets-value=\"([^\"]+)\"/",'',preg_replace("/data-sheets-userformat=\"([^\"]+)\"/",'',$str));
		die;*/
		$cond = '1';
		$cond.=$this->get_condition();
		$this->get_just_edited_id();
		require_once 'packages/core/includes/utils/paging.php';
		require_once 'cache/config/status.php';
		$item_per_page = Url::get('item_per_page',20);
		$total = ProductAdminDB::get_total_item($cond);
		$paging = paging($total,$item_per_page,5,false,'page_no',array('cmd','type','category_id','status'));
		//echo $cond;
		$items = ProductAdminDB::get_items($cond,'product.position DESC ,product.id DESC',$item_per_page);
		$item_per_page_list = array(20=>20,30=>30,50=>50,100=>100);
		$this->parse_layout('list',$this->just_edited_id+array(
			'items'=>$items,
			'paging'=>$paging,
			'total'=>$total,
			'category_id_list'=>array(Portal::language('select_category'))+String::get_list(ProductAdminDB::get_category()),
			'status_list'=>array(Portal::language('select_status'))+$status,
			'user_id_list'=>array(Portal::language('select_user'))+String::get_list(ProductAdminDB::get_user()),
			'item_per_page_list'=>$item_per_page_list
		));
	}
	function get_just_edited_id()
	{
		$this->just_edited_id['just_edited_ids'] = array();
		if (UrL::get('selected_ids'))
		{
			if(is_string(UrL::get('selected_ids')))
			{
				if (strstr(UrL::get('selected_ids'),','))
				{
					$this->just_edited_id['just_edited_ids']=explode(',',UrL::get('selected_ids'));
				}
				else
				{
					$this->just_edited_id['just_edited_ids']=array('0'=>UrL::get('selected_ids'));
				}
			}
		}
	}
	function get_condition()
	{
		$cond = '';
		if(Url::get('category_id') and DB::exists_id('category',intval(Url::sget('category_id'))))
		{
			$cond.= ' and '.IDStructure::child_cond(DB::structure_id('category',intval(Url::sget('category_id'))));
		}
		if(Url::get('user_id'))
		{
			$cond.= ' and product.user_id="'.Url::get('user_id').'"';
		}
		if(Url::get('status'))
		{
			$cond.= ' and product.status="'.Url::get('status').'"';
		}
		if(Url::get('search'))
		{
			$cond .= URL::get('search')? ' AND (product.code like "%'.addslashes(URL::sget('search')).'%" or (product.name_1) LIKE "%'.addslashes(URL::sget('search')).'%")':'';
		}
		return $cond;
	}
}
?>
