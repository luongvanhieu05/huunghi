<?php

class EditProductAdminForm extends Form{

	function EditProductAdminForm(){

		Form::Form('EditProductAdminForm');

		$languages = DB::select_all('language');

		$this->add('name_1',new TextType(true,'invalid_name_1',0,225));

		$this->link_css('assets/default/css/cms.css');

		//$this->link_css('assets/default/css/tabs/tabpane.css');

		//$this->link_js('assets/default/css/tabs/tabpane.js');

		$this->link_js('packages/core/includes/js/jquery/jquery.MultiFile.js');

		//$this->link_js('assets/standard/js/datetimepicker.js');

		//$this->link_css('assets/standard/css/datetimepicker.css');

	}

	function save_item(){



		$rows = array();

		$languages = DB::select_all('language');

		foreach($languages as $language){

			$rows += array('name_'.$language['id']=>Url::get('name_'.$language['id'],1));

			$rows += array('brief_'.$language['id']=>Url::get('brief_'.$language['id'],1));

			$rows += array('description_1'=>Url::get('description_1'));
			$rows += array('description_2'=>Url::get('description_2'));
			$rows += array('type_1'=>Url::get('type_1'));
			$rows += array('type_2'=>Url::get('type_2'));

			$rows += array('technical_info_'.$language['id']=>Url::get('technical_info_'.$language['id'],1));

		}

		require_once 'packages/core/includes/utils/vn_code.php';

		require_once 'packages/core/includes/utils/search.php';

		//$rows['keywords']=extend_search_keywords($rows['name_1']);		

		$rows += array(

		'status'

		,'code'

		,'price'=>System::calculate_number(Url::get('price'))

		,'publish_price'=>System::calculate_number(Url::get('publish_price'))

		,'currency_id'

		,'hitcount'

		,'rating'

		,'tags'

		,'user_id'=>Session::get('user_id')

		,'portal_id'=>PORTAL_ID

		,'related_ids'

		,'open_promotion_time'

		,'close_promotion_time'

		,'keywords'

		,'valuation'=>System::calculate_number(Url::get('valuation'))

		,'quantity'=>System::calculate_number(Url::get('quantity'))

		,'commentadmin'=>(Url::get('commentadmin'))

		);



		//if(Url::get('sex'))

		{

			//$rows['position'] = Url::get('sex');

		}



		if(Url::get('position')==''){

			$position = DB::fetch('select max(position)+1 as id from product');

			$rows['position'] = $position['id'];

		}else{

			$rows['position'] = Url::get('position');

		}

		$name_id = convert_utf8_to_url_rewrite($rows['name_1']);

		if(!DB::fetch('select name_id from product where name_id="'.$name_id.'" and portal_id="'.PORTAL_ID.'"')){

			$rows+=array('name_id'=>$name_id);

		}else{

			if(Url::get('id') and Url::get('cmd')=='edit'){

				$rows+=array('name_id'=>$name_id);

			}else{

				$this->error('name','duplicate_name');

			}

		}

		return ($rows);

	}

	function save_image($file,$id){

		require_once 'packages/core/includes/utils/upload_file.php';

		$dir = substr(PORTAL_ID,1).'/content/product_catalog';
		update_upload_file('small_thumb_url',$dir);
		//update_upload_file('image_url',$dir);
		update_upload_file('banve',$dir);
		//update_upload_file('cataloge',$dir);
		update_upload_file('video',$dir);
		//img 2
		
		update_upload_file('small_thumb_url_2',$dir);
		

		$row = array();

		//////////////////////////Update catalog//////////////////////////

		$catalogs = array();

		if(!($catalog = multi_upload_file('cataloge',$dir,'FILE'))){

			$catalog = array();

		}

		if($old = Url::get('old_cataloge')){

			$catalog = array_merge($catalog,$old);

		}

		foreach($catalog as $key=>$value){

			$catalogs[$key+1] = $value;

		}

		$row += array('cataloge'=>var_export($catalogs,true).';');

		///////////////////////////////////////////////////////



		if(Url::get('small_thumb_url')!=''){

			$row['small_thumb_url'] =Url::get('small_thumb_url');

		}
		if(Url::get('small_thumb_url_2')!=''){

			$row['small_thumb_url_2'] =Url::get('small_thumb_url_2');

		}

		if(Url::get('video')!=''){

			$row['video'] =Url::get('video');

		}

		if(Url::get('banve')!=''){

			$row['banve'] =Url::get('banve');

		}

		DB::update_id('product',$row,$id);

	}

	function on_submit(){

		if($this->check()){

			$rows = $this->save_item();

			if(!$this->is_error()){

				$rows += array('time'=>Date_Time::to_time(Url::get('time')));

				if(Url::get('cmd')=='edit' and $item = DB::exists_id('product',Url::get('id'))){

					$id = intval(Url::get('id'));

					$rows += array('last_time_update'=>time());

					DB::update_id('product',$rows,$id);

				}else{

					$id = DB::insert('product',$rows);

				}

				$this->update_product_price($id);

				$this->save_image($_FILES,$id);

				if((Url::get('edit_category') or Url::get('cmd')=='add') and isset($_REQUEST['category_id']) and !empty($_REQUEST['category_id'])){

					$this->update_category($id,$_REQUEST['category_id']);

				}

				if(isset($_REQUEST['color_id']) and !empty($_REQUEST['color_id'])){

					$this->update_color($id,$_REQUEST['color_id']);

				}

				if(isset($_REQUEST['size_id']) and !empty($_REQUEST['size_id'])){

					$this->update_size($id,$_REQUEST['size_id']);

				}



				//lưu thuộc tính (phong cach,dáng,loại cạp) SP

				//$this->update_thuoc_tinh($id,Url::get('phong_cach_id'));

				//$this->update_thuoc_tinh($id,Url::get('cap_id'));

				//$this->update_thuoc_tinh($id,Url::get('dang_id'));



				if($id){

					echo '<script>if(confirm("Bạn tiếp tục thêm sản phẩm tiếp theo?")){location="'.Url::build_current(array('cmd'=>'add')).'";}else{location="'.Url::build_current(array('cmd'=>'edit','id'=>$id)).'";}</script>';

				}

				//exit();

			}

		}

	}

	function draw(){

		$this->map = array();

		require_once 'cache/config/status.php';

		require_once 'cache/tables/currency.cache.php';

		$languages = DB::select_all('language');

		$arr = array('1'=>'YES','0'=>'NO');

		$images = array();

		$catalogs = array();

		$this->map['categories'] = 'N/A';

		$this->map['colors'] = 'N/A';

		$this->map['sizes'] = 'N/A';

		if(Url::get('cmd')=='edit' and Url::get('id') and $product = DB::exists_id('product',intval(Url::get('id')))){

			$this->map['categories'] = ProductAdminDB::get_categories($product['id']);

			$this->map['colors'] = ProductAdminDB::get_colors($product['id']);

			$this->map['sizes'] = ProductAdminDB::get_sizes($product['id']);

			$product['price'] = System::display_number($product['price']);

			$product['publish_price'] = System::display_number($product['publish_price']);

			$product['time'] = date('d/m/Y',$product['time']);

			foreach($product as $key=>$value){

				if(is_string($value) and !isset($_REQUEST[$key])){

					if($key=='cataloge' and $value){

						eval('$catalogs ='.$value.';');

					}else{

						$_REQUEST[$key] = $value;

					}

				}

				$images = DB::fetch_all('select * from product_image where product_id = '.Url::iget('id').'');

			}

			if(!isset($_REQUEST['mi_price'])){

				$_REQUEST['mi_price'] = $this->get_product_price($product['id']);

			}

		}

		$product_id = (isset($product)?$product['id']:false);

		$categories = ProductAdminDB::get_category($product_id);

		require_once 'packages/core/includes/utils/category.php';

		combobox_indent($categories);

		$category_options = '';

		foreach($categories as $value){

			$category_options .= '<option value="'.$value['id'].'" '.(($product_id and $value['product_id']==$product_id)?'selected':'').'>'.$value['name'].'</option>';

		}

		$colors = $this->get_thuoc_tinhs('MAU');

		$color_options = '';

		foreach($colors as $value){

			$color_options .= '<option value="'.$value['id'].'">'.$value['name'].'</option>';

		}

		$sizes = $this->get_thuoc_tinhs('SIZE');

		$size_options = '';

		foreach($sizes as $value){

			$size_options .= '<option value="'.$value['id'].'">'.$value['name'].'</option>';

		}

		$product_edit_id = DB::fetch('select * from product where id = '.Url::iget('id').'');

		$product_edit_id2 = DB::fetch_all('select product.id,product_thuoc_tinh_sp.product_id, thuoc_tinh_sp.type, thuoc_tinh_sp.id,thuoc_tinh_sp.name from product

 			left outer join product_thuoc_tinh_sp on product_thuoc_tinh_sp.product_id = product.id

 			left outer join thuoc_tinh_sp on product_thuoc_tinh_sp.thuoc_tinh_id = thuoc_tinh_sp.id

 			where product.id = '.Url::iget('id').' ');

//		System::debug($product_edit_id2);die;

		foreach($product_edit_id2 as $k => $v){

			if($v['type']== "PHONG_CACH") $_REQUEST['phong_cach_id'] = $v['id'];

			if($v['type']== "CAP") $_REQUEST['cap_id'] = $v['id'];

			if($v['type']== "DANG") $_REQUEST['dang_id'] = $v['id'];

		}

		//$_REQUEST['phong_cach_id'] = 300;

		$this->map['phong_cach_id_list'] = array('Chọn') + String::get_list($this->get_thuoc_tinhs('PHONG_CACH'));

		$this->map['cap_id_list'] = array('Chọn') + String::get_list($this->get_thuoc_tinhs('CAP'));

		$this->map['dang_id_list'] = array('Chọn') + String::get_list($this->get_thuoc_tinhs('DANG'));

		if(!Url::get('time')){

			$_REQUEST['time'] = date('d/m/Y');

		}

		$this->map['valuation_list'] = array('Chọn');

		for($i=1;$i<=5;$i++){

			$this->map['valuation_list'][$i] = $i.' sao';

		}

		$this->map += array(

			'category_options'=>$category_options,

			'color_options'=>$color_options,

			'size_options'=>$size_options,

			'status_list'=>$status,

			'languages'=>$languages,

			'currency_id_list'=>String::get_list($currency),

			'images'=>$images,

			'cataloges'=>$catalogs

		);

		$this->parse_layout('edit',$this->map);

	}

	function get_thuoc_tinhs($type='PHONG_CACH'){

		return DB::select_all('thuoc_tinh_sp','type="'.$type.'"');

	}

	function update_thuoc_tinh($product_id,$thuoc_tinh_id,$old_thuoc_tinh_id=false){

		if(!DB::exists('select id from product_thuoc_tinh_sp where product_id='.$product_id.' and thuoc_tinh_id= '.$thuoc_tinh_id)) {

			DB::insert('product_thuoc_tinh_sp',array(

				'product_id'=>$product_id,

				'thuoc_tinh_id'=>$thuoc_tinh_id

			));

		}else{

			if($old_thuoc_tinh_id and $row=DB::fetch('select id from product_thuoc_tinh_sp where product_id='.$product_id.' and thuoc_tinh_id='.$old_thuoc_tinh_id.'')){

				DB::update('product_thuoc_tinh_sp',array(

					'product_id'=>$product_id,

					'thuoc_tinh_id'=>$thuoc_tinh_id

				),'id='.$row['id']);

			}

		}

	}

	function update_category($product_id,$category_ids){

		if(!empty($category_ids)){

			DB::delete('product_category','product_id='.$product_id.'');

			foreach($category_ids as $key=>$value){

				DB::insert('product_category',array('product_id'=>$product_id,'category_id'=>$value));

			}

		}

		//exit();

	}

	function update_product_price($product_id){

		if(isset($_REQUEST['mi_price'])){

			foreach($_REQUEST['mi_price'] as $key=>$record){

				if($record['id']=='(auto)'){

					$record['id']=false;

				}

				$record['product_id'] = $product_id;

				$record['price'] = System::calculate_number($record['price']);

				if($record['id'] and DB::exists_id('product_price',$record['id'])){

					DB::update('product_price',$record,'id='.$record['id']);

				}else{

					unset($record['id']);

					$record['id'] = DB::insert('product_price',$record);

				}

				/////

				//$this->save_item_image('anh_dai_dien_'.$key,$record['id']);

			}

			if (isset($ids) and sizeof($ids)){

				$_REQUEST['selected_ids'].=','.join(',',$ids);

			}

			if(URL::get('deleted_ids')){

				$ids = explode(',',URL::get('deleted_ids'));

				foreach($ids as $id){

					DB::delete_id('product_price',$id);

				}

			}

		}

	}

	function get_product_price($product_id){

		{

			$sql = '

				select 

					product_price.*

				from 

					product_price

				WHERE

					product_price.product_id='.$product_id.'

				order by 

					product_price.open_promotion_time  DESC

				LIMIT

					0,100

			';

			$mi_price = DB::fetch_all($sql);

			foreach($mi_price as $key=>$value){

				$mi_price[$key]['price'] = System::display_number($value['price']);

			}

			return $mi_price;

		}

	}

	function update_color($product_id,$colors){



		if(!empty($colors)){

			DB::query('delete product_thuoc_tinh_sp from product_thuoc_tinh_sp inner join thuoc_tinh_sp on thuoc_tinh_sp.id = product_thuoc_tinh_sp.thuoc_tinh_id where  product_thuoc_tinh_sp.product_id='.$product_id.' and thuoc_tinh_sp.type="MAU"');

			foreach($colors as $key=>$value){

				DB::insert('product_thuoc_tinh_sp',array('product_id'=>$product_id,'thuoc_tinh_id'=>$value));

			}

		}

		//exit();

	}

	function update_size($product_id,$sizes){



		if(!empty($sizes)){

			DB::query('delete product_thuoc_tinh_sp from product_thuoc_tinh_sp inner join thuoc_tinh_sp on thuoc_tinh_sp.id = product_thuoc_tinh_sp.thuoc_tinh_id where  product_thuoc_tinh_sp.product_id='.$product_id.' and thuoc_tinh_sp.type="SIZE"');

			foreach($sizes as $key=>$value){

				DB::insert('product_thuoc_tinh_sp',array('product_id'=>$product_id,'thuoc_tinh_id'=>$value));

			}

		}

		

	}

}

?>

