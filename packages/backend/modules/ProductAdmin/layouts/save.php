<script src="assets/admin/scripts/tinymce/tinymce.min.js"></script>

<script>

/*

tinymce.init({

    selector: "#description_1",

		theme: "modern",height: 500,

    plugins: [

         "advlist autolink link image lists charmap print preview hr anchor pagebreak",

         "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",

         "table contextmenu directionality emoticons paste textcolor responsivefilemanager code"

   ],

   toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",

   toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",

   image_advtab: true ,



   external_filemanager_path:"/filemanager/filemanager/",

   filemanager_title:"Quản lý FILE pro" ,

   external_plugins: { "filemanager" : "/filemanager/filemanager/plugin.min.js"}

 });*/

tinymce.init({

  selector: '#description_1',

  height: 500,

  theme: 'modern',

  plugins: [

    'advlist autolink lists link image charmap print preview hr anchor pagebreak',

    'searchreplace wordcount visualblocks visualchars code fullscreen',

    'insertdatetime media nonbreaking save table contextmenu directionality',

    'emoticons template paste textcolor colorpicker textpattern imagetools'

  ],

  toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',

  toolbar2: 'print preview media | forecolor backcolor emoticons',

	theme_advanced_buttons1 : "openmanager",

  image_advtab: true,

  content_css: [

    'assets/admin/scripts/tinymce/skins/lightgray/skin.min.css'

  ],

	automatic_uploads: false,

	file_browser_callback: function() {

		window.open('<?php echo Url::build('file_manager')?>','File manager','width=880,height=600');

  },

	open_manager_upload_path: 'uploads/'

 });

</script>



<script src="packages/core/includes/js/multi_items.js"></script>

<span style="display:none">

	<span id="mi_price_sample">

		<div id="input_group_#xxxx#" class="multi-item-group">

      <span class="multi-edit-input" style="width:20px;"><input  type="checkbox" id="_checked_#xxxx#" tabindex="-1"></span>

      <span class="multi-edit-input" style="width:40px;"><input  name="mi_price[#xxxx#][id]" type="text" id="id_#xxxx#" class="multi-edit-text-input" style="text-align:right;" value="(auto)" tabindex="-1" readonly></span>

      <span class="multi-edit-input" style="width:150px;"><input  name="mi_price[#xxxx#][name]" class="multi-edit-text-input" type="text" id="name_#xxxx#"></span>

      <span class="multi-edit-input" style="width:100px;"><input  name="mi_price[#xxxx#][price]" class="multi-edit-text-input align-right" type="text" id="price_#xxxx#" onChange="this.value=numberFormat(this.value)"></span>

			<span class="multi-edit-input" style="width:150px;"><input  name="mi_price[#xxxx#][open_promotion_time]" class="multi-edit-text-input" type="text" id="open_promotion_time_#xxxx#"></span>

      <span class="multi-edit-input" style="width:150px;"><input  name="mi_price[#xxxx#][close_promotion_time]" class="multi-edit-text-input" type="text" id="close_promotion_time_#xxxx#"></span>

      <span class="multi-edit-input no-border" style="width:20px;"><img src="assets/admin/images/buttons/delete.png" onClick="mi_delete_row(getId('input_group_#xxxx#'),'mi_price','#xxxx#','');event.returnValue=false;" style="cursor:pointer;"/></span>

		</div>

    <br clear="all">

	</span>

</span>

<fieldset id="toolbar">

	<div id="toolbar-title">

		Cập nhật sản phẩm <span>[ <?php echo Portal::language(Url::get('cmd','list'));?> ]</span>

	</div>

	<div id="toolbar-content">

		<table align="right">

			<tbody>

			<tr>

				<td id="toolbar-save"  align="center"><a onclick="EditProductAdmin.submit();"> <span title="Edit"> </span> Ghi lại </a> </td>

				<td id="toolbar-back"  align="center"><a href="<?php echo Url::build_current(array('cmd'=>'list'));?>#"> <span title="New"> </span> List </a> </td>

			</tr>

			</tbody>

		</table>

	</div>

</fieldset>

<br clear="all">

<fieldset id="toolbar">

	<?php if(Form::$current->is_error()){echo Form::$current->error_messages();}?>

	<form name="EditProductAdmin" id="EditProductAdmin" method="post" enctype="multipart/form-data">

		<table class="table">

			<tr>

				<td valign="top">

					<table class="table">

						<tr style="display: none;">

						  <td align="left" width="15%">Mã SP</td>

						  <td width="30%" align="left"><input name="code" type="text" class="form-control input-sm" id="code" tabindex="3"></td>

							<td width="15%" align="left">Ngày đăng</td>

							<td width="30%" align="left"><div class="input-group date">

                <input name="time" type="text" id="time" class="form-control input-sm"/>

                <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>

            </div></td>

						</tr>

						<tr>

							<td align="left">[[.status.]]</td>

							<td align="left"><select name="status" class="form-control input-sm" id="status" tabindex="2"></select></td>

							<td align="left">Vị trí</td>

							<td align="left"><input name="position" type="text" class="form-control input-sm" id="position" tabindex="-1"></td>

						</tr>

						<tr style="display: none;">

							<td align="left">Tình trạng kho</td>

							<td align="left"><input name="product_status" type="text" class="form-control input-sm" id="product_status" tabindex="4" /></td>

							<td align="left">SP liên quan</td>

							<td align="left"><input name="related_ids" type="text" class="form-control input-sm" id="related_ids" tabindex="8" />

								Ví dụ: mã 1,mã 2,mã 3</td>

						</tr>

						<tr style="">

							<td align="left" class="price-label">Giá</td>

							<td align="left"><input name="price" type="text" id="price" tabindex="5"  class="form-control input-sm" /></td>

							<!-- <td align="left" class="price-label">Tồn kho</td> -->

							<!-- <td align="left"><input name="quantity" type="text" class="form-control input-sm" id="quantity" tabindex="7" /></td> -->

						</tr>

						<tr style="" class="hidden">

						  <td align="left" class="price-label">&nbsp;</td>

						  <td align="left">&nbsp;</td>

						  <td align="left" class="price-label">Đánh giá của admin</td>

						  <td align="left"><select name="valuation" id="valuation" class="form-control input-sm" tabindex="7"></select></td>

					  </tr>

					</table>

          <div class="multi-item-wrapper hidden">

          	<h4>Set giá theo thời gian</h4>

            <span id="mi_price_all_elems">

              <span style="white-space:nowrap;">

                <span class="multi-edit-input header" style="width:20px;"><input type="checkbox" value="1" onclick="mi_select_all_row('mi_price',this.checked);"></span>

                <span class="multi-edit-input header" style="width:40px;">ID</span>

                <span class="multi-edit-input header" style="width:150px;">Tên</span>

                <span class="multi-edit-input header" style="width:100px;">Giá</a></span>

                <span class="multi-edit-input header" style="width:150px;">Từ ngày</a></span>

                <span class="multi-edit-input header" style="width:150px;">đến ngày</a></span>

                <span class="multi-edit-input header no-border no-bg" style="width:20px;">&nbsp;</span>

                <br clear="all">

              </span>

            </span>

          </div>

          <br clear="all">

          <div class="hidden" style="padding:5px;"><input type="button" value="   Thêm mới   " onclick="mi_add_new_row('mi_price');jQuery('#open_promotion_time_'+input_count).datepicker({format: 'yyyy-mm-dd'});jQuery('#close_promotion_time_'+input_count).datepicker({format: 'yyyy-mm-dd'});"></div>

					<table class="table">

						<tr>

							<td>

								<!--LIST:languages-->

									<!--IF:cond([[=languages.id=]]==1)-->

									<div class="tab-page" id="tab-page-category-[[|languages.id|]]">

										<!--							<h2 class="tab">[[|languages.name|]]</h2>-->

										<div class="form_input_label"><h3>[[.name.]](<span class="require">*</span>)</h3></div>

										<div class="form_input">

											&nbsp;<input name="name_[[|languages.id|]]" type="text" id="name_[[|languages.id|]]" class="form-control input-sm" />

										</div>

										<br>

										<div class="form_input_label"><h3>Mô tả ngắn</h3></div>

										<br>

										<div class="form_input">

											<textarea id="brief_[[|languages.id|]]" name="brief_[[|languages.id|]]" cols="75" rows="20" style="width:99%; height:50px;overflow:hidden" ><?php echo Url::get('brief_'.[[=languages.id=]],'');?></textarea><br />

										</div>

										<div class="hidden">

											<br><div class="form_input_label" ><h3>Thông tin sản phẩm</h3></div> <br>

											<div class="form_input">

												<textarea id="technical_info_[[|languages.id|]]" name="technical_info_[[|languages.id|]]" cols="75" rows="20" style="width:99%; height:180px;overflow:hidden"><?php echo Url::get('technical_info_'.[[=languages.id=]],'');?></textarea><br />

											</div>

										</div>

										<div class="form_input_label"><h3>Chi tiết</h3></div>

										<br>

										<div class="form_input">

											<textarea id="description_[[|languages.id|]]" name="description_[[|languages.id|]]" cols="75" rows="20" style="width:99%; height:350px;overflow:hidden"><?php echo Url::get('description_'.[[=languages.id=]],'');?></textarea><br />

										</div>

										<div class="hidden">

											<br><div class="form_input_label" ><h3>Nhận xét admin về sản phẩm</h3></div> <br>

											<div class="form_input">

												<textarea id="commentadmin" name="commentadmin" cols="75" rows="20" style="width:99%; height:180px;overflow:hidden"><?php echo Url::get('commentadmin','');?></textarea><br />

											</div>

										</div>

										<br>

									</div>

									<!--/IF:cond-->

									<!--/LIST:languages-->

							</td>

						</tr>

					</table>

				</td>

				<td valign="top" width="30%">

					<table class="table">

						<tr valign="top">

						  <td width="130"><strong>Thuộc danh mục</strong></td>

						  <td style="color:#FF851A;">[[|categories|]]</td>

					  </tr>

						<tr valign="top">

						  <td><strong><?php echo (Url::get('cmd')=='edit')?'Sửa danh mục <input  name="edit_category" type="checkbox" id="edit_category" value="1">':'Thêm danh mục';?></strong><br>(Để chọn nhiều danh mục nhấn ctrl + chuột trái)<br><br></td>

						  <td style="height:220px;"><div id="categoryWrapper" style="float:left;width:100%;height:100px;position:absolute;display:none"><select  name="category_id[]" multiple="MULTIPLE" id="category_id" tabindex="1" style="height:200px;max-width:100%;">

						    [[|category_options|]]

						    </select></div></td>

					  </tr>

						<tr>

							<td><strong>[[.Hitcount.]]</strong></td>

							<td><?php echo Url::get('hitcount','0');?></td>

						</tr>

						<tr>

							<td><strong>[[.Modified.]]</strong></td>

							<td><?php echo Url::get('last_time_update')?date('h\h:i d/m/Y',Url::get('last_time_update')):'Not modified';?></td>

						</tr>



						<!--đang bị ẩn--------------------->

						<tr class="hidden"  valign="top">

							<td><strong>Mầu sắc</strong></td>

							<td style="color:#FF851A;">[[|colors|]]</td>

						</tr>





						<tr valign="top" class="hidden">

							<td><strong>Chọn mầu sắc</strong></td>

							<td style="height:100px;"><div style="width:100%;position:absolute;"><select  name="color_id[]" multiple="MULTIPLE" id="color_id" tabindex="1">

										[[|color_options|]]

									</select></div><br><br></td>

						</tr>

						<tr valign="top" class="hidden">

							<td><strong>Size</strong></td>

							<td style="color:#FF851A;">[[|sizes|]]</td>

						</tr>

						<tr class="hidden" valign="top" height="60">

							<td><strong>Chọn size</strong></td>

							<td><div style="width:100%;position:absolute;height:50px;"><select  name="size_id[]" multiple="MULTIPLE" id="size_id" tabindex="1">

										[[|size_options|]]

									</select></div></td>

						</tr>

						<!--and -------------------->



					</table>



					<!--đang bị ẩn--------------------->

					<div id="panel" class="hidden">

						<div id="panel_1"  style="margin-top:8px;">

							<label  style="width:100px;"> Phong cách</label>

							<select name="phong_cach_id" id="phong_cach_id" style="width:100px;"></select>

						</div>

						<div id="panel_1"  style="margin-top:8px;">

							<label  style="width: 100px;">Loại cạp</label>

							<select name="cap_id" id="cap_id" style="width:100px;"></select>

						</div>

						<div id="panel_1"  style="margin-top:8px;">

							<label style="width: 100px;">Dáng</label>

							<select name="dang_id" id="dang_id" style="width:100px;"></select>

						</div>

					</div>





					<div id="panel">

						<div id="panel_1"  style="margin-top:8px;">

							<span>[[.images.]]</span>

							<table>

								<tr>

									<td align="right"><div style="float:left;width:80px;">Ảnh nhỏ</div></td>

									<td align="left">

										<input name="small_thumb_url" type="file" id="small_thumb_url" class="file">

										<div id="delete_small_thumb_url"><?php if(Url::get('small_thumb_url') and file_exists(Url::get('small_thumb_url'))){?>[<a href="<?php echo Url::get('small_thumb_url');?>" target="_blank" style="color:#FF0000">[[.view.]]</a>]&nbsp;[<a href="<?php echo Url::build_current(array('cmd'=>'unlink','link'=>Url::get('small_thumb_url')));?>" onclick="jQuery('#delete_small_thumb_url').html('');" target="_blank" style="color:#FF0000">[[.delete.]]</a>]<?php }?></div>

									</td>

								</tr>

								<tr>

									<td align="right">Thư viện ảnh SP</td>

									<td align="left">

										<a href="#" onClick="window.open('<?php echo Url::build('upload_image',array('product_id'=>Url::iget('id')));?>','','width=980,height=500');return false;" class="button">Cập nhật ảnh sản phẩm >></a>

										<!--IF:images(isset([[=images=]]) and is_array([[=images=]]) and !empty([[=images=]]))-->

										<div style="float:left;height:200px;overflow:auto">

											<!--LIST:images-->

											<div style="float:left;margin:0px 5px 5px 0px;width:	60px;height:60px;overflow:hidden;text-align:center;border:1px solid #CCC;">

												<a target="_blank" title="Xem" href="[[|images.image_url|]]"><img src="[[|images.small_thumb_url|]]" width="70"></a>

											</div>

											<!--/LIST:images-->

										</div>

										<!--/IF:images-->

									</td>

								</tr>

								<tr class="hide">

									<td align="right">[[.video.]]</td>

									<td align="left"><input name="video" type="text" id="video" class="file"></td>

								</tr>

								<tr style="display:none;">

									<td align="right"><div style="float:left;width:80px;">Bản vẽ</div></td>

									<td align="left">

										<input name="banve" type="file" id="banve" class="file">

										<div id="delete_banve"><?php if(Url::get('banve') and file_exists(Url::get('banve'))){?>[<a href="<?php echo Url::get('banve');?>" target="_blank" style="color:#FF0000">[[.view.]]</a>]&nbsp;[<a href="<?php echo Url::build_current(array('cmd'=>'unlink','link'=>Url::get('banve')));?>" onclick="jQuery('#delete_banve').html('');" target="_blank" style="color:#FF0000">[[.delete.]]</a>]<?php }?></div>

									</td>

								</tr>

								<tr class="hidden">

									<td align="right">Catalog</td>

									<td align="left">

										<input  name="cataloge[]" type="file" id="cataloge" class="multi" maxlength="<?php echo 20-count([[=cataloges=]]); ?>">

										<!--IF:cataloge(isset([[=cataloges=]]) and is_array([[=cataloges=]]) and !empty([[=cataloges=]]))-->

										<!--LIST:cataloges-->

										<div>

											<img src="assets/default/images/buttons/uncheck.gif" onclick="Delete(this)" />

											<a target="_blank" title="[[.View.]]" href="[[|cataloges.value|]]"><?php echo [[=cataloges.name=]]; ?></a>

											<input  type="hidden" name="old_cataloge[][value]" value="[[|cataloges.value|]]" />

										</div>

										<!--/LIST:cataloges-->

										<!--/IF:cataloge-->

										<div class="warning">(Có thể chọn nhiều file bằng cách chọn lần lượt 1 file 1 để tạo thành danh sách file được chọn)</div>

									</td>

								</tr>

							</table>

						</div>

						<div id="panel_1"  style="margin-top:8px;">

							<span>[[.Metadata_information.]]</span>

							<table cellpadding="4" cellspacing="0" width="100%" border="1" bordercolor="#E9E9E9">

								<tr>

									<td width="30%" align="right">[[.keywords.]]</td>

									<td width="70%" align="left"><input name="keywords" type="text" id="keywords" class="input-large"></td>

								</tr>

								<tr valign="top">

									<td width="30%" align="right">[[.tags.]]</td>

									<td width="70%" align="left"><textarea name="tags" id="tags" class="input-large" rows="10"></textarea></td>

								</tr>

							</table>

						</div>

					</div>

				</td>

			</tr>

		</table>

    <input  name="deleted_ids" id="deleted_ids" type="hidden" value="<?php echo URL::get('deleted_ids');?>">

	</form>

</fieldset>

<br>

<script>function Delete(obj){jQuery(obj).parent().remove();}</script>

<script type="text/javascript" src="http://davidstutz.github.io/bootstrap-multiselect/dist/js/bootstrap-multiselect.js"></script>

<script>

	mi_init_rows('mi_price',<?php if(isset($_REQUEST['mi_price'])){echo String::array2js($_REQUEST['mi_price']);}else{echo '[]';}?>);

	jQuery(document).ready(function(){

		//jQuery('#category_id').multiselect();

		//jQuery('#color_id').multiselect();

		//jQuery('#size_id').multiselect();

		jQuery('#time').datepicker({

				format: "dd/mm/yyyy",

				language: "vi"

		});

		<!--IF:cond(Url::get('cmd')=='edit')-->

		jQuery("#categoryWrapper").css({'display':'none'});

		<!--ELSE-->

		jQuery("#categoryWrapper").css({'display':'block'});

		<!--/IF:cond-->

		jQuery('#edit_category').click(function(){

				if (jQuery(this).is(':checked')) {

						jQuery("#categoryWrapper").css({'display':'block'});

				} else {

						jQuery("#categoryWrapper").css({'display':'none'});

				}

	 });

		for(var i=0;i<=input_count;i++){

			if(getId('open_promotion_time_'+i)){

				jQuery('#open_promotion_time_'+i).datepicker({format: 'yyyy-mm-dd'});

				jQuery('#open_promotion_time_'+i).click(function(){

				//jQuery(this).handleDtpicker('setDate', new Date(<?php echo date('Y');?>, <?php echo date('m');?>, <?php echo date('d');?>, 0, 0, 0));





				});



				jQuery('#close_promotion_time_'+i).datepicker({format: 'yyyy-mm-dd'});

				jQuery('#close_promotion_time_'+i).click(function(){

				//jQuery(this).handleDtpicker('setDate', new Date(<?php echo date('Y');?>, <?php echo date('m');?>, <?php echo date('d');?>, 0, 0, 0));

				});

			}

		}

		jQuery('#discount').change(function(){

			updatePrice();

		});

		jQuery('#publish_price').change(function(){

			jQuery('#publish_price').val(numberFormat(jQuery('#publish_price').val()));

			updatePrice();

		});

		jQuery('#price').change(function(){

			jQuery('#price').val(numberFormat(jQuery('#price').val()));

		});

		//updateDiscountPercent();

	});

	function updatePrice(){

		var publicPrice = to_numeric(jQuery('#publish_price').val());

		var discountPercent = to_numeric(jQuery('#discount').val());

		var price = publicPrice - roundNumber(publicPrice*(discountPercent/100),2);

		price = numberFormat(price);

		jQuery('#price').val(price);

	}

	function updateDiscountPercent(){

		var publicPrice = to_numeric(jQuery('#publish_price').val());

		if(publicPrice){

			var price = to_numeric(jQuery('#price').val());

			var discount = ((publicPrice - price)/publicPrice)*100;

			jQuery('#discount').val(discount);

		}

	}

</script>