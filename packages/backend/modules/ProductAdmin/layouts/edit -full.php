<script>
	jQuery(document).ready(function(){
		jQuery('#open_promotion_time').appendDtpicker({'autodateOnStart':false,'closeOnSelected':true});
		jQuery('#open_promotion_time').click(function(){
			//jQuery(this).handleDtpicker('setDate', new Date(<?php echo date('Y');?>, <?php echo date('m');?>, <?php echo date('d');?>, 0, 0, 0));
		});

		jQuery('#close_promotion_time').appendDtpicker({'autodateOnStart':false,'closeOnSelected':true});
		jQuery('#close_promotion_time').click(function(){
			//jQuery(this).handleDtpicker('setDate', new Date(<?php echo date('Y');?>, <?php echo date('m');?>, <?php echo date('d');?>, 0, 0, 0));
		});
	});
</script>
<fieldset id="toolbar">
	<div id="toolbar-title">
		Cập nhật sản phẩm <span>[ <?php echo Portal::language(Url::get('cmd','list'));?> ]</span>
	</div>
	<div id="toolbar-content">
		<table align="right">
			<tbody>
			<tr>
				<td id="toolbar-save"  align="center"><a onclick="EditProductAdmin.submit();"> <span title="Edit"> </span> Ghi lại </a> </td>
				<td id="toolbar-back"  align="center"><a href="<?php echo Url::build_current(array('cmd'=>'list'));?>#"> <span title="New"> </span> List </a> </td>
			</tr>
			</tbody>
		</table>
	</div>
</fieldset>
<br clear="all">
<fieldset id="toolbar">
	<?php if(Form::$current->is_error()){echo Form::$current->error_messages();}?>
	<form name="EditProductAdmin" id="EditProductAdmin" method="post" enctype="multipart/form-data">
		<table class="table">
			<tr>
				<td valign="top">
					<table class="table">
						<tr>
							<td width="16%" align="left">Danh mục (<span class="require">*</span>)</td>
							<td width="28%" align="left"><select name="category_id" class="select-large" id="category_id" tabindex="1"></select></td>
							<td width="15%" align="left">Mã SP</td>
							<td width="44%" align="left"><input name="code" type="text" class="input" id="code" tabindex="3"></td>
						</tr>
						<tr>
							<td align="left">[[.status.]]</td>
							<td align="left"><select name="status" class="input" id="status" tabindex="2"></select></td>
							<td align="left">Vị trí</td>
							<td align="left"><input name="position" type="text" class="input" id="position" tabindex="-1"></td>
						</tr>
						<tr style="">
							<td align="left">Tình trạng kho</td>
							<td align="left"><input name="product_status" type="text" class="input" id="product_status" tabindex="4" /></td>
							<td align="left">SP liên quan</td>
							<td align="left"><input name="related_ids" type="text" class="input" id="related_ids" tabindex="8" />
								Ví dụ: mã 1,mã 2,mã 3</td>
						</tr>
						<tr style="">
							<td align="left" class="price-label">Giá gốc</td>
							<td align="left"><input name="publish_price" type="text" class="input" id="publish_price" tabindex="5" /></td>
							<td align="left">Thời gian mở KM</td>
							<td align="left"><input name="open_promotion_time" type="text" class="input" id="open_promotion_time" style="width:130px;" tabindex="9" /></td>
						</tr>
						<tr style="">
							<td align="left" class="price-label">Giảm giá</td>
							<td align="left"><input name="discount" type="text" class="input" id="discount" style="width:30px" tabindex="6" />
								%</td>
							<td align="left">Thời gian đóng KM</td>
							<td align="left"><input name="close_promotion_time" type="text" class="input" id="close_promotion_time" style="width:130px;" tabindex="10" /></td>
						</tr>
						<tr style="">
							<td align="left" class="price-label">Giá bán</td>
							<td align="left"><input name="price" type="text" class="input" id="price" tabindex="7" /></td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>

						<td align="left" class="price-label">Đánh giá (trong khoảng (1->5))</td>
						<td align="left"><input name="valuation" type="text" class="input" id="valuation" tabindex="7" /></td>
						<td align="left">&nbsp;</td>
						<td align="left">&nbsp;</td>
						</tr>

						<td align="left" class="price-label">Phân loại</td>
						<td align="left">
							<label><input name="sex" type="radio" class="input" value="nam" <?php if([[=sex=]]=='nam') echo 'checked '; ?>>Đồ nam</label>&nbsp;
							<label><input name="sex" type="radio" class="input" value="nu" <?php if([[=sex=]]=='nu') echo 'checked '?>>Đồ nữ</label>
							<label><input name="sex" type="radio" class="input" value="chung" <?php if([[=sex=]]=='chung') echo 'checked '?>>Chung</label>
						</td>

						<td align="left" class="price-label">Số lượng</td>
						<td align="left"><input name="qty" type="text" class="input" id="qty" tabindex="7" /></td>
						<td align="left">&nbsp;</td>
						<td align="left">&nbsp;</td>


						</tr>
					</table>
					<br>
					<table class="table">
						<tr>
							<td>
								<!--LIST:languages-->
									<!--IF:cond([[=languages.id=]]==1)-->
									<div class="tab-page" id="tab-page-category-[[|languages.id|]]">
										<!--							<h2 class="tab">[[|languages.name|]]</h2>-->
										<br>
										<div class="form_input_label"><strong>[[.name.]]</strong> (<span class="require">*</span>)</div>
										<br>
										<div class="form_input">
											&nbsp;<input name="name_[[|languages.id|]]" type="text" id="name_[[|languages.id|]]" class="input-big-huge" />
										</div>
										<br>
										<div class="form_input_label"><strong>Mô tả ngắn</strong></div>
										<br>
										<div class="form_input">
											<textarea id="brief_[[|languages.id|]]" name="brief_[[|languages.id|]]" cols="75" rows="20" style="width:99%; height:180px;overflow:hidden" ><?php echo Url::get('brief_'.[[=languages.id=]],'');?></textarea><br />
											<script>simple_mce('brief_[[|languages.id|]]');</script>
										</div>
										<br><div class="form_input_label" ><strong>Thông tin sản phẩm</strong></div> <br>
										<div class="form_input">
											<textarea id="technical_info_[[|languages.id|]]" name="technical_info_[[|languages.id|]]" cols="75" rows="20" style="width:99%; height:180px;overflow:hidden"><?php echo Url::get('technical_info_'.[[=languages.id=]],'');?></textarea><br />
											<script>advance_mce('technical_info_[[|languages.id|]]');</script>
										</div>

										<br><div class="form_input_label" ><strong>Nhận xét admin về sản phẩm</strong></div> <br>
										<div class="form_input">
											<textarea id="commentadmin" name="commentadmin" cols="75" rows="20" style="width:99%; height:180px;overflow:hidden"><?php echo Url::get('commentadmin','');?></textarea><br />
											<script>advance_mce('commentadmin');</script>
										</div>

										<br><div class="form_input_label"><strong>Chi tiết</strong></div>
										<br>
										<div class="form_input">
											<textarea id="description_[[|languages.id|]]" name="description_[[|languages.id|]]" cols="75" rows="20" style="width:99%; height:350px;overflow:hidden"><?php echo Url::get('description_'.[[=languages.id=]],'');?></textarea><br />
											<script>advance_mce('description_[[|languages.id|]]');</script>
										</div>
									</div>
									<!--/IF:cond-->
									<!--/LIST:languages-->
							</td>
						</tr>
					</table>
				</td>
				<td valign="top" width="30%">
					<table class="table">
						<tr>
							<td><strong>[[.Hitcount.]]</strong></td>
							<td><?php echo Url::get('hitcount','0');?></td>
						</tr>
						<tr>
							<td><strong>[[.Created.]]</strong></td>
							<td><?php echo date('h\h:i d/m/Y',Url::get('time',time()));?></td>
						</tr>
						<tr>
							<td><strong>[[.Modified.]]</strong></td>
							<td><?php echo Url::get('last_time_update')?date('h\h:i d/m/Y',Url::get('last_time_update')):'Not modified';?></td>
						</tr>
					</table>
					<div id="panel">
						<div id="panel_1"  style="margin-top:8px;">
							<label  style="width:100px;"> Phong cách</label>
							<select name="phong_cach_id" id="phong_cach_id" style="width:100px;"></select>
						</div>
						<div id="panel_1"  style="margin-top:8px;">
							<label  style="width:100px;">Mầu</label>
							<select name="mau_id" id="mau_id" style="width:100px;"></select>
						</div>
						<div id="panel_1"  style="margin-top:8px;">
							<label  style="width: 100px;">Loại cạp</label>
							<select name="cap_id" id="cap_id" style="width:100px;"></select>
						</div>
						<div id="panel_1"  style="margin-top:8px;">
							<label style="width: 100px;">Dáng</label>
							<select name="dang_id" id="dang_id" style="width:100px;"></select>
						</div>
					</div>
					<div id="panel">
						<div id="panel_1"  style="margin-top:8px;">
							<span>[[.images.]]</span>
							<table>
								<tr>
									<td align="right"><div style="float:left;width:80px;">Ảnh nhỏ</div></td>
									<td align="left">
										<input name="small_thumb_url" type="file" id="small_thumb_url" class="file">
										<div id="delete_small_thumb_url"><?php if(Url::get('small_thumb_url') and file_exists(Url::get('small_thumb_url'))){?>[<a href="<?php echo Url::get('small_thumb_url');?>" target="_blank" style="color:#FF0000">[[.view.]]</a>]&nbsp;[<a href="<?php echo Url::build_current(array('cmd'=>'unlink','link'=>Url::get('small_thumb_url')));?>" onclick="jQuery('#delete_small_thumb_url').html('');" target="_blank" style="color:#FF0000">[[.delete.]]</a>]<?php }?></div>
									</td>
								</tr>
								<tr>
									<td align="right">Thư viện ảnh SP</td>
									<td align="left">
										<a href="#" onClick="window.open('<?php echo Url::build('upload_image',array('product_id'=>Url::iget('id')));?>','','width=980,height=500');return false;" class="button">Cập nhật ảnh sản phẩm >></a>
										<!--IF:images(isset([[=images=]]) and is_array([[=images=]]) and !empty([[=images=]]))-->
										<div style="float:left;height:200px;overflow:auto">
											<!--LIST:images-->
											<div style="float:left;margin:0px 5px 5px 0px;width:	60px;height:60px;overflow:hidden;text-align:center;border:1px solid #CCC;">
												<a target="_blank" title="Xem" href="[[|images.image_url|]]"><img src="[[|images.small_thumb_url|]]" width="70"></a>
											</div>
											<!--/LIST:images-->
										</div>
										<!--/IF:images-->
									</td>
								</tr>
								<tr>
									<td align="right">[[.video.]]</td>
									<td align="left"><input name="video" type="text" id="video" class="file"></td>
								</tr>
								<tr style="display:none;">
									<td align="right"><div style="float:left;width:80px;">Bản vẽ</div></td>
									<td align="left">
										<input name="banve" type="file" id="banve" class="file">
										<div id="delete_banve"><?php if(Url::get('banve') and file_exists(Url::get('banve'))){?>[<a href="<?php echo Url::get('banve');?>" target="_blank" style="color:#FF0000">[[.view.]]</a>]&nbsp;[<a href="<?php echo Url::build_current(array('cmd'=>'unlink','link'=>Url::get('banve')));?>" onclick="jQuery('#delete_banve').html('');" target="_blank" style="color:#FF0000">[[.delete.]]</a>]<?php }?></div>
									</td>
								</tr>
								<tr style="display:none;">
									<td align="right">Catalog</td>
									<td align="left">
										<input  name="cataloge[]" type="file" id="cataloge" class="multi" maxlength="<?php echo 20-count([[=cataloges=]]); ?>">
										<!--IF:cataloge(isset([[=cataloges=]]) and is_array([[=cataloges=]]) and !empty([[=cataloges=]]))-->
										<!--LIST:cataloges-->
										<div>
											<img src="assets/default/images/buttons/uncheck.gif" onclick="Delete(this)" />
											<a target="_blank" title="[[.View.]]" href="[[|cataloges.value|]]"><?php echo [[=cataloges.name=]]; ?></a>
											<input  type="hidden" name="old_cataloge[][value]" value="[[|cataloges.value|]]" />
										</div>
										<!--/LIST:cataloges-->
										<!--/IF:cataloge-->
										<div class="warning">(Có thể chọn nhiều file bằng cách chọn lần lượt 1 file 1 để tạo thành danh sách file được chọn)</div>
									</td>
								</tr>
							</table>
						</div>
						<div id="panel_1"  style="margin-top:8px;">
							<span>[[.Metadata_information.]]</span>
							<table cellpadding="4" cellspacing="0" width="100%" border="1" bordercolor="#E9E9E9">
								<tr>
									<td width="30%" align="right">[[.keywords.]]</td>
									<td width="70%" align="left"><input name="keywords" type="text" id="keywords" class="input-large"></td>
								</tr>
								<tr valign="top">
									<td width="30%" align="right">[[.tags.]]</td>
									<td width="70%" align="left"><textarea name="tags" id="tags" class="input-large" rows="10"></textarea></td>
								</tr>
							</table>
						</div>
					</div>
				</td>
			</tr>
		</table>
	</form>
</fieldset>
<br>
<script>function Delete(obj){jQuery(obj).parent().remove();}</script>
<script>
	jQuery(document).ready(function(){
		jQuery('#discount').change(function(){
			updatePrice();
		});
		jQuery('#publish_price').change(function(){
			jQuery('#publish_price').val(numberFormat(jQuery('#publish_price').val()));
			updatePrice();
		});
		jQuery('#price').change(function(){
			jQuery('#price').val(numberFormat(jQuery('#price').val()));
		});
		updateDiscountPercent();
	});
	function updatePrice(){
		var publicPrice = to_numeric(jQuery('#publish_price').val());
		var discountPercent = to_numeric(jQuery('#discount').val());
		var price = publicPrice - roundNumber(publicPrice*(discountPercent/100),2);
		price = numberFormat(price);
		jQuery('#price').val(price);
	}
	function updateDiscountPercent(){
		var publicPrice = to_numeric(jQuery('#publish_price').val());
		if(publicPrice){
			var price = to_numeric(jQuery('#price').val());
			var discount = ((publicPrice - price)/publicPrice)*100;
			jQuery('#discount').val(discount);
		}
	}
</script>