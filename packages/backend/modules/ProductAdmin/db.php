<?php
class ProductAdminDB
{
	static function get_total_item($cond)
	{
		return DB::fetch(
			'select
				count(distinct product.id) as acount
			from
				product
				left outer join product_category on product_category.product_id = product.id
				left outer join category on category.id = product_category.category_id
			where
				'.$cond.'
				
				'
			,'acount');
	}
	static function get_items($cond,$order_by,$item_per_page)
	{
		$items = DB::fetch_all('
			SELECT
				product.id
				,product.status
				,product.price
				,product.position
				,product.user_id
				,product.image_url
				,product.small_thumb_url
				,product.time
				,product.open_promotion_time
				,product.close_promotion_time
				,product.hitcount
				,product.quantity
				,product.name_'.Portal::language().' as name
				,product.related_ids
				,category.name_'.Portal::language().' as category_name
				,category.structure_id
			FROM
				product
				left outer join product_category on product_category.product_id = product.id
				left outer join category on category.id = product_category.category_id
			WHERE
				'.$cond.'
			GROUP BY
				product.id
			ORDER BY
				'.$order_by.'
			LIMIT
				'.((page_no()-1)*$item_per_page).','.$item_per_page.'
		');
		$comments = DB::fetch_all(
			'SELECT
				count(*) as total_comment
				,product.id as id
			 FROM
				comment
				inner join product on comment.item_id = product.id
			WHERE
				1 and comment.type="PRODUCT"
			GROUP BY
				comment.item_id
			');
		$i = 1;
		foreach($items as $key =>$value)
		{
			$value['index'] = $i++;
			if(isset($comments[$key]))
			{
				$value['total_comment'] = $comments[$key]['total_comment'];
			}
			else
			{
				$value['total_comment'] = 0;
			}
			$items[$key] = $value;
			$items[$key]['categories'] = ProductAdminDB::get_categories($value['id']);
		}
		return ($items);
	}
	static function get_category($product_id=false)
	{
		return DB::fetch_all('
			SELECT
				category.id
				,category.name_'.Portal::language().' as name
				,category.structure_id
				'.($product_id?',product_category.product_id':'').'
			FROM
				category
				'.($product_id?'LEFT OUTER JOIN product_category ON product_category.category_id = category.id AND product_category.product_id='.$product_id.'':'').'
			WHERE
				category.type="PRODUCT"
				and category.structure_id <> '.ID_ROOT.'
			ORDER BY
				category.structure_id
		');
	}
	static function get_user()
	{
		return DB::fetch_all('
			SELECT
				distinct user_id as name
				,user_id as id
			FROM
				product
			WHERE
				user_id!=""
		');
	}
	static function get_categories($product_id){
		$str = '';
		$sql = 'select category.id,category.name_'.Portal::language().' as name from category inner join product_category as pc on pc.category_id=category.id where pc.product_id='.$product_id.'';
		$items = DB::fetch_all($sql);
		foreach($items as $key=>$value){
			$str .= ($str?',':'').$value['name'];
		}
		return $str;
	}
	static function get_colors($product_id){
		$str = '';
		$sql = 'select thuoc_tinh_sp.id,thuoc_tinh_sp.name from thuoc_tinh_sp inner join product_thuoc_tinh_sp on product_thuoc_tinh_sp.thuoc_tinh_id=thuoc_tinh_sp.id where product_thuoc_tinh_sp.product_id='.$product_id.' and thuoc_tinh_sp.type="MAU"';
		$items = DB::fetch_all($sql);
		foreach($items as $key=>$value){
			$str .= ($str?',':'').$value['name'];
		}
		return $str;
	}
	static function get_sizes($product_id){
		$str = '';
		$sql = 'select thuoc_tinh_sp.id,thuoc_tinh_sp.name from thuoc_tinh_sp inner join product_thuoc_tinh_sp on product_thuoc_tinh_sp.thuoc_tinh_id=thuoc_tinh_sp.id where product_thuoc_tinh_sp.product_id='.$product_id.' and thuoc_tinh_sp.type="SIZE"';
		$items = DB::fetch_all($sql);
		foreach($items as $key=>$value){
			$str .= ($str?',':'').$value['name'];
		}
		return $str;
	}
}
?>
