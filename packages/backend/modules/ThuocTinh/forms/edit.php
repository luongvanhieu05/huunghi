<?php
class EditThuocTinhForm extends Form{
	function EditThuocTinhForm(){
		Form::Form('EditThuocTinhForm');
		$this->add('thuoc_tinh_sp.ten',new TextType(true,'Chưa nhập tên vòng đấu',0,255));
		$this->link_css('assets/default/css/cms.css');
	}
	function on_submit(){
		if($this->check() and URL::get('confirm_edit') and !Url::get('search')){
			require_once 'packages/core/includes/utils/vn_code.php';
			if(isset($_REQUEST['mi_thuoctinh'])){
				foreach($_REQUEST['mi_thuoctinh'] as $key=>$record){
					if($record['id']=='(auto)'){
						$record['id']=false;
					}
					$record['name_id'] = convert_utf8_to_url_rewrite($record['name']);
					if($record['id'] and DB::exists_id('thuoc_tinh_sp',$record['id'])){
						DB::update('thuoc_tinh_sp',$record,'id='.$record['id']);
					}else{
						unset($record['id']);
						$record['id'] = DB::insert('thuoc_tinh_sp',$record);
					}
					/////
				}
				if (isset($ids) and sizeof($ids)){
					$_REQUEST['selected_ids'].=','.join(',',$ids);
				}
			}
			if(URL::get('deleted_ids')){
				$ids = explode(',',URL::get('deleted_ids'));
				foreach($ids as $id){
					DB::delete_id('thuoc_tinh_sp',$id);
				}
			}
			//update_mi_upload_file();
			//exit();
			Url::redirect_current(array());
		}
	}	
	function draw(){
		$this->map = array();
		$paging = '';
		$cond = '
			1=1
				'.(Url::get('keyword')?' AND thuoc_tinh_sp.name LIKE "%'.Url::get('keyword').'%"':'').'
			';		
		//if(!isset($_REQUEST['mi_thuoctinh']))
		{
			$item_per_page = 200;
			DB::query('
				select 
					count(*) as acount
				from 
					thuoc_tinh_sp
				where 
					1=1
			');
			$count = DB::fetch();
			$this->map['total'] = $count['acount'];
			DB::query('
				select 
					count(distinct thuoc_tinh_sp.id) as acount
				from 
					thuoc_tinh_sp
				where 
					'.$cond.'
			');
			$count = DB::fetch();
			require_once 'packages/core/includes/utils/paging.php';
			$paging = paging($count['acount'],$item_per_page);
			$sql = '
				select 
					thuoc_tinh_sp.*

				from 
					thuoc_tinh_sp
				WHERE
					'.$cond.'
				GROUP BY
					thuoc_tinh_sp.id
				order by 
					thuoc_tinh_sp.status ASC, thuoc_tinh_sp.id  DESC
				LIMIT
					'.((page_no()-1)*$item_per_page).','.$item_per_page.'
			';
			$mi_thuoctinh = DB::fetch_all($sql);
			$_REQUEST['mi_thuoctinh'] = $mi_thuoctinh;
		}
		$this->map['paging'] = $paging;
		$this->map['type_options'] = '<option value="">Chọn loại</option><option value="SIZE">Size</option><option value="MAU">Mầu</option><option value="DANG">Dáng</option><option value="PHONG_CACH">Phong cách</option><option value="CAP">Loại cạp</option>';
		$this->map['status_options'] = '<option value="">Trạng thái</option><option value="SHOW">Hiển thị</option><option value="HIDE">Ẩn</option>';
		$this->parse_layout('edit',$this->map);
	}
}
?>