<?php 
class ThuocTinh extends Module{
	function ThuocTinh($row){
		Module::Module($row);
		if(User::can_view(false,ANY_CATEGORY)){

			require_once 'forms/edit.php';
			$this->add_form(new EditThuocTinhForm());
		}else{
			URL::access_denied();
		}
	}

}
?>