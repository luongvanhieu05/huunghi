<script src="packages/core/includes/js/multi_items.js"></script>
	<span style="display:none">
	<span id="mi_thuoctinh_sample">
		<div id="input_group_#xxxx#" class="multi-item-group">
      <span class="multi-edit-input" style="width:20px;"><input  type="checkbox" id="_checked_#xxxx#" tabindex="-1"></span>
      <span class="multi-edit-input" style="width:40px;"><input  name="mi_thuoctinh[#xxxx#][id]" type="text" id="id_#xxxx#" class="multi-edit-text-input" style="width:40px;text-align:right;" value="(auto)" tabindex="-1"></span>
      <span class="multi-edit-input"><input  name="mi_thuoctinh[#xxxx#][name]" style="width:200px;" class="multi-edit-text-input" type="text" id="name_#xxxx#"></span>
      <span class="multi-edit-input"><select  name="mi_thuoctinh[#xxxx#][type]" style="width:100px;" class="multi-edit-text-input" id="type_#xxxx#">[[|type_options|]]</select></span>
            <span class="multi-edit-input"><select  name="mi_thuoctinh[#xxxx#][status]" style="width:100px;" class="multi-edit-text-input" id="status_#xxxx#">[[|status_options|]]</select></span>

      <span class="multi-edit-input no-border" style="width:20px;"><img src="assets/default/images/buttons/delete.gif" onClick="mi_delete_row(getId('input_group_#xxxx#'),'mi_thuoctinh','#xxxx#','');event.returnValue=false;" style="cursor:pointer;"/></span>
		</div>
    <br clear="all">
	</span>
</span>
<?php
$title = (URL::get('cmd')=='delete')?'Xóa thuộc tính':' Quản lý thuộc tính sản phẩm';?>
<div align="center"><br>
<form name="EditThuocTinhForm" method="post" enctype="multipart/form-data">
<table cellspacing="0" width="100%" class="multi-item-table">
	<tr valign="top" bgcolor="#FFFFFF">
		<td align="left">
        <table cellpadding="15" cellspacing="0" width="100%" border="0" bordercolor="#CCCCCC" class="table-bound">
            <tr>
                <td class="form-title" width="100%"><?php echo $title;?></td>
                <?php if(User::can_add(false,ANY_CATEGORY)){?><td width="1%"><a href="javascript:void(0)" onclick="EditThuocTinhForm.submit();"  class="button-medium-save">Ghi</a></td><?php }?>
                <td><input type="button" value="  Xóa  " onclick="mi_delete_selected_row('mi_thuoctinh');" /></td>
            </tr>
        </table>
		</td>
	</tr>
	<tr valign="top">
	<td>
	<input  name="selected_ids" type="hidden" value="<?php echo URL::get('selected_ids');?>">
	<input  name="deleted_ids" id="deleted_ids" type="hidden" value="<?php echo URL::get('deleted_ids');?>">
	<table width="100%" cellpadding="5" cellspacing="0">
	<?php if(Form::$current->is_error())
	{
	?><tr valign="top">
	<td><?php echo Form::$current->error_messages();?></td>
	</tr>
	<?php
	}
	?>
  <tr valign="top">
		<td>
		<div class="multi-item-wrapper">
      <span id="mi_thuoctinh_all_elems">
        <span style="white-space:nowrap;">
          <span class="multi-edit-input header" style="width:20px;"><input type="checkbox" value="1" onclick="mi_select_all_row('mi_thuoctinh',this.checked);"></span>
          <span class="multi-edit-input header" style="width:40px;">ID</span>
          <span class="multi-edit-input header" style="width:200px;">Tên thuộc tính</a></span>
          <span class="multi-edit-input header" style="width:100px;">Loại</a></span>
          <span class="multi-edit-input header no-border no-bg" style="width:20px;">&nbsp;</span>
          <br clear="all">
        </span>
      </span>
		</div>
    <br clear="all">
		<div style="padding:5px;"><input type="button" value="   Thêm mới   " onclick="mi_add_new_row('mi_thuoctinh');"></div>
		<div>[[|paging|]]</div>
    <hr>
		</td>
	</tr>
	</table>
    <input name="confirm_edit" type="hidden" value="1" />
    <input  name="do" type="hidden" value="<?php echo Url::get('do');?>" />
	</td>
</tr>
</table>
</form>
<script>
mi_init_rows('mi_thuoctinh',<?php if(isset($_REQUEST['mi_thuoctinh'])){echo String::array2js($_REQUEST['mi_thuoctinh']);}else{echo '[]';}?>);
</script>
