<?php
class EditManageDrivingKindForm extends Form
{
	function EditManageDrivingKindForm()
	{
		Form::Form('EditManageDrivingKindForm');
		$languages = DB::select_all('language');
		foreach($languages as $language)
		{
			$this->add('name_'.$language['id'],new TextType(true,'invalid_name_'.$language['id'],0,2000));
		}
		$this->link_css('assets/default/css/cms.css');
		$this->link_css('assets/default/css/tabs/tabpane.css');
	}
	function save_image($file,$id)
	{
		require_once 'packages/core/includes/utils/upload_file.php';
		$dir = substr(PORTAL_ID,1).'/content/';
		update_upload_file('image_url',$dir);
		$row = array();
		if(Url::get('image_url')!='')
		{
			$row +=array('image_url');
		}
		DB::update_id('driving_kind',$row,$id);
	}
	function save_item()
	{
		$rows = array();
		$languages = DB::select_all('language');
		foreach($languages as $language)
		{
			$rows += array('name_'.$language['id']=>Url::get('name_'.$language['id'],1));
			$rows += array('brief_'.$language['id']=>Url::get('brief_'.$language['id'],1));
		}
		$rows+=array('period','point');
		return ($rows);
	}
	function on_submit()
	{
		if($this->check())
		{
			$rows = $this->save_item();
			if(!$this->is_error())
			{
				if(Url::get('cmd')=='edit' and $item = DB::exists_id('driving_kind',Url::get('id')))
				{
					$id = intval(Url::get('id'));
					DB::update_id('driving_kind',$rows,$id);
				}
				else
				{
					$rows += array('time'=>time());
					$id = DB::insert('driving_kind',$rows);
				}
				$this->save_image($_FILES,$id);
				if($id)
				{
					echo '<script>if(confirm("'.Portal::language('update_success_are_you_continous').'")){location="'.Url::build_current(array('cmd'=>'add')).'";}else{location="'.Url::build_current(array('cmd'=>'list','just_edited_id'=>$id)).'";}</script>';
				}
			}
		}
	}
	function draw()
	{
		require_once 'cache/config/status.php';
		require_once Portal::template_js('core').'/tinymce/init_tinyMCE.php';
		$languages = DB::select_all('language');
		if(Url::get('cmd')=='edit' and Url::get('id') and $news = DB::exists_id('driving_kind',intval(Url::get('id'))))
		{
			foreach($news as $key=>$value)
			{
				if(is_string($value) and !isset($_REQUEST[$key]))
				{
					$_REQUEST[$key] = $value;
				}
			}
		}
		$this->parse_layout('edit',array(
			'status_list'=>$status,
			'languages'=>$languages
		));
	}
}
?>
