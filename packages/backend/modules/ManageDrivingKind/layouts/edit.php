<script src="assets/default/css/tabs/tabpane.js" type="text/javascript"></script>
<fieldset id="toolbar">
	<legend>[[.content_manage_system.]]</legend>
 	<div id="toolbar-title">
		[[.manage_driving_kind.]] <span>[ <?php echo Portal::language(Url::get('cmd','list'));?> ]</span>
	</div>
	<div id="toolbar-content" align="right">
	<table>
	  <tbody>
		<tr>
		  <td id="toolbar-preview"  align="center"><a href="<?php echo Url::build_current();?>#"> <span title="Move"> </span> [[.Preview.]] </a> </td>
		  <td id="toolbar-save"  align="center"><a onclick="EditManageDrivingKind.submit();"> <span title="Edit"> </span> [[.Save.]] </a> </td>
		  <td id="toolbar-cancel"  align="center"><a href="<?php echo Url::build_current(array('cmd'=>'list'));?>#"> <span title="New"> </span> [[.Cancel.]] </a> </td>
		  <td id="toolbar-help" align="center"><a href="<?php echo Url::build_current();?>#"> <span title="Help"> </span> [[.Help.]] </a> </td>
		</tr>
	  </tbody>
	</table>
 </fieldset>
  <br clear="all">
<fieldset id="toolbar">
	<?php if(Form::$current->is_error()){echo Form::$current->error_messages();}?>
	<form name="EditManageDrivingKind" id="EditManageDrivingKind" method="post" enctype="multipart/form-data">
		<table cellspacing="4" cellpadding="4" border="0" width="100%" style="background-color:#FFFFFF;">
		<tr>
		  <td valign="top">
		    <table cellpadding="4" cellspacing="0" border="0" width="100%" style="background-color:#F9F9F9;border:1px solid #D5D5D5">
				<tr>
					<td width="17%" align="right">[[.image_url.]]</td>
					<td width="83%"><input name="image_url" type="file" id="image_url" class="file" size="17"><div id="delete_image_url"><?php if(Url::get('image_url') and file_exists(Url::get('image_url'))){?>[<a href="<?php echo Url::get('image_url');?>" target="_blank" style="color:#FF0000">[[.view.]]</a>]&nbsp;[<a href="<?php echo Url::build_current(array('cmd'=>'unlink','link'=>Url::get('image_url')));?>" onclick="jQuery('#delete_image_url').html('');" target="_blank" style="color:#FF0000">[[.delete.]]</a>]<?php }?></div></td>
				</tr>
				<tr>
					<td align="right">[[.period.]]</td>
					<td><input name="period" type="text" id="period"></td>
				</tr>
				<tr>
					<td align="right">[[.point_pass_exam.]]</td>
					<td><input name="point" type="text" id="point"></td>
				</tr>
				<tr>
						<td colspan="2">
						<div class="tab-pane-1" id="tab-pane-category">
						<!--LIST:languages-->
						<div class="tab-page" id="tab-page-category-[[|languages.id|]]">
							<h2 class="tab">[[|languages.name|]]</h2>
							<div class="form_input_label">[[.name_kind.]] (<span class="require">*</span>)</div>
							<div class="form_input">
								 <input name="name_[[|languages.id|]]" type="text" id="name_[[|languages.id|]]" class="input" style="width:60%"  />
							</div>
							<div class="form_input_label">[[.brief.]]</div>
							<div class="form_input">
								<textarea id="brief_[[|languages.id|]]" name="brief_[[|languages.id|]]" cols="75" rows="20" style="width:100%; height:350px;overflow:hidden"><?php echo Url::get('brief_'.[[=languages.id=]],'');?></textarea><br />
								<script>advance_mce('brief_[[|languages.id|]]');</script>
							</div>
						</div>
						<!--/LIST:languages-->
						</div>
						</td>
				   </tr>
				</table>
		  </td>
		</tr>
		</table>
	</form>
</fieldset>