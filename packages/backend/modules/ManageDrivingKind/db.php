<?php
class ManageDrivingKindDB
{
	function get_total_item($cond)
	{
		return DB::fetch(
			'select
				count(*) as acount
			from
				driving_kind
			where
				'.$cond.'
				'
			,'acount');
	}
	function get_items($cond,$order_by,$item_per_page)
	{
		return DB::fetch_all('
			SELECT
				driving_kind.*
				,driving_kind.name_'.Portal::language().' as name
				,driving_kind.brief_'.Portal::language().' as brief
			FROM
				driving_kind
			WHERE
				'.$cond.'
			ORDER BY
				'.$order_by.'
			LIMIT
				'.((page_no()-1)*$item_per_page).','.$item_per_page.'
		');
	}
}
?>
