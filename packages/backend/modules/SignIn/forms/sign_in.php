<?php

class SignInForm extends Form{

	function SignInForm(){

		Form::Form('SignInForm');

		$this->link_js('packages/core/includes/js/jquery/jquery.cookie.js');

		$this->add('user_id',new TextType(true,'invalid_user_id',3,255));

		$this->add('password',new PasswordType(false,'invalid_password'));

		if(Url::check('user_id') and Url::check('password')){

			$this->sign_in();

		}

		$this->link_js('assets/standard/scripts/jquery-1.7.1.min.js');

		//$this->link_css('assets/standard/css/main.css');

	}

	function on_submit(){

		if($this->check() and !User::is_login()){

			$this->sign_in();

		}

	}

	function sign_in(){

		$user = trim(Url::sget('user_id'));

		if(strpos($user,'0',0) === 0){

			$user = (84).substr($user,-(strlen($user) - 1),strlen($user) - 1);

		}

		$password = trim(Url::sget('password'));

		$is_login = false;

		if(!$row=DB::fetch('select account.id,account.password,account.type,account.create_date,account.last_online_time,account.is_active,party.kind,party.email,full_name from account inner join party on party.user_id=account.id where (account.id = "'.$user.'" OR party.email = "'.$user.'") and account.type="USER" and password="'.User::encode_password($password).'"')){

			$this->error('user_id','invalid_username_or_password');

		}else{

			if(!$row['is_active']){

				$this->error('user_not_actived','user_not_actived');

			}else{

				$is_login = true;

			}

		}

		if($is_login){

			$today=getdate();

			$check_date = strtotime($today['year'].'/'.$today['mon'].'/'.$today['mday']);

			Session::set('user_id',$row['id']);

			Session::set('user_data',$row);

			if(Url::get('save_password')){

				setcookie('forgot_user',$row['id'].'_'.Url::get('password'));

			}

			DB::query('update account set last_online_time='.time().' where id="'.Session::get('user_id').'"');

			//Url::redirect('index',array(),REWRITE);

			if(User::can_admin(MODULE_PRODUCTADMIN,false)){

				echo '<script>window.location="quantri";</script>';

			}else{

				echo '<script>window.location="quantri";</script>';//trang-ca-nhan.html

			}

		}

	}

	function draw(){


		if(User::is_login()){

			$this->map = $_SESSION['user_data'];//User::$current->data;

			$layout = 'account_info';

		}else{

			$this->map = array();

			$layout = 'sign_in';

			if(System::check_user_agent()){

				$layout = 'm_sign_in';

				$this->map['abc'] = 1;

			}

		}

		$this->parse_layout($layout,$this->map);

	}

}

?>