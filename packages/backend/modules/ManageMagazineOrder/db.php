<?php
class ManageMagazineOrderDB
{
	function get_total($cond = '1')
	{
		return DB::fetch('
			SELECT
				count(*) as acount
			FROM
				magazine_order
			WHERE
				'.$cond.'
		');
	}
	function get_items($item_per_page,$cond = '1')
	{
		return DB::fetch_all('
			SELECT
				magazine_order.*
			FROM
				magazine_order
			WHERE
				'.$cond.'
			ORDER BY
				magazine_order.checked,magazine_order.time desc
			limit '.((page_no()-1)*$item_per_page).','.$item_per_page.'
		');
	}
}
?>