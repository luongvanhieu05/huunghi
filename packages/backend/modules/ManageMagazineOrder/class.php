<?php
/******************************
COPY RIGHT BY CATBELOVED PORTAL
WRITTEN BY KHOAND
******************************/
class ManageMagazineOrder extends Module
{
	function ManageMagazineOrder($row)
	{
		Module::Module($row);
		require_once 'db.php';
		if(User::can_view(false,ANY_CATEGORY))
		{
			if(Url::get('page') == 'danh-sach-dat-tap-chi')
			{
				switch(Url::get('cmd'))
				{
					case 'delete':
						$this->delete_contact();
						break;
					case 'check':
						$this->check_contact();
						break;
					default:
						require_once 'forms/list.php';
						$this->add_form(new ManageMagazineOrderForm());
						break;
				}
			}
			else
			{
				require_once 'forms/newsletter.php';
				$this->add_form(new ManageNewsletterForm());
			}
		}
		else
		{
			Url::access_denied();
		}
	}
	function delete_contact()
	{
		if(User::can_delete(false,ANY_CATEGORY) and Url::get('id') and $item = DB::exists_id('magazine_order',intval(Url::get('id'))))
		{
			DB::delete('magazine_order','id='.intval(Url::get('id')));
			save_log(Url::get('id'));
		}
		Url::redirect_current();
	}
	function check_contact()
	{
		if(User::can_edit(false,ANY_CATEGORY) and  Url::get('id') and $contact = DB::fetch('select id,checked from magazine_order where id='.Url::iget('id')))
		{
			DB::update_id('magazine_order',array('checked'=>$contact['checked']==0?'1':'0'),$contact['id']);
			Url::redirect_current(array('cmd'=>'success'));
		}
	}
}
?>