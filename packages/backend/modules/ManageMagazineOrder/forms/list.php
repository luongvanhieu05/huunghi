<?php
class ManageMagazineOrderForm extends Form
{
	function ManageMagazineOrderForm()
	{
		Form::Form('ManageMagazineOrderForm');
		$this->link_css('assets/default/css/cms.css');
	}
	function on_submit()
	{
	}
	function draw()
	{
		$this->map = array();
		$item_per_page = 100;
		$cond = ' 1 ';
		if(Url::get('code')){
			$cond .= ' AND magazine_order.id = '.str_replace('TGTT','',Url::get('code')).'';
		}
		if($keyword = Url::get('keyword')){
			$keyword = trim(Url::get('keyword'));
			$cond .= ' AND (
										 magazine_order.description LIKE  "%'.str_replace('TGTT','',$keyword).'%"
									OR magazine_order.full_name LIKE  "%'.$keyword.'%"
									OR magazine_order.email LIKE  "%'.$keyword.'%"
									OR magazine_order.phone LIKE  "%'.$keyword.'%"
									OR magazine_order.address LIKE  "%'.$keyword.'%"
								)';
		}
		if(Url::get('date_from')){
			$cond .= ' AND magazine_order.time >= '.Date_Time::to_time(Url::get('date_from')).'';
		}
			if(Url::get('date_from')){
			$cond .= ' AND magazine_order.time < '.(Date_Time::to_time(Url::get('date_to')) + 24*3600).'';
		}
		$count = ManageMagazineOrderDB::get_total($cond);
		require_once 'packages/core/includes/utils/paging.php';
		$paging = paging($count['acount'],$item_per_page);
		$items = ManageMagazineOrderDB::get_items($item_per_page,$cond);
		$this->parse_layout('list',array(
			'paging'=>$paging,
			'items'=>$items
		));
	}
}
?>