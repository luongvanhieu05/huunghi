<fieldset id="toolbar">
	<div id="toolbar-personal">Danh sách đặt tạp chí</div>
	<div id="toolbar-content" align="right">
	
	</div>
</fieldset>
<br>
<fieldset id="toolbar">
<form name="ContactList" method="post" action="?<?php echo htmlentities($_SERVER['QUERY_STRING']);?>">
	<table cellpadding="0" cellspacing="6" width="100%" class="search-box">
		<tr>
			<td width="100%">
      	Mã đơn hàng:
				<input name="code" type="text" id="code">
        Từ khóa:
				<input name="keyword" type="text" id="keyword"  class="input-large">
				[[.date_from.]] <input name="date_from" type="text" id="date_from"  class="input"/>
				[[.date_to.]] <input name="date_to" type="text" id="date_to"  class="input"/>
				<button onclick="document.ManageCommentForm.submit();">&nbsp;Tìm kiếm&nbsp;</button>
			</td>
			<td nowrap="nowrap">
			</td>
		</tr>
	</table>
<?php $i=1;?>
<!--LIST:items-->
<div style="margin-top:5px;"></div>
	<table width="98%" cellpadding="5" cellspacing="0">
		<tr bgcolor="#EFEFEF">
			<td width="1%" valign="top">
				<img src="assets/default/images/buttons/copy.png" width="16" height="16" /></td>
			<td width="1%" valign="top" style="line-height:18px;"><?php echo $i++;?></td>
			<td width="5%" valign="top" align="left"><?php echo 'TGTT'.str_pad([[=items.id=]],4,'0',STR_PAD_LEFT);?></td>
			<td width="1%" valign="top" style="line-height:18px;">&nbsp;</td>
			<td width="60%" valign="top" style="line-height:18px;">
					<b>[[.full_name.]]:</b>&nbsp;<strong><?php echo ([[=items.gender=]]=='Nam')?'Ông':'Bà';?> [[|items.full_name|]]</strong><br />
					<!--IF:cond([[=items.time=]])--><b>[[.date_send.]]</b> :&nbsp;<?php echo date('H:i d/m/Y',[[=items.time=]]);?> <br />
					<!--/IF:cond-->
					<!--IF:cond([[=items.phone=]])--><b>[[.phone.]]:</b>&nbsp;[[|items.phone|]] <br />
					<!--/IF:cond-->
					<!--IF:cond([[=items.address=]])--><b>[[.address.]]:</b> &nbsp;[[|items.address|]]<br>
					<!--/IF:cond-->
					<!--IF:cond([[=items.email=]])--><b>[[.email.]]:</b> <a href="mailto:[[|items.email|]]">&nbsp;[[|items.email|]]</a><br>
					<!--/IF:cond-->
		  </td>
			<td width="20%" valign="top" align="left" bgcolor="#FFFFCC">[[|items.description|]]</td>
			<td valign="top" nowrap="nowrap" align="right">
				<!--IF:cond([[=items.checked=]]==1)-->
				Đã xem &nbsp; 
				<!--ELSE-->
				<a href="<?php echo URL::build_current(array('cmd'=>'check'));?>&id=[[|items.id|]]" style="color:#F00;">[Chưa xem]</a>&nbsp; 
				<!--/IF:cond-->
				<a href="<?php echo URL::build_current(array('cmd'=>'delete'));?>&id=[[|items.id|]]" onclick="if(!confirm('Bạn có chắc muốn xóa không?')){return false;}" title="Xóa đơn hàng"><img src="assets/default/images/buttons/delete.jpg" /></a>
			</td>
		</tr>
	</table>
<!--/LIST:items-->
</form>
<table width="100%" cellpadding="6" cellspacing="0">
<tr>
	<td>[[|paging|]]&nbsp;</td>
</tr>
</table>
</fieldset>
<script>
	jQuery('#date_from').datepicker();
	jQuery('#date_to').datepicker();	
</script>