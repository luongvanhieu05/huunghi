<?php
/******************************
COPY RIGHT BY NYN PORTAL - TCV
WRITTEN BY thedeath
******************************/
class Publisher extends Module
{
	function Publisher($row)
	{
		if(User::can_admin(MODULE_PUBLISHER,ANY_CATEGORY))
		{
			Module::Module($row);
			require_once 'db.php';
			require_once 'forms/list.php';
			$this->add_form(new PublisherForm());
		}
		else
		{
			Url::access_denied();
		}
	}
}
?>