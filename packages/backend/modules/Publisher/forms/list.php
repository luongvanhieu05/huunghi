<?php
class PublisherForm extends Form
{
	function PublisherForm()
	{
		Form::Form('PublisherForm');
		$this->link_css('assets/default/css/cms.css');
	}
	function save()
	{
		if(Url::get('ids'))
		{
			DB::update('news',array('publish'=>'0'),'id in ('.Url::get('ids').')');
			DB::update('news',array('front_page'=>'0'),'id in ('.Url::get('ids').')');
		}
		foreach($_REQUEST as $key=>$value)
		{
			if(preg_match('/status_([0-9]+)/',$key,$match))
			{
				DB::update_id('news',array('status'=>$value),$match[1]);
			}
			if(preg_match('/front_page_([0-9]+)/',$key,$match1))
			{
				DB::update_id('news',array('front_page'=>1),$match1[1]);
			}
			if(preg_match('/publish_([0-9]+)/',$key,$match2))
			{
				DB::update_id('news',array('publish'=>1),$match2[1]);
			}
		}
	}
	function delete()
	{
		if(isset($_REQUEST['selected_ids']) and  count($_REQUEST['selected_ids'])>0)
		{
			foreach($_REQUEST['selected_ids'] as $key)
			{
				if($item = DB::exists_id('news',$key))
				{
					save_recycle_bin('news',$item);
					DB::delete_id('news',$key);
					save_log($key);
				}
			}
		}
	}
	function on_submit()
	{
		if(Url::get('cmd'))
		{
			switch(Url::get('cmd'))
			{
				case 'delete':
					$this->delete();
					break;
				default:
					$this->save();
					break;
			}
		}
		Url::redirect_current();
	}
	function draw()
	{
		require_once 'packages/core/includes/utils/paging.php';
		require_once 'cache/config/status.php';
		$table = 'news';
		$cond = '1 and news.type="NEWS"';
		$order_by = 'id DESC';
		$item_per_page = 50;
		$count = PublisherDB::GetTotal($table,$cond);
		$paging = paging($count['acount'],$item_per_page,10,false,'page_no',array('type'));
		$items = PublisherDB::GetItems($table,$cond,$order_by,$item_per_page);
		$this->parse_layout('list',array(
			'items'=>$items,
			'paging'=>$paging,
			'total'=>$count['acount'],
			'status'=>$status
		));
	}
}
?>