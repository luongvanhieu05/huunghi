<script>
function make_cmd(cmd)
{
	jQuery('#cmd').val(cmd);
	document.AccountSettingForm.submit();
}
</script><fieldset id="toolbar">
	<legend>[[.config_manage.]]</legend>
	<div id="toolbar-info">[[.Account_setting.]]</div>
	<div id="toolbar-content">
	<table align="right">
	  <tbody>
		<tr>
		<td id="toolbar-save"  align="center"><a onclick="make_cmd('save');"><span title="[[.Save.]"> </span> [[.Save.]] </a> </td>
		
		</tr>
	  </tbody>
	</table>
	</div>
</fieldset>
<div style="height:8px;"></div>
 <fieldset id="toolbar">
<form name="AccountSettingForm" method="post" action="?<?php echo htmlentities($_SERVER['QUERY_STRING']);?>" id="AccountSettingForm" enctype="multipart/form-data">
    <div class="tab-pane-1" id="tab-pane-category">
	<!--IF:cond1(User::can_admin(false,ANY_CATEGORY))-->
    <div class="tab-page" id="tab-page-category-1">
    <h2 class="tab">[[.Front_back_config.]]</h2>
    <div id="front_back_config" class="form_input">
		<br>
			<table  cellpadding="6" cellspacing="0" width="100%" style="#width:99%;margin-top:4px;" border="1" bordercolor="#E7E7E7" align="center">
				<tr style="background-color:#F0F0F0">
					<th width="20%" align="left"><a>[[.Setting_name.]]</a></th>
					<th width="80%" align="left"><a>[[.Value.]]</a></th>
				</tr>
				<tr>
				  <td width="20%" align="left" valign="top" title="site_title">[[.site_title.]]</td>
				  <td width="80%" align="left"><input  name="config_[[|prefix|]]site_title" type="text" id="[[|prefix|]]site_title" value="<?php echo $_REQUEST['config_'.[[=prefix=]].'site_title'];?>" class="input-big-huge"></td>
				  </tr>
				<tr>
					<td width="20%" align="left" valign="top" title="site_name">[[.site_name.]]</td>
					<td width="80%" align="left"><input  name="config_[[|prefix|]]site_name" type="text" id="[[|prefix|]]site_name" value="<?php echo $_REQUEST['config_'.[[=prefix=]].'site_name'];?>" class="input-big-huge"></td>
				</tr>
				<tr>
					<td width="20%" align="left" valign="top" title="site_icon">[[.icon_on_address.]] (*.icon)</td>
					<td width="80%" align="left">
						<input name="config_[[|prefix|]]site_icon" type="file" id="[[|prefix|]]site_icon" class="file">
						<div id="delete_site_icon"><?php if(Url::get('config_'.[[=prefix=]].'site_icon') and file_exists(Url::get('config_'.[[=prefix=]].'site_icon'))){?>[<a href="<?php echo Url::get('config_'.[[=prefix=]].'site_icon');?>" target="_blank" style="color:#FF0000">[[.view.]]</a>]&nbsp;[<a href="<?php echo Url::build_current(array('cmd'=>'unlink','link'=>Url::get('config_'.[[=prefix=]].'site_icon')));?>" onclick="jQuery('#delete_site_icon').html('');" target="_blank" style="color:#FF0000">[[.delete.]]</a>]<?php }?></div>
					</td>
				</tr>
                  <tr>
				    <td align="left" valign="top" title="email_support">Mã giảm giá</td>
				    <td align="left"><input name="config_discount_code" type="text" id="config_discount_code" style="width:500px;" /></td>
			    </tr>
                <tr>
					<td width="20%" align="left" valign="top" title="email_support_online_bac">Email hỗ trợ KH</td>
					<td width="80%" align="left"><input name="config_email_support_online_bac" type="text" id="email_support" class="input-big-huge"></td>
				</tr>
                <tr>
					<td width="20%" align="left" valign="top" title="email_webmaster">[[.email_webmaster.]]</td>
					<td width="80%" align="left"><input name="config_email_webmaster" type="text" id="email_webmaster" class="input-big-huge"></td>
				</tr>
		        <tr>
		          <td align="left" valign="top" title="phone_ban_buon">Số bán buôn</td>
				          <td align="left"><input name="config_phone_ban_buon" type="text" id="phone_ban_buon" class="input-big-huge"></td>
	          </tr>
		        <tr>
				  <td width="20%" align="left" valign="top" title="hot_line">[[.Hot_line.]] ([[.name.]]:[[.Phone_number.]],..) - hot_line</td>
				  <td width="80%" align="left"><input name="config_hot_line" type="text" id="hot_line" class="input-big-huge"></td>
				  </tr>
                <tr bgcolor="#C8FFBB">
                  <td colspan="2" align="left" valign="top"><strong>Mục thông tin liên hệ</strong></td>
                </tr>
                <tr bgcolor="#FFF">
                  <td align="left" valign="top" title="contact_text">Thông tin liên hệ dạng text</td>
                  <td align="left"><textarea name="config_contact_text" type="text" id="contact_text" class="input-big-huge" rows="10"></textarea>
                  </td>
                </tr>
                <tr bgcolor="#FFFFCC">
					<td width="20%" align="left" valign="top" title="company_name_1">Tên tổ chức (Tiếng Việt)</td>
					<td width="80%" align="left"><input name="config_company_name_1" type="text" id="config_company_name" class="input-big-huge"></td>
				</tr>  
                <tr bgcolor="#FFFFCC">
					<td width="20%" align="left" valign="top" title="company_name_3">Tên tổ chức (Tiếng Anh)</td>
					<td width="80%" align="left"><input name="config_company_name_3" type="text" id="company_name_3" class="input-big-huge"></td>
				</tr>
                <!--Dia chi phia bac-->
                <tr bgcolor="#FFCC99">
                  <td align="left" valign="top" title="company_address_1">ĐC trụ sở chính (Tiếng Việt)</td>
                  <td align="left"><input name="config_company_address_1" type="text" id="company_address_1" class="input-big-huge" /></td>
                </tr>
                <tr bgcolor="#FFCC99">
                  <td align="left" valign="top" title="config_company_address_3">ĐC trụ sở chính (Tiếng Anh)</td>
                  <td align="left"><input name="config_company_address_3" type="text" id="company_address_3" class="input-big-huge" /></td>
                </tr>
                <tr bgcolor="#FFCC99">
					<td width="20%" align="left" valign="top" title="company_phone_bac">Điện thoại</td>
					<td width="80%" align="left"><input name="config_company_phone_bac" type="text" id="company_phone_bac" class="input-big-huge"></td>
				</tr>
                <tr bgcolor="#FFCC99">
                  <td align="left" valign="top" title="company_fax">Skype</td>
                  <td align="left"><input name="config_skype" type="text" id="skype" class="input-big-huge"></td>
                </tr>
                <tr bgcolor="#FFCC99">
					<td width="20%" align="left" valign="top" title="company_fax">[[.company_fax.]]</td>
					<td width="80%" align="left"><input name="config_company_fax" type="text" id="company_fax" class="input-big-huge"></td>
				</tr>
                <tr bgcolor="#FFCC99">
					<td width="20%" align="left" valign="top" title="company_email">Email</td>
					<td width="80%" align="left"><input name="config_company_email" type="text" id="company_email" class="input-big-huge"></td>
				</tr>
                <tr bgcolor="#EFEFEF">
                  <td align="left" valign="top" title="company_address_nam_1">ĐC văn phòng Miền Nam (Tiếng Việt)</td>
                  <td align="left"><input name="config_company_address_nam_1" type="text" id="company_address_nam_1" class="input-big-huge" /></td>
                </tr>
                <tr bgcolor="#EFEFEF">
                  <td align="left" valign="top" title="company_address_nam_3">ĐC văn phòng Miền Nam (Tiếng Anh)</td>
                  <td align="left"><input name="config_company_address_nam_3" type="text" id="company_address_nam_3" class="input-big-huge" /></td>
                </tr>
                <tr bgcolor="#EFEFEF">
                  <td width="20%" align="left" valign="top" title="company_phone_nam">Điện thoại VPMN</td>
                  <td width="80%" align="left"><input name="config_company_phone_nam" type="text" id="company_phone_nam" class="input-big-huge"></td>
            </tr>
                <tr bgcolor="#EFEFEF">
                  <td align="left" valign="top" title="company_email_nam">Email VPMN</td>
                  <td align="left"><input name="config_company_email_nam" type="text" id="company_email_nam" class="input-big-huge"></td>
                </tr>
                <tr bgcolor="#EFEFEF">
                  <td align="left" valign="top" bgcolor="#D7FFD9" title="company_address_nn1_1">ĐC văn phòng nước ngoài 1 (Tiếng Việt)</td>
                  <td align="left" bgcolor="#D7FFD9"><input name="config_company_address_nn1_1" type="text" id="company_address_nn1_1" class="input-big-huge" /></td>
            </tr>
                <tr bgcolor="#EFEFEF">
                  <td align="left" valign="top" bgcolor="#D7FFD9" title="company_address_nn1_3">ĐC văn phòng nước ngoài 1 (Tiếng Anh)</td>
                  <td align="left" bgcolor="#D7FFD9"><input name="config_company_address_nn1_3" type="text" id="company_address_nn1_3" class="input-big-huge" /></td>
            </tr>
                <tr bgcolor="#EFEFEF">
                  <td align="left" valign="top" bgcolor="#D7FFD9" title="company_phone_nn1">Điện thoại</td>
                  <td align="left" bgcolor="#D7FFD9"><input name="config_company_phone_nn1" type="text" id="company_phone_nn1" class="input-big-huge"></td>
            </tr>
                <tr bgcolor="#EFEFEF">
                  <td align="left" valign="top" bgcolor="#D7FFD9" title="company_email_nn1">Email </td>
                  <td align="left" bgcolor="#D7FFD9"><input name="config_company_email_nn1" type="text" id="company_email_nn1" class="input-big-huge"></td>
            </tr>
                <tr>
                  <td align="left" valign="top" bgcolor="#C5FCFF" title="company_address_nn2_1">ĐC văn phòng nước ngoài 2 (Tiếng Việt)</td>
                  <td align="left" bgcolor="#C5FCFF"><input name="config_company_address_nn2_1" type="text" id="company_address_nn2_1" class="input-big-huge" /></td>
            </tr>
                <tr>
                  <td align="left" valign="top" bgcolor="#C5FCFF" title="company_address_nn2_3">ĐC văn phòng nước ngoài 2 (Tiếng Anh)</td>
                  <td align="left" bgcolor="#C5FCFF"><input name="config_company_address_nn2_3" type="text" id="company_address_nn2_3" class="input-big-huge" /></td>
            </tr>
                <tr>
                  <td align="left" valign="top" bgcolor="#C5FCFF" title="company_phone_nn2">Điện thoại</td>
                  <td align="left" bgcolor="#C5FCFF"><input name="config_company_phone_nn2" type="text" id="company_phone_nn2" class="input-big-huge"></td>
            </tr>
                <tr>
                  <td align="left" valign="top" bgcolor="#C5FCFF" title="company_email_nn2">Email </td>
                  <td align="left" bgcolor="#C5FCFF"><input name="config_company_email_nn2" type="text" id="company_email_nn2" class="input-big-huge"></td>
            </tr>
                <tr>
                  <td width="20%" align="left" valign="top">&nbsp;</td>
                  <td width="80%" align="left">&nbsp;</td>
            </tr>
			</table>
	</div>
    </div> <!-- End #tab-page-category-1 -->
    <div class="tab-page" id="tab-page-category-2">
    <h2 class="tab">[[.System_config.]]</h2>
	<div id="system_config" class="form_input">
		<br>
			<table  cellpadding="6" cellspacing="0" width="100%" style="#width:99%;margin-top:4px;" border="1" bordercolor="#E7E7E7" align="center">
				<tr style="background-color:#F0F0F0">
				  <th width="20%" align="left"><a>[[.Setting_name.]]</a></th>
				  <th width="80%" align="left"><a>[[.Value.]]</a></th>
				  </tr>
				<tr>
				  <td align="left" title="use_cache">Kích hoạt site</td>
				  <td align="left"><select name="config_is_active" id="config_is_active"></select>
				    <script>jQuery('#config_is_active').val(<?php echo Url::get('config_is_active',0)?>);</script></td>
				  </tr>
				<tr>
				  <td width="20%" align="left" title="portal_template">Thông báo dừng site</td>
				  <td width="80%" align="left"><input name="config_notification_when_interrption" type="text" id="notification_when_interrption" class="input-big-huge"></td>
				  </tr>
				<tr>
					<td width="20%" align="left" title="size_upload">[[.size_upload.]] /(1[[.times.]] [[.upload.]])</td>
					<td width="80%" align="left"><input name="config_size_upload" type="text" id="size_upload" class="input-large"></td>
				</tr>
				<tr>
					<td width="20%" align="left" title="type_image_upload">[[.type_image_upload.]]</td>
					<td width="80%" align="left"><input name="config_type_image_upload" type="text" id="type_image_upload" class="input-big-huge"></td>
				</tr>
				<tr>
					<td width="20%" align="left" title="type_file_upload">[[.type_file_upload.]]</td>
					<td width="80%" align="left"><input name="config_type_file_upload" type="text" id="type_file_upload" class="input-big-huge"></td>
				</tr>
				<tr>
					<td width="20%" align="left" title="use_cache">[[.Use_cache.]]</td>
					<td width="80%" align="left">
						<select  name="config_use_cache" class="select" id="use_cache"></select>
						<script>jQuery('#use_cache').val(<?php echo Url::get('config_use_cache',0)?>);</script>
					</td>
				</tr>
				<tr>
					<td valign="top"  align="left" title="rewrite">[[.rewrite_url.]]</td>
					<td  align="left"><select  name="config_rewrite" id="rewrite" class="select"></select>
					<script>jQuery('#rewrite').val(<?php echo Url::get('config_rewrite',0)?>);</script>
					</td>
				</tr>
				<tr>
					<td valign="top"  align="left" title="use_double_click">[[.use_double_click.]]</td>
					<td  align="left"><select  name="config_use_double_click" class="select" id="use_double_click"></select>
					<script>jQuery('#use_double_click').val(<?php echo Url::get('config_use_double_click',0)?>);</script>
					</td>
				</tr>
				<tr>
					<td valign="top"  align="left" title="use_log">[[.use_log.]]</td>
					<td  align="left"><select  name="config_use_log" class="select" id="use_log"></select>
					<script>jQuery('#use_log').val(<?php echo Url::get('config_use_log',0)?>);</script>
					</td>
				</tr>
				<tr>
					<td valign="top"  align="left" title="use_recycle_bin">[[.use_recycle_bin.]]</td>
					<td  align="left"><select  name="config_use_recycle_bin" class="select" id="use_recycle_bin"></select>
					<script>jQuery('#use_recycle_bin').val(<?php echo Url::get('config_use_recycle_bin',0)?>);</script>
					</td>
				</tr>
			</table>
	</div>
    </div> <!-- End #tab-page-category-2 -->
	<!--/IF:cond1-->
</div>
<input name="cmd" type="hidden" id="cmd" value="save">
</form>
<table width="100%" cellpadding="6" cellspacing="0" style="background-color:#F0F0F0;border:1px solid #E7E7E7;height:8px;#width:99%" align="center">
	<tr>
		<td align="right">&nbsp;</td>
	</tr>
</table>
<div style="#height:8px;"></div>
</fieldset>
