<?php
class SettingForm extends Form
{
	function SettingForm()
	{
		Form::Form('SettingForm');
		$this->link_css('assets/default/css/cms.css');
		$this->link_css('assets/default/css/tabs/tabpane.css');
		$this->link_js('assets/default/css/tabs/tabpane.js');
	}
	function on_submit()
	{
		if(Url::get('cmd')=='seo')
		{
			$languages = DB::select_all('language');
			foreach($languages as $key=>$value)
			{		
				Portal::set_setting('website_keywords_'.$key,Url::get('website_keywords_'.$key),false,'PORTAL');
				Portal::set_setting('website_description_'.$key,Url::get('website_description_'.$key),false,'PORTAL');
			}
			Portal::set_setting('google_analytics',Url::get('google_analytics'),false,'PORTAL');
			Portal::set_setting('auto_link',Url::get('auto_link'),false,'PORTAL');
			Session::delete('portal');
			Url::redirect_current(array('cmd'=>'seo'));
		}
	}
	function draw()
	{
		$languages = DB::select_all('language');
		foreach($languages as $key=>$value)
		{
			$_REQUEST['website_keywords_'.$key] = Portal::get_setting('website_keywords_'.$key,'');
			$_REQUEST['website_description_'.$key] = Portal::get_setting('website_description_'.$key,'');
		}
		$this->map['google_analytics'] = Portal::get_setting('google_analytics','');
		$this->map['auto_link'] = Portal::get_setting('auto_link','');
		$this->map['languages'] = $languages;
		$this->map['prefix'] = PREFIX;
		$this->parse_layout('list',$this->map);
	}
}
?>