<fieldset id="toolbar">
 	<div id="toolbar-personal">
		[[.user_admin.]] <span style="font-size:16px;color:#0B55C4;">[ <?php echo Portal::language(Url::get('cmd','list'));?> ]</span>
	</div>
	<div id="toolbar-content">
	<table align="right">
	  <tbody>
		<tr>
		  <td id="toolbar-save"  align="center"><a onclick="EditUserAdminForm.submit();"> <span title="Edit"> </span> [[.Save.]] </a> </td>
		  <td id="toolbar-cancel"  align="center"><a href="<?php echo Url::build_current(array('cmd'=>'list'));?>#"> <span title="[[.Cancel.]]"> </span> [[.Cancel.]] </a> </td>
		</tr>
	  </tbody>
	</table>
	</div>
</fieldset>
<div style="height:6px;"></div>
<fieldset id="toolbar">
	<legend>[[.information.]]</legend>
	<?php if(Form::$current->is_error()){echo Form::$current->error_messages();}?>
	<form name="EditUserAdminForm" method="post" >
		<input type="hidden" name="privilege_deleted_ids" value=""/>
		<input type="hidden" name="group_deleted_ids" value=""/>
		<table cellpadding="4" cellspacing="0" border="0" width="100%">
			<tr>
				<td align="right">[[.user_name.]]</td>
				<td><input name="id" type="text" id="id" class="input-large" /></td>
				<td align="right">[[.password.]]</td>
				<td><input name="password" type="text" id="password" class="input-large" /> </td>
			</tr>
			<tr>
				<td align="right">[[.email.]]</td>
				<td><input name="email" type="text" id="email" class="input-large"/></td>
				<td align="right">[[.full_name.]]</td>
				<td><input name="full_name" type="text" id="full_name" class="input-large"/></td>
			</tr>
			<tr>
				<td align="right">[[.gender.]]</td>
				<td><select name="gender" id="gender" class="select"></select></td>
				<td align="right">[[.active.]]</td>
				<td><input name="active" id="active" type="checkbox" value="1" <?php echo (URL::get('active')?'checked':'');?> /></td>
			</tr>
			<tr>
				<td align="right">[[.block.]]</td>
				<td><input name="block" id="block" type="checkbox" value="1" <?php echo (URL::get('block')?'checked':'');?>/></td>
				<td align="right">[[.birth_date.]]</td>
				<td><input name="birth_date" type="text" id="birth_date" class="input"></td>
			</tr>
			<tr>
				<td align="right">[[.address.]]</td>
				<td><input name="address" type="text" id="address" class="input-large"/></td>
				<td align="right">[[.phone_number.]]</td>
				<td><input name="phone" type="text" id="phone" class="input"/></td>
			</tr>
			<tr>
				<td align="right">[[.zone_id.]]</td>
				<td><select name="zone_id" id="zone_id" class="select-large"></select></td>
				<td align="right">[[.join_date.]]</td>
				<td><input name="join_date" type="text" id="join_date" class="input"></td>
			</tr>
			<tr>
		</table>
		<input type="hidden" value="1" name="confirm_edit" >
	</form>
</fieldset>