<?php
/*---
WRITTEN BY :THEDEATH
---*/
class UserAdmin extends Module
{
	function UserAdmin($row)
	{
		if(User::can_admin(MODULE_USERADMIN,ANY_CATEGORY))
		{
			Module::Module($row);
			switch(URL::get('cmd'))
			{
				case 'edit':
				case 'add':
					require_once 'forms/edit.php';
					$this->add_form(new EditUserAdminForm());
					break;
				default:
					require_once 'forms/list.php';
					$this->add_form(new ListUserAdminForm());
					break;
			}
		}
		else
		{
			URL::access_denied();
		}
	}
}
?>
