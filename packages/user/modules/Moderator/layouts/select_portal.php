<script>
var data = <?php echo String::array2suggest([[=users=]]);?>;
jQuery(document).ready(function(){
	jQuery("#account_id").autocomplete(data,{
		minChars: 0,
		width: 145,
		matchContains: true,
		autoFill: false,
		formatItem: function(row, i, max) {
			return '<span style="color:#993300"> ' + row.name + '</span>';
		},
		formatMatch: function(row, i, max) {
			return row.id + ' ' + row.name;
		},
		formatResult: function(row) {
			return row.id;
		}
	});
});
function check_selected()
{
	var status = false;
	jQuery('form :checkbox').each(function(e){
		if(this.checked)
		{
			status = true;
		}
	});
	return status;
}
function make_cmd(cmd)
{
	jQuery('#cmd').val(cmd);
	document.ListModeratorForm.submit();
}
</script>
<fieldset id="toolbar">
	<legend>[[.System_manage.]]</legend>
	<div id="toolbar-info">
		[[.grant_privilege.]] <span style="font-size:16px;color:#0B55C4;">[ <?php echo Portal::language(Url::get('cmd','list'));?> ]</span>
	</div>
	<div id="toolbar-content" align="right">
	<table>
	  <tbody>
		<tr>
		  <?php if(User::can_edit(false,ANY_CATEGORY)){?> <td id="toolbar-save"  align="center"><a onclick="GrantPrivilege.submit();"> <span title="Save"> </span> [[.Save.]] </a> </td><?php }?>
		  <?php if(User::can_view(false,ANY_CATEGORY)){?> <td id="toolbar-list"  align="center"><a href="<?php echo Url::build_current();?>#"> <span title="[[.List.]]"> </span> [[.List.]] </a> </td><?php }?>
		  <td id="toolbar-help" align="center"><a href="<?php echo Url::build_current();?>#"> <span title="Help"> </span> [[.Help.]] </a> </td>
		</tr>
	  </tbody>
	</table>
	</div>
</fieldset>
<div style="height:8px;"></div>
<fieldset id="toolbar">
<a name="top_anchor"></a>
<?php if(Form::$current->is_error()){echo Form::$current->error_messages();}?>
<form name="GrantPrivilege" method="post">
<table cellpadding="6" cellspacing="0" width="100%" style="#width:99%;margin-top:8px;" border="1" bordercolor="#E7E7E7" align="center">
	<tr bgcolor="#EFEFEF" valign="top">
    <th width="16%" align="left"><a>[[.portal_id.]]</a></th>
    <th width="17%"  align="left"><a>[[.account_id.]]</a></th>
    <th width="26%"  align="left"><a>[[.Grant.]]</a></th>
    <th width="26%"  align="left"><a>[[.category.]]</a></th>
  </tr>
  <tr style="padding:10px">
    <td width="16%" valign="top"><input name="portal_id" type="text" id="portal_id" size="20" readonly/></td>
    <td width="17%" valign="top"><input name="account_id" type="text" id="account_id" size="20"></td>
    <td width="26%" valign="top">
		<!--LIST:privilege-->
			<div style="line-height:20px;">
				<div style="float:left"><input name="privilege_id[]" type="checkbox" value="[[|privilege.id|]]" id="privilege_id_[[|privilege.id|]]"></div>
				<div>&nbsp;[[|privilege.title|]]</div>
			</div>
			<div style="clear:both"></div>
		<!--/LIST:privilege-->
		<script>
			<?php if(Url::get('privilege_id')){?>
				jQuery('#privilege_id_<?php echo Url::get('privilege_id');?>').attr('checked', true);
			<?php }?>
		</script>
     </td>
	 <td valign="top">
       <select name="categories[]" <?php if(!URL::get('id')) echo ' size="20" multiple="multiple"';?> style="width:300px;overflow:auto" id="categories[]"></select>
	</td>
  </tr>
</table>
 <table width="100%" cellpadding="6" cellspacing="0" style="background-color:#F0F0F0;border:1px solid #E7E7E7;height:8px;#width:99%" align="center">
 	<tr>
		<td>&nbsp;</td>
	</tr>
</table>
</form>
</fieldset>