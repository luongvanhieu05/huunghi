<?php
class PersonalInformationForm extends Form
{
	function PersonalInformationForm()
	{
		Form::Form('personal');
		$this->add('full_name',new TextType(true,'invalid_full_name',0,50));
		$this->add('address',new TextType(false,'invalid_address',0,200));
		$this->add('phone',new PhoneType(false,'invalid_phone_number'));
		$this->add('birth_date',new DateType(false,false,0,32));
		$this->add('email',new EmailType(false,'email_invalid'));
		$this->link_css('assets/default/css/cms.css');
		$this->link_js('packages/core/includes/js/jquery/datepicker.js');
		$this->link_css('assets/default/css/jquery/datepicker.css');
		
	}
	function on_submit()
	{
		if($this->check())
		{
			$row = array(
				'full_name'
				,'address'
				,'birth_date'=>Date_Time::to_sql_date(Url::get('birth_date'))
				,'identity_card'
				,'gender'
				,'phone'
				,'zone_id'
				,'fax'
				,'yahoo'
				,'skype'
				,'email'
				,'website'
			);
			DB::update('party',$row,' user_id = "'.Session::get('user_id').'"');
			require_once 'packages/core/includes/utils/upload_file.php';
			$dir = substr(PORTAL_ID,1).'/icon/';
			update_upload_file('image_url',$dir);
			if(Url::get('image_url')!='')
			{
				$row = DB::fetch('select id,image_url from party where user_id="'.Session::get('user_id').'"');
				@unlink($row['image_url']);
				DB::update('party',array('image_url'),' user_id = "'.Session::get('user_id').'"');
			}
			Url::redirect_current(array('action'=>'succesful'));
		}
	}
	function draw()
	{
		$sql = '
			SELECT
				party.*
				,`account`.last_online_time
				,`account`.`create_date` as create_date
			FROM
				`account`
				inner join party on party.user_id=account.id
			WHERE
				`account`.id="'.Session::get('user_id').'"
				 and party.type="USER"';
   	    $row = array();
		if($row = DB::fetch($sql))
		{
			$row['birth_date'] = Date_Time::to_common_date($row['birth_date']);
			foreach($row as $key=>$value)
			{
				if(is_string($value) and !isset($_REQUEST[$key]))
				{
					$_REQUEST[$key] = $value;
				}
			}
		}
		require_once 'cache/tables/zone.cache.php';
		$this->parse_layout('information',array(
			'gender_list'=>array('1'=>Portal::language('male'),'0'=>Portal::language('female'))
			,'zone_id_list'=>String::get_list($zone)
		));
	}
}
?>
