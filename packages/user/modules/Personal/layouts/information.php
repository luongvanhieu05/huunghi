<fieldset id="toolbar">
	<div id="toolbar-normal">
		<img src="<?php if(Url::get('image_url') and file_exists(Url::get('image_url'))){ echo Url::get('image_url');}?>" onerror="this.src='assets/default/images/cms/header/icon-48-user.png'" style="border:1px solid #CCCCCC;width:50px;padding:1px;">
			THÔNG TIN TÀI KHOẢN
	</div>
	<div id="toolbar-content">
	<table align="right">
	  <tbody>
		<tr>
		<td id="toolbar-save"  align="center"><a onclick="document.EditUser.submit();"> <span title="[[.Save.]]"> </span> Ghi lại </a> </td>
		<td id="toolbar-param"  align="center"><a href="<?php echo Url::build_current(array('cmd'=>'change_pass'));?>" title="[[.Change_pass.]]"> <span></span>Đổi MK</a></tr>
	  </tbody>
	</table>
	</div>
</fieldset>
<div style="height:6px;"></div>
<fieldset id="toolbar">
	<legend><b>[[.change_information_user.]]&nbsp;:&nbsp;<a><?php echo Session::get('user_id');?></a></b></legend>
<?php if(Form::$current->is_error()){echo Form::$current->error_messages();}?>
	<form name="EditUser" method="post" id="EditUser" enctype="multipart/form-data">
		<table cellpadding="4" cellspacing="0" width="100%" align="center">
				<tr>
					<td width="32%" align="right">[[.full_name.]]</td>
				  <td width="68%"><input name="full_name" type="text" id="full_name" class="input-huge"></td>
				</tr>
				<tr>
					<td width="32%" align="right">[[.address.]]</td>
					<td width="68%"><input name="address" type="text" id="address" class="input-huge"></td>
				</tr>
				<tr>
					<td width="32%" align="right">[[.zone.]]</td>
					<td width="68%"><select name="zone_id" class="select-large" id="zone_id"></select></td>
				</tr>
				<tr>
					<td width="32%" align="right">[[.gender.]]</td>
					<td width="68%"><select name="gender" class="select" id="gender"></select></td>
				</tr>
				<tr>
					<td width="32%" align="right">[[.birth_date.]]</td>
					<td width="68%"><input name="birth_date" type="text" id="birth_date" class="input">
					  dd/mm/yyyy</td>
				</tr>
				<tr>
					<td width="32%" align="right">[[.identity_card.]]</td>
					<td width="68%"><input name="identity_card" type="text" id="identity_card" class="input" maxlength="9"></td>
				</tr>
				<tr>
					<td width="32%" align="right">[[.phone_number.]]</td>
					<td width="68%"><input name="phone" type="text" id="phone" class="input-huge"></td>
				</tr>
				<tr>
					<td width="32%" align="right">[[.fax.]]</td>
					<td width="68%"><input name="fax" type="text" id="fax" class="input-huge"></td>
				</tr>
				<tr>
					<td width="32%" align="right">[[.skype.]]</td>
					<td width="68%"><input name="skype" type="text" id="skype" class="input-huge"></td>
				</tr>
				<tr>
					<td width="32%" align="right">[[.email.]]</td>
					<td width="68%"><input name="email" type="text" id="email" class="input-huge"></td>
				</tr>
				<tr style="display:none;">
					<td width="32%" align="right">[[.openid.]]</td>
					<td width="68%"><input name="openid" type="text" id="openid" class="input-huge" readonly></td>
				</tr>
				<tr>
					<td width="32%" align="right">[[.website.]]</td>
					<td width="68%"><input name="website" type="text" id="website" class="input-huge"></td>
				</tr>
				<tr>
					<td width="32%" align="right" valign="top">[[.avatar.]]</td>
				  <td width="68%"><input name="image_url" type="file" id="image_url">
				  &nbsp;<span class="require">100x100 pixel (*.jpg, *.jpeg, *.gif)</span> </td>
				</tr>
		</table>
	</form>
<div style="#height:8px;"></div>
</fieldset>
<script>
	jQuery('#birth_date').datepicker();
</script>