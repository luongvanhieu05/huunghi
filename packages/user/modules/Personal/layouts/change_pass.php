<fieldset id="toolbar">
	<div id="toolbar-title">[[.Change_password.]]</div>
	<div id="toolbar-content" align="right">
	<table align="right">
	  <tbody>
		<tr>
		<td id="toolbar-save"  align="center"><a onclick="document.ChangePassword.submit();"> <span title="[[.Save.]]"> </span> Ghi lại </a> </td>
		<td id="toolbar-config"  align="center"><a href="<?php echo Url::build_current();?>"> <span title="[[.information.]]"></span>[[.Information.]]</a> </td>
		</tr>
	  </tbody>
	</table>
	</div>
</fieldset>
<div style="height:6px;"></div>
<fieldset id="toolbar">
	<?php if(Form::$current->is_error()){echo Form::$current->error_messages();}?>
	<form method="post" name="ChangePassword" action="?<?php echo htmlentities($_SERVER['QUERY_STRING']);?>" id="ChangePassword">
		<table cellpadding="4" cellspacing="0" width="100%" style="#width:99%;margin-top:8px;" border="0" bordercolor="#E7E7E7" align="center">
			 <tr align="center">
				<td colspan="3" style="height:10px"></td>
			</tr>
			<tr class="change_pass_text">
				<td align="right" class="change_pass_text">[[.old_password.]]</td>
				<td align=left><input name="old_password" type="password" id="old_password" class="input-large"></td>
			</tr>
			<tr class="change_pass_text">
				<td width=37% align="right" class="change_pass_text">[[.new_password.]]</td>
				<td width=63% align=left><input name="new_password" type="password" id="new_password" class="input-large"></td>
			</tr>
			<tr class="change_pass_text">
				<td width=37% align="right">[[.retype_new_password.]]</td>
				<td width=63% align=left><input name="retype_new_password" type="password" id="retype_new_password" class="input-large"></td>
			</tr>
			 <tr align="center">
				<td colspan="3" style="height:10px"></td>
			</tr>
		</table>
	</form>
</fieldset>
