<div class="content">
    <div class="list">
        <div class="title"><h2>[[.Services.]]:</h2></div><!--End .title-->
        <ul>
        	<!--LIST:items-->
            <li>
                <a href="<?php echo Url::build([[=items.category_name=]],array('name_id'=>[[=items.name_id=]]),REWRITE);?>">
                    <img src="[[|items.image_url|]]"/>
                    <h2>[[|items.name|]]</h2>
                    <p><?php echo strip_tags([[=items.brief=]]);?></p>
                </a>
            </li>
            <!--/LIST:items-->
        </ul>
    </div><!--End .list-->
</div>    
