<?php
class HomeSolutionsDB{
	function get_HomeSolutions($cond)
	{
		return DB::fetch_all('
			SELECT
				news.id, news.name_'.Portal::language().' as name, news.name_id,
				news.small_thumb_url,news.image_url,
				news.brief_'.Portal::language().' as brief,
				category.name_id as category_name
			FROM
				news
				inner join category on category.id = news.category_id
			WHERE
				'.$cond.'
			LIMIT
				0,4
		');
	}
}
?>