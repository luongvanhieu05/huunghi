<?php
class HomeSolutionsForm extends Form
{
	function HomeSolutionsForm()
	{
		Form::Form('HomeSolutionsForm');
		$this->link_css(Portal::template().'css/news.css');
		$this->link_js('packages/core/includes/js/jquery/jCarousel.js');
	}
	function draw()
	{
		$this->map = array();
		$category_id = 582;
		if($category = DB::exists_id('category',$category_id))
		{
			$cond = 'news.status<>"HIDE" and news.portal_id = "'.PORTAL_ID.'" and publish
						and '.IDStructure::child_cond($category['structure_id']).'
					';
			$this->map['category_brief'] = $category['brief_'.Portal::language()];
			$this->map['items'] = HomeSolutionsDB::get_HomeSolutions($cond);
		}
		$this->parse_layout('list',$this->map);
	}
}
?>