<div id="newsletter" class="newsletter-bound">
	<form id="newsletterForm" name="newsletter-register" method="post">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
	            <td class="newsletter-title">[[.register_receive_news.]]</td>
                <td width="1%" nowrap="nowrap"><input name="email" type="text" id="email" value="[[.email.]]" onfocus="this.value = ''" class="newsletter-text" /></td>
                <td width="30px" nowrap="nowrap" align="right"><input type="button" id="send" class="newsletter-button" value="[[.send.]]" /></td>
            </tr>
        </table>
	</form>
</div>
<script>
jQuery(document).ready(function(){
	jQuery('#send').click(function(){
		var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if(jQuery('#email').val()==''){
			alert('[[.email_requirement.]]');
		}else{
			if(!jQuery('#email').val().match(filter)){
				alert('[[.email_invalid.]]');
			}else{
				alert('[[.thank_you_send_news_letter.]]');
				jQuery('form').submit();
			}
		}
	});
});
</script>