<?php
class NewsLetterForm extends Form
{
	function NewsLetterForm()
	{
		Form::Form('NewsLetterForm');
		$this->link_css(Portal::template('anshop').'/css/home.css');
	}
	function on_submit(){
		$filename = 'upload/'.str_replace('#','',PORTAL_ID).'/newsletter.txt';
		if($filename and file_get_contents($filename)) $somecontent = ','.Url::get('email');
		else $somecontent = Url::get('email');
		$content = file_get_contents($filename);
		$pos = strpos($content,Url::get('email'));
		if($pos===false) $mode = 1;
		else $mode = 0;
		if($mode){
			if (!$handle = fopen($filename, 'a')) {
				echo "Cannot open file ($filename)";
				exit;
			}
			if (fwrite($handle, $somecontent) === FALSE) {
				echo "Cannot write to file ($filename)";
				exit;
			}
			fclose($handle);
		}
	}
	function draw(){
		$this->map = array();
		$this->parse_layout('list',$this->map);
	}
}
?>