<div class="legal-bound">
	<div class="legal-search-bound">
    	<div class="legal-search-image"></div>
      <form method="post" name="Legal" action="<?php substr($_SERVER['REQUEST_URI'],1); ?>">
             <div class="legal-search-input">
                <table cellpadding="0" cellspacing="0">
                    <tr valign="top">
                        <td class="search-input-td">
                        	<span style="font-weight:bold; line-height:30px;">[[.name_text_search.]]</span>
                        </td>
                        <td><input style="width:130px; height:23px; border:1px solid #abadb3;" name="keywords" id="keywords" type="text" onblur="if(this.value=='') {this.value='[[.key_word.]]';jQuery(this).css('color','#515151');}else { jQuery(this).css('color','red');}" onfocus="if(this.value=='[[.key_word.]]'){this.value='';}" value="[[.key_word.]]"  />
                        </td>
                    </tr>
                    <tr valign="top">
                        <td class="search-input-td">
                        	<span style="font-weight:bold; line-height:20px;">[[.published_date.]]</span>
                        </td>
                        <td>
                        <input name="published_date" style="width:50px;" type="text" id="published_date" />                        </td>
                    </tr>
                    <tr valign="top">
                        <td class="search-input-td">
                        	<span style="font-weight:bold; line-height:20px;">[[.type_text.]]</span>
                        </td>
                        <td>
                        	<select name="type_text" id="type_text" style=" border:1px solid #abadb3; color:#515151;"  onchange="jQuery(this).css('color','red');"><option style="display:none; color:#999"></option></select>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td class="search-input-td">
                        	<span style="font-weight:bold; line-height:20px;">[[.agency.]]</span>
                        </td>
                        <td>
                        	<select name="agency" id="agency" style=" border:1px solid #abadb3; color:#515151;"  onchange="jQuery(this).css('color','red');"></select>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="legal-search-button">
            <input class="legal-search-submit" type="submit" name="submit" id="submit" value="[[.search.]]"  onclick="if(check_require()){return true;}else{return false;}" />
            </div>
			</form>
    </div>
    <!--NEU KHONG TIM DUOC TIN NAO-->
    <!--IF:quyet( isset([[=warning=]]))-->
    <div style="text-align: center;color: red;font-weight: bold;font-size: 15px;">[[.dont_have_any.]]</div>
    <!--/IF:quyet-->
    <!--LIST:category-->
	<!--IF:cond(isset([[=category.items=]]) and count([[=category.items=]])>0)-->
    <div class="legal-content-bound">
    	<div class="legal-title">
        	<div class="fr-ht-b-left"><span>[[|category.name|]]</span></div><div class="fr-ht-b-right">
            </div>
        </div>
        <?php $vitri=1;?>
        <!--LIST:category.items-->
        <div class="legal-content">
        	<div class="legal-content-title">
            	<div class="legal-content-title-name"><a href="<?php echo Url::build('xem-trang-tin',array('name_id'=>[[=category.items.name_id=]]),REWRITE)?>">[[|category.items.name|]]</a></div>
                <!--IF:cond(file_exists([[=category.items.file=]]))-->
            	<div class="legal-content-title-download">
                	<a href="[[|category.items.file|]]">
                    	<div style="float:left;"><img src="assets/hotel/images/download.jpg" border="0"  /></div>
                        <div style="margin-top:0px; float:left;">Download</div>
                    </a>
                </div>
                <!--ELSE-->
                <!--/IF:cond-->
            </div>
            <div class="legal-content-brief">
                <?php echo String::display_sort_title([[=category.items.brief=]],40);?>
            </div>
            <?php if($vitri == 3 ){echo '';} else {echo '<hr color="#dfdfdf" size="1" />';}?><?php $vitri ++;?>
        </div>
        <!--/LIST:category.items-->
        <div class="legal-content-detail"><a href="<?php echo Url::build('danh-sach-tin',array('name_id'=>[[=category.name_id=]]),REWRITE)?>">[[.view_all.]] >></a></div>
    </div>
    <!--ELSE-->
    <!--/IF:cond-->
	<!--/LIST:category-->
</div>
<!--LIST:items-->
	<div class="search-result-item">
		<div class="search-name" valign="top"><a href="[[|items.href|]]" target="_blank">[[|items.name_search|]]</a></div>
	</div>
<!--/LIST:items-->
<!--Datepicker-->
<script type="text/javascript">
	jQuery(function() {
		jQuery( "#published_date" ).datepicker();
	});
</script>
