<?php
class LegalDB
{
	function get_new_s($cond_s)
	{
		$items = DB::fetch_all('
		SELECT
			*,
			name_id,
			name_'.Portal::language().' as name
		FROM
			category
		WHERE
			'.$cond_s.'
		');
		return ($items);
	}
	function get_newss($conds)
	{
		$items = DB::fetch_all('
		SELECT
			*,name_'.Portal::language().' as name
		FROM
			category
		WHERE
			'.$conds.'
		');
		return ($items);
	}
	function get_news($cond,$limit='0,3')
	{
		$sql ='
			SELECT
				id,
				name_'.Portal::language().' as name,
				brief_'.Portal::language().' as brief,
				description_'.Portal::language().' as description,
				image_url,
				small_thumb_url,
				name_id,
                file
			FROM
				news
			WHERE
				'.$cond.'
			ORDER BY
				position DESC, id DESC
			LIMIT
				'.$limit.'
		';
		return DB::fetch_all($sql);
	}
	function get_category($cond)
	{
		$sql='
			SELECT
				id,
				name_'.Portal::language().' as name,
				name_id,
				url,
				structure_id,
                image_url
			FROM
				category
			WHERE
				'.$cond.'
			ORDER BY
				structure_id
			';
		$items = DB::fetch_all($sql);
		return $items;
	}
	function get_categoryx()
	{
		return DB::fetch_all('
			SELECT
				category.id,category.name_'.Portal::language().',category.structure_id ,category.status
			FROM
				category
			WHERE
				portal_id="'.PORTAL_ID.'" and category.status<>"HIDE"
			ORDER BY
				 structure_id
		');
	}
	function get_count($cond,$type)
	{
		return DB::fetch('
			SELECT
				 count(*) as acount
			FROM
				'.$type.'
				INNER JOIN category ON category.id = '.$type.'.category_id
			WHERE
				'.$cond.'
		');
	}
	function get_all($cond,$item_per_page,$type)
	{
		return DB::fetch_all('SELECT
				'.$type.'.id,
				'.$type.'.name_id,
				'.$type.'.name_'.Portal::language().' as name
				,'.$type.'.brief_'.Portal::language().' as brief
				,'.$type.'.category_id
				,'.$type.'.time
				,category.name_'.Portal::language().' as category
			FROM
				'.$type.'
				INNER JOIN category ON category.id = '.$type.'.category_id
			WHERE
				'.$cond.'
			ORDER BY
				'.(URL::get('order_by')?' '.URL::get('order_by').'':' '.$type.'.time').' '.(URL::get('order_by_cond')?' '.URL::get('order_by_cond').' item.time desc':' desc').'
			LIMIT
				'.((page_no()-1)*$item_per_page).','.$item_per_page.'
		');
	}
}
?>