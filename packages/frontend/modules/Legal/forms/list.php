<?php
class LegalForm extends Form
{
	function LegalForm()
	{
		Form::Form('LegalForm');
		$this->link_css(Portal::template('hotel').'/css/legal.css');
        $this->link_css('assets/default/css/jquery/datepicker.css');
        $this->link_js('packages/core/includes/js/jquery/datepicker.js');
	}
	function draw()
	{
       require_once 'cache/config/type_text.php';
	   //System::debug($_REQUEST);
		////////////////////////////////////////
        //echo convert_utf8_to_telex(Url::get('keywords'));
		$this->map = array();
		$cond_s=IDStructure::direct_child_cond( DB::structure_id('category','523') );
		$conds=IDStructure::direct_child_cond( DB::structure_id('category','528') );
		$new_s = LegalDB::get_new_s($cond_s);
		$newss = LegalDB::get_newss($conds);
		$this->map['agency_list']= array(''=>Portal::language('select_all')) + String::get_list($new_s,'name','true','0','');
        $this->map['type_text_list']= array(''=>Portal::language('select_all')) + $type_text;
		$cond='type="NEWS" and status!="HOME" and portal_id="'.PORTAL_ID.'" and '.IDStructure::direct_child_cond(DB::structure_id('category','523')).' ';
        //Van ban phap ly
        //if (Url::get('agency')!=''){ $cond.=' AND category.id='.Url::get('agency'); }
		$this->map['category'] =LegalDB::get_category($cond);
        /////////////////////////////////////////////////////////
        // NVQ
        //
        $cond='';
		if( convert_utf8_to_telex(Url::get('keywords')) != 'Teen vawn barn ' ){
			{
				if(preg_match("/\"[\w]+[\t\s\r]+[\w]+\"/",URL::get('keyword')))
				{
					$cond .= URL::get('keywords')? ' AND ((`news`.`name`) LIKE "%'.((str_replace('"','',URL::sget('keywords')))).'%" OR (`item`.`brief`) LIKE "%'.((str_replace('"','',URL::sget('keyword')))).'%" OR (`item`.`description`) LIKE "%'.((str_replace('"','',URL::sget('keywords')))).'%")  IN BOOLEAN MODE':'';
				}
				else
				{
					require_once 'packages/core/includes/utils/search.php';
					require_once 'packages/core/includes/utils/vn_code.php';
					$cond .= Url::get('keywords')?' and MATCH(`news`.keywords) AGAINST("'.DB::escape(str_to_search_keyword(extend_search_keywords(convert_utf8_to_telex((Url::sget('keywords')))),$search_hightlight)).'")':'';
				}
			}
        }
        /////////////LOAI VAN BAN//////////////////
        if ( Url::get('type_text')!=''){
            $cond.=' AND `news`.type_text="'.Url::get('type_text').'"';
        }
        ////////////CO QUAN BAN HANH////////////
        if ( Url::get('agency')!=''){
            $cond.=' AND `news`.category_id='.Url::get('agency');
        }
        ///////////NGAY DANG///////////////////
            if (convert_utf8_to_telex(Url::sget('published_date')) != '') {
                //Lay tat ca tu bang news
                //$news = DB::fetch_all('SELECT * FROM news left outer join category ON category.id=news.category_id AND '.IDStructure::child_cond(DB::structure_id('category','523')));
                $news = DB::fetch_all('SELECT * FROM news');
                $first = true;
                $danh_sach = '';
                //System::debug($news);
                foreach($news as $key1=>$value1)
                {
                    $temp = date('d/m/Y', $value1['published_date'] );
                    if ($temp == Url::get('published_date')){
                        if ($first) {$danh_sach.= $value1['id'];$first=false;}else{$danh_sach.=','.$value1['id'];}
                    }
                }
                //System::debug($danh_sach);
                if($danh_sach!=''){$cond.= ' AND ( `news`.id IN('.$danh_sach.'))';}else
                {
                    $cond.=' AND 0';
                }
            }
        /////////////////////////////////////////////////
        /// To mau vang nhung chu da tim kiem
        //
        $tongsotin=0;
        foreach($this->map['category'] as $key=>$value){
		  $this->map['category'][$key]['items'] = LegalDB::get_news('category_id='.$this->map['category'][$key]['id'].$cond);
          if (count($this->map['category'][$key]['items']) > 0){$tongsotin++;}
                //Neu co tim kiem
                if (convert_utf8_to_telex(Url::get('keywords')) != 'Teen vawn barn '){
    				if(isset($search_hightlight))
    				{
    					$search_hightlight  = array();
    					str_to_search_keyword(Url::get('keywords'),$search_hightlight);
    					foreach($this->map['category'][$key]['items'] as $k1=>$v1)
    					{
    						$this->map['category'][$key]['items'][$k1]['name']=hightlight_keyword(strip_tags($this->map['category'][$key]['items'][$k1]['name']), $search_hightlight);
    						$this->map['category'][$key]['items'][$k1]['brief']=hightlight_keyword(strip_tags($this->map['category'][$key]['items'][$k1]['brief']), $search_hightlight);
    					}
    				}
                }
		}
        if ($tongsotin==0){
            $this->map['warning']='QUYET';
        }
        //System::debug( count( $this->map['category'] ) );
        //System::debug( $this->map['category'] );
        $this->parse_layout('list',$this->map);
	}
}
?>