<?php
class PartnerDB{
	function get_news($cond,$limit='0,1')
	{
		$sql ='
			SELECT
				news.id,
				news.name_'.Portal::language().' as name,
				news.brief_'.Portal::language().' as brief,
				news.small_thumb_url,
				news.name_id,
				category.name_id as category_name_id
			FROM
				news
				INNER JOIN category ON category.id = news.category_id
			WHERE
				'.$cond.'
			ORDER BY
				news.position DESC, news.id DESC
			LIMIT
				'.$limit.'
		';
		return DB::fetch_all($sql);
	}
}
?>
