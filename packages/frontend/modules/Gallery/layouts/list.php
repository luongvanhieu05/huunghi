<section id="recent-works" class="gallery">
  <div class="container">
    <div class="col-sm-12">
        <ol class="breadcrumb">
          <li><a href="#"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a></li>
          <li><a href="album-anh">Thư viện ảnh</a></li>
           <!--IF:cond([[=category_name_id=]] != 'album-anh')-->
           <li><a href="album-anh/[[|category_name_id|]].html">[[|category_name|]]</a></li>         
           <!--/IF:cond-->
        </ol>
    </div>
    <div class="center">
        <h1>[[|category_name|]]</h1>
    </div>
    <div class="row sub-gallery">
    	<div class="col-xs-12 col-sm-4 col-md-12">
    	 <div class="desc"><?php $desc = preg_replace("/width=\"[^\"]+\"/","",[[=description=]]); $desc = preg_replace("/height=\"[^\"]+\"/","",$desc); echo $desc; ?> </div>
     	</div>
      <!--LIST:photos-->
        <div class="col-xs-12 col-sm-4 col-md-3">
            <div class="recent-work-wrap">
                <a href="[[|photos.image_url|]]" rel="prettyPhoto[pp_gal]"><img src="[[|photos.small_thumb_url|]]" class="img-responsive" alt="[[|photos.name|]]"/></a>
                
            </div>
        </div>   
      <!--/LIST:photos-->
    </div><!--/.row-->
    <div class="paging">[[|paging|]]</div>
  </div>
</section>
<link href="assets/standard/js/prettyPhoto/css/prettyPhoto.css" rel="stylesheet">
<script src="assets/standard/js/prettyPhoto/jquery.prettyPhoto.js"></script>
<script type="text/javascript" charset="utf-8">
 jQuery(document).ready(function(){
	jQuery("a[rel^='prettyPhoto']").prettyPhoto({animation_speed:'normal',slideshow:5000, autoplay_slideshow: false,social_tools:false,inline_markup: '<div class="pp_inline">123</div>'});
	// jQuery(".gallery:gt(0) a[rel^='prettyPhoto']").prettyPhoto({animation_speed:'fast',slideshow:10000, hideflash: true});
 });
</script>