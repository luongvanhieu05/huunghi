<section id="recent-works" class="gallery">
  <div class="container">
    <div class="col-sm-12">
        <ol class="breadcrumb">
          <li><a href="#"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a></li>
          <li><a href="album-anh.html">Thư viện ảnh</a></li>
        </ol>
    </div>
    <div class="center">
        <h1>Thư viện ảnh</h1>
    </div>
    <div class="row">
      <!--LIST:albums-->
        <div class="col-xs-12 col-sm-4 col-md-3">
            <div class="recent-work-wrap">
                <a href="album-anh/[[|albums.name_id|]].html" title="[[|albums.name_1|]]"><img src="[[|albums.icon_url|]]" class="img-responsive" alt="[[|albums.name_1|]]"/></a>
                <div class="overlay">
                    <div class="recent-work-inner">
                        <h3><a href="[[|albums.image_url|]]">[[|albums.name_1|]]</a> </h3>
                    </div> 
                </div>
            </div>
        </div>   
      <!--/LIST:albums-->
    </div><!--/.row-->
  </div>
</section>