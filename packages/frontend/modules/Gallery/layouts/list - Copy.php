<div class="container">
	<div class="clm-md-12 gallery">
        <div class="gallery-bound">
            <div class="gallery-title">
            	<div>Thư viện ảnh</div>
            </div>
            <div class="gallery-content-bound">
            	<div class="gallery-category-bound-detail">
                	<ul class="gallery-category-img-detail">
                    <!--LIST:photo-->
                        <ul class="gallery-border-detail">
                            <li>
                            	<div><a rel="lightbox-tour" href="[[|photo.image_url|]]"><img src="[[|photo.image_url|]]" onerror="this.src='assets/default/images/no_image.gif'" /></a></div>
                                <div style="padding-top:3px; text-align:center;">[[|photo.name|]]</div>
                            </li>
                        </ul>
                    <!--/LIST:photo-->
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
jQuery(function(){
	jQuery('.gallery-category-img-detail a').lightBox();
});
</script>
