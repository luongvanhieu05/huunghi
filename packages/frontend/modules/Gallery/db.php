<?php
class GalleryDB{
	function get_photo($cond,$item_per_page=20)
	{
		$items = DB::fetch_all('
			SELECT
				media.id,
				media.name_'.Portal::language().' as name,
				media.description_'.Portal::language().' as description,
				media.url,
				media.image_url,
				category.name_id as category
			FROM
				media
				inner join category on category.id = media.category_id
			WHERE
				'.$cond.'
			ORDER BY
				media.id DESC
			LIMIT
			'.((page_no()-1)*$item_per_page).','.$item_per_page.'
		');
		foreach($items as $key=>$value){
			$items[$key]['small_thumb_url'] = str_replace('image_url','small_thumb_url',$value['image_url']);
		}
		return $items;
	}
	function get_total_photo($cond)
	{
		return DB::fetch('
		SELECT
				count(*) as total
			FROM
				media
				inner join category on category.id = media.category_id
			WHERE
				'.$cond.'
		','total');
	}
	function get_category($category_name_id){
		$sql='
			SELECT
				category.id,
				category.name_'.Portal::language().' as name,
				category.description_'.Portal::language().' as description
			FROM
				category
			WHERE
				category.type="PHOTO" AND category.name_id = "'.$category_name_id.'"
			';
		$item = DB::fetch($sql);
		return $item;
	}
	static function get_albums(){
		$category_id = 274;//album anh
		return DB::select_all('category','type="PHOTO" AND '.IDStructure::child_cond(DB::structure_id('category',$category_id),true).'','structure_id');
	}
}
?>