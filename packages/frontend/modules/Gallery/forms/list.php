<?php
class GalleryForm extends Form
{
	function GalleryForm()
	{
		Form::Form('GalleryForm');
		//$this->link_css('');
	}
	function draw()
	{
		require_once'packages/core/includes/utils/paging.php';
		$this->map = array();
		$layout = 'list';
		if(Url::get('category_name_id')){
			$category_name_id = Url::get('category_name_id')?Url::get('category_name_id'):'album-anh';
			$category_id = DB::fetch('select id from category where type="PHOTO" and name_id="'.$category_name_id.'"','id');
			$item_per_page=20;
			$cond = 'media.status!="HIDE" and media.type = "PHOTO"
			AND '.IDStructure::child_cond(DB::structure_id('category',$category_id)).'';
			$this->map['photos'] = GalleryDB::get_photo($cond,$item_per_page);
			$this->map['total'] = GalleryDB::get_total_photo($cond);
			$this->map['paging'] = paging($this->map['total'],$item_per_page,3,REWRITE,'page_no',array('name_id','category_name_id'));
			$this->map['category_name_id'] = $category_name_id;
			$category = GalleryDB::get_category($category_name_id);
			$this->map['category_name'] = $category['name'];
			$this->map['description'] = $category['description'];
			//System::debug($this->map['photo']);
		}else{
			$this->map['albums'] = GalleryDB::get_albums();
			$layout = 'c_list';
		}
		$this->parse_layout($layout,$this->map);
	}
}
?>