<?php
class VideoListForm extends Form
{
	function VideoListForm()
	{
		Form::Form('VideoListForm');
		//$this->link_css(Portal::template().'css/news.css');
		$this->link_css('assets/hotel/css/news-pro.css');
		//$this->link_css('packages/frontend/templates/Frame/assets/home/style.css');
		//$this->link_css(Portal::template().'css/news-list.css');
	}
	function on_submit(){
	}
	function draw()
	{
		$this->map = array();
		$cond = 'news.type = "NEWS" and news.status<>"HIDE" and news.portal_id = "'.PORTAL_ID.'" and news.publish = 1';
		$item_per_page = 12;
		require_once'packages/core/includes/utils/paging.php';
		if(Url::sget('category_name') and $category = VideoList::$category){
			$this->map['category_name'] = $category['name'];
			$cond = 'news.type = "NEWS" and news.status<>"HIDE" and news.portal_id = "'.PORTAL_ID.'" and news.publish = 1 and '.IDStructure::child_cond($category['structure_id']);
			$count = VideoListDB::get_total_item($cond);
			$this->map['paging'] = paging($count['acount'],$item_per_page,3,0,'page_no',array('name_id','page','category_name'));
			$this->map['items'] = VideoListDB::get_items_by_category($cond,$item_per_page);
		}else{
			$cond.= ' AND '.IDStructure::child_cond( DB::structure_id('category','601'));
			$this->map['category_name'] = Portal::language('News');
			$count = VideoListDB::get_total_item($cond);
			$this->map['paging'] = paging($count['acount'],$item_per_page,3,0,'page_no',array('name_id','page'));
			$this->map['items'] = VideoListDB::get_items($cond,$item_per_page);
		}
		$this->parse_layout('list',$this->map);
	}
}
?>