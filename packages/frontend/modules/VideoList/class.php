<?php
class VideoList extends Module
{
	static $category = array();
	function VideoList($row)
	{
		Module::Module($row);
		require_once 'db.php';
		$cond = 'name_id="'.addslashes(Url::get('category_name')).'" and portal_id="'.PORTAL_ID.'"';
		if(Url::get('category_name') and $category = VideoListDB::get_category($cond))
		{
			VideoList::$category = $category;
			Portal::$document_title = $category['name'];
			$_REQUEST['category_id'] = $category['id'];
		}
		require_once 'forms/list.php';
		$this->add_form(new VideoListForm());
	}
}
?>