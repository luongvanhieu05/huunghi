<style>
    .justify-content-center{
        -webkit-box-pack: center!important;
        -webkit-justify-content: center!important;
        -ms-flex-pack: center!important;
        justify-content: center!important;
    }
    .d-flex{
        display: -webkit-box!important;
        display: -webkit-flex!important;
        display: -ms-flexbox!important;
        display: flex!important;
    }

</style>
<!--LIST:category_home-->
<br clear="all">
<div class="container">
    <div class="module_title"><a href="san-pham/[[|category_home.name_id|]]" style="border-bottom: 3px solid #74A82A;padding-bottom: 5px;">[[|category_home.name_1|]]</a></div>
    <!--Sản phẩm bán chạy-->

    <div class="row justify-content-center d-flex">
        <!--LIST:category_home.item_pro-->
            <div class="item col-lg-4">
                <div class="product-block product-thumb transition">
                    <div class="product-block-inner">

                        <div class="image">
                            <a href="/san-pham/[[|category_home.name_id|]]/[[|category_home.item_pro.name_id|]].html">
                                <img src="[[|category_home.item_pro.small_thumb_url|]]" class="img-responsive" alt="[[|category_home.item_pro.name|]]"/></a>

                            <div class="action">
                                <div class="action_inner">
                                    <div class="button-group">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="caption">
                            <h4><a href="/san-pham/[[|category_home.name_id|]]/[[|category_home.item_pro.name_id|]].html">[[|category_home.item_pro.name|]]</a></h4>

                            <p class="price">
                                <span class="price-new"><?php echo number_format([[=category_home.item_pro.price=]]); ?> đ</span> <span class="price-old"></span>
                            </p>


                        </div>
                    </div>
                </div>
            </div>
            <!--/LIST:category_home.item_pro-->
    </div>

</div>
<style>
    .calrousel-hieudev-[[|category_home.id|]] .owl-pagination{display: none;}
    .calrousel-hieudev-[[|category_home.id|]] .customNavigation a.next {
        background: transparent url(../../../../../assets/standard/images/fashion/megnor/sprite.png) no-repeat scroll -31px -31px;
        height: 32px;
        right: 0;
        top: 150px;
        transition: none 0s ease 0s;
        -webkit-transition: none 0s ease 0s;
        -moz-transition: none 0s ease 0s;
        -ms-transition: none 0s ease 0s;
        -o-transition: none 0s ease 0s;
        width: 32px;
        z-index: 1;
    }
    .calrousel-hieudev-[[|category_home.id|]] .customNavigation a.prev {
        background: transparent url(../../../../../assets/standard/images/fashion/megnor/sprite.png) no-repeat scroll 0 -31px;
        height: 32px;
        left: 0;
        top: 150px;
        transition: none 0s ease 0s;
        -webkit-transition: none 0s ease 0s;
        -moz-transition: none 0s ease 0s;
        -ms-transition: none 0s ease 0s;
        -o-transition: none 0s ease 0s;
        width: 32px;
        z-index: 1;
    }

</style>

<script>
    $(document).ready(function() {

        $("#calrousel-hieudev-[[|category_home.id|]]").owlCarousel({
            navigation : false,
            autoPlay: 3000, //Set AutoPlay to 3 seconds
            items : 4,
            itemsDesktop : [1199,3],
            itemsDesktopSmall : [979,3]

        });

        var owl = $('#calrousel-hieudev-[[|category_home.id|]]');
        $('.calrousel-hieudev-[[|category_home.id|]] .next').click(function(){
            owl.trigger('owl.next');
        });
        $('.calrousel-hieudev-[[|category_home.id|]] .prev').click(function(){
            owl.trigger('owl.prev');
        });

    });
</script>

<!--/LIST:category_home-->
