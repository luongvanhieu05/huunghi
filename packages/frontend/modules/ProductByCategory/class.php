<?php
class ProductByCategory extends Module
{
	function ProductByCategory($row)
	{
		Module::Module($row);
		require_once 'packages/frontend/includes/product.php';
		require_once 'db.php';
		require_once 'forms/list.php';
		$this->add_form(new ProductByCategoryForm());
	}
}
?>