<?php
class ProductByCategoryDB{
	static function get_items($cond,$item_per_page){
		$items = DB::fetch_all('
			SELECT
				product.id,product.status,product.name_'.Portal::language().' as name,
				product.small_thumb_url,
				product.name_id,
				product.description_1 as description,
				category.name_id AS category_name_id,
				product.publish_price,
				product.price
			FROM
				product
				INNER JOIN product_category ON product_category.product_id = product.id
				INNER JOIN category ON product_category.category_id = category.id AND category.type="PRODUCT"
			WHERE
				'.$cond.'
			GROUP BY
					product.id
			ORDER BY
				product.position DESC,product.time DESC
			LIMIT
				0,'.$item_per_page.'
		');
		foreach($items as $key=>$value){
			$items[$key]['brief'] = String::display_sort_title(strip_tags($value['description']),50);
		}
		return $items;
	}
	static function get_item($cond,$item_per_page){
		$order_by = 'product.position DESC,product.time DESC';
		switch(Url::get('order_by')){
			case 'price_down': $order_by = 'product.price DESC'; break;
			case 'price_up': $order_by = 'product.price ASC'; break;
			case 'promote': $order_by = 'product.promote DESC'; break;
			default: $order_by = 'product.time DESC'; break;
		}		
		$items = DB::fetch_all('
			SELECT
				product.id,product.status,product.name_'.Portal::language().' as name,
				product.small_thumb_url,
				product.name_id,
				product.publish_price,
				product.price,
				product.open_promotion_time,
				product.close_promotion_time,				
				category.name_id AS category_name_id
			FROM
				product
				INNER JOIN product_category ON product_category.product_id = product.id
				INNER JOIN category ON product_category.category_id = category.id AND category.type="PRODUCT"
			WHERE
				'.$cond.'
			GROUP BY
					product.id
			ORDER BY
				'.$order_by.'
			LIMIT
				'.((page_no()-1)*$item_per_page).','.$item_per_page.'
		');
		foreach($items as $key=>$value){
			if(PRODUCT::get_price($key) and PRODUCT::get_price($key) < $items[$key]['price']){
				$items[$key]['price'] = System::display_number(PRODUCT::get_price($key));
				$items[$key]['publish_price'] = System::display_number($value['price']);
			}else{
				$items[$key]['publish_price'] = 0;
				$items[$key]['price'] = System::display_number($value['price']);
				$items[$key]['publish_price'] = System::display_number($value['publish_price']);
			}			
		}
 		return $items;
	}




	static function get_category_home($item_per_page,$cond,$order_by){
		$items = DB::fetch_all('
			SELECT
				id
				,name_id
				,name_1
				,status
				,structure_id
			FROM
				category
			WHERE
				'.$cond.'
			ORDER BY
				'.$order_by.'
			LIMIT
				0,'.$item_per_page.'
		');
		foreach($items as $key=>$value){
			$items[$key]['item_pro'] = ProductByCategoryDB::get_items(' product.status!="HIDE" and '.IDStructure::child_cond($value['structure_id']).' ',10);
		}

		//System::Debug($items);die;
		return $items;
	}














	function get_total_item($cond)
	{
		return DB::fetch('
				SELECT
					count(*) as acount
				FROM
					product
					INNER JOIN product_category ON product_category.product_id = product.id
				INNER JOIN category ON product_category.category_id = category.id AND category.type="PRODUCT"
				WHERE
					'.$cond.'
				GROUP BY
					product.id
			');
	}

	function get_sanphammoi()
	{
		return DB::fetch_all('
				SELECT
				product.id,product.status,product.name_'.Portal::language().' as name,
				product.small_thumb_url,
				product.name_id,
				product.publish_price,
				product.price,
				product.valuation,
				category.name_id AS category_name_id
				FROM
					product
					INNER JOIN product_category ON product_category.product_id = product.id
				INNER JOIN category ON product_category.category_id = category.id AND category.type="PRODUCT"
				WHERE
					product.status = "NEW"
				GROUP BY
					product.id
				ORDER BY
					product.id DESC
				LIMIT
					0,10

			');
	}

	function get_sanphamnoibat()
	{
		return DB::fetch_all('
				SELECT
				product.id,
				product.status,product.name_'.Portal::language().' as name,
				product.small_thumb_url,
				product.name_id,
				product.publish_price,
				product.price,
				product.valuation,
				category.name_id AS category_name_id

				FROM
					product
					INNER JOIN product_category ON product_category.product_id = product.id
					INNER JOIN category ON product_category.category_id = category.id AND category.type="PRODUCT"
				WHERE
					product.status ="FEATURE"
				GROUP BY
					product.id
				ORDER BY
					product.id DESC
				LIMIT
					0,10

			');
	}

	function get_sanphambanchay()
	{
		return DB::fetch_all('
				SELECT
				product.id,product.status,product.name_'.Portal::language().' as name,
				product.small_thumb_url,
				product.name_id,
				product.publish_price,
				product.price,
				product.valuation,
				category.name_id AS category_name_id
				FROM
					product
					INNER JOIN product_category ON product_category.product_id = product.id
				INNER JOIN category ON product_category.category_id = category.id AND category.type="PRODUCT"
				WHERE
					product.status ="SELLING"
				GROUP BY
					product.id
				ORDER BY
					product.id DESC
				LIMIT
					0,10

			');
	}
}
?>