﻿<div class="main-slider">

    <div id="spinner"></div>

    <div id="slideshow0" class="owl-carousel" style="opacity: 1;">

        <!--LIST:items-->

        <div class="item">

            <a href="[[|items.url|]]"><img src="[[|items.image_url|]]" alt="mainbanner1" class="img-responsive"/></a>

        </div>

        <!--/LIST:items-->

    </div>

</div>





<script type="text/javascript">

    $('#slideshow0').owlCarousel({

        items: 6,

        autoPlay: 5000,

        singleItem: true,

        navigation: true,

        navigationText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>'],

        pagination: true

    });

</script>

<script type="text/javascript">

    // Can also be used with $(document).ready()

    $(window).load(function () {

        $("#spinner").fadeOut("slow");

    });

</script>