<?php

class HomeSlideDB{

	static function get_home_slide($cond,$table){

		$items = DB::fetch_all('

			SELECT

				'.$table.'.*,

				'.$table.'.name_'.Portal::language().' as name,

				'.$table.'.description_'.Portal::language().' as description,				

				category.name_id as category_name

			FROM

				'.$table.'

				INNER JOIN category ON '.$table.'.category_id = category.id

			WHERE

				'.$cond.'

			ORDER BY

				'.$table.'.position desc,'.$table.'.name_'.Portal::language().'

			LIMIT

				0,20

			');
		// echo '<pre>';
		// print_r($items);
		// echo '</pre>';die;

		return $items;

	}

}

?>