<div>
<div class="detail">
<div class="title"><h1>[[.product_order.]]</h1></div>
<div class="m-detail">
	<h3>[[.you_ordered_our_product_successfully.]]!</h3>
    <h3>[[.thanks_for_your_order.]]!</h3>
    <br /><br />
    <div>
    	<div class="buttons form-group">
            <div  class="col-sm-12">
            	<input type="button" class="btn btn-primary btn-lg cancel" style="width: 100%;" value="[[.buy_other_product.]]" onclick="window.location='product/'" />
            </div>
        </div>
    </div><br clear="all" />
</div>
</div>
</div><!--End .register-->