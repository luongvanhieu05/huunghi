<style>
	th,td{padding:5px !important;}
	.btn.btn-primary.btn-lg.disabled{
		background:#999;
	}
</style>
<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('#discount_code').change(function(){
			$code = jQuery(this).val();
			jQuery.ajax({
				url: "form.php?block_id=<?php echo Module::block_id(); ?>",
				data: {
					act:'cdc',
					code: $code
				},
				success:function(re){
					re = re.trim();
					jQuery('.discountWrapper').hide();
					jQuery('#discount_value').val('');
					if(re == 'false'){
						alert('Mã giảm giá này không tòn tại');
					}else if(re == 'expired'){
						alert('Mã giảm giá này đã hết hạn sử dụng');
					}else{
						jQuery('#discount_value').val(to_numeric(re));
						jQuery('.discountWrapper').show();
						updateTotal();
					}
				}
			});
		});
		jQuery('.services-image img').hover(
			function(){
				jQuery(this).css({'opacity':1,'filter':'alpha(opacity=100)'});
			},
			function(){
				jQuery(this).css({'opacity':0.6,'filter':'alpha(opacity=60)'});
			}
		);
		updateTotal();
	});
	function updateTotal(){
		$total = 0;
		<!--LIST:orders-->
		if(getId('quantity_[[|orders.id|]]') && to_numeric(getId('quantity_[[|orders.id|]]').value)>=1){
			$quantity = to_numeric(getId('quantity_[[|orders.id|]]').value);
			$price = to_numeric(<?php echo System::calculate_number([[=orders.price=]])?>);
			$amount = $price*$quantity;
			$total += $amount;
			getId('amount_[[|orders.id|]]').innerHTML = numberFormat($amount);
		}
		<!--/LIST:orders-->
		jQuery('#total_amount_before_discount').html(numberFormat($total));
		jQuery('#total').val(numberFormat($total));
		$disc = to_numeric(jQuery('#discount_value').val());
		$total = $total - $total*$disc/100;
		jQuery('#total_amount').val(numberFormat($total));
		jQuery('#total_after_discount').val(numberFormat($total));
	}
</script>
<script type="text/javascript" src="packages/core/includes/js/jquery/jquery.validate.js"></script>
<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery('#ProductCartForm').validate({
		success: function(label) {
			//label.text("").addClass("success");
		},
		rules: {
			full_name:{
				required: true
			},
			email: {
				required: false,
				email: true
			},
			phone:{
				required: true
			},
			verify_comfirm_code: {
				required: true,
				minlength: 4,
				remote : 'form.php?block_id=<?php echo Module::block_id(); ?>&cmd=check_ajax'
			}
		},
		messages: {
			full_name:{
				required: '[[.required.]]'
			},
			email: {
				required: '[[.required.]]',
				email: 'Nhập email đúng định dạng'
			},
			phone: {
				required: '[[.required.]]'
			},
			verify_comfirm_code: {
				required: '[[.required.]]',
				minlength: 'Nhập tối thiểu 4 ký tự',
				remote:'Không đúng'
			}
		}
	});
});
</script>
<div class="product-category-20_27_43 layout-2 left-col">
<div class="content_breadcum"></div>
<section id="feature" class="transparent-bg">
<form name="ProductCartForm" id="ProductCartForm" method="post">
 <div class="container">
  <div class="row">
  	<h1 class="page-title">Giỏ hàng</h1>
  	<div class="top-selection">
          <div class="col-md-12">
              <ul class="breadcrumb">
                 <li><a href="">[[.home.]]</a></li>
                  <li><a href="cart.html">Đặt mua hàng</a></li>
              </ul>
          </div>
      </div><!--End .top-selection-->
		<div class="col-md-8">
      <table class="table">
          <tr>
            <th width="20%">[[.product.]]</th>
              <th width="40%">[[.order_detail.]]</th>
            <th width="15%" align="center">[[.price.]] (đ)</th>
            <th width="10%" align="center">Số lượng</th>
            <th width="15%" align="center">Thành tiền (đ)</th>
            <th width="5%" align="center"></th>
          </tr>
          <!--LIST:orders-->
           <tr>
            <td align="center"><a target="_blank" href="san-pham/[[|orders.category_name_id|]]/[[|orders.name_id|]].html"><img src="[[|orders.small_thumb_url|]]" width="80" /></a></td>
              <td>[[|orders.name|]]</td>
            <td align="right">[[|orders.price|]]</td>
            <td align="center"><input  name="quantity_[[|orders.id|]]" type="text" id="quantity_[[|orders.id|]]" value="[[|orders.quantity|]]" style="height:20px;text-align:center;" class="form-control" onBlur="if(to_numeric(this.value) <= 0){alert('Quý khách vui lòng nhập số lượng mua hàng!');this.focus();}else{updateTotal();}"></td>
            <td align="right"><span id="amount_[[|orders.id|]]"></span></td>
            <td align="center"><a href="cart.html?id=[[|orders.id|]]&act=del">[[.delete.]]</a></td>
           </tr>
          <!--/LIST:orders-->
          <tr>
            <td colspan="4" align="right">[[.total_amount.]]</td>
            <td align="right"><strong id="total_amount_before_discount">[[|total|]]</strong></td>
            <td align="left">&nbsp;</td>
          </tr>
      </table>
      <!--IF:cond(!empty([[=orders=]]))-->
      <div class="register-form form-horizontal">
      <h3>[[.order_information.]]</h3>
          <div class="form-group">
            <label class="col-sm-4">[[.full_name.]]: <strong>*</strong></label>
            <div class="col-sm-8">
              <input name="full_name" type="text" id="full_name" class="form-control"/>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4">Email :</label>
            <div class="col-sm-8">
              <input name="email" type="text" id="email" class="form-control"/>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4">[[.phone.]] : <strong>*</strong></label>
            <div class="col-sm-8">
              <input name="phone" type="text" id="phone" class="form-control"/>
            </div>
          </div>
          <div class="form-group">
              <label for="verify_comfirm_code" class="col-sm-4">[[.capcha.]] <strong>*</strong></label>
              <span class="col-sm-2"><img id="imgCaptcha" src="capcha.php" style="margin:0 !important;" /> [<a href="#" onclick="jQuery('#imgCaptcha').attr('src','capcha.php?'+Math.random());return false">Refresh</a>]</span>
              <div class="col-sm-6">
              <input name="verify_comfirm_code" type="text"  class="form-control" id="verify_comfirm_code"/>
              </div>
              <div class="clear"></div>
          </div>
          <div class="form-group">
           <label class="col-sm-4">[[.total_amount.]] :</label>
            <div class="col-sm-8">
            <input  name="total" type="text" id="total" value="[[|total|]]"  class="form-control" style="width:190px;float:left;text-align:right;margin-right:5px;height:20px;border:0px;background:none;font-size:14px;font-weight:bold;" readonly />
            <input  name="total_amount" type="hidden" id="total_amount" value="[[|total|]]" />
            <span style="float:left;">đ</span>
            <br clear="all" />
            </div>
          </div>
           <div class="form-group hide">
            <label class="col-sm-4">[[.discount_code.]] : <strong>*</strong></label>
            <div class="col-sm-8">
              <input name="discount_code" type="text" id="discount_code" class="form-control"  style="text-align:center;width:136px;float:left;" onkeydown="this.value = this.value.toUpperCase();" />
              <input  name="discount_value" type="text" id="discount_value" class="form-control" value="0" style="float:left;text-align:center;width:50px;color:#F00;font-weight:bold;margin-left:5px;margin-right:5px;" readonly /> %
            </div>
          </div> 
          <div class="form-group discountWrapper" style="display:none;">
           <label class="col-sm-4">[[.total_after_discount.]] :</label>
            <div class="col-sm-8">
           	<input  name="total_after_discount" type="text" id="total_after_discount"  class="form-control" style="width:190px;float:left;text-align:right;margin-right:5px;height:20px;border:0px;background:none;font-size:14px;font-weight:bold;" readonly /> <span style="float:left;">đ</span><br clear="all" />
            </div>
          </div>
           <div class="form-group">
            <input type="checkbox" name="conditions" id="conditions" style="margin-left: 15px !important;" 	onclick="Checked_(this);" />
           <label for="conditions">
              [[.i_agree_with.]] <a href="trang-tin/chinh-sach-va-dieu-khoan/dieu-khoan-mua-hang.html" target="_blank">Điều khoản mua hàng</a></label>
           </div>
          <div class="buttons form-group">
              <div  class="col-sm-12">
                  <input type="button" class="btn btn-primary btn-lg cancel" style="width: 49%;" value="[[.buy_other_product.]]" onclick="window.location='san-pham.html'" />
                  <input id="book" type="submit" class="btn btn-primary btn-lg disabled" style="width: 49%;" value="Hoàn thành mua hàng" />
              </div>
          </div>
      </div>   
      <!--/IF:cond-->
      </div>
      <div class="col-md-4">
      	<img src="assets/standard/images/fashion/promotion.jpg" class="img-responsive">
      </div>
  </div>
</div> 
<input type="hidden" name="Title" value="VPC 3-Party"/>
<input type="hidden" name="virtualPaymentClientURL" size="63" value="http://mtf.onepay.vn/vpcpay/vpcpay.op" maxlength="250"/>
<input type="hidden" name="vpc_Version" value="2" size="20" maxlength="8"/>
<input type="hidden" name="vpc_Command" value="pay" size="20" maxlength="16"/>
<input type="hidden" name="vpc_AccessCode" value="6BEB2546" size="20" maxlength="8" />
<input type="hidden" name="vpc_Merchant" id="vpc_Merchant" value="TESTONEPAY" />
<input type="hidden" name="vpc_MerchTxnRef" value="<?php echo date('YmdHis') . rand();?>" size="20" maxlength="40"/>
</form> 
</form>
</section>
</div>
<script>
function Checked_(obj){
	if(obj.checked){
		jQuery('#book').attr('class','btn btn-primary btn-lg');
	}else{
		jQuery('#book').attr('class','btn btn-primary btn-lg disabled');
	}
}
</script>