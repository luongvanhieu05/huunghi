<?php
class ProductCartForm extends Form
{
	function ProductCartForm()
	{
		Form::Form('ProductCartForm');
		$this->link_css(Portal::template().'css/news.css');
		$this->add('full_name',new TextType(true,'full_name',0,255));
		$this->add('email',new TextType(true,'email',0,255));
	}
	function on_submit(){
		if($this->check()){			
			ProductCartDB::updateData();
			//header('location:service_order.html?&success=true');
			exit();
		}
	}
	function draw()
	{
		$this->map = array();
		$this->map['total'] = 0;
		$orders = ProductCartDB::get_order_detail();
		foreach($orders as $value){
			$this->map['total'] += $value['price'];
		}
		$this->map['orders'] = $orders;
		$this->parse_layout('sign_in',$this->map);
	}
}
?>