<?php
class ProductCartForm extends Form
{
	function ProductCartForm()
	{
		Form::Form('ProductCartForm');
		$this->add('full_name',new TextType(true,'full_name',0,255));
		$this->add('email',new TextType(true,'email',0,255));
		if(Url::iget('product_id')){
			ProductCartDB::updateCart(Url::iget('product_id'));
		}
	}
	function on_submit(){
		if($this->check()){	
			$order_id = ProductCartDB::updateData();
			//$this->onepay_payment($order_id);
			header('location:cart.html?success=true');
			exit();
		}
	}
	function draw()
	{
		$this->map = array();
		$this->map['exchange_rate'] = DB::fetch('select exchange from currency where id="USD"','exchange');
		$this->map['total'] = 0;
		$orders = ProductCartDB::get_order_detail();
		foreach($orders as $key=>$value){
			$this->map['total'] += System::calculate_number($value['price']);
		}
		$this->map['total_vnd'] = System::display_number($this->map['total']*$this->map['exchange_rate']);		
		$this->map['total'] = System::display_number($this->map['total']);
		$this->map['orders'] = $orders;
		if(Session::is_set('member_id') and $member = DB::select('member','id='.Session::get('member_id').'')){
			$member['full_name'] = $member['first_name'].' '.$member['last_name'];
			$member['phone'] = $member['mobile_phone']?$member['mobile_phone']:$member['telephone'];
			foreach($member as $key=>$value){
				if(!isset($_REQUEST[$key])){
					$_REQUEST[$key] = $value;
				}
			}
		}
		$this->map += array(
			'gender_list'=>array(''=>Portal::language('select')) + array('1'=>Portal::language('male'),'0'=>Portal::language('female'))
		);
		if(Url::get('success') == 'true'){
			$layout = 'success';
		}else{
			$layout = 'list';
		}
		$this->parse_layout($layout,$this->map);
	}
	function onepay_payment($order_id){
		$exchange_rate = DB::fetch('select exchange from currency where id="USD"','exchange');
		 date_default_timezone_set('Asia/Krasnoyarsk');
		$SECURE_SECRET = "6D0870CDE5F24F34F3915FB0045120DB";
		// add the start of the vpcURL querystring parameters
		$vpcURL = $_POST["virtualPaymentClientURL"] . "?";
		unset($_POST["virtualPaymentClientURL"]); 
		// Remove the Virtual Payment Client URL from the parameter hash as we 
		// do not want to send these fields to the Virtual Payment Client.
		unset($_POST["full_name"]); 
		unset($_POST["passport"]);
		unset($_POST["nationality_id"]);
		unset($_POST["email"]);
		unset($_POST["phone"]);
		unset($_POST["verify_comfirm_code"]);
		unset($_POST["vpc_Message"]);
		unset($_POST["form_block_id"]);
		$_POST['vpc_Amount'] = System::calculate_number($_POST["total_amount"])*100;
		unset($_POST["total"]);
		unset($_POST["total_amount"]);
		unset($_POST["discount_code"]);
		unset($_POST["discount_value"]);
		unset($_POST["total_after_discount"]);		
		unset($_POST["total_vnd"]);
		$_POST['vpc_OrderInfo'] = 'Thanh toan hoa don mua hang tai Shisha2go.vn: SS'.$order_id;
		$_POST['vpc_Locale'] = (Portal::language()==1)?'vn':'en';
		$_POST['vpc_ReturnURL'] = 'http://'.$_SERVER['HTTP_HOST'].'/cart.html?product_id='.Url::get('product_id').'&cmd=finish&order_id='.md5('xyz'.$order_id).'';
		// The URL link for the receipt to do another transaction.
		// Note: This is ONLY used for this example and is not required for 
		// production code. You would hard code your own URL into your application.
		
		// Get and URL Encode the AgainLink. Add the AgainLink to the array
		// Shows how a user field (such as application SessionIDs) could be added
		$_POST['AgainLink'] = urlencode($_SERVER['HTTP_REFERER']);
		//$_POST['AgainLink']=urlencode($_SERVER['HTTP_REFERER']);
		// Create the request to the Virtual Payment Client which is a URL encoded GET
		// request. Since we are looping through all the data we may as well sort it in
		// case we want to create a secure hash and add it to the VPC data if the
		// merchant secret has been provided.
		//$md5HashData = $SECURE_SECRET; Khởi tạo chuỗi dữ liệu mã hóa trống
		$md5HashData = "";
		
		ksort ($_POST);
		
		// set a parameter to show the first pair in the URL
		$appendAmp = 0;
		foreach($_POST as $key => $value) {
		
			// create the md5 input and URL leaving out any fields that have no value
			if (strlen($value) > 0) {
				
				// this ensures the first paramter of the URL is preceded by the '?' char
				if ($appendAmp == 0) {
					$vpcURL .= urlencode($key) . '=' . urlencode($value);
					$appendAmp = 1;
				} else {
					$vpcURL .= '&' . urlencode($key) . "=" . urlencode($value);
				}
				//$md5HashData .= $value; sử dụng cả tên và giá trị tham số để mã hóa
				if ((strlen($value) > 0) && ((substr($key, 0,4)=="vpc_") || (substr($key,0,5) =="user_"))) {
					$md5HashData .= $key . "=" . $value . "&";
				}
			}
		}
		//xóa ký tự & ở thừa ở cuối chuỗi dữ liệu mã hóa
		$md5HashData = rtrim($md5HashData, "&");
		// Create the secure hash and append it to the Virtual Payment Client Data if
		// the merchant secret has been provided.
		if (strlen($SECURE_SECRET) > 0) {
			//$vpcURL .= "&vpc_SecureHash=" . strtoupper(md5($md5HashData));
			// Thay hàm mã hóa dữ liệu
			$vpcURL .= "&vpc_SecureHash=" . strtoupper(hash_hmac('SHA256', $md5HashData, pack('H*',$SECURE_SECRET)));
		}
		
		// FINISH TRANSACTION - Redirect the customers using the Digital Order
		// ===================================================================
		header("Location: ".$vpcURL);
		//echo $vpcURL;exit();
		// *******************
		// END OF MAIN PROGRAM
	}
}
?>