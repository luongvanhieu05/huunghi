<?php
class ProductCart extends Module
{
	public static $item = array();
	function ProductCart($row)
	{
		Module::Module($row);
		require_once 'db.php';
		if(Url::get('act') == 'cdc'){
			echo $this->check_discount_code(Url::get('code'));
			exit();
		}	
		if(Url::iget('id') and Url::get('act')=='del'){
			ProductCartDB::deleteOrderedService(Url::iget('id'));
			header('location:cart.html?del=success');
		}
		switch(Url::get('cmd')){
			case 'check_ajax':
				$this->check(); exit();
				break;
			case 'finish':
				if($this->check_onepay_result() == 1){
					if(Url::iget('product_id') and Url::get('order_id')){
						$this->finish_onepay_payment(Url::get('product_id'),Url::get('order_id'));
					}
					require_once 'forms/finish.php';
					$this->add_form(new FinishProductCartForm());
				}else{
					header('location:cart.html');
					exit();
				}
				break;
			default:
				require_once 'forms/list.php';
				$this->add_form(new ProductCartForm());
				break;
		}
	}
	function check(){
		if($comfirm_code = Url::get('verify_comfirm_code'))
		{
			if($comfirm_code == Session::get('security_code')) echo 'true';
			else echo 'false';
		}
	}
	function finish_onepay_payment($product_id,$order_id){
		if($item = DB::fetch('select id,paid from order_invoice where md5(concat("xyz",id)) = "'.$order_id.'"')){
			if($item['paid'] <> 1){
				DB::update('order_invoice',array('paid'=>1),'id='.$item['id'].'');
				if(isset($_SESSION['order'])){
					unset($_SESSION['order']);
				}
			}
		}
	}
	function check_onepay_result(){
		// *********************
		// START OF MAIN PROGRAM
		// *********************
		
		// Define Constants
		// ----------------
		// This is secret for encoding the MD5 hash
		// This secret will vary from merchant to merchant
		// To not create a secure hash, let SECURE_SECRET be an empty string - ""
		// $SECURE_SECRET = "secure-hash-secret";
		$SECURE_SECRET = "6D0870CDE5F24F34F3915FB0045120DB";
		
		// get and remove the vpc_TxnResponseCode code from the response fields as we
		// do not want to include this field in the hash calculation
		$vpc_Txn_Secure_Hash = (isset($_GET["vpc_SecureHash"]) and $_GET["vpc_SecureHash"])?$_GET["vpc_SecureHash"]:'';
		$vpc_MerchTxnRef = $_GET["vpc_MerchTxnRef"];
		//$vpc_AcqResponseCode = $_GET["vpc_AcqResponseCode"];
		unset($_GET["vpc_SecureHash"]);
		// set a flag to indicate if hash has been validated
		$errorExists = false;
		
		if (strlen($SECURE_SECRET) > 0 && $_GET["vpc_TxnResponseCode"] != "7" && $_GET["vpc_TxnResponseCode"] != "No Value Returned") {
		
			ksort($_GET);
			//$md5HashData = $SECURE_SECRET;
			//khởi tạo chuỗi mã hóa rỗng
			$md5HashData = "";
			// sort all the incoming vpc response fields and leave out any with no value
			foreach ($_GET as $key => $value) {
		//        if ($key != "vpc_SecureHash" or strlen($value) > 0) {
		//            $md5HashData .= $value;
		//        }
		//      chỉ lấy các tham số bắt đầu bằng "vpc_" hoặc "user_" và khác trống và không phải chuỗi hash code trả về
				if ($key != "vpc_SecureHash" && (strlen($value) > 0) && ((substr($key, 0,4)=="vpc_") || (substr($key,0,5) =="user_"))) {
					$md5HashData .= $key . "=" . $value . "&";
				}
			}
		//  Xóa dấu & thừa cuối chuỗi dữ liệu
			$md5HashData = rtrim($md5HashData, "&");
		
		//    if (strtoupper ( $vpc_Txn_Secure_Hash ) == strtoupper ( md5 ( $md5HashData ) )) {
		//    Thay hàm tạo chuỗi mã hóa
			if (strtoupper ( $vpc_Txn_Secure_Hash ) == strtoupper(hash_hmac('SHA256', $md5HashData, pack('H*',$SECURE_SECRET)))) {
				// Secure Hash validation succeeded, add a data field to be displayed
				// later.
				$hashValidated = "CORRECT";
			} else {
				// Secure Hash validation failed, add a data field to be displayed
				// later.
				$hashValidated = "INVALID HASH";
			}
		} else {
			// Secure Hash was not validated, add a data field to be displayed later.
			$hashValidated = "INVALID HASH";
		}
		
		// Define Variables
		// ----------------
		// Extract the available receipt fields from the VPC Response
		// If not present then let the value be equal to 'No Value Returned'
		
		// Standard Receipt Data
		$amount = $this->null2unknown($_GET["vpc_Amount"]);
		$locale = $this->null2unknown($_GET["vpc_Locale"]);
		//$batchNo = $this->null2unknown($_GET["vpc_BatchNo"]);
		$command = $this->null2unknown($_GET["vpc_Command"]);
		$message = $this->null2unknown($_GET["vpc_Message"]);
		$version = $this->null2unknown($_GET["vpc_Version"]);
		//$cardType = $this->null2unknown($_GET["vpc_Card"]);
		$orderInfo = $this->null2unknown($_GET["vpc_OrderInfo"]);
		///$receiptNo = $this->null2unknown($_GET["vpc_ReceiptNo"]);
		$merchantID = $this->null2unknown($_GET["vpc_Merchant"]);
		//$authorizeID = $this->null2unknown($_GET["vpc_AuthorizeId"]);
		$merchTxnRef = $this->null2unknown($_GET["vpc_MerchTxnRef"]);
		//$transactionNo = $this->null2unknown($_GET["vpc_TransactionNo"]);
		//$acqResponseCode = $this->null2unknown($_GET["vpc_AcqResponseCode"]);
		$txnResponseCode = $this->null2unknown($_GET["vpc_TxnResponseCode"]);
		// 3-D Secure Data
		$verType = array_key_exists("vpc_VerType", $_GET) ? $_GET["vpc_VerType"] : "No Value Returned";
		$verStatus = array_key_exists("vpc_VerStatus", $_GET) ? $_GET["vpc_VerStatus"] : "No Value Returned";
		$token = array_key_exists("vpc_VerToken", $_GET) ? $_GET["vpc_VerToken"] : "No Value Returned";
		$verSecurLevel = array_key_exists("vpc_VerSecurityLevel", $_GET) ? $_GET["vpc_VerSecurityLevel"] : "No Value Returned";
		$enrolled = array_key_exists("vpc_3DSenrolled", $_GET) ? $_GET["vpc_3DSenrolled"] : "No Value Returned";
		$xid = array_key_exists("vpc_3DSXID", $_GET) ? $_GET["vpc_3DSXID"] : "No Value Returned";
		$acqECI = array_key_exists("vpc_3DSECI", $_GET) ? $_GET["vpc_3DSECI"] : "No Value Returned";
		$authStatus = array_key_exists("vpc_3DSstatus", $_GET) ? $_GET["vpc_3DSstatus"] : "No Value Returned";
		
		// *******************
		// END OF MAIN PROGRAM
		// *******************
		
		// FINISH TRANSACTION - Process the VPC Response Data
		// =====================================================
		// For the purposes of demonstration, we simply display the Result fields on a
		// web page.
		
		// Show 'Error' in title if an error condition
		$errorTxt = "";
		
		// Show this page as an error page if vpc_TxnResponseCode equals '7'
		if ($txnResponseCode == "7" || $txnResponseCode == "No Value Returned" || $errorExists) {
			$errorTxt = "Error ";
		}
		
		// This is the display title for 'Receipt' page 
		$title = $_GET["Title"];
		
		// The URL link for the receipt to do another transaction.
		// Note: This is ONLY used for this example and is not required for 
		// production code. You would hard code your own URL into your application
		// to allow customers to try another transaction.
		//TK//$againLink = URLDecode($_GET["AgainLink"]);
		
		
		$transStatus = "";
		if($hashValidated=="CORRECT" && $txnResponseCode=="0"){
			return 1;// thanh cong
		}elseif ($hashValidated=="INVALID HASH" && $txnResponseCode=="0"){
			return 2; // pedding
		}else {
			return -1;// that bai
		}
	}
	function null2unknown($data)
	{
		if ($data == "") {
			return "No Value Returned";
		} else {
			return $data;
		}
	}
	function check_discount_code($code){
		$tmp_code = Portal::get_setting('discount_code',false,'#default');
		if($tmp_code){
			$arr = explode(';',$tmp_code);
			foreach($arr as $key=>$value){
				$arr1 = explode(':',$value);
				if($arr1[0] ==  $code){
					if(isset($arr1[2]) and $arr1[2]){
						$expired = Date_Time::to_time($arr1[2]);
						if($expired <= Date_Time::to_time(date('d/m/Y'))){
							return 'expired';
						}else{
							return intval($arr1[1]);
						}
					}
				}
			}
		}
		return 'false';
	}
}
?>