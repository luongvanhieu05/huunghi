<?php
class ProductCartDB{
	function get_products($cond)
	{
		$items = DB::fetch_all('
			SELECT
				product.*,product.name_'.Portal::language().' as name
			FROM
				product
				INNER JOIN product_category ON product_category.product_id = product.id
				INNER JOIN category ON product_category.category_id = category.id AND category.type="PRODUCT"
			WHERE
				'.$cond.'
			LIMIT
				0,20
		');
		foreach($items as $key=>$value){
			$items[$key]['brief'] = $value['name'];
		}
		return $items;
	}
	function updateCart($id){
		if(Session::is_set('order')){
			$orders = Session::get('order');
		}else{
			$orders = array();
		}
		if($item=DB::select('product','id='.$id)){
			//if($orders[$id])
			{
				$orders[$id]['id'] = $id;
				$orders[$id]['name'] = $item['name_'.Portal::language()];
				$orders[$id]['name_id'] = $item['name_id'];
				$orders[$id]['category_name_id'] = DB::fetch('select id,name_id from category where id = '.$item['category_id'].'','name_id');
				$orders[$id]['small_thumb_url'] = $item['small_thumb_url'];
				$orders[$id]['code'] = $item['code'];
				$orders[$id]['description'] = $item['description_'.Portal::language()];
				$orders[$id]['price'] = System::display_number($item['price']);
				$orders[$id]['quantity'] = System::display_number(Url::get('quantity_'.$id)?Url::get('quantity_'.$id):1);
			}
		}
		Session::set('order',$orders);
	}
	function deleteOrderedService($id){
		if(isset($_SESSION['order']) and isset($_SESSION['order'][$id])){
			unset($_SESSION['order'][$id]);
		}
	}
	function updateData($send_email = true){
		if(Session::is_set('order')){
			$orders = Session::get('order');
			$total = 0;
			$array = array(
				'full_name'=>Url::get('full_name'),
				'email'=>Url::get('email'),
				'phone'=>Url::get('phone'),
				'time'=>time(),
				'account_id'=>Session::get('user_id'),
				'total'=>System::calculate_number(trim(Url::get('total_amount'))),
				'discount_code',
				'discount_value',
				'paid'=>0
			);
			$invoice_id = DB::insert('order_invoice',$array);
			
			foreach($orders as $key=>$value){
				if(Url::get('quantity_'.$key) > 0){
					$array1 = array('invoice_id'=>$invoice_id,'product_id'=>$key,'name'=>$value['code'].': '.$value['name'],'price'=>System::calculate_number($value['price']),'quantity'=>Url::get('quantity_'.$key));
					DB::insert('order_invoice_detail',$array1);
				}
			}
			unset($_SESSION['order']);	
			///////////////////////////////
			$subject = 'Hangjeans.com: Dat hang ma: #'.$invoice_id.'...!';
			$to = $array['email'];
			$mail_content = @file_get_contents('cache/email_template/order.html');
			$arr_replace = array(
				'[[date]]'=>date('d/m/Y'),
				'[[full_name]]'=>$array['full_name'],
				'[[email]]'=>$array['email'],
				'[[phone]]'=>$array['phone'],
				'[[id]]'=>$invoice_id,
				'[[href]]'=>'http://'.$_SERVER['HTTP_HOST'].'/?page=manage_order&id='.$invoice_id.''
			);
			$mail_content = strtr($mail_content,$arr_replace);
			
			if($to){
				System::send_mail(Portal::get_setting('company_email'),$to,$subject,$mail_content);				
			}
			
			///////////////////////////////
			return $invoice_id;
		}else{
			return false;
		}
	}
	static function get_order_detail(){
		if(Session::is_set('order')){
			$orders = Session::get('order');
		}else{
			$orders = array();
		}
		return $orders;
	}
}
?>