<!--tin tức-->

<div class="testimonials-container " id="testimonial">

    <div class="testimonial_inner">

        <div class="cms-box-heading">Tin tức & Sự kiện</div>

        <div class="homepage-testimonials-inner products block_content">

            <div class="products product-carousel" id="testimonial-carousel">

                <!--LIST:items-->

                <div class="slider-item">

                    <div class="product-block-image">

                        <div class="product-block" style="height: 379px;">

                            <div class="blog-left">

                                <div class="testi-image">

                                    <a href="/trang-tin/[[|items.category_name_id|]]/[[|items.name_id|]].html" class="image-block"><img src="[[|items.small_thumb_url|]]" alt=""></a>

                                    <div class="post-date" style="height: auto;color:#fff;">
                                        <?php echo date("d- m-Y",[[=items.time=]]) ?>
                                    </div>

                                </div>

                            </div>

                            <div class="blog-right">

                                <div class="title-block"> <div class="testi-name" style="padding-bottom: 5px;">

                                        <a href="/trang-tin/[[|items.category_name_id|]]/[[|items.name_id|]].html"><?php echo String::display_sort_title(strip_tags([[=items.name=]]),15);?></a></div>

                                </div>

                                <div class="box-content">

                                    <p>

                                        <?php echo String::display_sort_title(strip_tags([[=items.brief=]]),15);?>

                                    </p>



                                </div>



                            </div>



                        </div>



                    </div>



                </div>

                <!--/LIST:items-->







            </div>







        </div>



        <div class="testimonial_default_width" style="display: none; visibility: hidden;">&nbsp;</div>



    </div>



</div>

<!--thương hiệu-->

<div id="carousel-0" class="banners-slider-carousel hide">

    <div class="module_title">Thương hiệu</div>

    <div class="customNavigation">

        <a class="prev">&nbsp;</a>

        <a class="next">&nbsp;</a>

    </div>

    <div class="product-carousel" id="module-0-carousel">

        <!--LIST:thuonghieu-->

        <div class="slider-item">

            <div class="product-block">

                <div class="product-block-inner">

                    <a href="/trang-tin/[[|thuonghieu.category_name_id|]]/[[|thuonghieu.name_id|]].html"><img src="[[|thuonghieu.small_thumb_url|]]"

                                     alt="logo1"/></a>

                </div>

            </div>

        </div>

        <!--/LIST:thuonghieu-->



    </div>

</div>

<span class="module_default_width" style="display:none; visibility:hidden"></span>