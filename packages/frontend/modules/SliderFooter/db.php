
<?php
/*
* Personnel system: Erase Memories
* Create by: TCV., JSC
*/
class SliderFooterDB{
    //Phuong thuc lay danh sach items
    function get_items($cond = '1'){
        $sql = '
                    SELECT
                        media.id,
                        media.url,
                        media.image_url
                    FROM
                        media
                    WHERE
                        media.type = \'Partner\'
        ';
        return DB::fetch_all( $sql );
    }//End  function get_items(){
	function get_category($cond)
	{
		$sql='
			SELECT
				id,
				name_'.Portal::language().' as name,
				name_id,
				url,
				structure_id,
                image_url
			FROM
				category
			WHERE
				'.$cond.'
			ORDER BY
				structure_id
			';
		$items = DB::fetch_all($sql);
		foreach($items as $key=>$value){
		$items[$key]['items'] = DB::fetch_all('
				SELECT
					id,
					name_'.Portal::language().' as name,
					name_id,
					url,
					structure_id
				FROM
					category
				WHERE
					 '.IDStructure::child_cond($value['structure_id']).' and id!='.$value['id'].'
				ORDER BY
					position DESC, id ASC
			');
		}
		return $items;
	}
}
?>
        