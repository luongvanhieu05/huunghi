
<?php
/*
* Personnel system: Erase Memories
* Create by: TCV., JSC
* Date: 18-11-2011
*/
class ListSliderFooterForm extends Form{
    function ListSliderFooterForm(){
        Form::Form('ListSliderFooterForm');
		//$this->link_js('packages/core/includes/js/jquery/jquery.jcarousel.min.js');
		$this->link_css(Portal::template().'css/slide-footer.css');
        $this->link_js('packages/core/includes/js/jquery/jCarousel.js');
    }//End function ListSliderFooterForm(){
    function draw(){
        $this->map = array();
        //Truy van danh sach doi tac
		$cond='type="NEWS" and status!="HIDE" and portal_id="'.PORTAL_ID.'" and '.IDStructure::direct_child_cond(DB::structure_id('category','498') );
        $this->map['news'] = SliderFooterDB::get_category($cond);
        $this->parse_layout('list',$this->map);
       	//System::debug($this->map['news']);
    }//End function function draw(){
}
?>
        