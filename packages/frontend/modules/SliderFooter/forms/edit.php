
<?php
/*
* Personnel system: Erase Memories
* Create by: TCV., JSC
* Date: 18-11-2011
*/
class EditSliderFooterForm extends Form{
    function EditSliderFooterForm(){
        Form::Form('EditSliderFooterForm');
    }//End function EditSliderFooterForm(){
    function draw(){
        if(Url::get('cmd') == 'edit' && !$id = Url::get('id',false)){
            System::alert('This id invalid',Url::build_current(array('cmd' => 'list')));
        }else{
             SliderFooterDB::get_item($id);
            $this->parse_layout('edit',$this->map);
        }
    }//End function draw(){
    function on_submit(){
        if($this->check()){
            $this->save_item();
        }else{
            if($this->error_message){
                $this->error_message();
            }
        }
    }//End  function on_submit(){
    function save_item(){
    }//End   function save_item(){
}
?>
        