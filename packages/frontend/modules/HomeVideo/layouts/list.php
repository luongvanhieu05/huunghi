<div class="other-full">
  <div class="blue_top"></div>
  <div class="blue_screen">
      <div class="m-video">
          <div class="button-all"><a href="video">Toàn bộ video</a></div>
          <?php $i=1;?>
          <!--LIST:video-->
          <div class="col-video <?php echo ($i%3==0)?'last':'';$i++?>">
              <h2 class="title" title="[[|video.name|]]"><?php echo String::display_sort_title(trim([[=video.name=]]),2);?></h2>
              <p><?php echo String::display_sort_title(trim([[=video.brief=]]),20);?></p>
              <span>
                  <a href="<?php echo Url::build([[=video.category_name_id=]],array('name_id'=> [[=video.name_id=]]),REWRITE); ?>">
                    <img src="[[|video.image_url|]]" width="276" height="155" />
                  </a>
              </span>
              <h6><a href="<?php echo Url::build([[=video.category_name_id=]],array('name_id'=> [[=video.name_id=]]),REWRITE); ?>" class="button-ef">Xem video ngay</a></h6>
          </div><!--End .col-video-->
          <!--/LIST:video-->
          <div style="clear: both;"></div>
      </div><!--End .m-video-->
  </div>
  <div class="blue_bottom"></div>
</div><!--End .other-full-->