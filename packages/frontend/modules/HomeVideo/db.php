<?php
class HomeVideoDB{
	/*function get_video($cond)
	{
		return DB::fetch('select id,name_'.Portal::language().' as name,url,image_url from media where '.$cond.' ORDER BY id DESC LIMIT 0,1');
	}*/
	function get_video($cond)
	{
		$items = DB::fetch_all('
			select
				news.id,
				news.name_'.Portal::language().' as name,
				news.name_id,
				news.brief_'.Portal::language().' as brief,
				news.image_url,
				category.name_id AS category_name_id
			from
				news
				INNER JOIN category ON category.id = news.category_id
			where
				'.$cond.'
			ORDER BY
				news.id DESC LIMIT 0,3
		');
		return $items;
	}
}
?>
