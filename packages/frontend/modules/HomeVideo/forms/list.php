<?php
class HomeVideoForm extends Form
{
	function HomeVideoForm()
	{
		Form::Form('HomeVideoForm');
		$this->link_css(Portal::template().'css/video.css');
        $this->link_js('packages/core/includes/js/jwplayer/jwplayer.js');
        $this->link_js('packages/core/includes/js/flowplayer/js/flowplayer-3.2.6.min.js');
	}
	function draw(){
		$this->map = array();
		$category_id = 601;
		$cond ='news.type = "NEWS" AND news.status = "HOME" AND news.publish = 1 AND news.category_id = '.$category_id.'';
		$this->map['video'] = HomeVideoDB::get_video($cond);
		$this->parse_layout('list',$this->map);
		//System::debug($this->map);
	}
}
?>