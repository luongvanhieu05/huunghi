<?php 
$page = (Portal::language() == 1)?'san-pham':'product';
?>
<section id="recent-works">
  <div class="container"><br>
  		<div class="col-sm-12">
          <ol class="breadcrumb">
              <li><a href=""><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a></li>
              <li><a href="<?php echo $page.'/';?>">[[.product.]]</a></li>
              <!--IF:cond([[=category_name_id=]]!='san-pham' and [[=category_name_id=]]!='product')-->
              <li><a href="<?php echo $page.'/';?>[[|category_name_id|]]/">[[|category_name|]]</a></li>
              <!--/IF:cond-->
          </ol>
      </div>
      <div class="center wow fadeInDown animated" style="visibility: visible; animation-name: fadeInDown; -webkit-animation-name: fadeInDown;">
          <h2 class="text-center" style="font-size: 30px;;">TÌM KIẾM</h2></br>
          <p class="lead text-center">Cùng với chúng tôi xây dựng phong cách và thể hiện cá tính của bạn! </p></br>
      </div>
      <div class="row">
      	<!--LIST:items-->
        	<?php $url = $page.'/'.([[=items.name_id=]]).'/'.(Url::get('tags')?'tags/'.Url::get('tags'):'');?>
          <div class="col-xs-12 col-sm-4 col-md-4  home-product">
              <div class="recent-work-wrap">
                  <a href="<?php echo [[=items.url=]]?[[=items.url=]]:$url;?>"><img src="[[|items.image_url|]]" class="img-responsive"/></a>
              </div>
              <div class="name"><a href="san-pham/[[|items.name_id|]]"><p class="name-category-c_list" ><?php echo strip_tags([[=items.name=]]);?></p></a> </div>
          </div>
        <!--/LIST:items-->
      </div><!--/.row-->
      <br>
      <br>
      <br>
      <br>
      <br>
  </div><!--/.container-->
</section>
<br clear="all">