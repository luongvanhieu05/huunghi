<?php
/* 	AUTHOR 	:	KHOAND
	DATE	:	21/01/2016
*/
class LocalSearch extends Module
{
	function LocalSearch($row)
	{
		Module::Module($row);
		require_once 'packages/frontend/includes/product.php';
		require_once 'db.php';
		$this->update_seo();
		$this->update_item_per_page();
		require_once 'forms/list.php';
		$this->add_form(new LocalSearchForm());
		
	}
	function update_seo(){
		if(Url::get('category_name_id') and $item = DB::fetch('SELECT id,name_id,name_'.Portal::language().' as name,brief_'.Portal::language().' as brief from category WHERE name_id="'.Url::get('category_name_id').'" and type="PRODUCT"')){
			Portal::$document_title = Portal::get_setting(PREFIX.'site_title').' - '.$item['name'];
			Portal::$meta_description = strip_tags($item['brief']);
		}
		if(Url::get('page_no') > 1){
			Portal::$document_title .= ' - Trang '.Url::get('page_no');
		}
	}
	function update_item_per_page(){
		if(Url::get('sptrentrang')){
			$_SESSION['sptrentrang'] = Url::get('sptrentrang');
		}
	}
}
?>