<?php
class LocalSearchForm extends Form{
	function LocalSearchForm(){
		Form::Form('LocalSearchForm');
	}
	function on_submit(){
		$this->get_condition();

	}
	function get_condition(){
		$cond = '';
		if(Url::get('sex')){
			$cond = '  product.sex = "'.Url::get('sex').' " and ';
		}
		if(Url::get('khoanggia')){
			if(Url::get('khoanggia')==1){
				$cond .= '  (product.publish_price >= "100000" and product.publish_price <= 500000) and';
			}
			if(Url::get('khoanggia')==2){
				$cond .= '  (product.publish_price > "500000" and product.publish_price <= 1000000) and';
			}
			if(Url::get('khoanggia')==3){
				$cond .= '  (product.publish_price > "1000000" ) and';
			}

		}
		return $cond;
	}
	function draw(){	
		$this->map = array();
		$cond = $this->get_condition();
		$cond .= ' product.status <> "HIDE" ';
		if(Url::get('keyword')){		
			require_once 'packages/core/includes/utils/search.php';
			require_once 'packages/core/includes/utils/vn_code.php';
			$cond .= Url::get('keyword')?' AND (product.code LIKE "%'.Url::sget('keyword').'%" OR product.name_'.Portal::language().' LIKE "%'.Url::sget('keyword').'%" OR product.description_'.Portal::language().' LIKE "%'.DB::escape(str_to_search_keyword(addslashes(Url::sget('keyword')),$search_hightlight)).'%")':'';
		}
		$cond .= Url::get('parent_category_name_id')?' 
			AND '.IDStructure::child_cond($parent['structure_id']).'':'
		';
		$cond .= Url::get('category_name_id')?' 
			AND '.IDStructure::child_cond($category['structure_id']).'':'
		';
		$cond .= '
			'.(Url::get('tags')?' AND (product.tags like "%'.str_replace('-',' ',Url::sget('tags')).'%")':'').'
		';
		$item_per_page = Session::get('sptrentrang')?Session::get('sptrentrang'):24;
		require_once'packages/core/includes/utils/paging.php';
		$count = LocalSearchDB::get_total_item($cond);
		$this->map['total'] = $count['acount'];		
		$this->map['paging'] = paging($count['acount'],$item_per_page,3,true,'page_no',array('parent_category_name_id','category_name_id','tags'),'Trang');
		$items = LocalSearchDB::get_items($cond,$item_per_page);
		if(isset($search_hightlight)){
			$search_hightlight  = array();
			str_to_search_keyword(Url::sget('keyword'),$search_hightlight);
			foreach($items as &$item){
				$item['name'] = hightlight_keyword(strip_tags($item['name']), $search_hightlight);
			}
		}
		$this->map['items'] = $items;
		$layout = 'list';
		$this->map['order_by_list'] = array(''=>'==Sắp xếp==') + array('price_down'=>'Giá gảm dần','price_up'=>'Giá tăng dần','promote'=>'KM');
		$this->parse_layout($layout,$this->map);
	}
}
?>