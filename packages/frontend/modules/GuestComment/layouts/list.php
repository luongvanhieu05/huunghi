<!--IF:cond(!empty([[=comments=]]))-->
<script type="text/javascript" src="packages/core/includes/js/jquery/jquery.validate.js"></script>
<script>
jQuery(function(){
	jQuery('#NewsCommentForm').validate({
	rules: {
			full_name: {
				required: true
			},
			email: {
				required:true,
				email:true
			},
			content:{
				required: true
			},
			verify_comfirm_code: {
				required: true,
				minlength: 4,
				remote : 'form.php?block_id=<?php echo Module::block_id(); ?>&cmd=check_ajax'
			}
		},
	messages: {
			full_name: {
				required: 'Yêu cầu phải nhập'
			},
			email: {
				required: 'Yêu cầu phải nhập',
				email:'Yêu cầu phải nhập đúng định dạng email'
			},
			content: {
				required: 'Yêu cầu phải nhập'
			},
			verify_comfirm_code: {
				required: 'Bạn chưa nhập mã xác nhận',
				minlength: 'Bạn phải nhập ít nhất 4 ký tự',
				remote:'Mã xác nhận không hợp lệ'
			}
		}
	});
});
</script>
<a name="guest_comment"></a>
<div class="comment" id="comment-wrapper">
  <div class="other-comment">
      <div class="kind-of">
          <div class="col-md-6">
          <h2>[[|total_comment|]] lời bình</h2>
          </div>
          <!--<div class="col-md-6">
          <ul>
              <li><a href="#">Mới nhất</a></li>
              <li><a href="#">Được trả lời nhiều nhất</a></li>
          </ul>
          </div> -->
      </div><!--End .kind-of-->
	<!--LIST:comments-->      
      <div class="main-comment">
          <div class="text-new">
              <small>[[|comments.full_name|]]<span> - <?php echo date('H:i\' d/m/Y',[[=comments.time=]]);?></span></small>
              <div class="comt_content"><div class="arrows"></div><p>[[|comments.content|]]</p></div>
          </div><!--End .text-new-->
          <div class="comment-down">
          		<!--IF:cond(isset([[=comments.replies=]]))-->
          		<!--LIST:comments.replies-->
          		<div class="reply-comment">
                  <small>[[|comments.replies.full_name|]]<span> - <?php echo date('H:i\' d/m/Y',[[=comments.replies.time=]]);?></span></small>
                  <p>[[|comments.replies.content|]]</p>
              </div><!--End .text-new-->
              <!--/LIST:comments.replies-->
              <!--/IF:cond-->
        		<!--<a class="heading">Trả lời ([[|comments.total_reply|]])</a>
            <div class="main-comment down">
                <div class="col-md-10">
                    <div class="info-bound" style="float:left;">
                        <input name="full_name_[[|comments.id|]]" type="text" id="full_name_[[|comments.id|]]" placeholder="Họ và tên" class="text">
                    </div>
                    <div class="info-bound">
                        <input name="email_[[|comments.id|]]" type="text" id="email_[[|comments.id|]]" placeholder="Email của bạn" class="text">
                    </div>
                    <textarea id="content_[[|comments.id|]]" placeholder="Để lại lời bình"></textarea>
                    <div>
                        <input type="button" id="button_content_[[|comments.id|]]" class="button" value="Gửi bình luận">
                    </div>
                </div>
            </div><!--End .main-comment -->
          </div><!--End .comment-down-->
          </div><!--End .main-comment-->
          <!--/LIST:comments-->
          <div class="ajax-paging">
          <div class="preview <?php if([[=page=]]<=1) echo 'disabled';?>"  <?php if([[=page=]]>1) echo 'onclick="preview();"';?> id="page_preview" title="Trước">Trước</div>
					<div class="next <?php if([[=page=]] >= Url::get('total_page')) echo 'disabled'; ?>" <?php if([[=page=]] < Url::get('total_page')) echo 'onclick="next();"'; ?> id="page_next" title="Sau">Sau</div>
          </div><!--End .ajax-paging-->
  </div><!--End .other-comment--->
</div>
<script type="text/javascript">
	jQuery(document).ready(function() {
		//toggle the componenet with class msg_body
		jQuery(".heading").click(function()
				{
				if(jQuery(this).hasClass("heading")){
					jQuery(this).removeClass('heading');
					jQuery(this).addClass('activeing').next(".down").slideToggle(500);
				}
				else
				{
						jQuery(this).removeClass('activeing');
					jQuery(this).addClass('heading').next(".down").slideToggle(500);
				}
				})
	});
</script>
<script>
var page = [[|page|]];
var total_page = <?php echo Url::get('total_page');?>;
var name_id = "[[|name_id|]]";
var id = "[[|id|]]";
function guest_review(name_id,page)
{
	jQuery.ajax({
		method: "POST",
		url: 'form.php?block_id=<?php echo Module::block_id(); ?>',
		data : {
			'name_id':name_id,
			'page_no':page,
			'cmd':'get_page',
			'total_page':total_page
		},
		success: function(content){
			document.getElementById('comment-wrapper').innerHTML=content;
		}
	});
}
function next()
{
	page++;
	guest_review(name_id,page);
	jQuery("html, body").delay(1000).animate({scrollTop: jQuery('#comment-wrapper').offset().top }, 1000);
}
function preview()
{
	page--;
	guest_review(name_id,page);
	jQuery("html, body").delay(1000).animate({scrollTop: jQuery('#comment-wrapper').offset().top }, 1000);
}
</script>
<!--/IF:cond-->