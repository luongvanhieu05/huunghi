<?php
class GuestCommentDB
{
	static function get_comment($cond,$number_per_page,$page)
	{
		$sql = '
			SELECT
				news_comment.*
			FROM
				news_comment
				inner join news on news.id = news_comment.news_id
			WHERE
				'.$cond.'
			ORDER BY
				news_comment.time 
			LIMIT
				'.($page-1)*$number_per_page.','.$number_per_page.'
		';
		if($items = DB::fetch_all($sql)){
			foreach($items as $key=>$value){
				$cond = 'news_comment.topic_id = "'.$value['id'].'" and news_comment.checked';
				$items[$key]['replies'] = GuestCommentDB::get_comment($cond,1,100);
				$items[$key]['total_reply'] = sizeof($items[$key]['replies']);
			}
			return $items;
		}else{
			return array();
		}
	}
	static function get_total($cond)
	{
		$sql = '
			SELECT
				count(distinct news_comment.id) as acount
			FROM
				news_comment
				inner join news on news_comment.news_id = news.id
			WHERE
				'.$cond.'
		';
		return DB::fetch($sql);
	}
	function get_news($name_id){
		$sql = '
			select 
				news.id,news.name_id
			from 
				news
			where 
				news.name_id = "'.$name_id.'"';
		if($news=DB::fetch($sql)){
			return $news;
		}else{
			return false;
		}
	}
	function update_news_comment($news_id){
		$arr = array(
			'news_id'=>$news_id,
			'full_name',
			'email',
			'content',
			'time'=>time(),
		);
		DB::insert('news_comment',$arr);
	}
}
?>