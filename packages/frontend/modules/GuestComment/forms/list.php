<?php
class GuestCommentForm extends Form
{
	function GuestCommentForm()
	{
		Form::Form('GuestCommentForm');
	}
	function draw()
	{
		if($name_id = Url::get('name_id') and $cond = 'news.name_id = "'.$name_id.'" and news_comment.checked' and $news = GuestCommentDB::get_news($name_id))
		{
			$name_id = $news['name_id'];
			$this->map = $news;
			$this->map['page'] = $this->page_no();
			$number_per_page = 2;
			$total = GuestCommentDB::get_total($cond);
			$this->map['total_comment'] = $total['acount'];
			$this->map['total_page'] = ceil($total['acount']/$number_per_page);
			$_REQUEST['total_page'] = $this->map['total_page'];
			$this->map['comments'] = GuestCommentDB::get_comment($cond,$number_per_page,$this->map['page']);
			$this->map['can_comment'] = false;
			if($this->can_comment($news['id'])){
				$this->map['can_comment'] = true;	
			}
			$layout = 'list';
			$this->parse_layout($layout,$this->map);
		}
	}
	function page_no($page_name='page_no')
	{
		if($page_no = intval(Url::get($page_name)) and $page_no>0)
		{
			return intval(Url::get($page_name));
		}else
		{
			return 1;
		}
	}
	function can_comment($news_id){
		return true;
		/*if(User::is_login()){
			if(User::can_admin(false,ANY_CATEGORY) or DB::exists('select id from booking where hotel_id='.$news_id.' and user_id=\''.Session::get('user_id').'\'')){				
				return true;	
			}else{
				return false;	
			}
		}elseif($row = DB::fetch('select booking.id,booking.email,booking.user_id from booking where MD5(CONCAT("reviewxyz",booking.id))="'.Url::get('booking_info').'" and hotel_id='.$news_id.'')){
			if(!DB::exists('select id from news_comment where hotel_id='.$news_id.' and (email=\''.$row['email'].'\')')){
				return true;	
			}else{
				return false;	
			}
		}else{
			return false;
		}*/
	}
}
?>
