<?php
class ProductDetailForm extends Form
{
	function ProductDetailForm()
	{
		Form::Form('ProductDetailForm');
		$this->add('full_name',new TextType(true,'full_name',0,255));
		$this->add('email',new TextType(true,'email',0,255));
	}	
	function on_submit(){
		if($this->check()){			
			$array = array(
				'full_name',
				'email',
				'phone',
				'address',
				'zone_id',
				'note',
				'total'=>String::convert_to_vnnumeric(Url::get('price')),
				'time'=>time(),
				'paid'=>0
			);//'total'=>String::convert_to_vnnumeric(Url::get('price')),
			$invoice_id = DB::insert('order_invoice',$array);
			
			$detail_array = array(
				'invoice_id'=>$invoice_id,
				'product_id'=>Url::get('product_id'),
				'name'=>Url::get('product_name'),
				'price'=>Url::get('product_price')
			);//'total'=>String::convert_to_vnnumeric(Url::get('price')),
			DB::insert('order_invoice_detail',$detail_array);
			
			header('location:'.$_SERVER['REQUEST_URI'].'?success=true');
			exit();
		}
	}
	function draw()
	{
		require_once 'packages/frontend/includes/product.php';
		$this->map = array();
		//System::debug(ProductDetail::$product);		die;

		$this->map += ProductDetail::$product;
		if(PRODUCT::get_price($this->map['id']) and PRODUCT::get_price($this->map['id']) < $this->map['price']){
			$this->map['publish_price'] = System::display_number($this->map['price']);
			$this->map['price'] = System::display_number(PRODUCT::get_price($this->map['id']));
		}else{
			$this->map['publish_price'] = System::display_number($this->map['publish_price']);
			$this->map['price'] = System::display_number($this->map['price']);
		}
		if($pre_product = DB::fetch('SELECT id,category_id FROM product WHERE id > '.$this->map['id'].' and product.category_id = '.$this->map['category_id'].' ORDER BY id ASC LIMIT 0,1')){
			$this->map['pre_id'] = $pre_product['id'];
			$this->map['pre_category_id'] = $pre_product['category_id'];
		}else{
			$this->map['pre_id'] = 0;
			$this->map['pre_category_id'] = 0;
		}
		if($next_product = DB::fetch('SELECT id,category_id FROM product WHERE id < '.$this->map['id'].' and product.category_id = '.$this->map['category_id'].' ORDER BY id DESC LIMIT 0,1')){
			$this->map['next_id'] = $next_product['id'];
			$this->map['next_category_id'] = $next_product['category_id'];
		}else{
			$this->map['next_id'] = 0;
			$this->map['next_category_id'] = 0;
		}
		require_once'packages/core/includes/utils/paging.php';
		//$this->map['total'] = $count['acount'];		
		//$this->map['paging'] = paging($count['acount'],$product_per_page,3,false,'page_no',array('keyword'),Portal::language('page'));
		//$count = ProductDetailDB::get_total_product($cond);
		$product_per_page = 10;
		$cond = 'product.status !="HIDE" AND product.id <> '.$this->map['id'].' AND category.name_id = "'.$this->map['category_name_id'].'" ';
		$order_by = 'rand()';
		$products = ProductDetailDB::get_product($cond,$product_per_page,$order_by);
		
		$this->map['same_category_products'] = $products;
		//System::debug($this->map['same_category_products']);die;
		
		$product_per_page = 10;
		if($this->map['related_ids']){
			$cond = 'product.status<>"HIDE" AND product.id IN ('.$this->map['related_ids'].')';
			$order_by = 'product.id DESC';
			$products = ProductDetailDB::get_product($cond,$product_per_page,$order_by);
			$this->map['related_products'] = $products;		
		}else{
			$this->map['related_products'] = array();		
		}
		$images = DB::fetch_all('select * from product_image where product_id = '.$this->map['id'].'');
		//$current = current($catalogs);
		
		$this->map['images'] = $images;
		$this->map['image_url'] = '';
		foreach($images as $value){
			$this->map['image_url'] = $value['image_url'];
			break;
		}
		
		
		$category_detail = DB::select('category',' type="PRODUCT" and name_id = "'.Url::get('category_name_id').'"');
		$this->map['icon_categories'] = $category_detail['icon_url'];
		$this->map['list_categories'] = DB::select_all('category','type="PRODUCT" and '.IDStructure::direct_child_cond(DB::structure_id('category',69)));

			// System::debug($this->map['list_categories']);die;


		
		$this->parse_layout('detail',$this->map);
	}
	function update_view(){
		if(Url::iget('id')){
			DB::query('UPDATE product SET view = view + 1 WHERE id = '.Url::iget('id').'');
		}
	}
}
?>