<?php
/* 	AUTHOR 	:	KHOAND
	DATE	:	03/02/2015
*/
class ProductDetail extends Module
{
	public static $product = array();
	function ProductDetail($row){
		Module::Module($row);
		if(Url::get('cmd') == 'get_content'){
			$type = Url::get('data-type');
			$product = DB::select('product',' id="'.Url::get('id').'" ');
			//System::debug($product);die;
			if($type==1){
				echo $product['description_1'];exit();
			}
			if($type==2){
				echo $product['description_2'];exit();
			}
			exit();
		}
		if(Url::get('cmd') == 'check_ajax'){
			$this->check(); exit();
		}
		if(Url::get('name_id') and ProductDetail::$product = DB::fetch('
			SELECT
				product.id,product.status,
				product.name_id,
				product.name_'.Portal::language().' as name,
				product.brief_'.Portal::language().' as brief,				
				product.description_'.Portal::language().' as description,
				product.description_2,
				product.keywords,
				product.meta_description,
				product.technical_info_'.Portal::language().' as technical_info,
				product.image_url,
				product.small_thumb_url,
				product.small_thumb_url_2,
				type_1,
				type_2,
				product.price,
				product.publish_price,
				product.category_id,
				product.related_ids,
				product.open_promotion_time,
				product.close_promotion_time,
				product.tags,
				product.video,
				product.banve,				
				product.code,
				product.size,
				product.time,
				product.valuation,
				product.quantity,
				product.commentadmin,
				category.name_id AS category_name_id,
				category.name_'.Portal::language().' as category_name
			FROM
				product
				INNER JOIN product_category ON product_category.product_id = product.id
				INNER JOIN category ON product_category.category_id = category.id AND category.type="PRODUCT"
			WHERE
				product.name_id = "'.Url::get('name_id').'"')){
			if(file_exists(ProductDetail::$product['small_thumb_url'])){
				Portal::$image_url =  'http://'.$_SERVER['HTTP_HOST'].'/'.ProductDetail::$product['small_thumb_url'];
			}
			Portal::$canonical =  'http://'.$_SERVER['HTTP_HOST'].'/san-pham/'.ProductDetail::$product['category_name_id'].'/'.ProductDetail::$product['name_id'].'.html';
			Portal::$document_title =  ''.ProductDetail::$product['name'].'';
			Portal::$meta_keywords =  ProductDetail::$product['keywords'];
			Portal::$meta_description =  String::display_sort_title(strip_tags(ProductDetail::$product['meta_description']),40);
			//$this->addcookie('last_view_hotel',$hotel['id']);
			require_once 'db.php';
			require_once 'forms/detail.php';
			$this->add_form(new ProductDetailForm());
		}
	}
	function check(){
		if($comfirm_code = Url::get('verify_comfirm_code'))
		{
			if($comfirm_code == Session::get('security_code')) echo 'true';
			else echo 'false';
		}
	}
}
?>