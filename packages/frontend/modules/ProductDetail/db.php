<?php
class ProductDetailDB{
	static function get_product($cond,$product_per_page,$order_by='product.id'){
		return DB::fetch_all('
			SELECT
				product.id,product.status,
				product.name_'.Portal::language().' as name,
				product.brief_'.Portal::language().' as brief,				
				product.description_'.Portal::language().' as description,
				product.technical_info_'.Portal::language().' as technical_info,
				product.small_thumb_url,
				product.name_id,
				product.price,
				product.publish_price,				
				product.category_id,
				product.tags,
				product.video,
				category.name_id AS category_name_id,
				product.code
			FROM
				product
				INNER JOIN product_category ON product_category.product_id = product.id
				INNER JOIN category ON product_category.category_id = category.id AND category.type="PRODUCT"
			WHERE
				'.$cond.'
			ORDER BY
				'.$order_by.'
			LIMIT
				'.((page_no()-1)*$product_per_page).','.$product_per_page.'
		');
	}
	static function get_total_product($cond)
	{
		return DB::fetch('
			SELECT
				count(*) as acount
			FROM
				product
				INNER JOIN category ON category.id = product.category_id AND category.type="PRODUCT"
			WHERE
				'.$cond.'
		');
	}
	static function get_category(){
		$sql = '
			SELECT
				id,
				name_'.Portal::language().' as name,
				name_id,
				structure_id,
				status,
				type
			FROM
				category
			WHERE
				1
				and portal_id="'.PORTAL_ID.'"
				and '.IDStructure::child_cond(ID_ROOT,false).'
				'.$extra_cond.'
			ORDER BY
				structure_id';
		$categories = DB::fetch_all($sql);
	}
	static function get_thuoc_tinhs($product_id,$type='SIZE'){
		$sql = '
			select
				thuoc_tinh_sp.id,thuoc_tinh_sp.name
			from
				thuoc_tinh_sp
				inner join product_thuoc_tinh_sp as pt on pt.thuoc_tinh_id = thuoc_tinh_sp.id
			where
				pt.product_id = '.$product_id.'
				AND thuoc_tinh_sp.type="'.$type.'"';
		$items = DB::fetch_all($sql);
		return $items;		
	}
}
?>