<!--detail.html-->
<script type="text/javascript" src="assets/standard/js/fashion/detail/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="assets/standard/js/fashion/detail/datetimepickermoment.js"></script>
<script type="text/javascript" src="assets/standard/js/fashion/detail/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="assets/standard/css/fashion/detail/bootstrap-datetimepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/standard/css/fashion/detail/magnific-popup.css"/>
<link rel="stylesheet" type="text/css" href="assets/standard/css/fashion/detail/detail.css"/>
<!--detail.html-->

<div class="productpage" style="margin-top:50px;">
    <div class="row">
        <div class="col-sm-8 product-left">
            <img src="[[|small_thumb_url|]]" tile=""[[|name|]] >
        </div>
        <div class="col-sm-4 product-right">
            <h3 class="product-title">[[|name|]]</h3>
            <br>
            <ul class="list-unstyled">
                <li><h2 class="special-price">[[|price|]]</h2>
                <!--IF:cond([[=publish_price=]]>[[=price=]])-->
                    <span class="old-price" style="text-decoration: line-through;">[[|publish_price|]]</span>
                    <!--/IF:cond-->
                </li>
            </ul>
            <div id="product">
                <hr>
                <div class="form-group qty">
                      <items
                        item-name="[[|name|]]"
                        item-price="<?php echo System::calculate_number([[=price=]])?>"
                        item-quantity-id="input-quantity"
                        item-weight="1"
                        item-link="http://<?php echo $_SERVER['HTTP_HOST'].''.$_SERVER['REQUEST_URI']?>"
                        item-image="http://<?php echo $_SERVER['HTTP_HOST']?>/[[|small_thumb_url|]]" >
                      </items>
                    
                </div>
            </div>
            
            <p>[[|brief|]]</p>
        </div>
        <!--====chi tiet ve san pham===-->
        <div class="col-sm-12 product_tab" id="tabs_info">
            <div class="tab-content">
                <div class="row">
                    <div class="col-md-3">
                        <img src="assets/standard/images/otohonda/au.jpg" alt="oto hon da tay ho" style="max-width: 100%;">
                        <p style="font-weight: bold;font-size: 16px;color: #212121;margin-top:10px;">Đại diện PKD Honda Ôtô Tây Hồ</p>
                        <p style="font-weight: bold;font-size: 20px;color: #212121;">Nguyễn Trung Thành</p>
                        <p style="font-size: 30px;color: #f44336;font-weight: bold;">0981 738 999</p>
                        <p style="color:#000;">- Xe Ôtô Phù Hợp Nhất</p>
                        <p style="color:#000;">- Mức Giá Xe Honda Tốt Nhất.</p>
                        <p style="color:#000;">- Khuyến Mãi Mới Nhất</p>
                        <p style="color:#000;">- Trả Góp Lãi Suất Thấp Nhất</p>
                    </div>
                    <div class="col-md-9">
                        <p style="color:#000;font-size: 20px;font-weight: bold;">THÔNG TIN CHI TIẾT VỀ SẢN PHẨM</p>  <br>
                        <div class="tab-pane active" id="tab-description"><div class="cpt_product_description ">
                                <?php echo ([[=description=]]);?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 