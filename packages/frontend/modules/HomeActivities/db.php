<?php
class HomeActivitiesDB
{
	function get_news($cond)
	{
		$items = DB::fetch_all('
			SELECT
				news.id
				,news.publish
				,news.front_page
				,news.status
				,news.position
				,news.user_id
				,news.image_url
				,news.time
				,news.hitcount
				,news.name_'.Portal::language().' as name
                ,news.brief_'.Portal::language().' as brief
                ,news.name_id
                ,news.description_'.Portal::language().' as description
				,category.name_'.Portal::language().' as category_name
				,category.structure_id
			FROM
				news
				left outer join category on category.id = news.category_id
			WHERE
				'.$cond.'
				and news.portal_id="'.PORTAL_ID.'"
                ORDER BY news.position DESC,news.id DESC
                LIMIT 0,6
		');
		return ($items);
	}
	function get_category($cond)
	{
		$sql='
			SELECT
				id,
				name_'.Portal::language().' as name,
				name_id,
				url,
				structure_id,
                image_url
			FROM
				category
			WHERE
				'.$cond.'
			ORDER BY
				structure_id
				LIMIT 0,4
			';
		$items = DB::fetch_all($sql);
		foreach($items as $key=>$value){
		$items[$key]['items'] = DB::fetch_all('
				SELECT
					id,
					name_'.Portal::language().' as name,
					name_id,
					url,
					structure_id
				FROM
					category
				WHERE
					 '.IDStructure::child_cond($value['structure_id']).' and id!='.$value['id'].'
				ORDER BY
					position DESC, id ASC
			');
		}
		return $items;
	}
}
?>