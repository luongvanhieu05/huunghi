<?php
//  WRITEN BY	: MINHTC
//  DATE  		: 07/12/2009
//	EDITED BY Ngocub
//	TCV_KS
//	DATE :15/12/2009
class HomeActivities extends Module
{
	function HomeActivities($row)
	{
		Module::Module($row);
		require_once 'db.php';
		require_once 'forms/list.php';
		$this->add_form(new HomeActivitiesForm());
        switch(Url::get('cmd'))
			{
			     case 'change_new':
                    $this->return_change();
                    break;
                 default:
                 break;
		    }
	}
    function return_change(){
        //echo Url::get('category_name_id');
   		$cond='news.type="NEWS" and news.publish=1 and news.status!="HIDE" and category.name_id="'.Url::get('category_name_id').'"';
		$news =HomeActivitiesDB::get_news($cond);
        $vitri=1;
        $ketqua='';
        $chuoiketqua2='';
        foreach ($news as $key=>$value){
                if (file_exists($news[$key]['image_url'])){$image_url=$news[$key]['image_url'];}else{$image_url='assets/default/images/no_image.gif';}
            //Lay tin dau tien
            if ($vitri==1){
                $ketqua='<li class="title-content"><a href="'.Url::build('xem-trang-tin',array('name_id'=>$news[$key]['name_id']),1).'">'.String::display_sort_title(strip_tags($news[$key]['name']),15).'</a></li>
                <li class="img-1"><a href="'.Url::build('xem-trang-tin',array('name_id'=>$news[$key]['name_id']),1).'"><img class="img-top-1" src="'
                .$image_url
                .'"></a></li>
                <li class="brif-content-02">'.String::display_sort_title(strip_tags($news[$key]['brief']),24).'</li>';
                /*$ketqua.='|'.$news[$key]['brief'];*/
            }
            //lay cac tin tiep theo
            if($vitri>1){
                $chuoiketqua2.='<div id="warrap-content-right">
            	<li><a href="'.Url::build('xem-trang-tin',array('name_id'=>$news[$key]['name_id']),1).'"><img class="img-top-end" src="'.$image_url.'"></a></li>
            	<li class="brif-content-child"><a href="'.Url::build('xem-trang-tin',array('name_id'=>$news[$key]['name_id']),1).'">'.String::display_sort_title(strip_tags($news[$key]['name']),20).'</a></li>
                </div>';
            }
            $vitri++;
        }
        //System::debug($news);
        if (Portal::language()==1){$view_all='Xem tất cả';}else{$view_all='View all';}
        $xemtatca='<div class="detail"><span><a href="'.Url::build('danh-sach-tin/'.Url::get('category_name_id').'','',1).'">>>'.$view_all.'</a></span></div>';
        echo ($ketqua=='')?('|'.$xemtatca):($ketqua.'|'.$chuoiketqua2.$xemtatca);
        exit();
    }
}
?>