<div class="frame-home-bound" style="width: 678px;margin-top:15px;">
    <div class="frame-home-title">
        <div class="fr-ht-bound">
            <ul class="fr-ht-b-left">
        			[[.Activities.]]
            </ul>
            <ul class="fr-ht-b-right">
            </ul>
            <ul class="fr-ht-b-end-left">
            <?php $vitri=2;?>
                <!--LIST:category-->
                <ul><li><a class="a-li-main-02" onclick="change_news_02('[[|category.name_id|]]','change_new');jQuery('.a-li-main-02').css('color','#626362');jQuery(this).css('color','#009140');">[[|category.name|]]</a></li>
                <?php if($vitri!=count([[=category=]])) echo '<img />';?>
                </ul>
                <?php $vitri++; ?>
                <!--/LIST:category-->
            </ul>
            <ul class="img-load"><li><img src="assets/hotel/images/loading (1).gif" width="20px" height="20px" border="0" /></li></ul>
        </div><!--End .fr-ht-bound-->
    </div><!--End .frame-home-title-->
    <div style="clear: both;"></div>
    <div class="home-content">
    	<ul class="content-left-02">
            <?php $vitri=1; ?>
            <!--LIST:news-->
            <?php if ($vitri==1) {?>
                <li class="title-content"><a href="<?php echo Url::build('xem-trang-tin',array('name_id'=>[[=news.name_id=]]),1); ?>"><?php echo String::display_sort_title(strip_tags([[=news.name=]]),14); ?></a></li>
                <li class="img-1"><a href="<?php echo Url::build('xem-trang-tin',array('name_id'=>[[=news.name_id=]]),1); ?>"><img class="img-top-1" src="<?php echo (file_exists([[=news.image_url=]]))?([[=news.image_url=]]):('assets/default/images/no_image.gif');?>"/></a></li>
                <li class="brif-content-02"><?php echo String::display_sort_title( strip_tags([[=news.brief=]]),24); ?></li>
            <?php } ?>
            <?php $vitri++; ?>
            <!--/LIST:news-->
        </ul>
        <ul class="border-left-content">
            <img src="assets/hotel/images/border-left-content.png"  />
        </ul>
        <ul class="content-right-02">
            <!--IN RA 5 tin sau-->
            <?php $vitri=1; ?>
            <!--LIST:news-->
            <?php if ($vitri>1) {?>
        	<div id="warrap-content-right">
            	<li><a href="<?php echo Url::build('xem-trang-tin',array('name_id'=>[[=news.name_id=]]),1); ?>"><img class="img-top-end" src="<?php echo (file_exists([[=news.image_url=]]))?([[=news.image_url=]]):('assets/default/images/no_image.gif');?>" /></a></li>
            	<li class="brif-content-child"><a href="<?php echo Url::build('xem-trang-tin',array('name_id'=>[[=news.name_id=]]),1); ?>"><?php echo String::display_sort_title( strip_tags([[=news.name=]]),20); ?></a></li>
            </div>
            <?php } ?>
            <?php $vitri++; ?>
            <!--/LIST:news-->
            <div class="detail">
            	<span><a href="<?php echo Url::build('linh-vuc-hoat-dong','',REWRITE); ?>">>> [[.view_all.]]</a></span>
            </div>
        </ul>
    </div>
</div>
<script type="text/javascript">
//explode bang javascript
function explode (delimiter, string, limit) {
    // Splits a string on string separator and return array of components. If limit is positive only limit number of components is returned. If limit is negative all components except the last abs(limit) are returned.
    //
    // version: 1109.2015
    // discuss at: http://phpjs.org/functions/explode    // +     original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +     improved by: kenneth
    // +     improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +     improved by: d3x
    // +     bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)    // *     example 1: explode(' ', 'Kevin van Zonneveld');
    // *     returns 1: {0: 'Kevin', 1: 'van', 2: 'Zonneveld'}
    // *     example 2: explode('=', 'a=bc=d', 2);
    // *     returns 2: ['a', 'bc=d']
    var emptyArray = {        0: ''
    };
    // third argument is not required
    if (arguments.length < 2 || typeof arguments[0] == 'undefined' || typeof arguments[1] == 'undefined') {        return null;
    }
    if (delimiter === '' || delimiter === false || delimiter === null) {
        return false;    }
    if (typeof delimiter == 'function' || typeof delimiter == 'object' || typeof string == 'function' || typeof string == 'object') {
        return emptyArray;
    }
    if (delimiter === true) {
        delimiter = '1';
    }
     if (!limit) {
        return string.toString().split(delimiter.toString());
    }
    // support for limit argument
    var splitted = string.toString().split(delimiter.toString());    var partA = splitted.splice(0, limit - 1);
    var partB = splitted.join(delimiter.toString());
    partA.push(partB);
    return partA;
}
function change_news_02(category_name_id,cmd){
    jQuery.ajax({
        method: "POST",url: 'form.php?block_id=<?php echo Module::block_id();?>',
        data : {
            'cmd':cmd,
            'category_name_id':category_name_id
        },
        beforeSend: function(){
		jQuery(".img-load").css("display","block")
		jQuery(".home-content").ajaxComplete(function(){
		jQuery(".img-load").css("display","none"); });
        },
        success: function(content){
            //explode mang ket qua
            ra = explode('|',content);
            //alert(content);
            jQuery('.content-left-02').html(ra[0]);
            //jQuery('.brif-content-02').html(ra[1]);
            jQuery('.content-right-02').html(ra[1]);
        }
    });
}
</script>
<style>
.img-load{
	list-style:none;
	display:none;
}
.img-load img{
	padding-top:2px;
}
</style>