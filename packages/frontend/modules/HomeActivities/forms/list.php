<?php
class HomeActivitiesForm extends Form
{
	function HomeActivitiesForm()
	{
		Form::Form('HomeActivitiesForm');
		$this->link_css('packages/frontend/templates/Frame/assets/home/style.css');
		//$this->link_js('packages/core/includes/js/jquery/dropdown.js');
	}
	function draw()
	{
		$this->map = array();
		//$cond ='publish and portal_id="'.PORTAL_ID.'" and category_id=459';
		//$this->map['news'] =HomeTinTucDB::get_news($cond);
        //Truy van cac danh muc cua tin tuc
		$cond='type="NEWS" and portal_id="'.PORTAL_ID.'" and '.IDStructure::direct_child_cond( DB::structure_id('category','486'));
		$this->map['category'] =HomeActivitiesDB::get_category($cond);
        //Truy van cac tin tuc con
		$cond='news.type="NEWS" and news.publish=1 and news.status!="HIDE" and '.IDStructure::child_cond( DB::structure_id('category','486'));
		$this->map['news'] =HomeActivitiesDB::get_news($cond);
        $this->parse_layout('list',$this->map);
		//System::debug($this->map['news']);
	}
}
?>