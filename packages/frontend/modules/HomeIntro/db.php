<?php
class HomeIntroDB{
	static function get_comments($item_per_page=5){
		return DB::fetch_all('
			SELECT
				comment.*
			FROM
				comment
			WHERE
				comment.publish
			ORDER BY
				comment.position asc,comment.time DESC,comment.id DESC
			LIMIT
				0,'.$item_per_page.'
		');
	}
	static function get_faqs($item_per_page=5){
		$items = DB::fetch_all('
			SELECT
				news.id,
				news.brief_'.Portal::language().' as name,
				news.description_'.Portal::language().' as description
			FROM
				news
			WHERE
				news.type = "FAQ" AND news.status!="HIDE"
			LIMIT
				0,'.$item_per_page.'
		');
		foreach($items as $key=>$value){
			$items[$key]['name'] = strip_tags($value['name']);
		}
		return $items;
	}
	static function get_videos()
	{
		$items = DB::fetch_all('
			select
				media.id,
				media.name_'.Portal::language().' as name,
				media.name_id,
				media.image_url,
				media.embed,
				category.name_id AS category_name_id
			from
				media
				INNER JOIN category ON category.id = media.category_id
			where
				media.status="HOT"
			ORDER BY
				media.id DESC LIMIT 0,1
		');
		return $items;
	}
}
?>