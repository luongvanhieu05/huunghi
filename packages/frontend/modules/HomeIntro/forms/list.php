<?php
class HomeIntroForm extends Form
{
	function HomeIntroForm()
	{
		Form::Form('HomeIntroForm');
		
	}
	function draw()
	{
		$this->map = array();
		$this->map['videos'] = HomeIntroDB::get_videos();
		$this->map['comments'] = HomeIntroDB::get_comments();
		$this->map['faqs'] = HomeIntroDB::get_faqs();
		$this->parse_layout('list',$this->map);
	}
}
?>