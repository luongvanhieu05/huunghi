<?php
class LastestNewsForm extends Form
{
	function LastestNewsForm()
	{
		Form::Form('LastestNewsForm');
	}
	function draw(){
		$this->map = array();
		$category_id = 585;
		$cond ='news.publish and news.portal_id="'.PORTAL_ID.'" and '.IDStructure::child_cond(DB::structure_id('category',$category_id)).'';
		$limit ='0,5';
		$this->map['news'] = LastestNewsDB::get_news($cond,$limit);
		$this->parse_layout('list',$this->map);
	}
}
?>