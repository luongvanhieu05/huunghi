<!--IF:news([[=items=]])-->
<div class="news-list-bound">
        <li class="news-list-item">
            <!--LIST:items-->
            <div class="fr-ht-bound-2">
            	<div class="fr-ht-bound-3">
                <div class="fr-ht-b-left"><a href="<?php echo Url::build('danh-sach-tin',array('name_id'=>[[=items.name_id=]]),REWRITE)?>">[[|items.name|]]</a></div>
                <div class="fr-ht-b-right"></div>
           		</div>
            </div>
            <!--LIST:items.new-->
            <div class="brif-lvhd">
            	<ul class="brif-lvhd-img">
                	<a href="<?php echo Url::build('xem-trang-tin',array('name_id'=>[[=items.new.name_id=]]),REWRITE)?>"><img src="[[|items.new.image_url|]]" /></a>
                </ul>
            	<ul class="brif-lvhd-content">
                	<span class="title-lvhd" style="font-weight:bold;"><a href="<?php echo Url::build('xem-trang-tin',array('name_id'=>[[=items.new.name_id=]]),REWRITE)?>"><?php echo String::display_sort_title( strip_tags([[=items.new.name=]]),25); ?></a></span><br />
                	<?php echo String::display_sort_title( strip_tags([[=items.new.brief=]]),73); ?>
                </ul>
                <!--
                <div class="news-list-detail"><a href="<?php echo Url::build('xem-trang-tin',array('name_id'=>[[=items.new.name_id=]]),REWRITE)?>">[[.view_detail.]]</a></div>
            	-->
            <div style="clear:both"></div>
            </div>
            <!--/LIST:items.new-->
            <!--/LIST:items-->
        </li>
    <!--<div class="news-list-paging">[[|paging|]]</div>-->
</div>
<!--ELSE-->
<div class="news-list-bound">
	<div class="news-list-category-name">[[|category_name|]]</div>
		<div class="notice">[[.data_is_updating.]]</div>
	</div>
</div>
<!--/IF:news-->