<?php
class NewsListForm extends Form
{
	function NewsListForm()
	{
		Form::Form('NewsListForm');
		$this->link_css(Portal::template().'css/news.css');
		$this->link_css('packages/frontend/templates/Frame/assets/home/style.css');
	}
	function on_submit(){
	}
	function draw()
	{
		$this->map = array();
		$cond='type="NEWS" and portal_id="'.PORTAL_ID.'" and '.IDStructure::direct_child_cond( DB::structure_id('category','585'));
		$item_per_page = 10;
		//require_once'packages/core/includes/utils/paging.php';
		$count = NewsListDB::get_total_item($cond);
		$this->map['paging'] = paging($count['acount'],$item_per_page,3,REWRITE,'page_no',array('name_id'));
		$this->map['items'] = NewsListDB::get_category($cond);
		//System::debug($this->map['items']);
		//Truy van cac tin tuc con
		$cond='news.type="NEWS" and news.publish=1 and news.status!="HIDE" and '.IDStructure::child_cond( DB::structure_id('category','585'));
		$this->map['news'] =NewsListDB::get_news($cond);
		$this->parse_layout('list',$this->map);
		}
}
?>