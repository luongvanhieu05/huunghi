<?php
class NewsListDB
{
	function get_news($cond)
	{
		$items = DB::fetch_all('
			SELECT
				news.id
				,news.publish
				,news.front_page
				,news.status
				,news.position
				,news.user_id
				,news.image_url
				,news.time
				,news.hitcount
				,news.name_'.Portal::language().' as name
                ,news.brief_'.Portal::language().' as brief
                ,news.name_id
                ,news.description_'.Portal::language().' as description
				,category.name_'.Portal::language().' as category_name
				,category.structure_id
			FROM
				news
				left outer join category on category.id = news.category_id
			WHERE
				'.$cond.'
				and news.portal_id="'.PORTAL_ID.'"
                ORDER BY news.position DESC,news.id DESC
                LIMIT 0,5
		');
		return ($items);
	}
	static function get_category($cond){
		$sql='
			SELECT
				id,
				name_'.Portal::language().' as name,
				name_id,
				url,
				structure_id,
                image_url,
				description_1
			FROM
				category
			WHERE
				'.$cond.'
			ORDER BY
				structure_id
			';
		$items = DB::fetch_all($sql);
		foreach($items as $key=>$value){
		$items[$key]['new'] = DB::fetch_all('
			SELECT
				news.id
				,news.publish
				,news.front_page
				,news.status
				,news.position
				,news.user_id
				,news.image_url
				,news.time
				,news.hitcount
				,news.name_'.Portal::language().' as name
                ,news.brief_'.Portal::language().' as brief
                ,news.name_id
                ,news.description_'.Portal::language().' as description
			FROM
				news
			WHERE
				category_id='.$value['id'].
				' and news.portal_id="'.PORTAL_ID.'"
                ORDER BY news.position DESC,news.id DESC
                LIMIT 0,1
			');
		}
		return $items;
	}
}
?>