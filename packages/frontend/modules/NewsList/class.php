<?php
//	AUTHOR: MINHTC
//	DATE  : 14/09/2009
class NewsList extends Module
{
	static $category = array();
	function NewsList($row)
	{
		Module::Module($row);
		require_once 'db.php';
		$cond = 'name_id="'.addslashes(Url::get('name_id')).'" and portal_id="'.PORTAL_ID.'"';
		if(Url::get('name_id') and $category = NewsListDB::get_category($cond))
		{
			NewsList::$category = $category;
			Portal::$document_title = $category['name'];
			$_REQUEST['category_id'] = $category['id'];
		}
		require_once 'forms/list.php';
		$this->add_form(new NewsListForm());
	}
}
?>