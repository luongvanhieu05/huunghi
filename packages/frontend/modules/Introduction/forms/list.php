<?php
class IntroductionForm extends Form
{
	function IntroductionForm()
	{
		Form::Form('IntroductionForm');
		
	}
	function draw()
	{
		$this->map = array();
		$items = IntroductionDB::get_introduction();
		$this->parse_layout('list',array('items'=>$items));
	}
}
?>