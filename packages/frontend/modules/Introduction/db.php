<?php
class IntroductionDB{
	function get_introduction(){
		return DB::fetch_all('
			SELECT
				news.id,
				news.name_'.Portal::language().' as name,
				news.brief_'.Portal::language().' as brief,
				news.description_'.Portal::language().' as description,
				news.image_url,
				news.category_id,
				news.file,
				news.time,
				news.name_id,
				category.name_id as category_name
			FROM
				news
				INNER JOIN category ON news.category_id=category.id
			WHERE
				category_id = 581 and publish
			ORDER BY
				news.position asc,news.time DESC,news.id DESC
		');
	}
}
?>