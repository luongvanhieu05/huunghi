<?php
class NewsForm extends Form
{
	function NewsForm()
	{
		Form::Form('NewsForm');
		$this->link_css('assets/hotel/css/news-pro.css');
		//$this->link_js('packages/core/includes/js/jquery/dropdown.js');
	}
	function draw()
	{
		$this->map = array();
   		$item_per_page = 20;
		$category_id = 3;
		$this->map['category_name'] = 'Tin tức';
		$this->map['category_name_id'] = 'trang-tin';
		if(Url::get('category_name_id') and $row = DB::fetch('SELECT id,name_id,name_'.Portal::language().' AS name FROM category WHERE name_id ="'.Url::get('category_name_id').'"')){
			$category_id = $row['id'];
			$this->map['category_name'] = $row['name'];
			$this->map['category_name_id'] = $row['name_id'];
		}
		require_once'packages/core/includes/utils/paging.php';
		$cond='news.type="NEWS" and news.publish=1 and news.status!="HIDE" and '.IDStructure::child_cond( DB::structure_id('category',$category_id));
		if(Url::get('tags')){
			$cond .= ' AND (news.tags LIKE "%'.Url::sget('tags').'%")';
		}
		$count = NewsDB::get_total_item($cond);
		$this->map['paging'] = paging($count['acount'],$item_per_page,3,REWRITE,'page_no',array('name_id','category_name_id'));

		$this->map['news'] =NewsDB::get_total_news($cond,$item_per_page);
		$cond='news.type="NEWS" and news.publish=1 and news.status!="HIDE" and '.IDStructure::child_cond( DB::structure_id('category',$category_id));
		$this->map['lastest_news'] =NewsDB::get_total_news($cond,5,'news.hitcount DESC');

		$structure_id2 = DB::structure_id('category',3);
		$category_left = NewsDB::get_cate($structure_id2);
		
		$this->map['list_category'] = $category_left;
		// System::debug($_REQUEST);die;

    	$this->parse_layout('list',$this->map);
		//System::debug($this->map['news']);
	}
}
?>