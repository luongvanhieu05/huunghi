<link rel="stylesheet" type="text/css" href="assets/standard/css/fashion/product_list/product_list.css">
<link rel="stylesheet" type="text/css" href="assets/standard/css/fashion/news_list/news_list.css">
<div class="product-category-20_27_43 layout-2 left-col" style="margin-top:50px;">
    <div class="container ">
        <div class="row">
            <div style="padding:0px 15px;font-size: 20px;font-weight: bold;">TIN TỨC</div>
            <div class="col-sm-12">
                <div class="category_filter">
                    <div class="col-md-4 btn-list-grid">
                        <div class="btn-group">

                        </div>
                    </div>
                    <div class="compare-total"></div>
                </div>

                <div class="row">
                    <div class="product_content">
                        <!--LIST:news-->
                        <div class="product-layout product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="news-items">
                                <div class="product-block-inner">
                                    <div class="image">
                                        <a href="/trang-tin/[[|news.category_name_id|]]/[[|news.name_id|]].html"><img src="<?php if([[=news.small_thumb_url=]]!=null) echo [[=news.small_thumb_url=]];else echo 'assets/standard/images/fashion/megnor/img.png' ?>" class="img-responsive"></a>
                                    </div>
                                    <div class="caption">
                                        <h4><a style="line-height: 1.4;" class="tieu-de-tin-tuc" href="/trang-tin/[[|news.category_name_id|]]/[[|news.name_id|]].html"><?php echo String::display_sort_title(strip_tags([[=news.name=]]),25);?></a></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/LIST:news-->


                    </div>
                </div>
                <div class="pagination-wrapper">
                    <div class="col-sm-6 text-left page-link">
                        <div class="pt">[[|paging|]]</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>