<?php
class NewsDB{
	static function get_news($cond){
		$items = DB::fetch_all('
			SELECT
				news.id
        ,news.name_id
				,news.publish
				,news.front_page
				,news.status
				,news.position
				,news.user_id
				,news.small_thumb_url
				,news.time
				,news.hitcount
				,news.name_'.Portal::language().' as name
				,news.brief_'.Portal::language().' as brief
				,news.description_'.Portal::language().' as description
				,category.name_id as category_name_id
				,category.name_'.Portal::language().' as category_name
				,category.structure_id
			FROM
				news
				INNER JOIN category on category.id = news.category_id
			WHERE
				'.$cond.'
				and news.portal_id="'.PORTAL_ID.'"
			ORDER BY news.position DESC,news.id DESC
				LIMIT 0,15
		');
		foreach($items as $key=>$value){
			$items[$key]['brief'] = String::display_sort_title(strip_tags($value['brief']),40);
		}
		return ($items);
	}
	static function get_total_news($cond,$item_per_page,$order='news.position DESC,news.id DESC'){
		$items = DB::fetch_all('
			SELECT
				news.id
        ,news.name_id
				,news.publish
				,news.front_page
				,news.status
				,news.position
				,news.user_id
				,news.image_url
				,news.small_thumb_url 
				,news.time
				,news.hitcount
				,news.name_'.Portal::language().' as name
				,news.brief_'.Portal::language().' as brief
				,news.description_'.Portal::language().' as description
				,category.name_'.Portal::language().' as category_name
				,category.structure_id
				,category.name_id as category_name_id
			FROM
				news
				INNER JOIN category on category.id = news.category_id
			WHERE
				'.$cond.'
				and news.portal_id="'.PORTAL_ID.'"
                ORDER BY '.$order.'
                LIMIT
				'.((page_no()-1)*$item_per_page).','.$item_per_page.'
		');
		foreach($items as $key=>$value){
			$items[$key]['brief'] = String::display_sort_title(strip_tags($value['brief']),40);
		}
		return ($items);
	}
	static function get_total_item($cond){
		return DB::fetch('
			SELECT
				count(*) as acount
			FROM
				news
				inner join category on news.category_id = category.id
			WHERE
				'.$cond.'
		');
	}

	static function get_cate2($structure_id){
		$sql='select
				id,name_id,name_'.Portal::language().' as name,structure_id,brief_1 as brief,
				icon_url as image_url,url
			from
				category
			where
				type="NEWS" AND '.IDStructure::direct_child_cond($structure_id).'
				AND status <> "HIDE"
			order by
				structure_id
				';
		$items = DB::fetch_all($sql);
		return $items;
	}

	static function get_cate($structure_id){
		$sql='select
				id,name_id,name_'.Portal::language().' as name,structure_id,brief_1 as brief,
				icon_url as image_url,url
			from
				category
			where
				type="NEWS" AND '.IDStructure::direct_child_cond($structure_id).'
				AND status <> "HIDE"
			order by
				structure_id
				';
		$items = DB::fetch_all($sql);
		foreach($items as $key=>$value){
			if((IDStructure::level($value['structure_id']))<3){
				$cate_child = NewsDB::get_cate2($value['structure_id']);
				$items[$key]['cate_child'] = $cate_child;
			}
		}
		return $items;
	}


	static function get_category($cond){
		$sql='
			SELECT
				id,
				name_'.Portal::language().' as name,
				name_id,
				url,
				structure_id,
        image_url
			FROM
				category
			WHERE
				'.$cond.'
			ORDER BY
				structure_id
			';
		$items = DB::fetch_all($sql);
		foreach($items as $key=>$value){
		$items[$key]['items'] = DB::fetch_all('
				SELECT
					id,
					name_'.Portal::language().' as name,
					name_id,
					url,
					structure_id
				FROM
					category
				WHERE
					 '.IDStructure::child_cond($value['structure_id']).' and id!='.$value['id'].'
				ORDER BY
					position DESC, id ASC
			');
		}
		return $items;
	}
}
?>