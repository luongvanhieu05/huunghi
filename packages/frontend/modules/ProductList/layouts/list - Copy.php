<link rel="stylesheet" type="text/css" href="assets/standard/css/fashion/product_list/product_list.css">
<div class="" style="padding-top: 45px;">
<div class="content_breadcum"></div>
<div class="container ">
 <form name="ProductList" id="ProductList" method="post" enctype="multipart/form-data">
    <div class="row">
        <!--=====CONTENT LEFT=====-->
        <column id="column-left" class="col-sm-3 hidden-xs">
            <div class="box" style="padding-top:30px;">
                <img src="assets/standard/images/otohonda/au.jpg" alt="oto hon da tay ho" style="max-width: 100%;">
                <p style="font-weight: bold;font-size: 16px;color: #212121;margin-top:10px;">Đại diện PKD Honda Ôtô Tây Hồ</p>
                <p style="font-weight: bold;font-size: 20px;color: #212121;">Nguyễn Trung Thành</p>
                <p style="font-size: 30px;color: #f44336;font-weight: bold;">0981 738 999</p>
                <p style="color:#000;">- Xe Ôtô Phù Hợp Nhất</p>
                <p style="color:#000;">- Mức Giá Xe Honda Tốt Nhất.</p>
                <p style="color:#000;">- Khuyến Mãi Mới Nhất</p>
                <p style="color:#000;">- Trả Góp Lãi Suất Thấp Nhất</p>
            </div>
            
        </column>

        <!--=====CONTENT RIGHT=====-->
        <div class="col-sm-9">
            <div class="row category_thumb">
                <div class="col-sm-10 category_description"><p><br></p></div>
            </div>

            <div class="category_filter">
                <div class="col-md-4 btn-list-grid">
                    <div class="btn-group">
                        <button type="button" id="grid-view" class="btn btn-default grid active" data-toggle="tooltip" title="" data-original-title="Lưới"></button>
                        <button type="button" id="list-view" class="btn btn-default list" data-toggle="tooltip" title="" data-original-title="Danh sách"></button>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="product_content">
                    <?php $c=1;?>
                    <!--LIST:items-->
                    <div class="product-layout product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="product-block product-thumb">
                            <div class="product-block-inner">
                                <div class="image">
                                    <a href="/san-pham/[[|items.category_name_id|]]/[[|items.name_id|]].html"><img src="<?php if([[=items.small_thumb_url=]]!=null) echo [[=items.small_thumb_url=]];else echo 'assets/standard/images/fashion/megnor/img.png' ?>" class="img-responsive"></a>
                                    <div class="saleback">
                                        <!--<span class="saleicon sale">[[|items.name|]]</span>-->
                                    </div>
                                    <div class="action">
                                        <div class="action_inner">
                                        </div>
                                    </div>
                                </div>
                                <div class="caption">
                                    <h4><a href="/san-pham/[[|items.category_name_id|]]/[[|items.name_id|]].html">[[|items.name|]]</a></h4>
                                    <div class="rating_list">
                                        <?php $dem=isset([[=items.valuation=]]) ? [[=items.valuation=]] : '0';if([[=items.valuation=]]>5)[[=items.valuation=]]=0;if([[=items.valuation=]]<0)[[=items.valuation=]]=0; ?>
                                        <?php for($i=1;$i<=$dem;$i++){if([[=items.valuation=]]>0){ ?>
                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                        <?php  }}?>
                                        <?php  for($j=1;$j<=(5-$dem);$j++){?>
                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i></span>
                                        <?php   }?>
                                    </div>
                                    <p class="desc"> <?php echo String::display_sort_title(strip_tags([[=items.brief=]]),20) ?></p>
                                    <p class="price">
                                        <div class="price-new">[[.sell_price.]]: [[|items.price|]]</div>
                                        <!--IF:price_cond(intval([[=items.publish_price=]]) > intval([[=items.price=]]))-->
                                        <div class="price-old">[[.price.]]: [[|items.publish_price|]]</div>
                                        <!--/IF:price_cond-->
                                    </p>
                                    <div class="addtolinks_list">
                                        <div class="button-group">
                                            <button class="cart_button" type="button" onClick="window.location='cart.html?product_id=[[|items.id|]]';">Thêm vào giỏ</button>
                                        </div>
                                    </div>
                                    <div class="rating">
                                        <?php $dem=isset([[=items.valuation=]]) ? [[=items.valuation=]] : '0';if([[=items.valuation=]]>5)[[=items.valuation=]]=0;if([[=items.valuation=]]<0)[[=items.valuation=]]=0; ?>
                                            <?php for($i=1;$i<=$dem;$i++){if([[=items.valuation=]]>0){ ?>
                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                        <?php  }}?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/LIST:items-->
                </div>
            </div>
            <div class="pagination-wrapper">
                <div class="col-sm-6 text-left page-link">
                    <div class="pt">[[|paging|]]</div>
                </div>
            </div>
        </div>
    </div>
    </form>
</div>
</div>