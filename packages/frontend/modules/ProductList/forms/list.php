<?php
class ProductListForm extends Form{
	function ProductListForm(){
		Form::Form('ProductListForm');
	}
	function on_submit(){
		$this->get_condition();

	}
	function get_condition(){
		$cond = '';
		if(Url::get('sex')){
			$cond = '  product.sex = "'.Url::get('sex').' " and ';
		}
		if(Url::get('khoanggia')){
			if(Url::get('khoanggia')==1){
				$cond .= '  (product.price >= "100000" and product.price <= 500000) and';
			}
			if(Url::get('khoanggia')==2){
				$cond .= '  (product.price > "500000" and product.price <= 1000000) and';
			}
			if(Url::get('khoanggia')==3){
				$cond .= '  (product.price > "1000000" ) and';
			}

		}
		return $cond;
	}
	function draw(){	
		if(Url::get('category_name_id')){
			$cate = DB::select('category','type="PRODUCT" and name_id="'.Url::get('category_name_id').'"');
			$product = ProductListDB::get_item_prd('category.name_id="'.Url::get('category_name_id').'"');
			//System::debug($product);die;
			$var = '/san-pham/'.$product['category_name_id'].'/'.$product['name_id'].'.html';
			echo '<script>window.location.href = "'.$var.'";</script>';
			exit();

		}
		$this->map = array();
		//$_REQUEST['category_name_id'] = '';
		$this->map['category_name'] = 'Sản phẩm';
		$this->map['category_name_id'] = 'san-pham';
		$parent_structure_id = false;
		$structure_id = ID_ROOT;
		$level = 0;
		$have_category = false;
		$have_parent = false;
		//System::Debug($_REQUEST);
		if(Url::get('category_name_id') and $category = DB::select('category','name_id="'.Url::get('category_name_id').'"')){
			$have_category = true;		
			$this->map['category_name'] = $category['name_'.Portal::language()];
			$this->map['category_name_id'] = $category['name_id'];
			$this->map['category_id'] = $category['id'];
			$structure_id = $category['structure_id']; 
			$parent_structure_id = $structure_id;
			$level = IDStructure::level($category['structure_id']);
			if(Url::get('parent_category_name_id') and $parent = DB::select('category','name_id="'.Url::get('parent_category_name_id').'"')){
				$parent_structure_id = $parent['structure_id'];
				$have_parent = true;			
			}
		}
		if($level  == 0){
			$structure_id = ID_ROOT;
		}
		/*if($level  == 0){
			if($level  == 0){
				$structure_id = ID_ROOT;
			}
			$this->map['items'] = ProductListDB::get_categories($structure_id);
			$layout = 'c_list';
		}else*/
		{
			$sex = Url::get('sex');
			$khoanggia = Url::get('khoanggia');
			$this->map['sex']=isset($sex) ? $sex : '';
			$this->map['khoanggia']=isset($khoanggia) ? $khoanggia : '';

			$cond = $this->get_condition();
			$cond .= ' product.status <> "HIDE" ';
			if(Url::get('keyword')){		
				require_once 'packages/core/includes/utils/search.php';
				require_once 'packages/core/includes/utils/vn_code.php';
				$cond .= Url::get('keyword')?' AND (product.name_'.Portal::language().' LIKE "%'.Url::sget('keyword').'%" OR product.description_'.Portal::language().' LIKE "%'.DB::escape(str_to_search_keyword(addslashes(Url::sget('keyword')),$search_hightlight)).'%")':'';
			}
			$cond .= $have_parent?' 
				AND '.IDStructure::child_cond($parent['structure_id']).'':'
			';
			$cond .= $have_category?' 
				AND '.IDStructure::child_cond($category['structure_id']).'':'
			';
			$cond .= '
				'.(Url::get('tags')?' AND (product.tags like "%'.str_replace('-',' ',Url::sget('tags')).'%")':'').'
			';
			$item_per_page = Session::get('sptrentrang')?Session::get('sptrentrang'):24;
			require_once'packages/core/includes/utils/paging.php';
			$count = ProductListDB::get_total_item($cond);
			$this->map['total'] = $count['acount'];		
			$this->map['paging'] = paging($count['acount'],$item_per_page,3,true,'page_no',array('parent_category_name_id','category_name_id','tags'),'Trang');
			$items = ProductListDB::get_items($cond,$item_per_page);
			if(isset($search_hightlight)){
				$search_hightlight  = array();
				//str_to_search_keyword(Url::sget('keyword'),$search_hightlight);
				foreach($items as &$item){
					//$item['description'] = hightlight_keyword(strip_tags($item['description']), $search_hightlight);
				}
			}
			$this->map['items'] = $items;
			$structure_id2 = $parent_structure_id?$parent_structure_id:ID_ROOT;
			$category_left = ProductListDB::get_cate($structure_id2);
			$this->map['list_category'] = $category_left;
			$this->map['sp_noibat'] = ProductListDB::get_sp_noibat();
			//System::debug($this->map['sp_noibat']);die;

			//$this->map['dang_quan'] = ProductListDB::get_thuoc_tinh('DANG');
			//$this->map['phong_cach'] = ProductListDB::get_thuoc_tinh('PHONG_CACH');
			//$this->map['mau'] = ProductListDB::get_thuoc_tinh('MAU');
			//$this->map['cap'] = ProductListDB::get_thuoc_tinh('CAP');
			$layout = 'list';
		}
		$this->map['order_by_list'] = array(''=>'==Sắp xếp==') + array('price_down'=>'Giá gảm dần','price_up'=>'Giá tăng dần','promote'=>'KM');
		$this->parse_layout($layout,$this->map);
	}
}
?>