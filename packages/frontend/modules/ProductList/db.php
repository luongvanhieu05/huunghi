<?php
class ProductListDB{
	static function get_item_prd($cond){
		$items = DB::fetch('
			SELECT
				product.id,product.status,product.name_'.Portal::language().' as name,
				product.small_thumb_url,
				product.name_id,
				product.description_1 as description,
				category.name_id AS category_name_id,
				product.publish_price,
				product.price
			FROM
				product
				INNER JOIN product_category ON product_category.product_id = product.id
				INNER JOIN category ON product_category.category_id = category.id AND category.type="PRODUCT"
			WHERE
				'.$cond.'
		');
		
		return $items;
	}

	static function get_categories($structure_id){
		$categories = DB::fetch_all('
			select 
				id,name_id,name_'.Portal::language().' as name,structure_id,
				icon_url as image_url,url
			from 
				category 
			where 
				type="PRODUCT" AND '.IDStructure::direct_child_cond($structure_id).' 
				AND status <> "HIDE"
			order by 
				structure_id
		');	
		return $categories;
	}

	static function get_sp_noibat(){
		$items = DB::fetch_all('
			SELECT
				product.id,
				product.status,product.name_'.Portal::language().' as name,
				product.small_thumb_url,
				product.name_id,
				product.publish_price,
				product.valuation,
				product.price,
				product.open_promotion_time,
				product.close_promotion_time,
				category.name_id AS category_name_id

				FROM
					product
					INNER JOIN product_category ON product_category.product_id = product.id
					INNER JOIN category ON product_category.category_id = category.id AND category.type="PRODUCT"
				WHERE
					product.status ="HOT"
				ORDER BY
					product.id DESC
				LIMIT
					0,5
		');
		foreach($items as $key=>$value){
			$items[$key]['price'] = System::display_number($value['price']);
			if(strtotime($value['open_promotion_time']) <= time() and strtotime($value['close_promotion_time']) >= time()){
				$items[$key]['publish_price'] = System::display_number($value['publish_price']);
			}else{
				$items[$key]['publish_price'] = 0;
			}
		}
		return $items;
	}

	 static function get_cate2($structure_id){
		$sql='select
				id,name_id,name_'.Portal::language().' as name,structure_id,brief_1 as brief,
				icon_url as image_url,url
			from
				category
			where
				type="PRODUCT" AND '.IDStructure::direct_child_cond($structure_id).'
				AND status <> "HIDE"
			order by
				structure_id
				';
		$items = DB::fetch_all($sql);
		return $items;
	}

	static function get_cate($structure_id){
		$sql='select
				id,name_id,name_'.Portal::language().' as name,structure_id,brief_1 as brief,
				icon_url as image_url,url
			from
				category
			where
				type="PRODUCT" AND '.IDStructure::direct_child_cond($structure_id).'
				AND status <> "HIDE"
			order by
				structure_id
				';
		$items = DB::fetch_all($sql);
		foreach($items as $key=>$value){
			$items[$key]['category_name_id'] = '';
			$items[$key]['level1_name_id'] = '';
			$level = IDStructure::level($value['structure_id']);
			if($level==2){
				$items[$key]['level1_name_id'] = DB::fetch('select name_id from category where structure_id = '.IDStructure::parent($value['structure_id']).'','name_id');
			}
			if($level<3){
				$cate_child = ProductListDB::get_cate2($value['structure_id']);
				foreach($cate_child as $k=>$v){
					$cate_child[$k]['level1_name_id'] = DB::fetch('select name_id from category where structure_id = '.IDStructure::parent($v['structure_id']).'','name_id');		
					$child_level4 = ProductListDB::get_cate2($v['structure_id']);
					$cate_child[$k]['cate_child'] = $child_level4;
				}
				$items[$key]['cate_child'] = $cate_child;
			}
		}
		return $items;
	}
	static function get_items($cond,$item_per_page){
		$order_by = 'product.position DESC,product.time DESC';
		switch(Url::get('order_by')){
			case 'price_down': $order_by = 'product.price DESC'; break;
			case 'price_up': $order_by = 'product.price ASC'; break;
			case 'promote': $order_by = 'product.promote DESC'; break;
			default: $order_by = 'product.position DESC,product.time DESC'; break;
		}		
		$items = DB::fetch_all('
			SELECT
				product.id,product.status,
				product.name_'.Portal::language().' as name,
				product.description_'.Portal::language().' as description,
				product.small_thumb_url,
				product.name_id,
				product.publish_price,
				product.price,
				product.valuation,
				product.open_promotion_time,
				product.close_promotion_time,
				product.brief_1 as brief,
				category.name_id AS category_name_id
			FROM
				product
				INNER JOIN product_category ON product_category.product_id = product.id
				INNER JOIN category ON product_category.category_id = category.id AND category.type="PRODUCT"
				
			WHERE
				'.$cond.'
			GROUP BY
				product.id
			ORDER BY
				'.$order_by.'
			LIMIT
				'.((page_no()-1)*$item_per_page).','.$item_per_page.'
		');/*LEFT OUTER JOIN product_thuoc_tinh_sp AS ptt ON ptt.product_id = product.id
				LEFT OUTER JOIN thuoc_tinh_sp AS tt ON tt.id = ptt.thuoc_tinh_id*/
		foreach($items as $key=>$value){
			if(PRODUCT::get_price($key) and PRODUCT::get_price($key) < $items[$key]['price']){
				$items[$key]['price'] = System::display_number(PRODUCT::get_price($key));
				$items[$key]['publish_price'] = System::display_number($value['price']);
			}else{
				$items[$key]['publish_price'] = 0;
				$items[$key]['price'] = System::display_number($value['price']);
				$items[$key]['publish_price'] = System::display_number($value['publish_price']);
			}
		}
 		return $items;
	}
	static function get_total_item($cond)
	{
		return DB::fetch('
				SELECT
					count(distinct product.id) as acount
				FROM
					product
					INNER JOIN product_category ON product_category.product_id = product.id
					INNER JOIN category ON product_category.category_id = category.id AND category.type="PRODUCT"
					
				WHERE
					'.$cond.'
				
			');/*LEFT OUTER JOIN product_thuoc_tinh_sp AS ptt ON ptt.product_id = product.id
					LEFT OUTER JOIN thuoc_tinh_sp AS tt ON tt.id = ptt.thuoc_tinh_id*/
	}

	static function get_thuoc_tinh($type){
		$items = DB::fetch_all('
			SELECT
				thuoc_tinh_sp.id,
				thuoc_tinh_sp.name,
				thuoc_tinh_sp.name_id,
				thuoc_tinh_sp.status,
				thuoc_tinh_sp.type
				FROM
					thuoc_tinh_sp
				WHERE
					thuoc_tinh_sp.type ="'.$type.'"
				ORDER BY
					thuoc_tinh_sp.id DESC
				LIMIT
					0,10
		');
		return $items;
	}
}
?>