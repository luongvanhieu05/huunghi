<link rel="stylesheet" type="text/css" href="assets/standard/css/fashion/product_list/product_list.css">
<link rel="stylesheet" type="text/css" href="assets/standard/css/fashion/news_list/news_list.css">
<div class="" style="padding-top:50px;">
    <div class="container ">
        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-8">
                <div class="detail">
                    <h1 class="title">[[|name|]]</h1>
                    [[|description|]]
                </div>
                <br><br>
                <div class="other-news">
                    <div class="title">Tin liên quan</div>
                    <ul>
                        <!--LIST:item_related-->
                        <li><a href="trang-tin/[[|item_related.category_name_id|]]/[[|item_related.name_id|]].html"><span class="glyphicon glyphicon-hand-right" aria-hidden="true"></span>
                                <?php echo str_replace(array('&nbsp;','+','-'),'',[[=item_related.name=]]);?>
                            </a></li>
                        <!--/LIST:item_related-->
                    </ul>
                </div>
                <!--/IF:cond-->
            </div>
            <div class="col-sm-2"></div>
        </div>
    </div>
</div>