<?php
class NewsDetail extends Module{
	static $item = false;
	function NewsDetail($row){
		if(Url::get('cmd')=='check_ajax'){
			$this->check();
			exit();
		}
		Module::Module($row);
		require_once 'db.php';
		$this->init();
		require_once 'forms/list.php';
		$this->add_form(new NewsDetailForm());
	}
	function init(){
		if(Url::get('name_id') and $item = NewsDetailDB::get_item('news.name_id="'.addslashes(Url::sget('name_id')).'" and news.portal_id="'.PORTAL_ID.'"')){
			NewsDetail::$item = $item;
			$_REQUEST['category_id'] = $item['category_id'];
			Portal::$document_title = $item['name'];
			Portal::$meta_keywords = str_replace(array(' ','"','!','\'','"','"'),', ',strip_tags($item['name']));
			Portal::$meta_description = strip_tags($item['brief']);
			if($item['small_thumb_url']){
				Portal::$image_url = 'http://'.$_SERVER['HTTP_HOST'].'/'.$item['small_thumb_url'];
			}
		}
	}
	function check(){
		if($comfirm_code = Url::get('verify_comfirm_code')){
			if($comfirm_code == Session::get('security_code')) echo 'true';
			else echo 'false';
		}
		exit();
	}
}
?>