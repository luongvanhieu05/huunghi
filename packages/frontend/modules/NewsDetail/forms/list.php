<?php
class NewsDetailForm extends Form
{
	function NewsDetailForm()
	{
		Form::Form('NewsDetailForm');
		$this->add('full_name',new TextType(true,'full_name',0,255));
		$this->add('email',new EmailType(true,'email'));
		$this->add('content',new TextType(true,'content',0,2000));
	}
	function on_submit(){
		$security_code = Session::get('security_code');
		if($this->check()){
			if(($_REQUEST["verify_comfirm_code"] == $security_code) && (!empty($_REQUEST["verify_comfirm_code"]) && !empty($security_code))){
				if(Url::get('id')){
					NewsDetailDB::update_news_comment(Url::iget('id'));
					if(Url::iget('id') and $news = DB::select('news','id='.Url::iget('id').'')){
							echo '<script>
											alert("Bạn đã gửi phản hồi bài viết thành công. Phản hồi sẽ được hiển thị sau khi Tòa soạn kiểm duyệt nội dung!");
											window.location = "'.Url::get('name_id').'.html";	
									</script>';
							exit();
					}
				}
			}else{
			 	$this->error('verify_comfirm_code','invalid_confirm_code');
			}
		}
	}
	function draw()
	{
		//System::debug($_REQUEST);die;
		$cond = 'and news.portal_id="'.PORTAL_ID.'" and  news.status!="HIDE"  and news.type="NEWS"';
		$item = array('id'=>'0','category_id'=>'0');
		$this->map = array();
		$mode = false;
		$this->map['category_id'] = 216;
		require_once 'packages/core/includes/utils/format_text.php';
		if($item = NewsDetail::$item)
		{
			$category = DB::fetch('select id, name_'.Portal::language().' as category_name,name_id from category where id = '.$item['category_id']);
			$this->map['category_name'] = $category['category_name'];
			$this->map['category_name_id'] = $category['name_id'];
			$this->map['category_id'] = $category['id'];
			$mode = true;
			DB::update_hit_count('news',$item['id']);

			// echo $item['category_id'];die;
			$cond = 'news.portal_id="'.PORTAL_ID.'" and '.IDStructure::child_cond(DB::structure_id('category',$item['category_id'])).' and  news.status!="HIDE"';
			$this->map['item_related'] = NewsDetailDB::get_items($cond);
			// System::debug($this->map['item_related']);die;


			$cond = 'news.portal_id="'.PORTAL_ID.'" and '.IDStructure::child_cond(DB::structure_id('category',$item['category_id'])).' and  news.status!="HIDE" and news.id<>'.$item['id'];
			if($this->map['category_name_id'] == 'video'){
				$cond = 'news.portal_id="'.PORTAL_ID.'" and '.IDStructure::child_cond(DB::structure_id('category',$item['category_id'])).' and news.status!="HIDE" and news.id<>'.$item['id'];	
			}
			$this->map['item_related'] = NewsDetailDB::get_items($cond);
			// System::debug($this->map['item_related']);die;
			$cond=' category.id!="6" && category.id!="208" && category.id!="209"';
			$this->map['lastest_news'] = NewsDetailDB::get_items($cond);
			// System::debug($this->map['lastest_news']);die;
			//$this->map['lastest_news'] = NewsDetailDB::get_items('news.portal_id="'.PORTAL_ID.'" and '.IDStructure::child_cond(DB::structure_id('category',$this->map['category_id'])).' and news.status!="HIDE" and news.id<>'.$item['id']);
			//Ten loai danh muc cua tin
			$item['tags'] = String::create_tags($item['tags'],'trang-tin');
			$this->map['main_category']= NewsDetailDB::get_field_category('category.id='.$item['category_id'],'name_'.Portal::language());
			$structure_id2 = DB::structure_id('category',3);
			$category_left = NewsDetailDB::get_cate($structure_id2);
			$this->map['list_category'] = $category_left;
			//System::Debug($item);die;
			$this->parse_layout('list',$mode?$item+$this->map:$this->map);
		}
	}
}
?>
