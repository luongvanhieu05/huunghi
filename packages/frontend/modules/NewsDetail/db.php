<?php
class NewsDetailDB{
    static function get_field_category($cond=' 1',$field='id'){
        return DB::fetch('SELECT * FROM category WHERE '.$cond,$field);
    }
	static function get_item($cond)
	{
		return DB::fetch('
			SELECT
				news.id,
				news.name_'.Portal::language().' as name,
				news.brief_'.Portal::language().' as brief,
				news.description_'.Portal::language().' as description,
				news.image_url,
				news.category_id,
				news.file,
				news.time,
       	news.name_id,
				news.keywords,
				news.tags,
				news.small_thumb_url,
				category.name_id as category_name_id
			FROM
				news
				inner join category on category.id = news.category_id
			WHERE
				'.$cond.'
		');
	}
	static function get_items($cond=" 1 = 1")
	{
		$items = DB::fetch_all('
			SELECT
				news.id,
				news.name_'.Portal::language().' as name,
				news.name_id,
				news.brief_'.Portal::language().' as brief,
				news.description_'.Portal::language().' as description,
				news.image_url,
				news.small_thumb_url,
				news.category_id,
				news.time,
				news.file,
				news.name_id,
				news.small_thumb_url,
				category.name_id as category_name_id
			FROM
				news
				inner join category on news.category_id=category.id
			WHERE
				news.status !="HIDE"  and news.type="NEWS" and '.$cond.'
			ORDER BY
				news.position desc,news.id desc
			LIMIT
				0,10
		');
		return $items;
	}
	static function get_category($cond){
		return DB::fetch('
			SELECT
				news.id,news.name_id,category.id as category_id,category.name_'.Portal::language().' as category_name,category.name_id as category_name_id
			FROM
				news
				INNER JOIN category ON item.category_id=category.id
			WHERE
				'.$cond.'
		');
	}
	static function update_news_comment($news_id){
		$arr = array(
			'news_id'=>$news_id,
			'full_name',
			'email',
			'content',
			'time'=>time(),
		);
		DB::insert('news_comment',$arr) and $news =  DB::select('news','id='.$news_id);
	}

	static function get_cate2($structure_id){
		$sql='select
				id,name_id,name_'.Portal::language().' as name,structure_id,brief_1 as brief,
				icon_url as image_url,url
			from
				category
			where
				type="NEWS" AND '.IDStructure::direct_child_cond($structure_id).'
				AND status <> "HIDE"
			order by
				structure_id
				';
		$items = DB::fetch_all($sql);
		return $items;
	}

	static function get_cate($structure_id){
		$sql='select
				id,name_id,name_'.Portal::language().' as name,structure_id,brief_1 as brief,
				icon_url as image_url,url
			from
				category
			where
				type="NEWS" AND '.IDStructure::direct_child_cond($structure_id).'
				AND status <> "HIDE"
			order by
				structure_id
				';
		$items = DB::fetch_all($sql);
		foreach($items as $key=>$value){
			if((IDStructure::level($value['structure_id']))<3){
				$cate_child = NewsDetailDB::get_cate2($value['structure_id']);
				$items[$key]['cate_child'] = $cate_child;
			}
		}
		return $items;
	}
}
?>
