<?php
class RelatedNewsDB
{
	function get_news($cond)
	{
		$sql='
			SELECT
				news.id,
				news.name_'.Portal::language().' as name,
				news.name_id,
                news.small_thumb_url,
				category.name_id as category_name_id,
				category.name_'.Portal::language().' as category_name
			FROM
				news
				INNER JOIN category on category.id = news.category_id
			WHERE
				'.$cond.'
			ORDER BY
				news.time DESC
			LIMIT 
				0,10
			';
		$items = DB::fetch_all($sql);
		return $items;
	}
}
?>