<?php
class RelatedNewsForm extends Form
{
	function RelatedNewsForm()
	{
		Form::Form('RelatedNewsForm');
		$this->link_css(Portal::template().'/css/related-news.css');
	}
	function draw(){
		//System::debug($_REQUEST);
		$this->map = array();
		$category_id = 623;
		$cond=''.IDStructure::child_cond(DB::structure_id('category',$category_id)).'';
		$this->map['news'] = RelatedNewsDB::get_news($cond);
       // System::debug($this->map['category']);
		$this->parse_layout('list',$this->map);
	}
}
?>