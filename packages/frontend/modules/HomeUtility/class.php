<?php
class HomeUtility extends Module
{
	function HomeUtility($row)
	{
		Module::Module($row);
		require_once 'db.php';
		require_once 'packages/core/includes/utils/xml.php';
		// gold rate
		if(file_exists('cache/utils/golden.xml')){
			$golden = XML::fetch_all('cache/utils/golden.xml');
			if(time()-filemtime('cache/utils/golden.xml') >= 3600 or !$golden){
				$this->update_gold_rates();
			}
		}else{
			$this->update_gold_rates();
		}
		// weather
		if(file_exists('cache/utils/weather.xml')){
			$weather = XML::fetch_all('cache/utils/weather.xml');
			if(time()-filemtime('cache/utils/weather.xml') >= 3600 or !$weather){
				$this->update_weather();
			}
		}else{
			$this->update_weather();
		}
		// currency
		if(file_exists('cache/utils/currency.xml')){
			$currency = XML::fetch_all('cache/utils/currency.xml');
			if(time()-filemtime('cache/utils/currency.xml') >= 3600 or !$currency){
				$this->update_currency();
			}
		}else{
			$this->update_currency();
		}
		require_once 'forms/utility.php';
		$this->add_form(new HomeUtilityForm());
	}
	function update_weather()
	{
		$url = 'http://www.nchmf.gov.vn/web/vi-VN/81/Default.aspx';
		$content = @file_get_contents($url);
		$province = '/\<td width=\"20%\" align=left class=\"thoitiet_hientai rightline\"\>([^\<]+)\<\/td\>/';
		$weather = '/\<img src=\"http\:\/\/www\.khituongvietnam\.gov\.vn\/web\/Upload\/WeatherSymbol\/([^\"]+)" border=0>/';
		$temperature = '/class=\"thoitiet_hientai rightline\"\>\<strong\>([0-9]+)/';
		//Duc them
		$doam = '/\<td width=\"15%\" align=\"center\" class=\"thoitiet_hientai rightline\"\>([^<]+)<\/td\>/';
		$extra1 = '/\<td width=\"30\%\" align=\"left\" ([^>]+)\>([^<]+)\<\/td\>/';
		$extra2 = '/\<span class=\"thoitiet_hientai\"  style=\"Display:Yes\"\>([^<]+)\<\/span\>/';
		//$humidity = '/class=\"thoitiet_hientai rightline\"\>\<strong\>([0-9]+)/'; // do am
		if(preg_match_all($province,$content,$matches))
		{
			$name_province = $matches[1];
		}
		if(preg_match_all($weather,$content,$matches))
		{
			$images= $matches[1];
		}
		if(preg_match_all($temperature,$content,$matches))
		{
			$temperatures = $matches[1];
		}
		if(preg_match_all($extra1,$content,$matches))
		{
			$extra1 = $matches[2];
		}
		if(preg_match_all($extra2,$content,$matches))
		{
			$extra2 = $matches[1];
		}
		if(preg_match_all($doam,$content,$matches))
		{
			$doams = $matches[1];
		}
		if(isset($name_province) and isset($temperatures) and isset($images))
		{
			$items = array();
			foreach($images as $key=>$value)
			{
				$sou = 'http://www.nchmf.gov.vn/Upload/WeatherSymbol/'.$value;
				$des = 'upload/default/icon/'.substr($sou,strrpos($sou,'/')+1);
				if(!file_exists($des))
				{
					@copy($sou,$des);
				}
				$items[$key+1]['id'] = $key+1;
				$items[$key+1]['images'] = 'http://'.$_SERVER['SERVER_NAME'].'/'.$des;
				$items[$key+1]['province'] = $name_province[$key];
				$items[$key+1]['temperature'] = $temperatures[$key];
				$items[$key+1]['extra1'] = isset($extra1[$key])?$extra1[$key]:'';
				$items[$key+1]['extra2'] = isset($extra2[$key])?$extra2[$key]:'';
				$items[$key+1]['doam'] = isset($doams[$key])?$doams[$key]:'';
			}
			XML::create_xml('cache/utils/weather',$items);
		}
	}
	function update_currency()
	{
		$source = 'http://vietcombank.com.vn/ExchangeRates/ExrateXML.aspx';
		$content = file_get_contents($source);
		if(preg_match_all('/Exrate CurrencyCode="(.*)" CurrencyName="(.*)" Buy="(.*)" Transfer="(.*)" Sell="(.*)"/',$content,$matches) and count($matches)>0)
		{
			foreach($matches[1] as $key=>$value)
			{
				$row = array(
					'id'=>$value
					,'name'=>$matches[2][$key]
					,'exchange'=>$matches[5][$key]
					,'position'=>$key
				);
				if($item = DB::exists_id('currency',$value))
				{
					DB::update_id('currency',$row,$item['id']);
				}
				else
				{
					DB::insert('currency',$row);
				}
			}
		}
		$items = DB::fetch_all('
			SELECT
				*
			FROM
				currency
			ORDER BY
				position desc,id desc
		');
		XML::create_xml('cache/utils/currency',$items);
	}
	function update_gold_rates(){
		$gold = array();
		$source = 'http://www.sjc.com.vn/xml/tygiavang.xml';
		$doc = new DOMDocument();
		$doc->load($source);
		$cities = $doc->getElementsByTagName("city");
		$i=1;
		foreach($cities as $city){
			$item = $city->getElementsByTagName("item");
			$gold[$i]['id'] = $i;
			$gold[$i]['name'] = $city->getAttribute('name');
			$gold[$i]['buy'] = $item->item(0)->getAttribute('buy');
			$gold[$i]['sell'] = $item->item(0)->getAttribute('sell');
			$i++;
		}
		XML::create_xml('cache/utils/golden',$gold);
	}
}
?>
