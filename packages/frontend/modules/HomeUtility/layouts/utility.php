<div class="utility-bound">
	<div class="utility-item-bound">
        <div class="utility-item-title" content="gold_rate"><div class="utility-gold-rate-title"><img src="assets/hotel/images/incon-giavang.png" />[[.gold_rate.]] <span>ĐVT:tr.đ/lượng</span></div></div>
        <div class="utility-gold-rate">
        <table cellpadding="2" width="100%" border="1" bordercolor="#CCCCCC" style="margin:auto;background-color:#FFF;font-size: 11px;margin-top: 2px;">
        	<thead>
            	<th>&nbsp;[[.vang_sjc_1l.]]</th>
            	<th align="center">[[.buy.]]</th>
            	<th align="center">[[.sell.]]</th>
            </thead>
            <!--LIST:goldrate-->
            <tr>
            	<td>[[|goldrate.name|]]</td>
                <td align="center">[[|goldrate.buy|]]</td>
                <td align="center">[[|goldrate.sell|]]</td>
            </tr>
            <!--/LIST:goldrate-->
        </table>
        </div>
        <div class="source">[[.source.]]: sjc.com.vn</div>
    </div>
    <div class="utility-item-bound">
        <div class="utility-item-title" content="exchange_rate"><div class="utility-exchange-title"><img src="../../../../../assets/hotel/images/icon-tygia.png" /> [[.currency_exchange.]]</div></div>
        <div class="utility-exchange-rate-bound">
            <div class="utility-exchange-rate">
            <table cellpadding="2" width="100%" border="1" bordercolor="#CCCCCC" style="margin:auto;border-collapse:collapse; background-color:#FFF;font-size: 11px;">
                <!--LIST:currency-->
                <tr>
                    <td width="60%">[[|currency.id|]]</td>
                    <td align="right"><?php echo System::display_number_report([[=currency.exchange=]])?></td>
                </tr>
                <!--/LIST:currency-->
            </table>
            </div>
<!--
	        <div class="source">[[.source.]]: vietcombank.com.vn</div>
-->
        </div>
    </div>
</div>
<script language="javascript">
jQuery(document).ready(function(){
	//set Default HN
	init();
	jQuery('.utility-exchange-title').click(function(){
		jQuery('.utility-exchange-rate-bound').slideToggle(100);
	});
	jQuery('.utility-gold-rate-title').click(function(){
		jQuery('.utility-gold-rate').slideToggle(100);
	});
});
function init()
{
	change_id(4);
}
function change_id(id)
{
	var HN = jQuery('.utility-weather-content[id='+id+']').html();
	jQuery('.utility-weather-template').html(HN);
	jQuery('#weather_zone option[value='+id+']').attr('selected','selected');
}
function load_ajax(pram,block)
{
	jQuery.ajax({
		method: "POST",url: 'form.php?block_id='+block,
		data : pram,
		beforeSend: function(){
			jQuery('#loading').fadeIn(100);
		},
		success: function(content){
			//jQuery(obj).html('<b>[[.da_luu.]]</b>');
			//alert(content);
			//jQuery('#loading-layer').fadeOut(100);
			jQuery('#loading').hide()
			document.getElementById('module_'+block).innerHTML=content;
		}
	});
}
</script>
