<div class="gallery-bound">
	<div class="frame-home-title" style="width: 676px;">
        <div class="fr-ht-bound">
            <div class="fr-ht-b-left">
            	[[.gallery_detail.]]
            </div>
            <div class="fr-ht-b-right"></div>
            <div class="fr-ht-b-end-left"></div>
        </div><!--End .fr-ht-bound-->
        <!--Gach ngang cuoi title-->
        <div style="width: 675px;height: 1px;background:#e5e5e5;"></div>
        <div style="clear: both;"></div>
    </div>
    <div class="bound-content-gallery-detail">
        <div class="gallery-category-bound-detail">
        	<!--LIST:items-->
            <ul class="gallery-detail-title">
            	<span>[[|items.name|]]</span>
            </ul>
            <!--/LIST:items-->
            <ul class="gallery-category-img-detail">
            <!--LIST:photo-->
                <ul class="gallery-border-detail">
                    <li><a rel="lightbox-tour" href="[[|photo.image_url|]]"><img src="[[|photo.image_url|]]" onerror="this.src='assets/default/images/no_image.gif'" /></a></li>
                </ul>
            <!--/LIST:photo-->
            </ul>
         </div>
     </div>
</div>
<script type="text/javascript">
jQuery(function(){
	jQuery('.gallery-category-img-detail a').lightBox();
});
</script>