<?php
class DetailGalleryForm extends Form
{
	function DetailGalleryForm()
	{
		Form::Form('DetailGalleryForm');
		$this->link_css(Portal::template().'css/gallery.css');
		$this->link_css(Portal::template().'css/news.css');
		$this->link_css(Portal::template(false).'/css/jquery/css/jquery.lightbox-0.5.css');
		$this->link_js('packages/core/includes/js/jquery/jquery.lightbox-0.5.min.js');
		//$this->link_js(Portal::template().'js/jquery.prettyPhoto.js');
		//$this->link_css(Portal::template().'css/prettyPhoto.css');
	}
	function draw()
	{
		$this->map = array();
		$id=Url::get('id');
		$this->map['photo'] = DetailGalleryDB::get_photo($id);
		$this->map['items'] = DetailGalleryDB::get_items($id);
		$this->parse_layout('list',$this->map);
		//System::debug($this->map['photo']);
	}
}
?>