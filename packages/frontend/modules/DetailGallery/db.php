<?php
class DetailGalleryDB{
	function get_photo($id)
	{
		return DB::fetch_all('
			SELECT *FROM
			 		media_image
			WHERE
					media_id='.$id.'
		');
	}
	function get_items($id){
		return DB::fetch_all('
			SELECT
				media.id,
				media.name_'.Portal::language().' as name,
				media.description_'.Portal::language().' as description,
				media.url,
				media.image_url,
				category.name_id as category
			FROM
				media
				inner join category on category.id = media.category_id
			WHERE
				media.id='.$id.'
		');
	}
}
?>