<div class="newshot-bound">
	<div class="newshot-main-content">
    	<!--IF:cond(isset([[=news=]]) and [[=news=]])-->
        	<!--LIST:news-->
            <div class="newshot-main-content-bound">
                <div class = "newshot-tittle"><a href="<?php echo Url::build('xem-trang-tin',array('name_id'=>[[=news.name_id=]]),REWRITE);?>">[[|news.name|]]</a></div>
                <div class = "newshot-content">
                    <?php if(file_exists([[=news.small_thumb_url=]])) {?>
                    <a href="<?php echo Url::build('xem-trang-tin',array('name_id'=>[[=news.name_id=]]),REWRITE);?>"><img class="newshot-img" src="[[|news.small_thumb_url|]]"/></a>
                    <?php }elseif(file_exists([[=news.image_url=]])) {?>
                    <a href="<?php echo Url::build('xem-trang-tin',array('name_id'=>[[=news.name_id=]]),REWRITE);?>"><img class="newshot-img" src="[[|news.image_url|]]"/></a><?php }?>
                    <div class="newshot-brief"><?php echo String::display_sort_title(strip_tags([[=news.brief=]]),200);?></div>
                    <div class="newshot-link"><a href="<?php echo Url::build('xem-trang-tin',array('name_id'=>[[=news.name_id=]]),REWRITE);?>">[[.view_detail.]]</a></div>
                </div>
            </div>
                <!--/LIST:news-->
                <div class="clear"></div>
        <!--/IF:cond-->
    </div>
</div>