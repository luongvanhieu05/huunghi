<div class="container-fix">
  <div class="container">
      <div class="col-left">
        <h2 class="title">Thông tin du lịch</h2>
        <ul class="list_2">
        	<?php $i=1;?>
        	<!--LIST:news-->
          <li>
          <span class="list_count"><?php echo str_pad($i,2,"0",STR_PAD_LEFT);$i++;?>. </span>
          <div>
          <strong><a href="<?php echo Url::build([[=news.category_name_id=]],array('name_id'=>[[=news.name_id=]]),REWRITE);?>"><?php echo String::display_sort_title(strip_tags([[=news.name=]]),10);?></a></strong>
          <span><?php echo String::display_sort_title(strip_tags([[=news.brief=]]),200);?></span>
          </div>
          <div class="clear"></div>
          </li>
          <!--/LIST:news-->
        </ul>
      </div><!--End .col-left-->
      <div class="col-right">
          [[--right_region--]]
      </div>
  </div><!--End .container-->
</div><!--End .container-fix-->