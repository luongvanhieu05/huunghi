<?php
class FeatureNewsForm extends Form
{
	function FeatureNewsForm()
	{
		Form::Form('FeatureNewsForm');
		$this->link_css(Portal::template('hotel').'/css/ngocub.css');
	}
	function draw(){
		$this->map = array();
		$category_id = 600;// thong tin du lich
		$cond ='news.type = "NEWS" AND news.publish = 1 AND '.IDStructure::child_cond(DB::structure_id('category',$category_id)).'';
		$limit ='0,1';
		$this->map['news'] = FeatureNewsDB::get_news($cond,$limit);
		$this->parse_layout('list',$this->map);
	}
}
?>