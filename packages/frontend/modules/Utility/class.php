<?php
class Utility extends Module
{
	function Utility($row)
	{
		$filename = 'cache/utils/weather.xml';
		if(date('d/m/Y',@filemtime($filename))!=date('d/m/Y'))
		{
			require_once 'packages/backend/modules/Utils/class.php';
			require_once 'packages/core/includes/utils/xml.php';
			Utils::update_weather();
			Utils::update_golden();
			Utils::update_currency();
			//Url::redirect('utils',array('cmd'=>'auto_update'));
		}
		Module::Module($row);
		require_once 'forms/utility.php';
		$this->add_form(new UtilityForm());
	}
}
?>
