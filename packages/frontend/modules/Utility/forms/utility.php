<?php
class UtilityForm extends Form
{
	function UtilityForm()
	{
		Form::Form('UtilityForm');
	}
	function draw()
	{
		require_once 'packages/core/includes/utils/xml.php';
		$this->map['currency'] = XML::fetch_all('cache/utils/currency.xml');
		$this->map['weathers'] = XML::fetch_all('cache/utils/weather.xml');
		$gold = XML::fetch_all('cache/utils/golden.xml');
		$this->map += isset($gold[5])?$gold[5]:array('buy'=>'','sell'=>'');
		$this->parse_layout('utility',$this->map);
	}
}
?>