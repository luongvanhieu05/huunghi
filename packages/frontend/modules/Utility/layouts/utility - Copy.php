<div class="utility-bound">
	<div class="utility-exchange-bound">
		<div class="utility-title" content="weather">[[.Weather.]]</div>
		<div class="utility-exchange-rate" id="weather">
		<table width="90%" style="margin:auto">
			<!--LIST:weathers-->
			<tr>
				<td width="70%" align="left" style="padding-left:10px;border-bottom:1px solid #C6C6C6">[[|weathers.province|]]</td>
				<td align="center" style="border-bottom:1px solid #C6C6C6">[[|weathers.temperature|]]<sup>o</sup>C</td>
			</tr>
			<!--/LIST:weathers-->
		</table>
		</div>
	</div>
	<div class="utility-exchange-bound">
		<div class="utility-title" content="gold_rate">[[.gold_price.]]</div>
		<div class="utility-exchange-rate" id="gold_rate">
		<table width="90%" style="margin:auto">
			<tr>
				<td width="50%" align="center" style="border-right:1px solid #C6C6C6;border-bottom:1px solid #C6C6C6;border-top:1px solid #C6C6C6;">[[.Buy.]]</td>
				<td width="50%" align="right" style="padding-right:8px;border-bottom:1px solid #C6C6C6;border-top:1px solid #C6C6C6;">[[|buy|]]</td>
			</tr>
			<tr>
				<td align="center" style="border-right:1px solid #C6C6C6;border-bottom:1px solid #C6C6C6;">[[.Sell.]]</td>
				<td align="right" style="padding-right:8px;border-bottom:1px solid #C6C6C6;">[[|sell|]]</td>
			</tr>
		</table>
		</div>
	</div>
	<div class="utility-exchange-bound">
		<div class="utility-title" content="exchange_rate">[[.currency_exchange.]]</div>
		<div class="utility-exchange-rate" id="exchange_rate">
		<table width="90%" style="margin:auto">
			<!--LIST:currency-->
			<tr>
				<td width="40%" align="center" style="border-top:1px solid #C6C6C6;border-right:1px solid #C6C6C6;">[[|currency.name|]]</td>
				<td align="right" style="padding-right:8px;border-top:1px solid #C6C6C6;">[[|currency.exchange|]]</td>
			</tr>
			<!--/LIST:currency-->
		</table>
		</div>
	</div>
</div>
<!--IF:cond(User::can_admin(MODULE_NEWSADMIN,ANY_CATEGORY))-->
<div align="right"><a target="_blank" href="<?php echo Url::build('utils',array('cmd'=>'update'));?>">[[.update.]]</a></div>
<!--/IF:cond-->
<script>
jQuery(function(){
	jQuery('.utility-title').click(function(){
		var id = jQuery(this).attr('content');
		jQuery('#'+id).slideToggle('100',function(){
			if(jQuery('#'+id).is(":hidden"))
			{
				jQuery('span[content='+id+'] img').attr({
					  src: "assets/mhc/images/plus.png",
					  title: "[[.Show.]]",
					  alt: ""
				});
			}else
			{
				jQuery('span[content='+id+'] img').attr({
					  src: "assets/mhc/images/button.png",
					  title: "[[.Hide.]]",
					  alt: ""
				});
			}
		});
	});
});
</script>