<?php
class HomeGalleryDB{
	function get_photo()
	{
		return DB::fetch_all('
			SELECT
				media.id,
				media.name_'.Portal::language().' as name,
				media.url,
				media.image_url,
				category.name_id as category
			FROM
				media
				inner join category on category.id = media.category_id
			WHERE
				media.status!="HIDE" and media.type = "PHOTO"
			ORDER BY
				media.position desc
			LIMIT
				0,4
		');
	}
}
?>
<!-- and '.IDStructure::child_cond(DB::structure_id('category',537)).'-->