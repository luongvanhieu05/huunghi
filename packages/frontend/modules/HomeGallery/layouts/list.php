<div class="home-gallery-bound">
	<div class="home-gallery">
    <!--LIST:photo-->
    	<div class="home-gallery-img-bound">
        	<div class="home-gallery-img">
                <a rel="lightbox-tour" href="[[|photo.image_url|]]"><img src="[[|photo.image_url|]]" onerror="this.src='assets/default/images/no_image.gif'" /></a>
        	</div>
            <div style="text-align:center; margin-top:5px; color:11px;"><?php echo String::display_sort_title(strip_tags([[=photo.name=]]),1);?></div>
        </div>
    <!--/LIST:photo-->
    </div>
    <div class="home-gallery-view">
    	<div class="home-gallery-view-1"></div>
        <div class="home-gallery-view-2"><a href="<?php echo Url::build('thu-vien-anh');?>">[[.View all.]] >></a></div>
        <div class="home-gallery-view-3"></div>
    </div>
</div>
<script type="text/javascript">
jQuery(function(){
	jQuery('.home-gallery a').lightBox();
});
</script>