<?php
class HomeGalleryForm extends Form
{
	function HomeGalleryForm()
	{
		Form::Form('HomeGalleryForm');
		$this->link_css(Portal::template().'css/gallery.css');
		$this->link_css(Portal::template().'css/news.css');
		$this->link_css(Portal::template(false).'/css/jquery/css/jquery.lightbox-0.5.css');
		$this->link_js('packages/core/includes/js/jquery/jquery.lightbox-0.5.min.js');
		//$this->link_js(Portal::template().'js/jquery.prettyPhoto.js');
		//$this->link_css(Portal::template().'css/prettyPhoto.css');
	}
	function draw()
	{
		$this->map = array();
		$this->map['photo'] = HomeGalleryDB::get_photo();
		$this->parse_layout('list',$this->map);
		//System::debug($this->map['photo']);
	}
}
?>