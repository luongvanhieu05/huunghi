<?php
class HomeNavForm extends Form
{
	function HomeNavForm()
	{
		Form::Form('HomeNavForm');
		$this->link_css('assets/tonkin/css/home.css');
	}
	function draw()
	{
		$this->map = array();
		$cond = '1 AND '.IDStructure::direct_child_cond(DB::structure_id('category','582')).' AND id<>582 AND status<>"HIDE"';
		$arr = HomeNavDB::get_menu($cond);
		foreach($arr as $key=>$value)
		{
			$arr[$key]['childs'] = HomeNavDB::get_news('news.type="NEWS" and news.publish=1 and news.status!="HIDE" and news.category_id='.$key);
		}
		$this->map['items'] = $arr;
		$this->parse_layout('list',$this->map);
	}
}
?>