<?php
class ProductHotDB{
	static function get_item($cond,$item_per_page){
		$order_by = 'product.position DESC,product.time DESC';
		switch(Url::get('order_by')){
			case 'price_down': $order_by = 'product.price DESC'; break;
			case 'price_up': $order_by = 'product.price ASC'; break;
			case 'promote': $order_by = 'product.promote DESC'; break;
			default: $order_by = 'product.position DESC,product.time DESC'; break;
		}		
		$items = DB::fetch_all('
			SELECT
				product.id,
				product.status,
				product.name_'.Portal::language().' as name,
				product.small_thumb_url,
				product.name_id,
				product.publish_price,
				product.price,
				category.name_id AS category_name_id
			FROM
				product
				INNER JOIN product_category ON product_category.product_id = product.id
				INNER JOIN category ON product_category.category_id = category.id AND category.type="PRODUCT"
			WHERE
				'.$cond.'
			GROUP BY
				product.id
			ORDER BY
				'.$order_by.'
			LIMIT
				0,'.$item_per_page.'
		');
		foreach($items as $key=>$value){
			if(PRODUCT::get_price($key) and PRODUCT::get_price($key) < $items[$key]['price']){
				$items[$key]['price'] = System::display_number(PRODUCT::get_price($key));
				$items[$key]['publish_price'] = System::display_number($value['price']);
			}else{
				$items[$key]['publish_price'] = 0;
				$items[$key]['price'] = System::display_number($value['price']);
				$items[$key]['publish_price'] = System::display_number($value['publish_price']);
			}		
		}
 		return $items;
	}
	function get_total_item($cond)
	{
		return DB::fetch('
				SELECT
					count(*) as acount
				FROM
					product
					INNER JOIN product_category ON product_category.product_id = product.id
				INNER JOIN category ON product_category.category_id = category.id AND category.type="PRODUCT"
				WHERE
					'.$cond.'
			');
	}
}
?>