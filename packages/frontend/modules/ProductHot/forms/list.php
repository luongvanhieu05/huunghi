<?php
class ProductHotForm extends Form
{
	function ProductHotForm()
	{
		Form::Form('ProductHotForm');
	}	
	function draw()
	{	
		$this->map = array();
		$cond = 'product.status = "HOT"';
		$item_per_page = 5;
		$items = ProductHotDB::get_item($cond,$item_per_page);
		$this->map['items'] = $items;
		$this->parse_layout('list',$this->map);
	}
}
?>