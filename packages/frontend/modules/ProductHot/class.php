<?php
/* 	AUTHOR 	:	KHOAND
	DATE	:	13/08/2015
*/
class ProductHot extends Module
{
	function ProductHot($row)
	{
		Module::Module($row);
		require_once 'packages/frontend/includes/product.php';
		require_once 'db.php';
		require_once 'forms/list.php';
		$this->add_form(new ProductHotForm());
	}
}
?>