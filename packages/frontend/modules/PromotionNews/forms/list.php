<?php
class PromotionNewsForm extends Form
{
	function PromotionNewsForm()
	{
		Form::Form('PromotionNewsForm');
		$this->link_css(Portal::template('hotel').'/css/ngocub.css');
	}
	function draw(){
		$this->map = array();
		$category_id = 599;// thong tin khuyen mai
		$cond ='news.type = "NEWS" AND news.publish = 1 AND '.IDStructure::child_cond(DB::structure_id('category',$category_id)).'';
		$limit ='0,1';
		$this->map['news'] = PromotionNewsDB::get_news($cond,$limit);
		$this->parse_layout('list',$this->map);
	}
}
?>