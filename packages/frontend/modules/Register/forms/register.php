<?php
class RegisterForm extends Form{
	function RegisterForm(){
		Form::Form('RegisterForm');
		$this->add('email',new EmailType(true,'invalid_email'));
	}
	function on_submit(){
		$security_code = Session::get('security_code');
		if($this->check()){
			if ( ($_REQUEST["verify_comfirm_code"] == $security_code) && (!empty($_REQUEST["verify_comfirm_code"]) && !empty($security_code))){
				$this->register();
			}else{
			 	$this->error('verify_comfirm_code','invalid_confirm_code');
			}
		}
	}
	function draw(){
		$this->map = array();
		$this->map['zone_id_list'] = array('Chọn tỉnh thành') + String::get_list(RegisterDB::get_zones());
		$genders = array(
			1=>'Nam',
			0=>'Nữ'
		);
		$this->map['gender_list'] = $genders;
		$this->map['zone_id'] = 201; // Vietnam
//		$this->map['clb_id_list'] = array(''=>'Chọn CLB') + String::get_list(get_clbs(false,'trang_thai <> "OFF"'),'ten');
		$this->parse_layout('register',$this->map);
	}
	function register(){
		$new_id = strtolower(Url::sget('email'));
		$password = Url::sget('register_password');
		$retypepassword = Url::sget('retype_password');
		if ($password!=$retypepassword){
			$this->error('retypepassword','password_retype_is_not_true');
		}
		else{
			if (!DB::exists('select * from `party` where `user_id`="'.$new_id.'" or `email`="'.Url::get('email','').'"') and !DB::exists('select * from `account` where `id`="'.$new_id.'"')){
				$arr=array(	
					'user_id'=>$new_id,
					'full_name',
					'address',
					'phone'=>Url::get('phone'),
					'gender',
					'email'=>Url::get('email'),
					'type'=>'USER',
					'time'=>time(),
					'status'=>'SHOW',
					'identity_card',
					'last_update_time'=>time(),
					'portal_id'=>PORTAL_ID,
					'zone_id',
					'kind'=>2// nguoi choi
				);
				DB::insert('party',$arr);
				$account=array(
					'id'=>$new_id,
					'last_online_time'=>time(),
					'password'=>User::encode_password($password),
					'create_date'=>Date_Time::to_sql_date(date('d/m/Y',time())),
					'is_active'=>0,
					'type'=>'USER'
				);
				DB::insert('account',$account);
				require_once('cache/config/gender.php');
				if($arr['email']){
					$from = 'dangkhoa115@gmail.com';
					$subject = 'Chào mừng bạn đến với '.Portal::get_setting('site_name').'!';
					$content = file_get_contents('cache/email_template/register.html');
					$link = 'http://'.$_SERVER['HTTP_HOST'].'/'.Url::build('register',array('cmd'=>'active','user'=>$new_id,'code'=>md5($arr['email'].'catbeloved')),false);
					$data = array(
						'[[id]]',
						'[[full_name]]',
						'[[phone]]',
						'[[email]]',
						'[[cmtnd]]',
						'[[date]]',
						'[[activation_link]]',
						'[[address]]'
					);
					$replace = array(
						Url::get('email'),
						Url::get('full_name'),
						Url::get('phone'),
						Url::get('email'),
						Url::get('identity_card'),
						date('d/m/Y'),
						$link
					);
					$content = str_replace($data,$replace,$content);
					/*$mail_content = '<b>Dear '.$genders[$_REQUEST['gender']].' '.$_REQUEST['full_name'].'</b>,<br />';
					$mail_content.='Cám ơn bạn đã đăng ký tài khoản tại '.Portal::get_setting('site_name').'.<br />';
					$link = 'http://'.$_SERVER['HTTP_HOST'].'/'.Url::build('register',array('cmd'=>'active','user'=>$new_id,'code'=>md5($arr['email'].'catbeloved')),false);
					$mail_content.='Nhấn chuột vào " <a href="'.$link.'"><strong>Đây</strong></a>" để kích hoạt tài khoản.<br />';
					$mail_content.='Thông tin đăng nhập:<br />';
					$mail_content.='<b>Tên đăng nhập: '.$new_id.'</b><br />';
					$mail_content.='<b>Mật khẩu: : '.$password .'</b><br />';
					$mail_content.='Mọi câu hỏi có thể liên hệ: '.Portal::get_setting('support_email').'.<br />';	
					$mail_content.='<br><b><i>Best regards</i></b>,<br />';	
					$mail_content.='<b><a href="'.Portal::get_setting('site_name').'">'.Portal::get_setting('site_name').'</a></b><br />';	
					$mail_content.='<b>'.Portal::get_setting('site_name').'</b><br />';					
					*/
					System::send_mail('info@hangjeans.com.vn',$arr['email'],$subject,$content);
				}
				Url::redirect_current(array('act'=>'success'));
				//header('location:/register.html?act=success');
				exit();
			}
			else{
				$this->error('user_id','Tên đăng nhập hoặc email đã được đăng ký');
			}
		}
	}
}
?>
