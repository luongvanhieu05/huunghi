<?php
class RegisterDB{
	static function get_zones(){
		$items = DB::fetch_all('
			select 
				zone.id	
				,zone.structure_id
				,zone.status 
				,zone.image_url 
				,zone.name
        ,zone.type
				,zone.description
				,zone.lat
				,zone.long
				,zone.flag
			from 
			 	zone
			where
				zone.structure_id	<> "'.ID_ROOT.'"
				AND '.IDStructure::direct_child_cond(ID_ROOT).'
			order by 
				zone.structure_id
		');
		return $items;
	}
}
?>