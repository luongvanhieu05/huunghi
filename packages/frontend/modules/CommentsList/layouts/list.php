<div class="faq-list-bound">
	<div class="faq-list-title">[[.Comments.]]</div>
    <div class="faq-list-content">
        <!--IF:cond_item([[=faqs=]])-->
		<?php $i=1; ?>
        <!--LIST:faqs-->
            <div class="faq-list-item">
                <?php echo $i++; ?>. <?php echo strip_tags([[=faqs.name=]]); ?> - <?php echo strip_tags([[=faqs.brief=]])?>
                <div class="faq-list-answer">[[|faqs.description|]]</div>
            </div>
        <!--/LIST:faqs-->
        <div class="faq-list-paging">[[|paging|]]</div>
        <!--ELSE-->
        <div class="notice">[[.data_is_updating.]]</div>
        <!--/IF:cond_item-->
    </div>
</div>