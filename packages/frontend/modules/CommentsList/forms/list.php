<?php
class CommentsListForm extends Form
{
	function CommentsListForm()
	{
		Form::Form('CommentsListForm');
		$this->link_css(Portal::template('hotel').'css/comment.css');
	}
	function draw()
	{
		$this->map = array();
		$cond = 'news.publish and news.portal_id="'.PORTAL_ID.'" and news.category_id=464';
		$this->map['category_name'] = Portal::language('comments');
		if(Url::get('category_id') and $category = DB::select_id('category',intval(Url::sget('category_id'))))
		{
			$this->map['category_name'] = $category['name_'.Portal::language()];
			$cond.= ' and '.IDStructure::child_cond($category['structure_id']);
		}
		$item_per_page = 20;
		require_once'packages/core/includes/utils/paging.php';
		$count = CommentsListDB::get_total_item($cond);
		$this->map['paging'] = paging($count['acount'],$item_per_page,3,false,'page_no',array('category_id'),Portal::language('page'));
		$this->map['faqs'] = CommentsListDB::get_comment($cond,$item_per_page);
		$this->parse_layout('list',$this->map);
	}
}
?>