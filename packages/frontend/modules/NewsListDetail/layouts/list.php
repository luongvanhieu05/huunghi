<div class="container-fix">
<div class="container">
    <div class="col-left">
    <h2 class="title">[[|category_name|]]</h2>
    <ul class="list_3">
      <!--LIST:items-->
      <li>
          <h3><a href="<?php echo Url::build([[=items.category_name_id=]],array('name_id'=> [[=items.name_id=]]),REWRITE); ?>"><?php echo String::display_sort_title(strip_tags([[=items.name=]]),25) ?></a></h3>
          <a href="<?php echo Url::build([[=items.category_name_id=]],array('name_id'=> [[=items.name_id=]]),REWRITE); ?>"><img src="[[|items.image_url|]]" width="220" /></a>
          <p>[[|items.brief|]]</p>
          <h6><a href="<?php echo Url::build([[=items.category_name_id=]],array('name_id'=> [[=items.name_id=]]),REWRITE); ?>" class="button-ef">Xem chi tiết</a></h6>
      </li>
      <!--/LIST:items-->
    </ul>
    <div class="paging">[[|paging|]]</div>
    </div><!--End .col-left-->
    <div class="col-right">
       [[--right_region--]]
    </div><!--End .col-right-->
</div><!--End .container-->
</div><!--End .container-fix-->