<div class="content-bound">
	<div class="content-news-list-detail content">
        <div class="list_news">
            <div class="title">
                <h2>[[|category_name|]]</h2>
            </div>
            <div class="news-list-detail-content-bound">
            	<div class="home-content-news">
                    <div style="clear:both">
                    <div class="warrap-content-relation">
                        <!--LIST:items-->
                        <ul class="wrrap-content-relation-2">
                            <ul class="img-2"><a href="<?php echo Url::build([[=items.category_name_id=]],array('name_id'=> [[=items.name_id=]]),REWRITE); ?>"><img src="[[|items.image_url|]]" /></a></ul>
                            <ul class="relation-content-right">
                                <li class="title-news-relation"><a href="<?php echo Url::build([[=items.category_name_id=]],array('name_id'=> [[=items.name_id=]]),REWRITE); ?>"><?php echo String::display_sort_title(strip_tags([[=items.name=]]),25) ?></a></li>
                                <li class="brif-news-relation"><?php echo String::display_sort_title(strip_tags([[=items.brief=]]),50) ?></li>
                            </ul>
                        </ul>
                        <!--/LIST:items-->
                    </div>
                    <div style="clear:both">
                    <div class="news-list-paging" style="text-align:center;margin-bottom:10px;">[[|paging|]]</div>
                </div>
            </div>
        </div>
    </div>
</div>
