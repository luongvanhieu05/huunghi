<?php
class NewsListDetailDB
{
	static function get_items($cond,$item_per_page)
	{
		$items = DB::fetch_all('
			SELECT
				news.id,
				news.name_'.Portal::language().' as name,
				news.brief_'.Portal::language().' as brief,
				news.image_url,
				news.small_thumb_url,
				news.category_id,
				news.time,
				news.author,
				news.name_id,
				category.name_id as category_name_id
			FROM
				news
				INNER JOIN category ON news.category_id=category.id
			WHERE
				'.$cond.'
			ORDER BY
				news.position desc,news.time DESC,news.id DESC
			LIMIT
				'.((page_no()-1)*$item_per_page).','.$item_per_page.'
		');
		return $items;
	}
	static function get_total_item($cond)
	{
		return DB::fetch('
			SELECT
				count(*) as acount
			FROM
				news
				inner join category on news.category_id = category.id
			WHERE
				'.$cond.'
		');
	}
	static function get_items_by_category($cond,$item_per_page)
	{
		return DB::fetch_all('
			SELECT
				news.id,
				news.name_'.Portal::language().' as name,
				news.brief_'.Portal::language().' as brief,
				news.image_url,
				news.small_thumb_url,
				news.time,
				news.author,
				news.category_id,
				news.name_id,
				news.type,
				category.name_'.Portal::language().' as category_name,
				category.name_id as category_name_id
			FROM
				news
				inner join category on category.id = news.category_id and category.type="NEWS"
			WHERE
				'.$cond.'
			ORDER BY
				news.position DESC,news.time DESC
			LIMIT
				'.((page_no()-1)*$item_per_page).','.$item_per_page.'
		');
	}
	static function get_category($cond){
		return DB::fetch('
			SELECT
				category.id,category.name_'.Portal::language().' as name,structure_id
			FROM
				category
			WHERE
				'.$cond.'
			ORDER BY
				category.position ASC
		');
	}


	function get_cate2($structure_id){
		$sql='select
				id,name_id,name_'.Portal::language().' as name,structure_id,brief_1 as brief,
				icon_url as image_url,url
			from
				category
			where
				type="NEWS" AND status = "HOME" AND '.IDStructure::direct_child_cond($structure_id).'
				AND status <> "HIDE"
			order by
				structure_id
				';
		$items = DB::fetch_all($sql);
		return $items;
	}

	static function get_cate($structure_id){
		$sql='select
				id,name_id,name_'.Portal::language().' as name,structure_id,brief_1 as brief,
				icon_url as image_url,url
			from
				category
			where
				type="NEWS" AND status = "HOME" AND '.IDStructure::direct_child_cond($structure_id).'
				AND status <> "HIDE"
			order by
				structure_id
				';
		$items = DB::fetch_all($sql);
		foreach($items as $key=>$value){
			if((IDStructure::level($value['structure_id']))<3){
				$cate_child = NewsDB::get_cate2($value['structure_id']);
				$items[$key]['cate_child'] = $cate_child;
			}
		}
		return $items;
	}

}
?>