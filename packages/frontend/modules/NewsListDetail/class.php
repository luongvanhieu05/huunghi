<?php
//	AUTHOR: MINHTC
//	DATE  : 14/09/2009
class NewsListDetail extends Module
{
	static $category = array();
	function NewsListDetail($row)
	{
		Module::Module($row);
		require_once 'db.php';
		$cond = 'name_id="'.addslashes(Url::get('category_name')).'" and portal_id="'.PORTAL_ID.'"';
		if(Url::get('category_name') and $category = NewsListDetailDB::get_category($cond))
		{
			NewsListDetail::$category = $category;
			Portal::$document_title = $category['name'];
			$_REQUEST['category_id'] = $category['id'];
		}
		require_once 'forms/list.php';
		$this->add_form(new NewsListDetailForm());
	}
}
?>