<style type="text/css">
	#tickerContainer{
		width:230px;
		overflow:hidden;
	}
	#ticker .title{
		font:normal 12px Arial, Helvetica, sans-serif;
		border-bottom:none;
		border-right:none;
		font-size:12px;
	}
	/*
	#ticker .title li{
		display:inline;
	}
	*/
	#ticker .description{
		margin-left:0;
		text-align:left;
		font:normal 12px Arial, Helvetica, sans-serif;
		color:#db4702;
		border-bottom:1px solid #aaaaaa;
		background-color:#f7f6f4;
		position:relative;
	}
	#ticker .warap{
		padding-bottom:10px;
		border-bottom:1px #CCCBCA solid;
	}
}
</style>
<div id = "tickerContainer" align="left">
	<div id = "ticker">
    <!--LIST:temp-->
    	<div class="warap">
    	<ul class="title">
            <li style="padding-top:5px; color:#ff5200;"><a href="<?php echo Url::build('xem-trang-tin',array('name_id'=>[[=temp.name_id=]]),REWRITE)?>" title="[[|temp.name|]]">[[|temp.name|]]</a></li>
        </ul>
        <div style="text-align:justify;"><?php echo String::display_sort_title(strip_tags([[=temp.brief=]]),30,true);?></div>
        <!--LIST:category-->
        	<!--LIST:category.items-->
     	<div class="comment-viewall"><a href="<?php echo Url::build('trang-tin',array('name_id'=>[[=category.items.name_id=]]),REWRITE);?>">[[.viewall.]] </a></div>
       		<!--/LIST:category.items-->
   		<!--/LIST:category-->
       	</div>
    <!--/LIST:temp-->
    </div><!-- End ticker-->
</div>
