<?php
class TopNewsDB{
	function get_news(){
		$sql = '
			SELECT * FROM
					news
					where status = "HOT"
			';
		return DB::fetch_all($sql);
	}
	static function get_items($cond){
		$items = DB::fetch_all('
			SELECT
				news.id,
				news.name_'.Portal::language().' as name,
				news.brief_'.Portal::language().' as brief,
				news.image_url,
				news.small_thumb_url,
				news.category_id,
				news.time,
				news.author,
				news.name_id
			FROM
				news
				INNER JOIN category ON news.category_id=category.id
			WHERE
				'.$cond.'
			ORDER BY
				news.position desc,news.time DESC,news.id DESC
			LIMIT 0,1
			');
		return $items;
	}
	function get_category($cond,$a)
	{
		$sql='
			SELECT
				id,
				name_'.Portal::language().' as name,
				name_id,
				url,
				structure_id
			FROM
				category
			WHERE
				'.$cond.'
			ORDER BY
				position DESC, id ASC
			';
		$items = DB::fetch_all($sql);
		foreach($items as $key=>$value){
		$items[$key]['items'] = DB::fetch_all('
				SELECT
					id,
					name_'.Portal::language().' as name,
					name_id,
					url,
					structure_id
				FROM
					category
				WHERE
					 '.IDStructure::child_cond($value['structure_id']).' and id!='.$value['id'].' and id = '.$a.'
				ORDER BY
					position DESC, id ASC
			');
		}
		return $items;
	}
}
?>