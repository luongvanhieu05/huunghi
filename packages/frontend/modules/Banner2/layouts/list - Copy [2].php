<script src="assets/standard/js/fashion/jquery-2.1.1.min.js" type="text/javascript"></script>
<script src="assets/standard/js/fashion/bootstrap.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="assets/standard/css/fashion/font-awesome.min.css"/>
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css"/>
<link href='//fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,700,900' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,900' rel='stylesheet' type='text/css'>
<link href="assets/standard/css/fashion/stylesheet.css" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="assets/standard/css/fashion/carousel.css"/>
<link rel="stylesheet" type="text/css" href="assets/standard/css/fashion/custom.css"/>
<link rel="stylesheet" type="text/css" href="assets/standard/css/fashion/bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/standard/css/fashion/animate.css"/>
<link rel="stylesheet" type="text/css" href="assets/standard/css/fashion/lightbox.css"/>
<link rel="stylesheet" type="text/css" href="assets/standard/css/fashion/owl.carousel.css" media="screen"/>

<!-- Megnor www.templatemela.com - Start -->
<script type="text/javascript" src="assets/standard/js/fashion/megnor/custom.js"></script>
<script type="text/javascript" src="assets/standard/js/fashion/megnor/jstree.min.js"></script>
<script type="text/javascript" src="assets/standard/js/fashion/megnor/carousel.min.js"></script>
<script type="text/javascript" src="assets/standard/js/fashion/megnor/megnor.min.js"></script>
<script type="text/javascript" src="assets/standard/js/fashion/megnor/jquery.custom.min.js"></script>
<script type="text/javascript" src="assets/standard/js/fashion/megnor/jquery.formalize.min.js"></script>
<script type="text/javascript" src="assets/standard/js/fashion/megnor/lightbox-2.6.min.js"></script>
<!-- Megnor www.templatemela.com - End -->
<script src="assets/standard/js/fashion/megnor/common.js" type="text/javascript"></script>
<script src="assets/standard/js/fashion/megnor/tabs.js" type="text/javascript"></script>
<script src="assets/standard/js/fashion/megnor/owl.carousel.min.js" type="text/javascript"></script>


<div class="header-container">
    <nav id="top">
        <div class="container">
            <div class="header_left">
                <div id="top-links" class="nav pull-right">
                    <ul class="list-inline">
                        <li class="dropdown myaccount">
                            <a href="" title="My Account" class="dropdown-toggle" data-toggle="dropdown">Tài khoản của tôi<i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu dropdown-menu-right myaccount-menu">
                                <li><a href="/dang-ky.html">Đăng ký</a></li>
                                <li><a href="/dang-nhap.html">Đăng nhập</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>

                <div class="tm_headerlinkmenu">
                    <div class="tm_headerlinks_inner">
                        <div class="headertoggle_img">&nbsp;</div>
                        <ul class="header_links">
                            <li><a href="">Register</a></li>
                            <li><a href="">Login</a></li>
                            <li><a href="" id="wishlist-total-tm" title="Wish List (0)">Wish List (0)</a></li>
                            <li><a href="" title="Checkout">Checkout</a></li>
                        </ul>
                    </div>
                </div>

            </div>
            <header class="header_center">
                <div class="col-sm-4 logo">
                    <div id="logo">
                        <a href=""><img src="assets/standard/images/fashion/logo.png" title="Your Store" alt="Your Store" class="img-responsive"/></a>
                    </div>
                </div>
            </header>
            <div class="header_right">
                <div class="col-sm-3 cart">
                    <div id="cart" class="btn-group btn-block">
                        <button type="button" data-toggle="dropdown" data-loading-text="Loading..." class="btn btn-inverse btn-block btn-lg dropdown-toggle">
                            <span id="cart-total" onClick="window.location='cart.html'">[[|total|]]</span>
                        </button>
                        <ul class="dropdown-menu pull-right cart-menu hide">
                            <li>
                                <p class="text-center">Giỏ hàng của bạn Đang trống!</p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-5 search">
                    <div id="search">
                        <div class="input-group">
                            <span class="input-group-btn">
                              <button type="button" class="btn btn-default btn-lg"></button>
                            </span>
                            <input type="text" name="search" value="" placeholder="Search" class="form-control input-lg"/>
                        </div>
                    </div>
                </div>
                <div class="lang_curr">
                    <div class="pull-left">
                        <form action="common/currency/currency" method="post" enctype="multipart/form-data"
                              id="currency">
                            <input type="hidden" name="code" value=""/>
                            <input type="hidden" name="redirect" value="common/home"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <nav class="nav-container" role="navigation">
        <div class="nav-inner">
            <!-- ======= Menu Code START ========= -->
            <!-- Opencart 3 level Category Menu-->
            <div class="container">
                <div id="menu" class="main-menu">
                    <div class="nav-responsive"><span>Menu</span>
                        <div class="expandable"></div>
                    </div>
                    <ul class="main-navigation">
                        <li><a href="">Trang chủ</a></li>
                        <li class="level0">
                            <a href="">Sản phẩm</a>
                            <span class="active_menu"></span>

                            <div class="categorybg">
                                <!--  <span class="active_menu"></span>-->
                                <div class="categoryinner">
                                    [[|product_categories|]]
                                </div>
                            </div>
                        </li>


                                    [[|item_ul_categories|]]


                    </ul>

                </div>
                <!--  =============================================== Mobile menu start  =============================================  -->
                <div id="res-menu" class="main-menu nav-container1">
                    <div id="res-menu1" class="main-menu1 nav-container2">
                        <div class="nav-responsive"><span>Menu</span>

                            <div class="expandable"></div>
                        </div>
                        <ul class="main-navigation">
                            <li><a href="">Clothing</a>

                                <ul>

                                    <li>
                                        <a href="7" class="activSub">Men</a>

                                        <ul>
                                            <li><a href="">Jeans</a></li>
                                            <li><a href="">t-shirt</a></li>
                                            <li><a href="">Shirt</a></li>
                                            <li><a href="">blazers</a></li>
                                        </ul>

                                    </li>

                                    <li>
                                        <a href="6" class="activSub">Women</a>

                                        <ul>
                                            <li><a href="">Maxi</a></li>
                                            <li><a href="">Jeans</a></li>
                                            <li><a href="">Tunics</a></li>
                                            <li><a href="">Dresses</a></li>
                                        </ul>

                                    </li>

                                    <li>
                                        <a href="" class="activSub">Accessories</a>

                                        <ul>
                                            <li><a href="">Watches</a></li>
                                            <li><a href="">clutches</a></li>
                                            <li><a href="">Belts</a></li>
                                            <li><a href="">wallet</a></li>
                                        </ul>

                                    </li>
                                </ul>

                            </li>
                            <li><a href="">Party Wear</a>
                            </li>
                            <li><a href="">Designers</a>
                            </li>
                            <li><a href="">Kids Wear</a>
                            </li>
                            <li><a href="">Accessories</a>
                            </li>
                        </ul>
                    </div>

                </div>
                <!--  ================================ Mobile menu end   ======================================   -->
                <!-- ======= Menu Code END ========= -->
            </div>
        </div>
    </nav>
</div>
<!--    <div class="content_breadcum"></div>-->