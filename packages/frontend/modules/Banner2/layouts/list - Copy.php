<link href="assets/standard/css/bootstrap.min.css" rel="stylesheet">
<link href="assets/standard/css/font-awesome.min.css" rel="stylesheet">
<link href="assets/standard/css/animate.min.css" rel="stylesheet">
<link href="assets/standard/css/main.css" rel="stylesheet">
<script src="assets/standard/js/jquery.js"></script>
<script src="assets/standard/js/bootstrap.min.js"></script>
<script src="assets/standard/js/jquery.isotope.min.js"></script>
<script src="assets/standard/js/main.js"></script>
<script src="assets/standard/js/wow.min.js"></script>
<div class="top-bar">
  <div class="container">
      <div class="row">
          <div class="col-sm-6 col-xs-4">
              <div class="top-number"><p><i class="fa fa-phone-square"></i> 096 204 2817</p></div>
          </div>
          <div class="col-sm-6 col-xs-8">
             <div class="social">
                  <span class="lg">
                      <div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'vi', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
        
        
                  </span>
                  <ul class="social-share">
                      <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                      <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                      <li><a href="#"><i class="fa fa-skype"></i></a></li>
                  </ul>
                  <div class="search">
                      <form role="form">
                          <input type="text" class="search-form" autocomplete="off" placeholder="Search">
                          <i class="fa fa-search"></i>
                      </form>
                 </div>
             </div>
          </div>
      </div>
  </div><!--/.container-->
</div><!--/.top-bar-->

<nav class="navbar navbar-inverse" role="banner">
  <div class="container">
      <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="">
              <img src="assets/standard/images/logo.png" alt="ANT HOUSING" height="50">
              <span>Công ty cổ phần thiết kế nhà ở ANT</span>
          </a>
      </div>

      <div class="collapse navbar-collapse navbar-right">
          <ul class="nav navbar-nav">
              <li <?php echo (!Url::get('page') or Url::get('page')=='home')?'class="active"':'';?>><a href="">Trang chủ</a></li>
              <li class="dropdown <?php echo ((Url::get('page')=='xem-trang-tin' or Url::get('page')=='trang-tin') and Url::get('category_name')=='gioi-thieu')?' active':'';?>">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Giới thiệu <i class="fa fa-angle-down"></i></a>
                    <ul class="dropdown-menu">
                    	<li><a href="trang-tin/gioi-thieu/ve-chung-toi.html">Về chúng tôi</a></li>
                        <li><a href="trang-tin/gioi-thieu/lich-su-hinh-thanh.html">Lịch sử hình thành</a></li>
                        <li><a href="trang-tin/gioi-thieu/su-menh-tam-nhin.html">Sứ mệnh tầm nhìn</a></li>
                        <li><a href="trang-tin/gioi-thieu/doi-ngu-nhan-su.html">Đội ngũ nhân sự</a></li>
                    </ul>
                </li>
              <li class="dropdown<?php echo (Url::get('page')=='san-pham')?' active':'';?>">
           		<a href="#" class="dropdown-toggle" data-toggle="dropdown">Sản phẩm <i class="fa fa-angle-down"></i></a>
              	<ul class="dropdown-menu">
                [[|product_categories|]]
                </ul>
              </li>
              <li<?php echo ((Url::get('page')=='xem-trang-tin' or Url::get('page')=='trang-tin') and Url::get('category_name')!='gioi-thieu' and Url::get('category_name')!='tuyen-dung')?' class="active"':'';?>><a href="trang-tin">Tin tức</a></li>
              <li<?php echo ((Url::get('page')=='xem-trang-tin' or Url::get('page')=='trang-tin') and Url::get('category_name')=='tuyen-dung')?' class="active"':'';?>><a href="trang-tin/tuyen-dung">Tuyển dụng</a></li> 
              <li><a href="lien-he.html">Liên hệ</a></li>                        
          </ul>
      </div>
  </div><!--/.container-->
</nav><!--/nav-->