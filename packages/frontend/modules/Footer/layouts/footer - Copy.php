<footer>

    <div id="footer" class="container">
        <div class="row">
            <?php echo Portal::get_setting('footer_html')?>
            <div class="col-sm-3 column">
                <h5>Sản phẩm</h5>
                <ul class="list-unstyled">
                    <!--LIST:products-->
                    <li><a href="/san-pham/san-pham/[[|products.name_id|]]">[[|products.name_1|]]</a></li>
                    <!--/LIST:products-->
                </ul>
            </div>

            <div class="col-sm-5 column">
                <h5>Tin tức</h5>
                <!--LIST:news-->
                    <li><a href="trang-tin/[[|news.category_name_id|]]/[[|news.name_id|]].html"><?php echo String::display_sort_title(strip_tags([[=news.name=]]),15);?></a></li>
                <!--/LIST:news-->
            </div>

        </div>
        
    </div>
    

    

    <!--IF:cond(User::is_admin() and 1==2)-->

    <center>

        <div>[[.total_query.]]: [[|number_query|]]</div>

        <pre>

        <div>[[|information_query_in_page|]]</div>

        </pre>

    </center>

     <!--/IF:cond-->

</footer>