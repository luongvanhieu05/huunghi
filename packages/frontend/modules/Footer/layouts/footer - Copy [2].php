<footer>
    <div class="content_footer_top">
        <div class="footer-top">
            <div class="home-about-me container">
                <div class="tm-about-text">
                    <div class="footer_title1">
                        Gửi email cho chúng tôi để nhận được những thông tin sẩn phẩm hữu ích và các chương trình
                        ưu đãi hấp dẫn
                    </div>
                </div>
            </div>
            <form name="FooterForm" id="FooterForm" method="post" onsubmit="return checkInput();">
                <div class="newletter-main">
                    <input name="newsletter_email" type="email" id="newsletter_email" class="form-control" placeholder="Nhập email của bạn" required>
                </div>
                <button name="send_newsletter_email" type="submit" class="btn btn-success footer-btn-email">Gửi đi</button>
            </form>
        </div>
    </div>
    <div id="footer" class="container">
        <div class="row">
            <div class="content_footer_left">
                <div id="footer_left_block" class="col-sm-3 column">

                    <h5>Hệ thống cửa hàng Hằng Jeans</h5>


                    <ul>
                        <li class="address">
                            115 Xã Đàn - Đống Đa - Hà Nội</br>
                            P102 Nhà B8 - Ngõ 8A - Hoàng Ngọc Phách - HN</br>
                            276 Tây Sơn - Đống Đa - Hà Nội</br>
                            107 Nhổn - Từ Liêm - Hà Nội</br>
                            98 Thạch Bàn – Gia Lâm – HN</br>
                            35 Đường Tây Cao Tốc KCN Bắc Thăng Long - HN</br>
                            Số 2 Trần Quốc Toản - Hoàn Kiếm - Hà Nội<br>
                            169 Ngô Xuân Quảng - Gia Lâm - HN
                        </li>
                        <li class="phoneno">
                            Hotline: 04 2212 9289/ 0969 05 11 05
                            Góp ý về dịch vụ và chất lượng: 091 353 8858
                            Bán buôn: 0962 606 058/0913 538 858
                        </li>
                    </ul>
                    <div class="social_block">
                        <h5 class="follow">Follow Us</h5>
                        <ul class="social_block">
                            <li class="facebook"><a href="https://www.facebook.com/hangjeans298"><i class="fa fa-facebook"></i> </a></li>
                            <li class="google"><a href="https://plus.google.com/u/0/111798140661384175033/posts"><i class="fa fa-google-plus"></i></a></li>
                            <li class="twitter"><a href="https://twitter.com/HangJeans"><i class="fa fa-twitter"></i></a></li>
                            <li class="instagrm"><a href="http://instagram.com/hangjeans"><i class="fa fa-instagram"></i></a></li>
                            <li class="pinterest"><a href="HTTP://WWW.PINTEREST.COM/HANGJEANS/"><i class="fa fa-pinterest"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-sm-3 column">
                <h5>Tài khoản của tôi</h5>
                <ul class="list-unstyled">
                    <li><a href="dang-nhap.html">Tài khoản của tôi</a></li>
                    <li><a href="dang-nhap.html">Lịch sử đơn hàng</a></li>
                    <li><a href="dang-nhap.html">Danh sách yêu thích</a></li>
                    <li><a href="dang-nhap.html">Thư thông báo</a></li>
                </ul>
            </div>

            <div class="col-sm-3 column">
                <h5>Hỗ trợ khách hàng</h5>
                <ul class="list-unstyled">
                    <li><a href="trang-tin/huong-dan/huong-dan-mua-hang.html">Hướng dẫn mua hàng</a></li>
                    <li><a href="trang-tin/huong-dan/cau-hoi-thuong-gap.html">Câu hỏi thường gặp</a></li>
                    <li><a href="trang-tin/huong-dan/huong-dan-doi-tra-hang.html">Hướng dẫn đổi trả hàng</a></li>
                    <li><a href="trang-tin/huong-dan/he-thong-cua-hang.html">Hệ thống cửa hàng</a></li>
                    <li class="hide"><a href="">Trả hàng</a></li>
                </ul>
            </div>
            <div class="col-sm-3 column">
                <h5>Thông tin</h5>
                <ul class="list-unstyled">
                    <li><a href="trang-tin/gioi-thieu/ve-chung-toi.html">Giới Thiệu</a></li>
                    <li><a href="trang-tin/huong-dan/dieu-khoan-dieu-kien.html">Điều khoản & điều kiện</a></li>
                    <li><a href="trang-tin/huong-dan/quyen-rieng-tu.html">Quyền riêng tư</a></li>
                    <li><a href="trang-tin/tuyen-dung/tuyen-dung-cong-ty.html">Tuyển Dụng</a></li>
                    <li><a href="san-pham/khuyen-mai_69/">Khuyến mãi</a></li>
                </ul>
            </div>

        </div>
        <p><strong>Hotline</strong>: <?php echo Portal::get_setting('hot_line')?><br>
				<strong>Góp ý về dịch vụ và chất lượng</strong>: 091 353 8858 - <strong><br>
        Bán buôn</strong>: <?php echo Portal::get_setting('phone_ban_buon')?></p>
    </div>
    <div class="content_footer_bottom">
        <div class="footer_bottom">
            <div class="footer_bottom_inner container">
                <div class="footer_left"></div>

                <div class="footer_right">
                    <ul class="payment_block">

                        <li class="visa"><a href="#"> </a></li>

                        <li class="mastro"><a href="#"> </a></li>

                        <li class="paypal"><a href="#"> </a></li>

                        <li class="mastercard"><a href="#"> </a></li>

                    </ul>

                </div>

            </div>

        </div>
    </div>
</footer>