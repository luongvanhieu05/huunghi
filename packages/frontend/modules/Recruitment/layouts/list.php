<div class="frame-home-bound" style="width: 678px;">

    <div class="frame-home-title">

        <div class="fr-ht-bound">

            <ul class="fr-ht-b-left">

        			[[.recruitment.]]

            </ul>

            <ul class="fr-ht-b-right">

            </ul>

            <ul class="fr-ht-b-end-left">

            </ul>

        </div><!--End .fr-ht-bound-->

    </div><!--End .frame-home-title-->

    <div style="clear: both;"></div>

    <div class="home-content-news">

        <div style="clear:both">

        <div class="warrap-content-relation">

        	<!--LIST:news_all-->

            <ul class="wrrap-content-relation-2">

                <ul class="img-2"><a href="<?php echo Url::build('xem-trang-tin',array('name_id'=> [[=news_all.name_id=]]),REWRITE); ?>"><img src="[[|news_all.image_url|]]" /></a></ul>

                <ul class="relation-content-right">

                    <li class="title-news-relation"><a href="<?php echo Url::build('xem-trang-tin',array('name_id'=> [[=news_all.name_id=]]),REWRITE); ?>"><?php echo String::display_sort_title(strip_tags([[=news_all.name=]]),25) ?></a></li>

                    <li class="brif-news-relation"><?php echo String::display_sort_title(strip_tags([[=news_all.brief=]]),55) ?></li>

                </ul>

            </ul>

            <!--/LIST:news_all-->

        </div>

        <div style="clear:both">

        <div class="news-list-paging" style="text-align:center;margin-bottom:10px;">[[|paging|]]</div>

    </div>

</div>

<script type="text/javascript">

//explode bang javascript

function explode (delimiter, string, limit) {

    // Splits a string on string separator and return array of components. If limit is positive only limit number of components is returned. If limit is negative all components except the last abs(limit) are returned.

    //

    // version: 1109.2015

    // discuss at: http://phpjs.org/functions/explode    // +     original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)

    // +     improved by: kenneth

    // +     improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)

    // +     improved by: d3x

    // +     bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)    // *     example 1: explode(' ', 'Kevin van Zonneveld');

    // *     returns 1: {0: 'Kevin', 1: 'van', 2: 'Zonneveld'}

    // *     example 2: explode('=', 'a=bc=d', 2);

    // *     returns 2: ['a', 'bc=d']

    var emptyArray = {        0: ''

    };

    // third argument is not required

    if (arguments.length < 2 || typeof arguments[0] == 'undefined' || typeof arguments[1] == 'undefined') {        return null;

    }

    if (delimiter === '' || delimiter === false || delimiter === null) {

        return false;    }

    if (typeof delimiter == 'function' || typeof delimiter == 'object' || typeof string == 'function' || typeof string == 'object') {

        return emptyArray;

    }

    if (delimiter === true) {

        delimiter = '1';

    }

     if (!limit) {

        return string.toString().split(delimiter.toString());

    }

    // support for limit argument

    var splitted = string.toString().split(delimiter.toString());    var partA = splitted.splice(0, limit - 1);

    var partB = splitted.join(delimiter.toString());

    partA.push(partB);

    return partA;

}

function change_news_01(category_name_id,cmd){

    jQuery.ajax({

        method: "POST",url: 'form.php?block_id=<?php echo Module::block_id();?>',

        data : {

            'cmd':cmd,

            'category_name_id':category_name_id

        },

        beforeSend: function(){

        },

        success: function(content){

            //explode mang ket qua

            ra = explode('|',content);

            //alert(content);

            jQuery('.content-left-01-news').html(ra[0]);

            //jQuery('.brif-content-01').html(ra[1]);

            jQuery('.content-right-01-news').html(ra[1]);

			load_content_after();

        }

    });

}

//NVQ hover sau ajax 06/02/2012

function load_content_after(){

    jQuery('.brif-content-child a').hover(

        function(){

          jQuery('#href-main').attr('href',jQuery(this).attr('href'));

          jQuery('.img-1 a').attr('href',jQuery(this).attr('href'));

		  jQuery('.name-top-1 a').html(jQuery(this).html());

		  jQuery('.img-top-1').attr('src',jQuery('.anh-'+jQuery(this).attr('idanh')).attr('img')   );

          },

        function(){

        }

    );

}

load_content_after();

</script>