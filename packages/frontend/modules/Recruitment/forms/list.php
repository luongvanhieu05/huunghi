<?php
class RecruitmentForm extends Form
{
	function RecruitmentForm()
	{
		Form::Form('RecruitmentForm');
		$this->link_css('skins/hotel/css/news-pro.css');
		//$this->link_js('packages/core/includes/js/jquery/dropdown.js');
	}
	function draw()
	{
		$this->map = array();
        $item_per_page = 6;
		require_once'packages/core/includes/utils/paging.php';
		//Truy van tat ca cac tin tuc
		//Truy van cac tin tuc con-slide-tabs
		$cond='news.type="NEWS" and news.publish=1 and news.status!="HIDE" and '.IDStructure::child_cond( DB::structure_id('category','495'));
		$count =  RecruitmentDB::get_total_item($cond);
		$this->map['paging'] = paging($count['acount'],$item_per_page,3,REWRITE,'page_no',array('name_id'));
		$this->map['news_all'] = RecruitmentDB::get_total_news($cond,$item_per_page);
        $this->parse_layout('list',$this->map);
		//System::debug($this->map['news']);
	}
}
?>