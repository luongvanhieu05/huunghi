<?php
class CommentDB
{
    function get_comment($cond='1'){
        return DB::fetch_all(
            'SELECT * FROM comment WHERE '
            .$cond
        );
    }
    function get_total_comment($cond='1'){
        return DB::fetch(
            'SELECT COUNT(*) as acount FROM comment WHERE '
            .$cond
        ,'acount');
    }
    //LAY NOI DUNG TIN
	static function get_news($cond= ' 1 ')
	{
		$sql ='
			SELECT
                *
			FROM
				news
            WHERE '
            .$cond.
			' ORDER BY
				news.last_time_update DESC
		';
		return DB::fetch_all($sql);
	}
}
?>