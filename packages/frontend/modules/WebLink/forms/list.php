<?php
class WebLinkForm extends Form
{
	function WebLinkForm()
	{
		Form::Form('WebLinkForm');
	}
	function draw()
	{
		$items = explode(',',Portal::get_setting('weblink'));
		$web_links = array();
		if($items and count($items)>0)
		{
			foreach($items as $key=>$value)
			{
				$item = explode(':h',$value);
				if(isset($item[1]))
				{
					$web_links['h'.$item[1]] = $item[0];
				}elseif(isset($item[0]) and $item[0])
				{
					$web_links[$item[0]] = $item[0];
				}
			}
		}
		$this->parse_layout('list',array(
			'web_links_list'=>array(''=>Portal::language('choose_link'))+$web_links
		));
	}
}
?>
