<?php
class ServicesDB{
	function get_services($cond)
	{
		return DB::fetch_all('
			SELECT
				news.id, news.name_'.Portal::language().' as name, news.name_id, news.small_thumb_url,news.image_url, category.name_id as category_id
			FROM
				news
				inner join category on category.id = news.category_id
			WHERE
				'.$cond.'
			LIMIT
				0,7
		');
	}
}
?>