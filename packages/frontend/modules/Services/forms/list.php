<?php
class ServicesForm extends Form
{
	function ServicesForm()
	{
		Form::Form('ServicesForm');
		$this->link_css(Portal::template().'css/news.css');
		$this->link_js('packages/core/includes/js/jquery/jCarousel.js');
	}
	function draw()
	{
		$this->map = array();
		$category_id = 454;
		$cond = 'news.status<>"HIDE" and news.portal_id = "'.PORTAL_ID.'" and publish
					and '.IDStructure::child_cond(DB::structure_id('category',$category_id)).'
				';
		$this->map['items'] = ServicesDB::get_services($cond);
		$category =current($this->map['items']);
		$this->map['category'] = $category['category_id'];
		$this->parse_layout('list',$this->map);
	}
}
?>