<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('.services-image img').hover(
			function(){
				jQuery(this).css({'opacity':1,'filter':'alpha(opacity=100)'});
			},
			function(){
				jQuery(this).css({'opacity':0.6,'filter':'alpha(opacity=60)'});
			}
		);
	})
</script>
<div class="services-bound">
	 <!--LIST:items-->
	<div class="services-item">
		<div class="services-name"><a href="<?php echo Url::build('xem-trang-tin',array('name_id'=>[[=items.name_id=]]),REWRITE)?>">[[|items.name|]]</a></div>
		<div class="services-image"><a href="<?php echo Url::build('xem-trang-tin',array('name_id'=>[[=items.name_id=]]),REWRITE)?>"><img title="[[|items.name|]]" alt="[[|items.name|]]" src="[[|items.small_thumb_url|]]" /></a></div>
    </div>
	<!--/LIST:items-->
    <div class="services-viewall"><a href="<?php echo Url::build('trang-tin',array('name_id'=>[[=category=]]));?>">[[.viewall.]]</a></div>
</div>
<div class="clear"></div>
