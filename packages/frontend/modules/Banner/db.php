<?php

class BannerDB

{

	static function get_category($cond)

	{

		$sql='

			SELECT

				id,

				name_'.Portal::language().' as name,

				name_id,

				url,

				structure_id,

                image_url

			FROM

				category

			WHERE

				'.$cond.'

			ORDER BY

				structure_id

			';

		$items = DB::fetch_all($sql);

		return $items;

	}

	static function get_slider($cond,$table){
		$items = DB::fetch_all('

			SELECT

				'.$table.'.*,

				'.$table.'.name_'.Portal::language().' as name,

				'.$table.'.description_'.Portal::language().' as description,				

				category.name_id as category_name

			FROM

				'.$table.'

				INNER JOIN category ON '.$table.'.category_id = category.id

			WHERE

				'.$cond.'

			ORDER BY

				'.$table.'.position desc,'.$table.'.name_'.Portal::language().'

			LIMIT

				0,20

			');
		return $items;

	}

}

?>