<div class="cnt-top"></div>
<div class="detail">
<div id="content_module">
<div class="col_left contact">
      <h2 class="title">[[.contact_us.]]</h2>
      <div class="indents9">
      <div class="t_contact">
          <h1><?php echo Portal::get_setting('company_name_'.Portal::language());?></h1>
          <ul>
              <li class="add"><?php echo Portal::get_setting('company_address_'.Portal::language());?></li>
              <li class="phone"><strong>Hotline</strong>: <strong><?php echo Portal::get_setting('company_phone_bac');?></strong></li>
          </ul>
          <p>Email: <a href="mailto:<?php echo Portal::get_setting('email_support_online_bac');?>"><?php echo Portal::get_setting('email_support_online_bac');?></a></p>
      </div><br clear="all" />
      </div><!--End .indents9-->
      <div class="m-contact">
          <h2 class="title">[[.send_contact.]]</h2>
          <?php if(Form::$current->is_error()){echo Form::$current->error_messages();}?>
          <form name="ContactUs" method="post" id="form">
              <div class="success" style="display: none;">Contact form submitted!<br>
              <strong>We will be in touch soon.</strong> </div>
              <fieldset>
              <div class="row">
                  <div class="col-md-4">
                  <label class="name"></label>
                  <input name="full_name" type="text" id="full_name" maxlength="255" placeholder="[[.full_name.]]" class="form-control"/>
                  <br class="clear"/>
                  </div>
                  <div class="col-md-4">
                  <label class="email"></label>
                  <input name="email" type="text" id="email" maxlength="100" placeholder="E-mail" class="form-control"/>
                  <br class="clear"/>
                  </div>
                  <div class="col-md-4">
                  <label class="phone"></label>
                  <input name="phone" type="text" id="phone" maxlength="20" placeholder="[[.phone.]]" class="form-control"/>
                  <br class="clear"/>
                    </div>
                </div>
              <div class="row">
                    <div class="col-md-6">
                        <label class="address"></label>
                  <input name="address" type="text" id="address" maxlength="255" placeholder="[[.address.]]" class="form-control"/>
                  <br class="clear"/>
                    </div>
                  <div class="col-md-6">
                  <label class="title"></label>
                  <input name="title" type="text" id="title" maxlength="255" placeholder="[[.title.]]" class="form-control"/>
                  <br class="clear"/>
                  </div>
              </div>
              <div class="row">
                                  <div class="col-md-12">
                  <label class="message"></label>
                  <textarea name="content" id="content" placeholder="[[.content.]]" class="form-control" style="height: 200px;"></textarea>
                  <br class="clear"/>
                  </div>
              </div>
               <div class="row">
                    <div class="col-md-4">
                  <label class="label" style="position: relative;top: 10px;">[[.secure_code.]]<span>(*)</span>:</label>
                  <a href="void(0)" onclick="jQuery('#capcha_image').attr('src','capcha.php?'+Math.random());return false"><img src="assets/standard/images/restart-button.png" style="float:left;margin-right:10px;margin-top:3px;"/></a> <img id="capcha_image" src="capcha.php" style="float:left;margin-right:10px;margin-top:3px;"/>
                </div>
                <div class="col-md-4">
                    <input name="security_code" type="text" id="security_code" maxlength="4"  class="form-control"/>
                </div>
                <div class="col-md-4">
                    <div class="btns"><button class="btn btn-primary btn-block" onclick="ContactUs.submit();">[[.send.]]</button>
                </div>
              <div class="clear"></div>
              </div>
              </fieldset>
          </form>
      </div><!--End .m-contact-->
      </div><!--End .contact-->
  </div><!--End .container-->
</div><!--End .container-fix-->
<div class="cnt-bottom"></div>