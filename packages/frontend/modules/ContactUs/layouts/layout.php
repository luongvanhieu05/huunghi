<div class="product-category-20_27_43 layout-2 left-col">

<section id="contact-page" style="padding-top: 50px;">

    <div class="container">

    	<h1 style="font-family: sans-serif;margin-bottom: 20px;" class="">LIÊN HỆ</h1>
        <p>Hãy liên hệ ngay Thành - <span style="color: #f44336;font-size: 20px;font-weight: bold;">0981 738 999</span><br><br>
         hoặc điền vào form Thông tin  bên dưới:</p>
         <p>Nếu bạn muốn Đăng ký lái thử xe, Nhận bảng giá, hoặc các thông tin khuyến mại</p>
         <br>

        <div class="row contact-wrap"> 

            <div class="status alert alert-success" style="display: none"></div>

            <form name="ContactUs" method="post" id="form">

                <div class="col-md-6">

                    <div class="form-group">

                        <input name="full_name" type="text" id="full_name" maxlength="255" placeholder="[[.full_name.]]" class="form-control" required title="Bạn vui lòng nhập họ và tên"/>

                    </div>

                    <div class="form-group">

                        <input name="email" type="email" id="email" class="form-control" required="required" placeholder="Email" title="Bạn vui lòng nhập email">

                    </div>

                    <div class="form-group">

                        <input name="phone" type="text" id="phone" maxlength="20" placeholder="[[.phone.]]" class="form-control" required title="Bạn vui lòng nhập số điện thoại"/>

                    </div>

                    <div class="form-group">

                        <input name="address" type="text" id="address" maxlength="255" placeholder="[[.address.]]" class="form-control" required/>

                    </div>                        

                    <div>

                    	<label style="position: relative;">[[.secure_code.]] *</label>

                  		<img id="capcha_image" src="capcha.php"/><a href="void(0)" onclick="jQuery('#capcha_image').attr('src','capcha.php?'+Math.random());return false">Refresh</a>

                        <input name="security_code" type="text" id="security_code" maxlength="4" placeholder="Mã bảo vệ" class="form-control" required style="width:200px;float:right;"/>

                    </div>

                </div>

                <div class="col-md-6">

                    <div class="form-group">

                        <textarea name="content" id="content" required class="form-control" rows="9" placeholder="Nội dung của bạn"></textarea>

                    </div>                        

                    <div class="form-group">

                        <button type="submit" name="submit" class="btn btn-primary btn-lg" required="required">Gửi đi</button>

                    </div>

                </div>

            </form> 

        </div><!--/.row-->

    </div><!--/.container-->

</section><!--/#contact-page-->

</div>