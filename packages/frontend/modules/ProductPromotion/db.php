<?php
class ProductPromotionDB{
	static function get_item($cond,$item_per_page){
		$order_by = 'product.position DESC,product.time DESC';
		switch(Url::get('order_by')){
			case 'price_down': $order_by = 'product.price DESC'; break;
			case 'price_up': $order_by = 'product.price ASC'; break;
			case 'promote': $order_by = 'product.promote DESC'; break;
			default: $order_by = 'product.time DESC'; break;
		}		
		$items = DB::fetch_all('
			SELECT
				product.id,product.status,product.name_'.Portal::language().' as name,
				product.brief_'.Portal::language().' as brief,
				product.description_'.Portal::language().' as description,
				product.small_thumb_url,
				product.image_url,				
				product.name_id,
				product.publish_price,
				product.price,
				category.name_id AS category_name_id
			FROM
				product
				INNER JOIN category ON product.category_id = category.id AND category.type="PRODUCT"
			WHERE
				'.$cond.'
			ORDER BY
				'.$order_by.'
			LIMIT
				'.((page_no()-1)*$item_per_page).','.$item_per_page.'
		');
		foreach($items as $key=>$value){
			$items[$key]['off'] = round((($value['publish_price'] - $value['price'])/$value['publish_price'])*100);
			$items[$key]['price'] = System::display_number($value['price']);
			$items[$key]['publish_price'] = System::display_number($value['publish_price']);			
			if($value['image_url']){
				eval('$images ='.$value['image_url'].';');
				$items[$key]['images'] = $images;
			}else{
				$items[$key]['images'] = array();
			}
		}
 		return $items;
	}
	function get_total_item($cond)
	{
		return DB::fetch('
				SELECT
					count(*) as acount
				FROM
					product
					INNER JOIN category ON product.category_id = category.id AND category.type="PRODUCT"
				WHERE
					'.$cond.'
			');
	}
}
?>