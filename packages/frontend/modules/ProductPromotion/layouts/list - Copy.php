<?php 
$page = (Portal::language() == 1)?'san-pham':'product';
?>
<div class="list-products">
  <div class="top-selection">
      <div class="col-md-6">
          <ul class="breadcrumb">
              <li><a href="">[[.home.]]</a></li>
              <li><a href="<?php echo $page.'/';?>">[[.product.]]</a></li>
          </ul>
      </div>
      <div class="col-md-6">
          <div class="selection">
              <div>
              	<form name="ProductPromotionForm" action="<?php echo $_SERVER['REQUEST_URI']?>" method="post">
                  <select name="order_by" id="order_by" onchange="ProductPromotionForm.submit()"></select>
                 </form>
              </div>
          </div>
      </div>
  </div><!--End .top-selection-->
  <div class="main-list" style="clear: both;">
  		<!--IF:cond(!empty([[=items=]]))-->
			<!--LIST:items-->
      <div class="col-md-4">
          <div class="product-item">
              <div class="img">
                  <a href="<?php echo $page;?>/[[|items.category_name_id|]]/[[|items.name_id|]].html"><img src="[[|items.small_thumb_url|]]"/></a>
                  <h3><a href="<?php echo $page;?>/[[|items.category_name_id|]]/[[|items.name_id|]].html">[[|items.name|]]</a></h3>
              </div>
              <div class="bottom-tt">
                  <div class="price">
                      <p>
                          <span>Giá: [[|items.publish_price|]] vnđ</span>
                          <span class="sale">Giá KM: [[|items.price|]] vnđ</span>
                      </p>
                  </div>
                  <div class="button-cart">
                      <a href="cart.html?product_id=[[|items.id|]]">Mua hàng</a>
                  </div>
              </div>
          </div><!--End .product-item-->
      </div>
     <!--/LIST:items-->
      <!--ELSE-->
      <div class="col-md-4">
          <div class="product-item">
            <div class="note">[[.product_was_not_update.]]</div>
          </div>
      </div>
      <!--/IF:cond-->
      <div class="pt">[[|paging|]]</div>
  </div><!--End .main-list-->
</div><!--End .list-products-->