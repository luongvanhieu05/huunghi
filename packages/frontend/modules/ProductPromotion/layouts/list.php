<?php 
$page = (Portal::language() == 1)?'san-pham':'product';
?>
<div class="row">
  <div class="col-md-7">
      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel"> 
      
        <!-- Wrapper for slides -->
        <div class="carousel-inner">
        	<?php $i=0;?>
        	<!--LIST:images-->
          <div class="item <?php echo ($i==0)?'active':'';?>">
            <img src="[[|images.value|]]" alt="[[|images.name|]]">
            <div class="carousel-caption">
              <p>[[|brief|]]</p>
            </div>
          </div>
          <?php $i++;?>
          <!--/LIST:images-->
        </div>
        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
          <span></span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
          <span></span>
        </a>
      </div>
  </div>
  <div class="col-md-5" style="padding: 0px;">
      <div class="deal-top">
          <div class="img">
              <span class="deal"></span>
              <span class="sale-top"><em>[[.off.]]</em>[[|off|]]%</span>
              <a href="<?php echo $page?>/[[|category_name_id|]]/[[|name_id|]].html">
              <img src="[[|small_thumb_url|]]"/>
              </a>
          </div>
          <div class="detail-deal">
              <h1>[[|name|]]</h1>
              <span class="pice">[[|price|]]đ</span>
              <p>[[|brief|]]</p>
              <ul>
                  <li class="ship"><span>[[.free_ship.]]</span></li>
                  <!--<li class="time"><span>[[.time_remains.]]: [[|time_remain|]]</span></li> -->
              </ul>
              <a href="<?php echo $page?>/[[|category_name_id|]]/[[|name_id|]].html" class="view-more">[[.view_detail.]]</a>
          </div><!--End .detail-deal-->
      </div><!--End .deal-top-->
  </div>
</div>
<!--IF:cond(!empty([[=items=]]))-->
<div class="row" style="margin-left: 0px;margin-top: 20px;margin-bottom: 20px;">
  <div class="main-list" style="clear: both;">
      <div class="title">[[.other_promotion_products.]]</div>
			<!--LIST:items-->
      <div class="col-md-3">
          <div class="product-item">
              <div class="img">
                  <a href="<?php echo $page;?>/[[|items.category_name_id|]]/[[|items.name_id|]].html"><img src="[[|items.small_thumb_url|]]"/></a>
                  <h3><a href="<?php echo $page;?>/[[|items.category_name_id|]]/[[|items.name_id|]].html">[[|items.name|]]</a></h3>
              </div>
              <div class="bottom-tt">
                  <div class="price">
                      <p>
                          <span>[[.price.]]: [[|items.publish_price|]] vnđ</span>
                          <span class="sale">[[.promotion_price.]]: [[|items.price|]] vnđ</span>
                      </p>
                  </div>
                  <div class="button-cart">
                      <a href="cart.html?product_id=[[|items.id|]]">[[.buy.]]</a>
                  </div>
              </div>
          </div><!--End .product-item-->
      </div>
     <!--/LIST:items-->
  </div><!--End .main-list-->
</div>
<!--/IF:cond-->