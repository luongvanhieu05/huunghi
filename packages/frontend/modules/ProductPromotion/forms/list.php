<?php
class ProductPromotionForm extends Form
{
	function ProductPromotionForm()
	{
		Form::Form('ProductPromotionForm');
	}	
	function draw()
	{	
		$this->map = array();
		$this->map['category_name'] = 'Sản phẩm';
		if(Url::get('category_name_id') and $category = DB::select('category','name_id="'.Url::get('category_name_id').'"')){
			$this->map['category_name'] = $category['name_'.Portal::language()];
		}
		$current_time = date('Y-m-d H:i');
		
		$cond = 'product.status <> "HIDE" 
			AND (product.open_promotion_time <= "'.$current_time.'" AND product.close_promotion_time > "'.$current_time.'")';
		if(Url::get('keyword')){		
			require_once 'packages/core/includes/utils/search.php';
			require_once 'packages/core/includes/utils/vn_code.php';
			$cond .= Url::get('keyword')?' AND (product.name_'.Portal::language().' LIKE "%'.Url::sget('keyword').'%" OR product.description_'.Portal::language().' LIKE "%'.DB::escape(str_to_search_keyword(addslashes(Url::sget('keyword')),$search_hightlight)).'%")':'';
		}
		$cond .= Url::get('category_name_id')?' AND '.IDStructure::child_cond($category['structure_id']).'':'';
		$cond .= '
			'.(Url::get('tag')?' AND (item.tags like "%'.str_replace('-',' ',Url::sget('tag')).'%")':'').'
		';
		$item_per_page = 3;
		require_once'packages/core/includes/utils/paging.php';
		$count = ProductPromotionDB::get_total_item($cond);
		$this->map['total'] = $count['acount'];		
		$this->map['paging'] = paging($count['acount'],$item_per_page,3,true,'page_no',array('keyword','category_name_id'),Portal::language('page'));
		$items = ProductPromotionDB::get_item($cond,$item_per_page);
		if(!empty($items)){
			$items_ = $items;
			$this->map += array_shift($items_);
			$this->map['items'] = $items;
			$this->map['order_by_list'] = array(''=>'==Sắp xếp==') + array('price_down'=>'Giá gảm dần','price_up'=>'Giá tăng dần','promote'=>'KM');
			$this->map['time_remain'] = '';
			$this->parse_layout('list',$this->map);
		}else{
			$this->parse_layout('no_promotion',$this->map);
		}
	}
}
?>