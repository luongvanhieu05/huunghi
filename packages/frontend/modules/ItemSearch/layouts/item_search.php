<div class="gallery-bound">
	<div class="frame-home-title" style="width: 676px;">
        <div class="fr-ht-bound">
            <div class="fr-ht-b-left">
            	[[.search.]]
            </div>
            <div class="fr-ht-b-right"></div>
            <div class="fr-ht-b-end-left"></div>
        </div><!--End .fr-ht-bound-->
        <!--Gach ngang cuoi title-->
        <div style="width: 675px;height: 1px;background:#e5e5e5;"></div>
        <div style="clear: both;"></div>
    </div>
    <div class="bound-content-gallery">
        <div>
            <form name="form1" method="post">
            <div id="main-display">
            <input name="keyword" type="text" id="keyword" class="main-search">
            <input class="main-search-button" type="submit" name="submit" value="[[.search.]]">
            <div class="search-option">
                [[.Search_with.]]:
                <label><input  type="radio" name="search_type" id="news" <?php if(Url::get('search_type')=='news' or !Url::get('search_type')){?>checked="checked"<?php }?> value="news"> [[.News.]]</label>
                <!--<label><input  type="radio" name="search_type" id="product" <?php //if(Url::get('search_type')=='product'){?>checked="checked"<?php //}?> value="product"> [[.product.]]</label>-->
            </div>
            </div>
            </form>
            <!--IF:cond(Url::get('keyword') and [[=items=]])-->
            <div class="paging-content" align="right">
                [[.result.]]: <strong><?php if(Url::get('page_no')>=2){ echo [[=item_per_page=]]*(Url::get('page_no')-1);}else{ echo 1;}?>-
                <?php if([[=item_per_page=]] >= [[=total=]]){
                        echo [[=total=]];
                      }else{
                        if(([[=item_per_page=]]*(Url::get('page_no')-1))+[[=item_per_page=]] > [[=total=]]){
                            echo [[=total=]];
                        }else{
                            echo [[=item_per_page=]];
                        }
                      }
                ?></strong> <?php echo Portal::language('in_total');?> <strong>[[|total|]]</strong> [[.result.]]<br />
            </div>
            <!--/IF:cond-->
        </div>
        <div class="search-result-bound">
        <!--IF:condd(Url::get('keyword'))-->
        <!--IF:cond([[=items=]])-->
        <!--LIST:items-->
            <div class="search-result-item">
                <!--IF:cond(file_exists([[=items.image_url=]]))-->
                <div class="search-result-img"><img src="[[|items.image_url|]]" border="0" /></div>
                <!--/IF:cond-->
                <div class="search-result-content">
                    <div class="search-name" valign="top">
                        <div><a href="[[|items.href|]]" target="_blank">[[|items.name_search|]]</a></div>
                        <div style="line-height:25px;"> [[.Update.]]: <?php echo date('H:i d/m/Y',[[=items.time=]])?></div>
                    </div>
                    <div class="search-brief" valign="top"><?php echo String::display_sort_title([[=items.brief=]],30);?></div>
                </div>
            </div>
        <!--/LIST:items-->
        <!--ELSE-->
            <div class="search-no-result">[[.no_result.]] [[.with.]] [[.keyword.]] <b style="background:#FFFF00; color:#000000;"><?php echo Url::get('keyword');?></b></div>
        <!--/IF:cond-->
        <!--/IF:condd-->
            <div class="search-paging" align="left">[[|paging|]]</div>
    </div>
    </div>
</div>