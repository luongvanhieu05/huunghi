<?php
class ListItemSearchForm extends Form
{
	function ListItemSearchForm()
	{
		Form::Form('ListItemSearchForm');
		$this->link_css(Portal::template('hotel').'/css/search.css');
		$this->link_css(Portal::template().'css/gallery.css');
	}
	function on_submit()
	{
	}
	function draw()
	{
		$this->map = array();
		$this->map['paging'] = '';
		$type = 'news';
		if(Url::get('search_type')=='product') $type = 'product';
		if(Url::get('keyword')){
			if(Url::get('item_per_page')) $item_per_page =intval(Url::sget('item_per_page'));
			else $item_per_page = 20;
			$this->map['item_per_page'] = $item_per_page;
			$cond = ' 1';
			if(URL::get('keyword'))
			{
				if(preg_match("/\"[\w]+[\t\s\r]+[\w]+\"/",URL::get('keyword')))
				{
					$cond .= URL::get('keyword')? ' AND ((`item`.`name`) LIKE "%'.((str_replace('"','',URL::sget('keyword')))).'%" OR (`item`.`brief`) LIKE "%'.((str_replace('"','',URL::sget('keyword')))).'%" OR (`item`.`description`) LIKE "%'.((str_replace('"','',URL::sget('keyword')))).'%")  IN BOOLEAN MODE':'';
				}
				else
				{
					require_once 'packages/core/includes/utils/search.php';
					require_once 'packages/core/includes/utils/vn_code.php';
					$cond .= Url::get('keyword')?' and MATCH('.$type.'.keywords) AGAINST("'.DB::escape(str_to_search_keyword(extend_search_keywords(convert_utf8_to_telex((Url::sget('keyword')))),$search_hightlight)).'")':'';
				}
			}
			/*if(Url::get('category_id') and $category=DB::fetch('select id,portal_id,structure_id from category where id='.intval(Url::get('category_id')).' and portal_id="'.PORTAL_ID.'"'))
			{
				$cond_struc=IDStructure::child_cond($category['structure_id']).'';
				$cond.=' and '.$cond_struc;
			}*/
			$cond.=' and '.$type.'.portal_id="'.PORTAL_ID.'"';
			$count = ItemSearchDB::get_count($cond,$type);
			require_once 'packages/core/includes/utils/paging.php';
			$this->map['total'] = $count['acount'];
			$this->map['paging'] = paging($count['acount'],$item_per_page,5,false,'page_no',array('keyword','search_type'));
			$items = ItemSearchDB::get_all($cond,$item_per_page,$type);
			if($items)
			{
				if($type == 'product')
				{
					$page = 'xem-san-pham';
				}
				else
				{
					$page = 'xem-trang-tin';
				}
				foreach($items as $key=>$value){
					$items[$key]['href'] = Url::build($page,array('name_id'=>$value['name_id']),REWRITE);
					//$items[$key]['name'] = hightlight_keyword($items['name'],Url::get('keyword'));
					//$items[$key]['brief'] = str_replace('\'','&quot;',hightlight_keyword($item['brief'],Url::get('keyword')));
				}
				if(isset($search_hightlight))
				{
					$search_hightlight  = array();
					str_to_search_keyword(Url::get('keyword'),$search_hightlight);
					foreach($items as &$item)
					{
						$item['name_search']=hightlight_keyword(strip_tags($item['name']), $search_hightlight);
						$item['brief']=hightlight_keyword(strip_tags($item['brief']), $search_hightlight);
					}
				}
			}
			$this->map['items'] = $items;
		}
		$this->parse_layout('item_search',$this->map);
	}
}
?>