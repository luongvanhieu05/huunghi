<?php
class ItemSearchDB
{
	function get_category()
	{
		return DB::fetch_all('
			SELECT
				category.id,category.name_'.Portal::language().',category.structure_id ,category.status
			FROM
				category
			WHERE
				portal_id="'.PORTAL_ID.'" and category.status<>"HIDE"
			ORDER BY
				 structure_id
		');
	}
	function get_count($cond,$type)
	{
		return DB::fetch('
			SELECT
				 count(*) as acount
			FROM
				'.$type.'
				INNER JOIN category ON category.id = '.$type.'.category_id
			WHERE
				'.$cond.'
		');
	}
	function get_all($cond,$item_per_page,$type)
	{
		return DB::fetch_all('SELECT
				'.$type.'.id,
				'.$type.'.name_id,
				'.$type.'.name_'.Portal::language().' as name
				,'.$type.'.brief_'.Portal::language().' as brief
				,'.$type.'.category_id
				,'.$type.'.time
				,'.$type.'.image_url
				,category.name_'.Portal::language().' as category
			FROM
				'.$type.'
				INNER JOIN category ON category.id = '.$type.'.category_id
			WHERE
				'.$cond.'
			ORDER BY
				'.(URL::get('order_by')?' '.URL::get('order_by').'':' '.$type.'.time').' '.(URL::get('order_by_cond')?' '.URL::get('order_by_cond').' item.time desc':' desc').'
			LIMIT
				'.((page_no()-1)*$item_per_page).','.$item_per_page.'
		');
	}
}
?>