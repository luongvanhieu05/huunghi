<div class="container-inner">
    <header id="header">[[|banner|]]</header>
    <div class="container">
        <div class="row">
            <div id="content" class="col-sm-12">
                [[|slide|]][[|center|]][[|footer|]]
            </div>
        </div>
    </div>
</div>