<?php
class PRODUCT{
	static function get_price($product_id){
		$prices = DB::fetch_all('select id,price,open_promotion_time,close_promotion_time from product_price where product_id='.$product_id);
		$price = 0;
		foreach($prices as $key=>$value){
			if(strtotime($value['open_promotion_time']) <= time() and strtotime($value['close_promotion_time']) >= time()){
				$price = $value['price'];
				break;
			}
		}
		return $price;
	}
}
?>