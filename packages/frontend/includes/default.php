<div class="default-layout-bound">
	<div class="default-layout-content">
		<div class="default-layout-banner">[[|banner|]]</div>
		<div class="default-layout-center">[[|center|]]</div>
		<div class="default-layout-footer">[[|footer|]]</div>
	</div>
</div>