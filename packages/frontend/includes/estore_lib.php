<?php
class EstoreLib
{
	function estore_can_edit()
	{
		return User::can_edit(MODULE_ESTORESETTING);
	}
	static function process()
	{
		$id = Session::get('user_id').'';
		if(Url::get('who_can_see_your_profile'))
		{
			Portal::set_setting('estore_who_can_see_your_profile', Url::get('who_can_see_your_profile'));
		}
		if(Url::get('who_can_post_on_your_guestbook'))
		{
			Portal::set_setting('estore_who_can_post_on_your_guestbook', Url::get('who_can_post_on_your_guestbook'));
		}
		$css_string = '';
		if(Url::get('link_color'))
		{
			$css_string .= 'a{color:'.Url::get('link_color').';}';
		}
		if(Url::get('hover_link_color'))
		{
			$css_string .= 'a:hover{color:'.Url::get('hover_link_color').';}';
		}
		if(Url::get('visited_link_color'))
		{
			$css_string .= 'a:visited{color:'.Url::get('visited_link_color').';}';
		}
		if(Url::get('estore_title_color'))
		{
			$css_string .= '.estore-title{color:'.Url::get('estore_title_color').';}';
		}
		if(Url::get('estore_description_color'))
		{
			$css_string .= '.estore-description{color:'.Url::get('estore_description_color').';}';
		}
		if(Url::get('main_color'))
		{
			$css_string .= 'body,td,div,font{color:'.Url::get('main_color').';}';
		}
		if(Url::get('font_family'))
		{
			$css_string .= 'body,td,div{font-family:'.Url::get('font_family').';}';
		}
		if(!empty($_FILES['background']))
		{
			if($_FILES['background']['size']<=200000)
			{
				$path = $_FILES['background']['tmp_name'];
				$new_path = 'images/estore/skin/'.$id.'_bg.jpg';
				if(move_uploaded_file($path,$new_path))
				{
					$css_string .= '.estore-layout{background:Url('.$new_path.');}';
				}
			}
		}
		if(!empty($_FILES['banner_background']))
		{
			if($_FILES['banner_background']['size']<=200000)
			{
				$path = $_FILES['banner_background']['tmp_name'];
				$new_path = 'images/estore/skin/'.$id.'_banner_bg.jpg';
				if(move_uploaded_file($path,$new_path))
				{
					$css_string .= '.estore-banner{background:Url('.$new_path.');}';
				}
			}
		}
		if(!empty($_FILES['estore_menu_background']))
		{
			if($_FILES['estore_menu_background']['size']<=200000)
			{
				$path = $_FILES['estore_menu_background']['tmp_name'];
				$new_path = 'images/estore/skin/'.$id.'_menu_bg.jpg';
				if(move_uploaded_file($path,$new_path))
				{
					$css_string .= '.estore-menu{background:Url('.$new_path.');}';
				}
			}
		}
		if(Url::get('estore_menu_item'))
		{
			$css_string .= '.estore-menu-item,.estore-menu-item a,.estore-menu-item a:visited{color:'.Url::get('estore_menu_item').';}';
		}
		if(Url::get('background_color'))
		{
			$css_string .= '.estore-body{background-color:'.Url::get('background_color').';}';
		}
		if(Url::get('estore_modules_background'))
		{
			$css_string .= '.estore-modules-background{background:'.Url::get('estore_modules_background').';}';
		}
		if(Url::get('estore_modules_title'))
		{
			$css_string .= '.estore-modules-title{color:'.Url::get('estore_modules_title').';}';
		}
		Portal::set_setting('estore_css', $css_string);
		Portal::set_setting('estore_layout', Url::get('estore_layout'));
		DB::update('account',array('cache_setting'=>''),1);
		@unlink('cache/default_settings.php');
		echo '{ success : "'.$id.'" }';
	}
}
?>