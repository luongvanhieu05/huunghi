<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="content-language" content="vi" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="CACHE-CONTROL" content="NO-CACHE" />
<meta name="keywords" content="<?php echo (Portal::$meta_keywords)?''.strip_tags(Portal::$meta_keywords):Portal::get_setting('website_keywords_'.Portal::language());?>" />
<meta name="description" content="<?php echo Portal::$meta_description?Portal::$meta_description:Portal::get_setting('website_description_'.Portal::language());?>" />
<meta name="ROBOTS" content="ALL" />
<meta name="author" content="<?php echo Portal::get_setting('site_name_'.Portal::language());?>" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="google-site-verification" content="-i9LD9qZVrpqB4DvpRVpKCadP3Wk1MRAk1S5JeO2u94" />
<meta property="fb:app_id" content="208527286162605"/>
<title><?php echo (Portal::$document_title ? Portal::$document_title : Portal::get_setting(PREFIX.'site_title_'.Portal::language()))?></title>
<base href="<?php echo 'http://',$_SERVER['HTTP_HOST'],'/';?>" />
<meta property="og:image" content="<?php echo Portal::$image_url?Portal::$image_url:('http://hay.tv/'.Portal::get_setting('og_image'));?>" />
<meta property="og:title" content="<?php echo (Portal::$document_title?(Portal::$document_title):Portal::get_setting('site_title_'.Portal::language()));?>" />
<meta property="og:description" content="<?php echo Portal::$meta_description?Portal::$meta_description:Portal::get_setting('website_description_'.Portal::language());?>" />
<meta property="og:url" content="<?php echo 'http://'.$_SERVER['HTTP_HOST'].''.$_SERVER['REQUEST_URI'];?>" />
<link rel="image_src" href="<?php echo Portal::$image_url?Portal::$image_url:('http://hay.tv/'.Portal::get_setting('og_image'));?>" />
<link rel="shortcut icon" href="<?php echo Portal::get_setting('site_icon');?>" />
<script type="text/javascript">	query_string = "?<?php echo urlencode($_SERVER['QUERY_STRING']);?>";PORTAL_ID = "<?php echo substr(PORTAL_ID,1);?>";</script>
<?php echo Portal::$extra_header;?>
</head>
<body class="common-home">