<?php
date_default_timezone_set('Asia/Saigon');
define('DEVELOPER',true);
define( 'ROOT_PATH', strtr(dirname( __FILE__ ) ."/",array('\\'=>'/')));
require_once ROOT_PATH.'packages/core/includes/system/config.php';
if(User::is_admin()){
	fix_size();
}
function fix_size(){
	//$size_arr = array(325,326,327,328,329,330,331);//for big size nu
	//$size_arr = array(328,329,330,331,332,333,334);//for big size nam
	//$size_arr = array(325,326,327,328);//for jeans dai nu
	//$size_arr = array(328,329,330,321,322);//for jeans dai nam
	//$size_arr = array(325,326,327,328);
	$size_arr = array(328,329,330,331,332,333,334);
	//$category_id = 78;//for big size nu
	//$category_id = 109; //for big size nam
	//$category_id = 80; //for jeans dai nu
	//$category_id = 111; //for jeans dai nam
	//$category_id = 81; //for sooc ngo
	$category_id = 116; //for sooc ngo
	$structure_id = DB::structure_id('category',$category_id);
	$sql = '
			SELECT
				product.id,product.status,
				product.name_'.Portal::language().' as name,
				product.description_'.Portal::language().' as description,
				product.small_thumb_url,
				product.name_id,
				product.publish_price,
				product.price,
				product.valuation,
				product.open_promotion_time,
				product.close_promotion_time,
				product.brief_1 as brief,
				category.name_id AS category_name_id
			FROM
				product
				INNER JOIN product_category ON product_category.product_id = product.id
				INNER JOIN category ON product_category.category_id = category.id AND category.type="PRODUCT"
			WHERE
				'.IDStructure::child_cond($structure_id).'
			GROUP BY
				product.id
		';
	$products = DB::fetch_all($sql);
	foreach($products as $key=>$value){
		update_size($key,$size_arr);
	}
}
function update_size($product_id,$sizes){
	if(!empty($sizes)){
		//DB::query('delete product_thuoc_tinh_sp from product_thuoc_tinh_sp inner join thuoc_tinh_sp on thuoc_tinh_sp.id = product_thuoc_tinh_sp.thuoc_tinh_id where  product_thuoc_tinh_sp.product_id='.$product_id.' and thuoc_tinh_sp.type="SIZE"');
		$i=1;
		foreach($sizes as $key=>$value)
		{
			if(!DB::exists('select id from product_thuoc_tinh_sp where product_id='.$product_id.' and thuoc_tinh_id='.$value)){
				echo '<br>'.$i++;
				DB::insert('product_thuoc_tinh_sp',array('product_id'=>$product_id,'thuoc_tinh_id'=>$value));
			}
		}
	}
	
}
?>